<?php
$title = 'À propos';
require('header.php');
?>
<div class="about">
    <section class="about__story">
        <div class="about__story-inner" data-section="Page Header">
            <div class="about-overlay">
                <h1 role="main">Notre histoire</h1>
            </div>
            <div class="about__slider swiper-container state-0">
                <div class="swiper-wrapper">
                    <div class="about__slide swiper-slide slide">
                        <div class="container">

                            <div class="img-block" style="background-image:url(images/row_1_1.jpg)" role="img">
                            </div>
                            <div class="img-block position" style="background-image:url(images/row_1_2.jpg)" role="img">
                            </div>
                            <div class="img-block" style="background-image:url(images/row_1_3.jpg)" role="img">
                            </div>
                            <div class="img-block" style="background-image:url(images/row_1_4.jpg)" role="img">
                            </div>

                            <div class="img-block" style="background-image:url(images/row_2_1.jpg)" role="img">
                            </div>
                            <div class="img-block position" style="background-image:url(images/about_sponge2.jpg)" role="img">
                            </div>
                            <div class="img-block" style="background-image:url(images/row_2_3.jpg)" role="img">
                            </div>
                            <div class="img-block" style="background-image:url(images/row_2_4.jpg)" role="img">
                            </div>

                            <div class="img-block" style="background-image:url(images/row_3_1.jpg)" role="img">
                            </div>
                            <div class="img-block" style="background-image:url(images/row_3_2.jpg)" role="img">
                            </div>
                            <div class="img-block position" style="background-image:url(images/row_3_3.jpg)" role="img">
                            </div>
                            <div class="img-block" style="background-image:url(images/4up_dudeswalking.jpg)" role="img">
                            </div>

                            <div class="img-block" style="background-image:url(images/row_4_1.jpg)" role="img">
                            </div>
                            <div class="img-block" style="background-image:url(images/row_4_2.jpg)" role="img">
                            </div>
                            <div class="img-block" style="background-image:url(images/row_4_3_v2.jpg)" role="img">
                            </div>
                            <div class="img-block" style="background-image:url(images/row_4_4.jpg)" role="img">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="home__welcome text-in" data-section="Overview Text">
            <div class="container">
                <div class="pseudo-title">OVERVIEW</div>
                <h4>Nous fabriquons<br /> les marques de produits <br />de papier les plus<br /> populaires au Canada
                </h4>
                <p>De grandes choses peuvent être réalisées lorsque les gens travaillent avec passion vers un but
                    commun. C’est ainsi que Produits Kruger est devenue le plus grand fabricant canadien de produits de
                    papier de qualité, à usages domestique, industriel et commercial. Nous sommes fiers de proposer un
                    portefeuille des marques de produits de papier préférées au Canada pour toutes les pièces de votre
                    maison.</p>
            </div>
        </div>
    </section>
    <div class="wrapper">
        <section class="about__names">
            <!-- <div class="col">
            <div class="container">
              <div class="pseudo-title">CONSUMER PRODUCTS – CANADA</div>
              <h4>A family of Canadian household names</h4>
              <p>We offer a portfolio of Canada's favourite tissue brand for every room in your home.</p>
            </div>
          </div> -->
            <div class="col full-height">
                <div class="row wipe-in down" data-section="Product Grid">
                    <div class="bg" style="background-image: url(images/woman_background.jpg)" role="img"></div>
                    <div class="info">
                        <!-- <a href="http://www.cashmere.ca/" target="_blank"> -->
                        <img src="images/names_bg_2_opt.png" alt="A package of Cashmere Bathroom Tissue">
                        <h6>La marque de papier hygiénique en 1<sup>re</sup> place au Canada</h6>
                        <!-- </a> -->
                        <div class="links">

                            <a href="https://www.facebook.com/Cashmere/" target="_blank" aria-label="VISITEZ-NOUS SUR FACEBOOK - open in a new tab">VISITEZ-NOUS SUR FACEBOOK</a>
                            <a href="http://www.cashmere.ca/index_fr.html#home" target="_blank" aria-label="VISITEZ CASHMERE.CA - open in a new tab">VISITEZ CASHMERE.CA</a>
                        </div>
                    </div>
                </div>
                <div class="row wipe-in down">
                    <div class="bg" style="background-image: url(images/names_bg_2.jpg)" role="img"></div>
                    <div class="info">
                        <!-- <a href="http://www.spongetowels.ca/" target="_blank"> -->
                        <img src="images/names_bg_1_opt.png" alt="A package of Sponge Towels Paper Towels">
                        <h6>La marque d’essuie-tout préférée au Canada</h6>
                        <!-- </a> -->
                        <div class="links">
                            <a href="https://www.facebook.com/SpongeTowels/" target="_blank" aria-label="VISITEZ-NOUS SUR
                                FACEBOOK - open in a new tab">VISITEZ-NOUS SUR
                                FACEBOOK</a>
                            <a href="http://www.spongetowels.ca/index-fr.php" target="_blank" aria-label="VISITEZ
                                SPONGETOWELS.CA - open in a new tab">VISITEZ
                                SPONGETOWELS.CA</a>
                        </div>
                    </div>
                </div>
                <div class="row wipe-in down">
                    <div class="bg" style="background-image: url(images/names_bg_4.jpg)" role="img"></div>
                    <div class="info">
                        <!-- <a href="http://www.scotties.ca" target="_blank"> -->
                        <img src="images/names_bg_4_opt.png" alt="A package of Scottie's Facial Tissue">
                        <h6>La marque de papiers-mouchoirs en 1<sup>re</sup> place au Canada</h6>
                        <!-- </a> -->
                        <div class="links">
                            <a href="https://www.facebook.com/ScottiesTissue/" target="_blank" aria-label="VISITEZ-NOUS SUR
                                FACEBOOK - Opens in a new tab">VISITEZ-NOUS SUR
                                FACEBOOK</a>
                            <a href="http://www.scotties.ca/fr/" target="_blank" aria-label="VISITEZ SCOTTIES.CA - Opens in a new tab">VISITEZ SCOTTIES.CA</a>
                        </div>
                    </div>
                </div>
                <div class="row wipe-in down">
                    <div class="bg" style="background-image: url(images/names_bg_3.jpg)" role="img"></div>
                    <div class="info">
                        <!-- <a href="http://purex.ca/" target="_blank"> -->
                        <img src="images/names_bg_3.png" alt="A package of Purex Bathroom Tissue">
                        <h6>La marque de papier hygiénique en 1<sup>re</sup> place dans l’Ouest canadien</h6>
                        <!-- </a> -->
                        <div class="links">
                            <a href="https://www.facebook.com/purexbathroomtissue/" target="_blank" aria-label="VISITEZ-NOUS SUR
                                FACEBOOK - Opens in a new tab">VISITEZ-NOUS SUR
                                FACEBOOK</a>
                            <a href="http://www.purex.ca/" target="_blank" aria-label="VISITEZ PUREX.CA - Opens in a new tab">VISITEZ PUREX.CA</a>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
            <div class="clearfix2"></div>
        </section>

        <section class="about__dark full-height wipe-in left" data-section="Commercial Tissue">
            <div class="container">
                <div>
                    <div class="pseudo-title vis">DIVISION DES PRODUITS HORS FOYER</div>
                    <h4>Le plus important fabricant de produits de papier commerciaux au Canada</h4>
                    <p>Notre division Produits hors foyer offre des produits économiques de haute qualité — du papier
                        hygiénique, des papiers-mouchoirs, des essuie-tout, des serviettes de table, des chiffons, des
                        produits de soins des mains et des distributrices — pour usage dans les segments des services
                        alimentaires, de la gestion immobilière, de la santé, de la fabrication, de l’éducation et du
                        logement à travers l’Amérique du Nord.</p>
                    <p>Pour en savoir plus, visitez <a href="http://afh.krugerproducts.ca/home.aspx?lang=fr-CA&"
                            target="_blank" aria-label="afh.krugerproducts.ca - Opens in a new tab">afh.krugerproducts.ca</a></p>
                </div>
            </div>
        </section>
        <style>
        .about .about__ceo .quotes h3,
        .about .about__ceo .quotes i {
            padding: 0px 120px;
            font-size: 50px;
            position: relative;
            z-index: 2;

            left: 30px;
        }

        .about .about__ceo .quotes .pseudo-title {

            top: 90%;
        }

        .about .about__ceo .quotes {
            padding-top: 40px;
        }

        @media screen and (max-width: 1024px) {
            .about .about__ceo .quotes .pseudo-title {
                top: 95%;
            }
        }

        @media screen and (max-width: 800px) {

            .about .about__ceo .quotes h3,
            .about .about__ceo .quotes i {
                font-size: 30px;
                margin-bottom: 0px;
                width: auto;
                left: auto;
                padding: 0px 0px;
            }

            .about .about__ceo .quotes {
                margin-top: 180px;
            }

        }
        </style>
        <section class="about__ceo full-height wipe-in" data-section="Dino Bianco Quote">
            <div class="vertical-align">
                <div class="container">
                    <img src="images/man-portrait.png" alt="Dino Bianco CEO">
                    <div class="quotes">
                        <h3>« Nous faisons tout notre possible pour surpasser les attentes de nos clients, consommateurs
                            et employés. »</h3>
                        <div class="pseudo-title">Dino Bianco, CHEF DE LA DIRECTION</div>
                    </div>
                    <div class="clearfix2"></div>
                </div>
            </div>
        </section>
        <section class="about__locations">

            <div class="map" role="img">

            </div>

            <div class="container">
                <div class="text text-in" data-section="Locations Map">
                    <div class="pseudo-title">our locations</div>
                    <h4>Trouvez-nous<br /> partout en<br /> Amérique du<br /> Nord</h4>
                    <p>Nos emplacements sont alignés stratégiquement sur les ressources naturelles que nous utilisons
                        pour fabriquer nos produits, ainsi que sur les centres de population qui représentent
                        respectivement 90 % et 60 % des consommateurs canadiens et américains.</p>
                    <p>Avec huit usines de fabrication, notre capacité totale de fabrication de papier surpasse tout
                        juste le tiers de la capacité totale de fabrication de produits de papier du Canada. Nous sommes
                        également fiers d’être la seule compagnie de produits de papier exerçant des activités de
                        fabrication dans l’Ouest canadien.</p>
                </div>

                <div class="mobile-map" role="img">

                </div>

                <div class="locations-wrap">
                    <div class="location">
                        <div>
                            <div class="pseudo-title red">FABRICATION DU PAPIER ET TRANSFORMATION</div>
                            <h5>New Westminster (Colombie-Britannique)</h5>
                            <!--<a target="_blank" href="http://www.produitskruger.ca/pdfs/KrugerFactSheetNewWestminster_FR.pdf" class="btn pseudo-title">FEUILLET D’INFORMATION
                  <svg class="icon">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-3"></use>
                  </svg>
                </a>-->
                        </div>
                    </div>
                    <div class="location">
                        <div>
                            <div class="pseudo-title red">FABRICATION DU PAPIER ET TRANSFORMATION</div>
                            <h5>Gatineau (Québec)</h5>
                        </div>
                    </div>
                    <div class="location">
                        <div>
                            <div class="pseudo-title red">Désintégration, pulpe, <br>FABRICATION DU PAPIER ET CONVERSION
                            </div>
                            <h5>Crabtree (Québec)</h5>
                        </div>
                    </div>
                    <div class="location">
                        <div>
                            <div class="pseudo-title red">FABRICATION DU PAPIER</div>
                            <h5>Lenoxville (Sherbrooke, Québec)</h5>
                        </div>
                    </div>
                    <div class="location">
                        <div>
                            <div class="pseudo-title red">Fabrication du papier et transformation</div>
                            <h5>Sherbrooke, Québec</h5>
                        </div>
                    </div>
                    <div class="location">
                        <div>
                            <div class="pseudo-title red">SIÈGE SOCIAL</div>
                            <h5>Mississauga (Ontario)</h5>
                        </div>
                    </div>
                    <div class="location">
                        <div>
                            <div class="pseudo-title red">TRANSFORMATION</div>
                            <h5>Scarborough (Toronto, Ontario)</h5>
                        </div>
                    </div>
                    <div class="location">
                        <div>
                            <div class="pseudo-title red">TRANSFORMATION</div>
                            <h5>Trenton (Ontario)</h5>
                        </div>
                    </div>
                    <div class="location">
                        <div>
                            <div class="pseudo-title red">FABRICATION DU PAPIER ET TRANSFORMATION</div>
                            <h5>Memphis (Tennessee)</h5>
                        </div>
                    </div>
                    <div class="clearfix2"></div>
                </div>
            </div>
        </section>
        <section class="about__focus wipe-in" data-section="What We Stand For">
            <img src="images/cone_opened.png" alt="pinecone">
            <div>
                <div class="container">
                    <div class="text">
                        <h2>Nos valeurs</h2>
                        <div class="slogan"><span class="sub-ttl">Notre vision:</span> Être le fabricant de papier tissu
                            le plus digne de confiance et le plus apprécié en Amérique du Nord.</div>
                        <div class="slogan"><span class="sub-ttl">Notre mission:</span> Rendre votre quotidien plus
                            confortable.</div>
                    </div>
                    <div class="col-c">
                        <div class="slogan"><span class="sub-ttl">Nos valeurs:</span></div>
                        <div class="col">
                            <h5>Entrepreneuriat</h5>
                            <p>Nous valorisons et encourageons un esprit d’entreprise au sein de notre organisation. À
                                mesure que nous continuons à faire croître notre entreprise et à diversifier nos
                                opérations, nous maintenons une vision à long terme et gardons l’esprit ouvert aux
                                nouvelles idées et à l’innovation. Nos employés incarnent cette philosophie
                                entrepreneuriale.</p>
                        </div>
                        <div class="col">
                            <h5>Communauté et famille</h5>
                            <p>Notre organisation est comme une grande famille. Et rien n’est plus important à nos yeux
                                que la famille et la communauté qui nous entourent. Nous favorisons un sentiment
                                d’appartenance en encourageant le travail d’équipe et en soutenant les initiatives de
                                nos employés qui contribuent à notre succès collectif. Nous nous efforçons également de
                                reconnaître la valeur et la contribution de chaque individu au sein de notre
                                organisation.</p>
                        </div>
                        <div class="col">
                            <h5>Engagement</h5>
                            <p>L’engagement est au cœur de tout ce que nous faisons. Notre souci du bienêtre des gens se
                                reflète par notre engagement à offrir à nos employés un lieu de travail stimulant, sain
                                et sécuritaire. Nous nous engageons aussi à protéger l’environnement et à maintenir des
                                relations étroites avec les communautés et les gens avec lesquels nous collaborons.</p>
                        </div>
                        <div class="col">
                            <h5>Intégrité</h5>
                            <p>Nous tenons parole et nous valorisons la confiance avant tout. Notre intégrité nous a
                                permis de résister à l’épreuve du temps et de construire des relations mutuellement
                                bénéfiques avec nos employés, nos partenaires d’affaires et la communauté.</p>
                        </div>
                        <div class="col">
                            <h5>Satisfaction client</h5>
                            <p>La confiance est essentielle à toute relation d’affaires; c’est pourquoi nous gagnons la
                                confiance de nos clients en offrant un service exceptionnel, en assurant la qualité de
                                nos produits et en simplifiant nos processus. Notre objectif est de générer de la valeur
                                pour chaque client en créant des solutions qui amélioreront leur rendement à long terme.
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="about__history full-height wipe-in left" data-section="Our Story">
            <div class="triangles"></div>
            <div class="slidePrev" title="Previous Slide" aria-label="Previous Slide" role="button" tabindex="0"></div>
            <div class="slideNext" title="Next Slide" aria-label="Next Slide" role="button" tabindex="0"></div>
            <div class="vertical-align" style="left:0;right:0;">
                <div class="container">
                    <div class="pseudo-title vis">NOTRE HISTOIRE</div>
                    <h4>Plus d’un siècle de succès</h4>
                    <div class="swiper-container timeline-top">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb"
                                        style="background-image: url(images/joseph_kruger.jpg); background-position: top -90px center;" role="img">
                                    </div>
                                    <div class="pseudo-title year">1904</div>
                                    <h5><span>La société Kruger Paper Company Limited, firme de distribution en gros de
                                            papiers, est fondée à Montréal par Joseph Kruger.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/1905.jpg);" role="img"></div>
                                    <div class="pseudo-title year">1905</div>
                                    <h5><span>Edwin Crabtree et Sons Limité, maintenant notre installation de fabrication de papier hygiénique à Crabtree, au Québec, commence ses activités.</span>
                                    </h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/1922.jpg);" role="img"></div>
                                    <div class="pseudo-title year">1922</div>
                                    <h5><span>Westminster Paper Mills Limited, maintenant notre usine de fabrication à
                                            New Westminster, en Colombie-Britannique, commence ses activités.</span>
                                    </h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/1957_2.png);" role="img"></div>
                                    <div class="pseudo-title year">1957</div>
                                    <h5><span>Acquisition de l’usine de fabrication à Crabtree, au Québec.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/1978_2.png);" role="img"></div>
                                    <div class="pseudo-title year">1978</div>
                                    <h5><span>Acquisition de l’usine de fabrication à Sherbrooke, au Québec.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb"
                                        style="background-image: url(images/stoh_horizontal.jpg); background-size: 90%;" role="img">
                                    </div>
                                    <div class="pseudo-title year">1982</div>
                                    <h5><span>Début de la commandite du Tournoi des cœurs de Scott (maintenant appelé le
                                            Tournoi des cœurs Scotties), championnat national de curling féminin du
                                            Canada.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/1988.png);" role="img"></div>
                                    <div class="pseudo-title year">1988</div>
                                    <h5><span>Acquisition de l’usine de fabrication à Gatineau, au Québec.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/1997.png);" role="img"></div>
                                    <div class="pseudo-title year">1997</div>
                                    <h5><span>Kruger Inc. fait l’acquisition des exploitations canadiennes de Papiers
                                            Scott, lui permettant ainsi de lancer des produits de papier de grande
                                            consommation au Canada.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb"
                                        style="background-image: url(images/1999.png); background-position: top -75px center;" role="img">
                                    </div>
                                    <div class="pseudo-title year">1999</div>
                                    <h5><span>Relancement de la marque White Cloud aux États-Unis.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/2002.jpeg);" role="img"></div>
                                    <div class="pseudo-title year">2002</div>
                                    <h5><span>Acquisition d’une grande usine à Memphis, au Tennessee, établissant notre
                                            présence manufacturière aux États-Unis.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/2004_2.png);" role="img"></div>
                                    <div class="pseudo-title year">2004</div>
                                    <h5><span>Inauguration de la collection Blanc Cashmere, qui met en vedette certains
                                            des meilleurs créateurs de mode canadiens.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/cashmere_timeline.png);" role="img">
                                    </div>
                                    <div class="pseudo-title year">2004</div>
                                    <h5><span>Cottonnelle commence sa transition vers Cashmere. Aujourd’hui, Cashmere
                                            est le papier hygiénique le plus vendu au Canada.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb"
                                        style="background-image: url(images/cbcf-logo-timeline.png);   background-size: 85%;" role="img">
                                    </div>
                                    <div class="pseudo-title year">2005</div>
                                    <h5><span>Début de notre commandite de la Fondation canadienne du cancer du sein.
                                            Nous sommes aujourd’hui l’un des 5 donateurs les plus importants à l’échelle
                                            nationale.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb"
                                        style="background-image: url(images/2005_1.png);   background-size: 140%;" role="img">
                                    </div>
                                    <div class="pseudo-title year">2005</div>
                                    <h5><span>ScotTowels commence sa transition vers SpongeTowels.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb"
                                        style="background-image: url(images/kruger_products_logo.jpg); background-size: 90%;" role="img">
                                    </div>
                                    <div class="pseudo-title year">2007</div>
                                    <h5><span>Papiers Scott porte désormais le nom de Produits Kruger.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb"
                                        style="background-image: url(images/2008.png);   background-size: 80%;" role="img"></div>
                                    <div class="pseudo-title year">2008</div>
                                    <h5><span>Lancement de notre gamme de produits EnviroPlus, la première gamme de
                                            produits de papier haut de gamme au Canada fabriqués de papier recyclé à 100
                                            %.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb"
                                        style="background-image: url(images/2015.png);   background-size: 70%;" role="img"></div>
                                    <div class="pseudo-title year">2010</div>
                                    <h5><span>Produits Kruger lance son programme Développement durable 2015, initiative
                                            de cinq ans ayant pour but la réduction de l’empreinte environnementale de
                                            la compagnie et le bien des communautés.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb"
                                        style="background-image: url(images/2011_2.png);   background-size: 90%;" role="img"></div>
                                    <div class="pseudo-title year">2011</div>
                                    <h5><span>Produits Kruger devient le premier fabricant canadien de produits de
                                            papier à obtenir la certification Forest Stewardship Council<sup>®</sup>
                                            (FSC<sup>®</sup>), créant l’un des plus grands portefeuilles de produits de
                                            papier certifiés par une tierce partie en Amérique du Nord.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/2011_1.jpeg);" role="img"></div>
                                    <div class="pseudo-title year">2011</div>
                                    <h5><span>30<sup>e</sup> anniversaire en tant que commanditaire principal du Tournoi
                                            des cœurs Scotties (championnat national de curling féminin du Canada),
                                            l’une des commandites du sport amateur de plus longue date de l’histoire du
                                            Canada.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/2012_2.jpg);" role="img"></div>
                                    <div class="pseudo-title year">2012</div>
                                    <h5><span>Produits Kruger gagne 5 prestigieux prix Cassies; elle est maintenant
                                            l’une des entreprises de marketing les plus décorées au Canada.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb"
                                        style="background-image: url(images/2012_1.png);   background-size: 80%;" role="img"></div>
                                    <div class="pseudo-title year">2012</div>
                                    <h5><span>KP Tissue Inc., qui détient une participation dans <?php echo $company_name;?>,
                                            devient une société ouverte inscrite à la Bourse de Toronto (symbole :
                                            KPT).</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/2013_2_v2.jpg);" role="img"></div>
                                    <div class="pseudo-title year">2013</div>
                                    <h5><span>Lancement de la machine à papier à technologie de séchage à air traversant
                                            (SAT) de fine pointe à notre usine de Memphis, au Tennessee, ce qui augmente
                                            considérablement nos capacités nord-américaines.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/2013_1.png);" role="img"></div>
                                    <div class="pseudo-title year">2013</div>
                                    <h5><span>Le défilé de la mode de la collection Blanc Cashmere du papier hygiénique
                                            Cashmere célèbre son 10<sup>e</sup> anniversaire, en faveur de la Fondation
                                            canadienne du cancer du sein.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/2014.jpg);" role="img"></div>
                                    <div class="pseudo-title year">2014</div>
                                    <h5><span>Pour accélérer la croissance de sa division Produits hors foyer, Produits
                                            Kruger fait l’acquisition des actifs de transformation de papier de Metro
                                            Paper Industries Inc.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/2015.jpg);" role="img"></div>
                                    <div class="pseudo-title year">2015</div>
                                    <h5><span>White Cloud est le papier hygiénique classé en 1<sup>re</sup> place aux
                                            États-Unis selon une éminente revue de recherche et de tests auprès des
                                            consommateurs.</span></h5>
                                </div>
                            </div>
                            <!-- <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2016_2.png);"></div>
                      <div class="pseudo-title year">2016-2</div>
                      <h5><span>SpongeTowels Ultra Strong wins two Canadian Grand Prix new product awards</span></h5>
                    </div>
                  </div> -->
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/home_grid_1_second.png);" role="img"></div>
                                    <div class="pseudo-title year">2016</div>
                                    <h5><span>Dans un sondage national auprès des détaillants canadiens, Produits Kruger
                                            est classée en 1<sup>re</sup> place parmi les fournisseurs de biens emballés
                                            pour une quatrième année consécutive, un accomplissement sans
                                            précédent.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb"
                                        style="background-image: url(images/WC-LOGO.png);  background-size: 65%;" role="img"></div>
                                    <div class="pseudo-title year">2017</div>
                                    <h5><span>La marque White Cloud augmente considérablement sa distribution à plus de
                                            12 000 magasins à travers les États-Unis, et ce nombre continue de
                                            croître.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/2017_2.png);" role="img"></div>
                                    <div class="pseudo-title year">2017</div>
                                    <h5><span>Nouvelle machine à papier n<sup>o</sup> 8 de l'usine de fabrication de
                                            Crabtree, au Québec ajoute une capacité de 30 000 TM</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/2018-forbes-logo.jpg);" role="img">
                                    </div>
                                    <div class="pseudo-title year">2018</div>
                                    <h5><span>Est 2<sup>e</sup> au classement des meilleurs employeurs au Canada 2018 de
                                            Forbes</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/2018-sherbrooke-plant.jpg);" role="img">
                                    </div>
                                    <div class="pseudo-title year">2018</div>
                                    <h5><span>Nouvelle machine à papier à technologie de SAT annoncée pour Sherbrooke,
                                            au Québec, ce qui permet d'ajouter une capacité de 70 000 TM (début des
                                            opérations en 2021)</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/2019-les-50.jpg);" role="img"></div>
                                    <div class="pseudo-title year">2019</div>
                                    <h5><span>Est 10<sup>e</sup> au classement des 50 meilleures entreprises citoyennes
                                            au Canada de Corporate Knights</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/foodbank-2020.jpg);" role="img"></div>
                                    <div class="pseudo-title year">2020</div>
                                    <h5><span>Nous avons remis les dons de #DéroulerVotreGénérosité à Banques alimentaires Canada, aux hôpitaux et au Fonds Ligne de Front qui soutient les travailleurs de la santé pendant la COVID-19.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/2020-krugerassist.jpg);" role="img"></div>
                                    <div class="pseudo-title year">2020</div>
                                    <h5><span>Lancement de La passe à la relève Kruger pour apporter une aide financière aux familles dans le besoin au moyen des associations de hockey mineur.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/new-sherbrooke-qc-plant.jpg);" role="img"></div>
                                    <div class="pseudo-title year">2021</div>
                                    <h5><span>Mise en service de la machine à papier à technologie de séchage à air traversant (SAT) à l’usine de Sherbrooke, au Québec.</span></h5>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="block">
                                    <div class="thumb" style="background-image: url(images/Reimagine2030.jpg);" role="img"></div>
                                    <div class="pseudo-title year">2021</div>
                                    <h5><span>Lancement de Reconcevoir 2030, la troisième initiative en matière de développement durable de Produits Kruger.</span></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="year-title">1904</div>
                    <!-- <div class="swiper-container timeline-bottom">
                  <div class="swiper-wrapper">
                    <div class="swiper-slide"><span class="pseudo-title">1941</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1942</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1943</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1944</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1945</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1946</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1947</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1948</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1949</span></div>
                  </div>
              </div> -->
                    <input type="range" min="0" max="34" step="1" value="0" data-orientation="horizontal" aria-label="slides counting">
                    <!-- <div class="swiper-button-next swiper-button-black"></div>
              <div class="swiper-button-prev swiper-button-black"></div> -->
                </div>
            </div>
        </section>
        <section class="about__ethical full-height wipe-in" data-section="Fair and Ethical">
            <div class="vertical-align">
                <div class="about__ethical-inner">
                    <h2>Pratiques commerciales justes et éthiques</h2>
                    <p>Notre politique est d’adopter une conduite éthique qui se conforme aux lois, aux règles et aux
                        réglementations applicables dans tous les pays où sommes présents.</p>
                    <a href="pdfs/Kruger_Code_etiques_FR_FINAL.pdf" target="_blank" class="btn pseudo-title" aria-label="opens in a new tab">LIRE LE
                        CODE DE CONDUITE</a>
                </div>
            </div>
        </section>
        <section class="about__organization text-in" data-section="Organization">
            <div class="pseudo-title">Structure de l’entreprise</div>
            <h2>Structure de l’entreprise</h2>
            <p>Notre organigramme illustre la relation qui existe entre <?php echo $company_name;?> et ses actionnaires. <a
                    href="http://www.kptissueinc.com/" target="_blank" aria-label="opens in a new tab">Papiers Tissu KP Inc.</a> est une société ouverte
                créée afin d’acquérir une participation dans <?php echo $company_name;?> (<?php echo $short_company_name;?>) et son activité se
                limite à la détention de cette participation. Les actions de <a href="http://www.kptissueinc.com/"
                    target="_blank" aria-label="opens in a new tab">Papiers Tissu KP Inc.</a> se négocient à la Bourse de Toronto sous le symbole KPT.
            </p>
            <div class="about__organization-scheme">
                <div class="container">
                    <div class="rings-wrap">
                        <span class="ring" role="presentation"></span>
                        <h6>Public investisseur</h6>
                    </div>
                    <div class="jumper">
                        <svg class="icon" role="presentation">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-5"></use>
                        </svg>
                        99,99 %
                    </div>
                    <div class="rings-wrap">
                        <span class="ring" role="presentation"></span>
                        <h6>Papiers Tissu KP Inc.</h6>
                        <span>Canada</span>
                    </div>
                    <div class="jumper">
                        <svg class="icon" role="presentation">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-5"></use>
                        </svg>
                        14,66%
                    </div>
                    <div class="circle">
                        <div class="vertical-align">
                            <h6><?php echo $company_name;?></h6>
                            <span>Québec</span>
                        </div>
                    </div>
                    <div class="jumper">
                        <svg class="icon" role="presentation">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-6"></use>
                        </svg>
                        moins de 0,01 %
                    </div>
                    <div class="rings-wrap">
                        <span class="ring" role="presentation"></span>
                        <h6>KPGP Inc.</h6>
                        <span>Canada</span>
                    </div>
                    <div class="jumper">
                        <svg class="icon" role="presentation">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-6"></use>
                        </svg>
                        100 %
                    </div>
                    <div class="rings-wrap">
                        <span class="ring" role="presentation"></span>
                        <h6>Kruger Inc.</h6>
                    </div>
                    <div class="over-jumper jumper">
                        <svg class="icon" role="presentation">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-6"></use>
                        </svg>
                        <span>85,33 %</span>
                    </div>
                    <div class="clearfix"></div>
                    <span class="pseudo-title vis"
                        style="margin-top: 50px; margin-bottom: 0; padding-bottom: 0; border: none; text-decoration: none;">En date du 7 mai 2021</span>
                </div>
            </div>
            <!-- <svg version="1.1" class="ring" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="101.559px" height="101.176px" viewBox="0 0 101.559 101.176" enable-background="new 0 0 101.559 101.176" xml:space="preserve">
              <path fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#00a453" stroke-width="7" stroke-miterlimit="10" d="M50.779,3.5c26.111,0,47.279,21.082,47.279,47.088S76.891,97.676,50.779,97.676S3.5,76.594,3.5,50.588S24.668,3.5,50.779,3.5z"/>
          </svg> -->

        </section>
        <section class="about__highlights wipe-in left" data-section="Recognition Highlights">
            <div class="container">
                <div class="pseudo-title vis">MEILLEURE PERFORMANCE</div>
                <h2>Distinctions <br>honorifiques notables</h2>
                <div class="swiper-container timeline-left">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide pseudo-title" role="button" tabindex="0">2022</div>
                        <div class="swiper-slide pseudo-title" role="button" tabindex="0">2021</div>
                        <div class="swiper-slide pseudo-title" role="button" tabindex="0">2020</div>
                        <div class="swiper-slide pseudo-title" role="button" tabindex="0">2019</div>
                        <div class="swiper-slide pseudo-title" role="button" tabindex="0">2018</div>
                    </div>
                </div>
                <div class="swiper-container timeline-right">
                    <div class="swiper-wrapper">
                      <!-- start 2022  -->
                      <div class="swiper-slide">
                        <div class="block corporate">
                            <h3><a href="https://reviews.canadastop100.com/top-employer-kruger-products"
                                    target="_blank" aria-label="opens in a new tab">
                                    L’un des meilleurs employeurs de la région du Grand Toronto pour une 10<sup>e</sup> année consécutive</a></h3>
                            <div class="pseudo-title">ENTREPRISE</div>
                        </div>
                        <div class="block innovation">
                            <h3><a href="https://www.newswire.ca/news-releases/product-of-the-year-canada-announces-2022-award-winners-896081855.html" target="_blank" aria-label="opens in a new tab">
                              Produit de l'année au Canada, essuie-tout à usage domestique, SpongeTowels Ultra Pro</a></h3>
                            <div class="pseudo-title ">INNOVATION</div>
                        </div>
                        <div class="block innovation">
                            <h3><a href="https://www.newswire.ca/news-releases/2022-best-new-product-awards-reveals-health-well-being-and-better-for-you-foods-are-top-of-mind-with-canadians-838824416.html" target="_blank" aria-label="opens in a new tab">
                              Meilleur nouveau produit, produits ménagers, SpongeTowels Ultra Pro</a></h3>
                            <div class="pseudo-title ">INNOVATION</div>
                        </div>
                        <div class="block individual">
                            <h3>Prix HR Impact Awards <span style="font-style: italic">Memphis Business Journal</span>, entreprise publique, Aarthi Kalyan, généraliste des RH, KTG É.-U.</h3>
                            <div class="pseudo-title">INDIVIDU</div>
                        </div>

                      </div>
                      <!-- end 2022  -->
                        <!-- start 2021  -->
                        <div class="swiper-slide">
                          <div class="block corporate">
                              <h3><a href="https://certificationparite.org/a-propos/"
                                      target="_blank" aria-label="opens in a new tab">
                                      La Gouvernance au féminin, certification Parité Bronze</a></h3>
                              <div class="pseudo-title">ENTREPRISE</div>
                          </div>
                            <div class="block corporate">
                                <h3><a href="https://www.canadianbusiness.com/best-managed-companies-2021-returning-winners/"
                                        target="_blank" aria-label="opens in a new tab">
                                        Les entreprises les mieux gérées au Canada pour la 4<sup>e</sup> année consécutive, membre or</a></h3>
                                <div class="pseudo-title">ENTREPRISE</div>
                            </div>
                            <div class="block corporate">
                                <h3><a href="https://www.corporateknights.com/reports/2021-best-50/meet-the-2021-best-50-canadian-corporate-citizens-16250328/"
                                        target="_blank" aria-label="opens in a new tab">
                                        Classement parmi les 50 meilleures entreprises citoyennes au Canada de Corporate Knights pour la 4<sup>e</sup> année consécutive</a></h3>
                                <div class="pseudo-title">ENTREPRISE</div>
                            </div>
                            <div class="block customer">
                                <h3>Au 1<sup>er</sup> rang parmi les fournisseurs de biens emballés au Canada, sondage du secteur</h3>
                                <div class="pseudo-title ">CLIENT</div>
                            </div>

                            <div class="block innovation">
                                <h3><a href="https://canadiangrocer.com/gics-2021-top-10-grocery-winners?utm_source=omeda&utm_medium=email&utm_campaign=NL_CanadianGrocer_REG&utm_keyword=" target="_blank" aria-label="opens in a new tab">Fédération Canadienne des Épiciers Indépendants, dans les 10 meilleurs en épicerie, meilleur produit de papier, SpongeTowels Ultra PRO</a></h3>
                                <div class="pseudo-title ">INNOVATION</div>
                            </div>

                            <div class="block sustainability">
                                <h3><a href="https://canadiangrocer.com/2021-impact-award-winners-sustainability?utm_source=omeda&utm_medium=email&utm_campaign=NL_CanadianGrocer_REG&utm_keyword=&oly_enc_id=9231B5825712I1T" target="_blank" aria-label="opens in a new tab">Prix Inaugural Impact Awards de <span style="font-style: italic">Canadian Grocer, &nbsp;</span>initiative de développement durable</a></h3>
                                <div class="pseudo-title ">DÉVELOPPEMENT DURABLE</div>
                            </div>
                            <div class="block sustainability">
                                <h3><a href="https://www.lacp.com/2020vision/sustainability-consumer.htm" target="_blank" aria-label="opens in a new tab">Prix Vision Awards de la League of American Communication Professionals (Ligue des professionnels américains de la communication), Rapport de développement durable, produits destinés aux consommateurs, Or</a></h3>
                                <div class="pseudo-title ">DÉVELOPPEMENT DURABLE</div>
                            </div>
                            <div class="block sustainability">
                                <h3><a href="https://mercommawards.com/arc/awardWinners/categoryWinners/pdf.htm" target="_blank" aria-label="opens in a new tab">Prix ARC International Awards, Rapport de développement durable : Amérique et Europe, distinction, rapport sur le développement durable 2020</a></h3>
                                <div class="pseudo-title ">DÉVELOPPEMENT DURABLE</div>
                            </div>
                            <div class="block sustainability">
                                <h3><a href="https://summit.awardsplatform.com/gallery/NvGPJZXO/bOVDQkoP?search=e1a3996a498dba10-289" target="_blank" aria-label="opens in a new tab">Prix Summit Creative Awards, argent, vidéo corporatif, Reconcevoir 2030</a></h3>
                                <div class="pseudo-title ">DÉVELOPPEMENT DURABLE</div>
                            </div>
                            <div class="block sustainability">
                                <h3><a href="https://hes32-ctp.trendmicro.com/wis/clicktime/v1/query?url=https%3a%2f%2fbit.ly%2f3tyBsQl&umid=54699e69-3b46-4e10-bacf-d02f514615ce&auth=f866ad79d1d31fea309cff3c0367b331b070a2ce-ce7f5a923bfe2ae7c08af5a3eec30d707e2aabd4" target="_blank" aria-label="opens in a new tab">Lauréat du Programme de partenariat en matière de durabilité d'ÉcoConnexions du CN</a></h3>
                                <div class="pseudo-title ">DÉVELOPPEMENT DURABLE</div>
                            </div>

                            <div class="block sustainability">
                                <h3><a href="https://www.cleanenergyministerial.org/news-clean-energy-ministerial/winners-2021-global-leadership-awards-energy-management-announced"
                                  target="_blank" aria-label="opens in a new tab">Énergie propre ministérielle, Prix de la gestion de l’énergie, usine de Gatineau (Québec)</a></h3>
                                <div class="pseudo-title ">DÉVELOPPEMENT DURABLE</div>
                            </div>
                            <div class="block sustainability">
                                <h3><a href="https://www.fortisbc.com/news-events/stories-and-news-from-fortisbc/2021-efficiency-in-action-awards-celebrate-top-energy-saving-bc-organizations" target="_blank" aria-label="opens in a new tab">Prix Efficiency in Action (Efficacité en action) de FortisBS, industriel, usine de New Westminster (Colombie-Britannique)</a></h3>
                                <div class="pseudo-title ">DÉVELOPPEMENT DURABLE</div>
                            </div>

                            <!-- <div class="block corporate">
                                <h3><a href="https://reviews.canadastop100.com/top-employer-kruger-products"
                                        target="_blank" aria-label="opens in a new tab">
                                        L’un des meilleurs employeurs de la région du Grand Toronto pour une 9<sup>e</sup> année consécutive</a></h3>
                                <div class="pseudo-title">ENTREPRISE</div>
                            </div>
                            <div class="block corporate">
                                <h3><a href="https://www.newswire.ca/news-releases/kruger-products-recognized-on-forbes-canada-s-best-employers-list-2021-for-fifth-consecutive-year-898094070.html" target="_blank" aria-label="Forbes Canada's Best Employers 2021 - opens in a new tab">
                                L’un des meilleurs employeurs au Canada de <span style="font-style: italic;">Forbes</span> pour la 5<sup>e</sup> année consécutive</a></h3>
                                <div class="pseudo-title">ENTREPRISE</div>
                            </div>
                            <div class="block corporate">
                                <h3><a href="https://paritycertification.org/"
                                        target="_blank" aria-label="Silver Level Parity Certification 2021 - opens in a new tab">
                                        La Gouvernance au féminin, certification Parité argent pour la 2<sup>e</sup> année consécutive</a></h3>
                                <div class="pseudo-title">ENTREPRISE</div>
                            </div> -->
                            <div class="block marketing">
                                <h3><a href="https://www.lovethework.com/awards/film-lions-141?year=2021"
                                        target="_blank" aria-label="Prix AtoMiC Shift - opens in a new tab">
                                        Lions Cannes, Autre film portant sur les biens de consommation courante, Lion de bronze, Publicité <span style="font-style: italic;">Humains avant tout&nbsp;</span></a></h3>
                                <div class="pseudo-title">MARKETING</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://clios.com/awards/winner/film-craft-editing/kruger-product-l-p-/unapologetically-human-86583"
                                        target="_blank" aria-label="Prix AtoMiC Shift - opens in a new tab">
                                        Prix Clio, Artisan du cinéma (montage), bronze, publicité <span style="font-style: italic;">Humains avant tout&nbsp;</span></a></h3>
                                <div class="pseudo-title">MARKETING</div>
                            </div>

                            <div class="block marketing">
                                <h3><a href="https://theica.ca/effie-2021-bronze"
                                        target="_blank" aria-label="Effie Canada - opens in a new tab">
                                        Effie Canada, articles ménagers, bronze, publicité <span style="font-style: italic;">Humains avant tout&nbsp;</span></a></h3>
                                <div class="pseudo-title">MARKETING</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://theica.ca/effie-2021-bronze"
                                        target="_blank" aria-label="Effie Canada - opens in a new tab">
                                        Effie Canada, réponse en temps de crise/pivot essentiel, bronze, publicité <span style="font-style: italic;">Humains avant tout&nbsp;</span></a></h3>
                                <div class="pseudo-title">MARKETING</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://toronto.iabc.to/2021-award-winners/"
                                        target="_blank" aria-label="Prix AtoMiC Shift - opens in a new tab">
                                        Prix d’excellence de l’AIPC/Toronto OVATION, responsabilité sociale de l’entreprise, #DéroulerVotreGénérosité</a></h3>
                                <div class="pseudo-title">MARKETING</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://toronto.iabc.to/2021-award-winners/"
                                        target="_blank" aria-label="Prix AtoMiC Shift - opens in a new tab">
                                        Prix du mérite de l'AIPC/Toronto OVATION, événements spéciaux et expérimentaux, la collection Cashmere 2020</a></h3>
                                <div class="pseudo-title">MARKETING</div>
                            </div>

                            <div class="block marketing">
                                <h3><a href="https://marketingawards.strategyonline.ca/winners/winner/2021/?e=116524&u=Craft&n=Unapologetically+Human"
                                        target="_blank" aria-label="Prix AtoMiC Shift - opens in a new tab">
                                        Prix marketing, catégorie artisan, Or, réalisation <span style="font-style: italic;">Humains avant tout&nbsp;</span></a></h3>
                                <div class="pseudo-title">MARKETING</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://marketingawards.strategyonline.ca/winners/winner/2021/?e=116524&u=Craft&n=Unapologetically+Human"
                                        target="_blank" aria-label="Prix AtoMiC Shift - opens in a new tab">
                                        Prix marketing, catégorie artisan, Or, édition <span style="font-style: italic;">Humains avant tout&nbsp;</span></a></h3>
                                <div class="pseudo-title">MARKETING</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://marketingawards.strategyonline.ca/winners/winner/2021/?e=116524&u=Craft&n=Unapologetically+Human"
                                        target="_blank" aria-label="Prix AtoMiC Shift - opens in a new tab">
                                        Prix marketing, catégorie artisan, Bronze, cinématographie <span style="font-style: italic;">Humains avant tout&nbsp;</span></a></h3>
                                <div class="pseudo-title">MARKETING</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://marketingawards.strategyonline.ca/winners/winner/2021/?e=116524&u=Craft&n=Unapologetically+Human"
                                        target="_blank" aria-label="Prix AtoMiC Shift - opens in a new tab">
                                        Prix marketing, catégorie artisan, Bronze, musique <span style="font-style: italic;">Humains avant tout&nbsp;</span></a></h3>
                                <div class="pseudo-title">MARKETING</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://marketingawards.strategyonline.ca/winners/winner/2021/?e=115837&u=Multicultural&n=Unapologetically+Human"
                                        target="_blank" aria-label="Prix AtoMiC Shift - opens in a new tab">
                                        Prix marketing, catégorie multiculturelle, Argent, publicité adaptée <span style="font-style: italic;">Humains avant tout&nbsp;</span></a></h3>
                                <div class="pseudo-title">MARKETING</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://marketingawards.strategyonline.ca/winners/winner/2021/?e=115837&u=Multicultural&n=Unapologetically+Human"
                                        target="_blank" aria-label="Prix AtoMiC Shift - opens in a new tab">
                                        Prix marketing, catégorie multiculturelle, Bronze, collaborations <span style="font-style: italic;">Humains avant tout&nbsp;</span></a></h3>
                                <div class="pseudo-title">MARKETING</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://marketingawards.strategyonline.ca/winners/winner/2021/?e=118330&u=Advertising&n=Unapologetically+Human"
                                        target="_blank" aria-label="Prix AtoMiC Shift - opens in a new tab">
                                        Prix marketing, catégorie publicité, Argent, publicité télévisée de plus de 30 secondes/forme longue <span style="font-style: italic;">Humains avant tout&nbsp;</span></a></h3>
                                <div class="pseudo-title">MARKETING</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://marketingawards.strategyonline.ca/winners/winner/2021/?e=118330&u=Advertising&n=Unapologetically+Human"
                                        target="_blank" aria-label="Prix AtoMiC Shift - opens in a new tab">
                                        Prix marketing, catégorie publicité, Bronze, publicité en ligne simple/forme longue <span style="font-style: italic;">Humains avant tout&nbsp;</span></a></h3>
                                <div class="pseudo-title">MARKETING</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://marketingawards.strategyonline.ca/winners/winner/2021/?e=118330&u=Advertising&n=Unapologetically+Human"
                                        target="_blank" aria-label="Prix AtoMiC Shift - opens in a new tab">
                                        Prix marketing, catégorie publicité, Mérite, campagne publicitaire télévisée de 30 secondes et moins <span style="font-style: italic;">Humains avant tout&nbsp;</span></a></h3>
                                <div class="pseudo-title">MARKETING</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://marketingawards.strategyonline.ca/winners/winner/2021/?e=118330&u=Advertising&n=Unapologetically+Human"
                                        target="_blank" aria-label="Prix AtoMiC Shift - opens in a new tab">
                                        Prix marketing, catégorie publicité, campagne publicitaire en ligne de 15 secondes et moins <span style="font-style: italic;">Humains avant tout&nbsp;</span></a></h3>
                                <div class="pseudo-title">MARKETING</div>
                            </div>

                            <div class="block marketing">
                                <h3><a href="https://thecma.ca/get-involved/cma-awards/winners?_cldee=c3RldmVuLnNhZ2VAa3J1Z2VycHJvZHVjdHMuY2E%3d&recipientid=contact-de8271008aecea11a815000d3a0c8cd2-eeb9d5ce25b24203bea6f78f50fbbdef&utm_source=ClickDimensions&utm_medium=email&utm_campaign=Top%205%20Picks&esid=f30db5ef-7e48-ec11-8c62-0022483d57ce"
                                        target="_blank" aria-label="opens in a new tab">
                                        Prix bronze de l’Association canadienne du marketing, impact de l’entreprise des produits de consommation, publicité Humains avant tout</a></h3>
                                <div class="pseudo-title">MARKETING</div>
                            </div>


                            <div class="block individual">
                                <h3><a href="https://www.grocerybusiness-digitalmagazine.com/grocerybusiness/july_august_2021/MobilePagedReplica.action?pm=2&folio=46#pg46"
                                        target="_blank" aria-label="Prix AtoMiC Shift - opens in a new tab">
                                        Classement dans le Hall of Fame de <span style="font-style: italic">Grocery Business&nbsp;</span> en tant que fournisseur inaugural, Dino Bianco, chef de la direction </a></h3>
                                <div class="pseudo-title">INDIVIDU</div>
                            </div>

                            <div class="block individual">
                                <h3><a href="https://strategyonline.ca/2021/01/26/2020-moy-no-mess-too-big-for-susan-irving/"
                                        target="_blank" aria-label="opens in a new tab">
                                        <span style="font-style: italic">Strategy</span>  — spécialiste marketing de l'année dans le secteur des biens emballés pour la vente au détail, Susan Irving, chef du marketing </a></h3>
                                <div class="pseudo-title">INDIVIDU</div>
                            </div>
                            <div class="block individual">
                                <h3><a href="https://thecma.ca/get-involved/cma-awards/winners?_cldee=c3RldmVuLnNhZ2VAa3J1Z2VycHJvZHVjdHMuY2E%3d&recipientid=contact-de8271008aecea11a815000d3a0c8cd2-eeb9d5ce25b24203bea6f78f50fbbdef&utm_source=ClickDimensions&utm_medium=email&utm_campaign=Top%205%20Picks&esid=f30db5ef-7e48-ec11-8c62-0022483d57ce"
                                        target="_blank" aria-label="opens in a new tab">
                                        Spécialiste marketing de l’année de l’Association canadienne du marketing, Susan Irving, chef du marketing</a></h3>
                                <div class="pseudo-title">INDIVIDU</div>
                            </div>



                            <div class="block individual">
                                <h3><a href="https://canadiangrocer.com/2021-star-women-grocery-winner-mina-fior-qa"
                                        target="_blank" aria-label="opens in a new tab">
                                        Prix Star Women in Grocery de <span style="font-style: italic;">Canadian Grocer</span>, Mina Fior, première V.-P. des ressources humaines</a></h3>
                                <div class="pseudo-title">INDIVIDU</div>
                            </div>
                            <div class="block individual">
                                <h3><a href="https://canadiangrocer.com/2021-star-women-grocery-winner-kristina-koprivica-qa"
                                        target="_blank" aria-label="opens in a new tab">
                                        Prix Star Women in Grocery de <span style="font-style: italic;">Canadian Grocer</span>, Kristina Koprivica, directrice du marketing commercial, Essuie-tout et serviettes de table</a></h3>
                                <div class="pseudo-title">INDIVIDU</div>
                            </div>
                            <div class="block individual">
                                <h3><a href="https://canadiangrocer.com/2021-generation-next-winners-announced?utm_source=omeda&utm_medium=email&utm_campaign=NL_CanadianGrocer_BreakingNews&utm_keyword=&oly_enc_id=9231B5825712I1T" target="_blank" aria-label="opens in a new tab">
                                  Prix « Generation Next » de <span style="font-style: italic;">Canadian Grocer</span>, Erin McKeever, directrice du marketing commerce en ligne et numérique</h3>
                                <div class="pseudo-title">INDIVIDU</div>
                            </div>
                        </div>
                        <!-- end 2021  -->
                        <!-- start 2020  -->
                        <div class="swiper-slide">
                            <div class="block corporate">
                                <h3><a href="https://www.canadianbusiness.com/lists-and-rankings/best-managed-companies/canadas-best-managed-companies-returning-winners/"
                                        target="_blank" aria-label="opens in a new tab">
                                        Les entreprises les mieux gérées au Canada pour la 3<sup>e</sup> année consécutive</a></h3>
                                <div class="pseudo-title">ENTREPRISE</div>
                            </div>
                            <div class="block sustainability">
                                <h3><a href="https://www.corporateknights.com/reports/2020-best-50/2020-best-50-results-15930648"
                                        target="_blank" aria-label="opens in a new tab">Classement des 50 meilleures entreprises citoyennes au Canada de Corporate Knights pour la 3<sup>e</sup> année consécutive</a></h3>
                                <div class="pseudo-title">DÉVELOPPEMENT DURABLE</div>
                            </div>
                            <div class="block corporate">
                                <h3><a href="https://www.forbes.com/canada-best-employers/#429823e6241f"
                                        target="_blank" aria-label="opens in a new tab">Les meilleurs employeurs au Canada de Forbes pour la 4<sup>e</sup> année consécutive</a>
                                </h3>
                                <div class="pseudo-title">ENTREPRISE</div>
                            </div>
                            <div class="block corporate">
                                <h3><a href="https://reviews.canadastop100.com/top-employer-kruger-products"
                                        target="_blank" aria-label="opens in a new tab">L’un des meilleurs employeurs de la région du Grand Toronto pour une 8<sup>e</sup> année consécutive</a></h3>
                                <div class="pseudo-title">ENTREPRISE</div>
                            </div>
                            <!-- <div class="block corporate">
                                <h3><a href="https://paritycertification.org/about/#certified-organizations-section"
                                        target="_blank">La Gouvernance au féminin, certification Parité argent pour la 2<sup>e</sup> année consécutive</a></h3>
                                <div class="pseudo-title ">ENTREPRISE</div>
                            </div> -->
                            <div class="block customer">
                                <h3>Au 1<sup>er</sup> rang parmi les fournisseurs de biens emballés au Canada, sondage du secteur</h3>
                                <div class="pseudo-title ">CLIENT</div>
                            </div>
                            <div class="block customer">
                                <h3>London Drugs, meilleur partenaire de l'année, intervention face à la COVID-19</h3>
                                <div class="pseudo-title ">CONSOMMATEUR</div>
                            </div>
                            <div class="block customer">
                                <h3>Canadian Tire, fournisseur de l’année, division Au foyer</h3>
                                <div class="pseudo-title ">CONSOMMATEUR</div>
                            </div>
                            <div class="block fabrication">
                                <h3><a href="https://www.ccgj.qc.ca/publications/excelsiors-2020" target="_blank" aria-label="opens in a new tab">Chambre de commerce du Grand Joliette, QC  Prix Excelsiors pour l'installation de Crabtree, au Québec, entreprises industrielles et manufacturières</a></h3>
                                <div class="pseudo-title ">FABRICATION</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://winners.epica-awards.com/2020/winners?company=Broken+Heart+Love+Affair" target="_blank" aria-label="opens in a new tab"><span style="font-style: italic;">Epica</span> Awards, santé et beauté, argent, publicité multimarque Humains avant tout</a></h3>
                                <div class="pseudo-title ">MARKETING</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://winners.epica-awards.com/2020/winners?company=Broken+Heart+Love+Affair" target="_blank" aria-label="opens in a new tab"><span style="font-style: italic;">Epica</span> Awards, rédaction et narration, bronze, publicité multimarque Humains avant tout</a></h3>
                                <div class="pseudo-title ">MARKETING</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://marketingawards.strategyonline.ca/winners/winner/2020/?e=98164&u=Multicultural&n=Love+Scotties" target="_blank" aria-label="opens in a new tab">Scotties remporte le prix de mérite pour la télévision multiculturelle de Strategy Marketing Awards</a></h3>
                                <div class="pseudo-title ">MARKETING</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://enter.hermesawards.com/winners/#/platinum/2020" target="_blank" aria-label="opens in a new tab">Prix platine Hermes Creative Awards, média électronique / médias sociaux / média interactif | vidéo, SpongeTowels,</a></h3>
                                <div class="pseudo-title ">MARKETING</div>
                            </div>
                            <div class="block individual">
                                <h3><a href="https://www.newswire.ca/news-releases/canada-s-marketing-hall-of-legends-reveals-its-2020-legend-inductees-886286237.html"
                                        target="_blank" aria-label="opens in a new tab">Nancy Marcus, ancienne chef du marketing, se classe dans le Canada’s Marketing Hall of Legends</a></h3>
                                <div class="pseudo-title ">INDIVIDU</div>
                            </div>
                            <div class="block individual">
                                <h3><a href="https://www.canadiangrocer.com/top-stories/2020-generation-next-winners-announced-99212"
                                        target="_blank" aria-label="opens in a new tab">Prix « GenNext » de <span style="font-style: italic;">Canadian Grocer</span>, Kim Lichtman, directrice du marketing, papier hygiénique</a></h3>
                                <div class="pseudo-title ">INDIVIDU</div>
                            </div>
                        </div>
                        <!-- end 2020  -->
                        <!-- start 2019  -->
                        <div class="swiper-slide">
                            <div class="block sustainability">
                                <h3><a href="https://www.newswire.ca/fr/news-releases/corporate-knights-classe-les-produits-kruger-parmi-les-10-meilleures-entreprises-citoyennes-du-canada-841264999.html"
                                        target="_blank" aria-label="opens in a new tab">Est 10<sup>e</sup> au classement des 50 meilleures entreprises citoyennes au Canada de Corporate Knights pour la 2<sup>e</sup> année consécutive</a></h3>
                                <div class="pseudo-title">DÉVELOPPEMENT DURABLE</div>
                            </div>
                            <div class="block corporate">
                                <h3><a href="https://www2.deloitte.com/ca/en/pages/canadas-best-managed-companies/articles/best-managed-winners-2019.html"
                                        target="_blank" aria-label="opens in a new tab">
                                        Les entreprises les mieux gérées au Canada pour la 2<sup>e</sup> année consécutive</a></h3>
                                <div class="pseudo-title">ENTREPRISE</div>
                            </div>
                            <div class="block corporate">
                                <h3><a href="https://www.forbes.com/canada-best-employers/#27f3a67f241f"
                                        target="_blank" aria-label="opens in a new tab">Les meilleurs employeurs au Canada de Forbes pour la 3<sup>e</sup> année consécutive</a></h3>
                                <div class="pseudo-title">ENTREPRISE</div>
                            </div>
                            <div class="block corporate">
                                <h3><a href="https://content.eluta.ca/top-employer-kruger-products"
                                        target="_blank" aria-label="opens in a new tab">L'un des meilleurs employeurs de la région du Grand Toronto pour une 7<sup>e</sup> année consécutive</a>
                                </h3>
                                <div class="pseudo-title">ENTREPRISE</div>
                            </div>
                            <div class="block corporate">
                                <h3><a href="https://www.newswire.ca/fr/news-releases/les-produits-kruger-s-est-vu-decerner-une-certification-argent-parite-par-la-gouvernance-au-feminin-803274397.html"
                                        target="_blank" aria-label="opens in a new tab">La Gouvernance au féminin, certification Parité argent</a></h3>
                                <div class="pseudo-title">ENTREPRISE</div>
                            </div>
                            <div class="block customer">
                                <h3>Au 2<sup>e</sup> rang parmi les fournisseurs de biens emballés au Canada, sondage du secteur</h3>
                                <div class="pseudo-title ">CLIENT</div>
                            </div>
                            <div class="block customer">
                                <h3>Meilleur fournisseur de marque maison de l'année par Hy-Vee, marché grand public américain</h3>
                                <div class="pseudo-title ">CLIENT</div>
                            </div>
                            <div class="block customer">
                                <h3>Finaliste de l'année pour la catégorie « Fournisseur de marques maison » chez Walmart Mexico et Amérique centrale, groupe papier hygiénique au Mexique</h3>
                                <div class="pseudo-title ">CLIENT</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://gq.iabc.com/winners/" target="_blank" aria-label="opens in a new tab">Prix d'excellence Gold Quill à l'échelle internationale de l'AIPC, le meilleur parmi les meilleurs, événements spéciaux et expérimentaux, collection Cashmere</a></h3>
                                <div class="pseudo-title ">MARKETING</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://gq.iabc.com/wp-content/uploads/2016/04/Winners-List-formatted-updated-5-31.pdf"
                                        target="_blank" aria-label="opens in a new tab">Prix d'excellence Gold Quill à l'échelle internationale de l'AIPC, responsabilité sociale de l'entreprise, collection Cashmere</a></h3>
                                <div class="pseudo-title ">MARKETING</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://gq.iabc.com/wp-content/uploads/2016/04/Winners-List-formatted-updated-5-31.pdf"
                                        target="_blank" aria-label="opens in a new tab">Prix d'excellence Gold Quill à l'échelle internationale de l'AIPC, événements spéciaux et expérimentaux, collection Cashmere</a></h3>
                                <div class="pseudo-title ">MARKETING</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://toronto.iabc.com/wp-content/uploads/2019/04/2019-OVATION-Awards-Winners-Announcement.pdf"
                                        target="_blank" aria-label="opens in a new tab">Prix d'excellence de l'AIPC/Toronto OVATION, responsabilité sociale de l'entreprise, collection Cashmere</a></h3>
                                <div class="pseudo-title ">MARKETING</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://toronto.iabc.com/wp-content/uploads/2019/04/2019-OVATION-Awards-Winners-Announcement.pdf"
                                        target="_blank" aria-label="opens in a new tab">Prix d'excellence de l'AIPC/Toronto OVATION, événements spéciaux, Cashmere</a></h3>
                                <div class="pseudo-title">MARKETING</div>
                            </div>
                            <div class="block individual">
                                <h3><a href="https://cfig.ca/blog/2019/09/19/2019-life-member-spirit-of-the-independent-recipients/"
                                        target="_blank" aria-label="opens in a new tab">Marché grand public canadien, Fédération canadienne des épiciers indépendants, prix de membre à vie--constructeur du secteur, Michel Manseau, premier V.-P. et directeur général, Marché grand public canadien</a></h3>
                                <div class="pseudo-title">INDIVIDUAL</div>
                            </div>
                        </div>
                        <!-- end 2019  -->
                        <!-- start 2018 -->
                        <div class="swiper-slide">
                            <div class="block corporate">
                                <h3><a href="http://strategyonline.ca/2017/06/13/marketing-awards-the-winners/" target="_blank" aria-label="opens in a new tab">Les entreprises les mieux gérées au Canada</a></h3>
                                <div class="pseudo-title">ENTREPRISE</div>
                            </div>
                            <div class="block corporate">
                                <h3><a href="https://www.forbes.com/canada-best-employers/list/" target="_blank" aria-label="opens in a new tab">Les meilleurs employeurs au Canada de Forbes, classée 2<sup>e</sup></a></h3>
                                <div class="pseudo-title">ENTREPRISE</div>
                            </div>
                            <div class="block corporate">
                                <h3><a href="https://content.eluta.ca/top-employer-kruger-products" target="_blank" aria-label="opens in a new tab">L'un des meilleurs employeurs de la région du Grand Toronto pour une 6<sup>e</sup> année consécutive</a></h3>
                                <div class="pseudo-title">ENTREPRISE</div>
                            </div>
                            <div class="block corporate">
                                <h3><a href="https://stevieawards.com/iba/website-awards-category-winners" target="_blank" aria-label="opens in a new tab">Prix Gold Stevie de la meilleure entreprise à l'échelle internationale—catégorie fabrication, site Web de l'entreprise Kruger Products</a></h3>
                                <div class="pseudo-title">ENTREPRISE</div>
                            </div>
                            <div class="block fabrication">
                                <h3><a href="https://www.youtube.com/watch?v=q_a438i5xhE" target="_blank" aria-label="opens in a new tab">Commission des normes, de l'équité, de la santé et de la sécurité du travail (CNESST), Grand Prix; santé et sécurité au travail  ̶  grandes entreprises, usine à Gatineau, au Québec</a></h3>
                                <div class="pseudo-title ">FABRICATION</div>
                            </div>
                            <div class="block sustainability">
                                <h3><a href="http://www.corporateknights.com/magazines/2018-best-50-issue/2018-best-50-results-15283499/" target="_blank" aria-label="opens in a new tab">Classement des 50 meilleures entreprises citoyennes au Canada de Corporate Knights</a></h3>
                                <div class="pseudo-title ">DÉVELOPPEMENT DURABLE</div>
                            </div>
                            <div class="block customer">
                                <h3>Au 1<sup>er</sup> rang parmi les fournisseurs de biens emballés au Canada, sondage du secteur pour une 6<sup>e</sup> année consécutive</h3>
                                <div class="pseudo-title ">CLIENT</div>
                            </div>
                            <div class="block customer">
                                <h3><a href="https://www.walmartmexico.com/sala-de-prensa/2018/03/22/walmart-de-mexico-y-centroamerica-reconoce-a-grupo-lala-como-proveedor-del-ano-2017" target="_blank" aria-label="opens in a new tab">Fournisseur de marques maison de l'année chez Walmart Mexico et Amérique centrale, groupe papier hygiénique au Mexique</a></h3>
                                <div class="pseudo-title ">CLIENT</div>
                            </div>
                            <div class="block customer">
                                <h3>Fournisseur de l'année des groupes de marketing adaptés, division des produits hors foyer</h3>
                                <div class="pseudo-title ">CLIENT</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="http://gq.iabc.com/wp-content/uploads/2016/04/winners-list-PDF-format.pdf"
                                        target="_blank" aria-label="opens in a new tab">Prix d'excellence Gold Quill à l'échelle internationale de l'AIPC, relations avec les médias, Cashmere</a></h3>
                                <div class="pseudo-title ">MARKETING</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://toronto.iabc.com/wp-content/uploads/2018/04/2018IABCTorontoOVATIONWinners.pdf" target="_blank" aria-label="opens in a new tab">Prix d'excellence de l'AIPC/Toronto OVATION, relation avec les médias, Cashmere</a></h3>
                                <div class="pseudo-title ">MARKETING</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="https://atomicawards.strategyonline.ca/winners/winner/2018/?e=54895&w=Now+Streaming" target="_blank" aria-label="opens in a new tab">Prix Strategy Atomic -- bronze, marque audio, Cashmere</a></h3>
                                <div class="pseudo-title">MARKETING</div>
                            </div>
                            <div class="block individual">
                                <h3><a href="http://strategyonline.ca/2018/01/09/2018-moy-keeping-the-momentum/"
                                        target="_blank" aria-label="opens in a new tab">Catégorie « Strategy »—marchands de l'année, Nancy Marcus, vice-présidente d'entreprise, marketing</a></h3>
                                <div class="pseudo-title">INDIVIDU</div>
                            </div>
                            <div class="block individual">
                                <h3>Association de sécurité Pulp & Paper—Prix Eagle d'excellence de la haute direction, Fred Ceruti, directeur général de l'usine de Memphis</h3>
                                <div class="pseudo-title">INDIVIDU</div>
                            </div>
                        </div>
                        <!-- end 2018 -->
                        <!-- 2017 -->
                      <!--  <div class="swiper-slide">
                            <div class="block corporate">
                                <h3>L’un des meilleurs employeurs de la région du Grand Toronto pour une 5<sup>e</sup>
                                    année de suite</h3>
                                <div class="pseudo-title">ENTREPRISE</div>
                            </div>
                            <div class="block sustainability">
                                <h3>Lauréat du Programme de partenariat – ÉcoConnexions du CN</h3>
                                <div class="pseudo-title">DÉVELOPPEMENT DURABLE</div>
                            </div>
                            <div class="block sustainability">
                                <h3>Classement des entreprises citoyennes les plus responsables de demain de
                                    <i>Corporate Knights</i></h3>
                                <div class="pseudo-title">DÉVELOPPEMENT DURABLE</div>
                            </div>
                            <div class="block marketing">
                                <h3>Prix du Meilleur nouveau produit – SpongeTowels Ultra Fort Minis</h3>
                                <div class="pseudo-title">Marketing</div>
                            </div>
                            <div class="block marketing">
                                <h3>Gagnant au Grand Prix canadien des produits nouveaux, Conseil canadien du commerce
                                    de détail – SpongeTowels Ultra Fort Minis</h3>
                                <div class="pseudo-title ">Marketing</div>
                            </div>
                            <div class="block marketing">
                                <h3>Projet primé du Grand Prix Grafika pour publicité extérieure en français, animation
                                    graphique – Scotties</h3>
                                <div class="pseudo-title ">Marketing</div>
                            </div>
                            <div class="block marketing">
                                <h3>Prix d’excellence de l’AIPE/Toronto OVATION, photographie – Cashmere</h3>
                                <div class="pseudo-title ">Marketing</div>
                            </div>
                            <div class="block marketing">
                                <h3>Prix de mérite de l’AIPE/Toronto OVATION, invitation – Cashmere</h3>
                                <div class="pseudo-title ">Marketing</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="http://strategyonline.ca/2017/06/13/marketing-awards-the-winners/"
                                        target="_blank" aria-label="opens in a new tab">Prix de Marketing, prix d' or, campagne d'affiches dans les
                                        transports en commun – Scotties</a></h3>
                                <div class="pseudo-title ">Marketing</div>
                            </div>
                            <div class="block marketing">
                                <h3><a href="http://strategyonline.ca/2017/06/13/marketing-awards-the-winners/"
                                        target="_blank" aria-label="opens in a new tab">Prix de Marketing, prix d'or, campagne dans les journaux –
                                        Scotties</a></h3>
                                <div class="pseudo-title ">Marketing</div>
                            </div>

                            <div class="block corporate">
                                <h3>Prix Fierté régionale de la Chambre de Commerce du Grand Joliette – Usine de
                                    Crabtree</h3>
                                <div class="pseudo-title ">ENTREPRISE</div>
                            </div>

                            <div class="block customer">
                                <h3>Au 1<sup>er</sup> rang parmi les fournisseurs de biens emballés au Canada, sondage
                                    du secteur pour une 5<sup>e</sup> année de suite</h3>
                                <div class="pseudo-title">Client</div>
                            </div>

                            <div class="block marketing">
                                <h3><a href="http://theadcc.ca/wp-content/uploads/2017/11/Directions-2017-Winners-List.pdf"
                                        target="_blank" aria-label="opens in a new tab">Prix d'argent d'Advertising & Design Club of Canada—Publicité
                                        dans les transports en commun, Scotties</a></h3>
                                <div class="pseudo-title">Marketing</div>
                            </div>


                            <div class="block marketing">
                                <h3><a href="http://theadcc.ca/wp-content/uploads/2017/11/Directions-2017-Winners-List.pdf"
                                        target="_blank" aria-label="opens in a new tab">Prix de mérite d'Advertising & Design Club of
                                        Canada—Illustration publicitaire, Scotties</a></h3>
                                <div class="pseudo-title">Marketing</div>
                            </div>

                            <div class="block marketing">
                                <h3><a href="http://concours.infopresse.com/archive/crea/2017" target="_blank" aria-label="opens in a new tab">Prix
                                        Créa, campagne dans les journaux, Scotties</a></h3>
                                <div class="pseudo-title">Marketing</div>
                            </div>

                            <div class="block marketing">
                                <h3><a href="http://concours.infopresse.com/archive/crea/2017" target="_blank" aria-label="opens in a new tab">Prix
                                        Créa, publicité extérieure, Scotties</a></h3>
                                <div class="pseudo-title">Marketing</div>
                            </div>

                            <div class="block marketing">
                                <h3><a href="https://www.appliedartsmag.com/winners_gallery/advertising/?id=511&year=2017&clip=1"
                                        target="_blank" aria-label="opens in a new tab">Arts appliqués, série d'articles de journaux, Scotties</a></h3>
                                <div class="pseudo-title">Marketing</div>
                            </div>

                            <div class="block marketing">
                                <h3><a href="https://www.appliedartsmag.com/winners_gallery/advertising/?id=525&year=2017&clip=1"
                                        target="_blank" aria-label="opens in a new tab">Arts appliqués, publicité extérieure, Scotties</a></h3>
                                <div class="pseudo-title">Marketing</div>
                            </div>

                            <div class="block marketing">
                                <h3><a href="http://awards.strategyonline.ca/Winners/Winner/2017" target="_blank" aria-label="opens in a new tab">Prix
                                        Stratégie, <i>Connection Strategy Bronze</i>, Cashmere</a></h3>
                                <div class="pseudo-title">Marketing</div>
                            </div>

                            <div class="block marketing">
                                <h3><a href="http://theadcc.ca/wp-content/uploads/2017/11/Directions-2017-Winners-List.pdf"
                                        target="_blank" aria-label="opens in a new tab">Prix de mérite d'Advertising & Design Club of Canada—Publicité,
                                        Cashmere</a></h3>
                                <div class="pseudo-title">Marketing</div>
                            </div>

                            <div class="block customer">
                                <h3><a href="https://www.grocerybusiness.ca/image-gallery/united-grocers-inc-recognizes-supplier-excellence"
                                        target="_blank" aria-label="opens in a new tab">Prix Fournisseur de l'année de United Grocers Inc.</a></h3>
                                <div class="pseudo-title">Client</div>
                            </div>
                            <div class="block sustainability">
                                <h3><a href="http://shortyawards.com/2nd-socialgood/leaders-in-sustainable-thinking-3"
                                        target="_blank" aria-label="opens in a new tab">Finaliste pour le prix Shorty Social Good, leaders en
                                        développement durable</a></h3>
                                <div class="pseudo-title">Développement durable</div>
                            </div>

                            <div class="block corporate">
                                <h3>Prix innovation en santé et sécurité au travail de Kruger Inc., usine de Richelieu
                                </h3>
                                <div class="pseudo-title">Entreprise</div>
                            </div>
                            <div class="block marketing">
                                <h3>Prix Summit Creative Awards, Choix des juges, campagne de publicité imprimée B2B ,
                                    produits hors foyer</h3>
                                <div class="pseudo-title">Marketing</div>
                            </div>
                        </div> -->
                        <!-- end 2017 -->
                        <!-- start 2016 -->
                        <!--<div class="swiper-slide">
                            <div class="block customer">
                                <h3>Au 1<sup>er</sup> rang parmi les fournisseurs de biens emballés au Canada, sondage
                                    du secteur pour une 4<sup>e</sup> année de suite</h3>
                                <div class="pseudo-title">Client</div>
                            </div>
                            <div class="block sustainability">
                                <h3>Prix Durabilité des fournisseurs de Walmart Canada</h3>
                                <div class="pseudo-title">Développement durable</div>
                            </div>
                            <div class="block marketing">
                                <h3>Prix d’excellence de la SCRP, Campagne de relations avec les médias de l’année par
                                    une agence – Cashmere</h3>
                                <div class="pseudo-title">Marketing</div>
                            </div>
                            <div class="block individual">
                                <h3>Prix des femmes STAR de Canadian Grocer – Lucie Martin, directrice d’entreprise,
                                    Gestion des talents</h3>
                                <div class="pseudo-title">Individu</div>
                            </div>
                            <div class="block marketing">
                                <h3>Prix de mérite, Prix Gold Quill de l’AIPE : Photographie – Cashmere</h3>
                                <div class="pseudo-title">Marketing</div>
                            </div>
                            <div class="block individual">
                                <h3>Prix Canada’s Clean50 – Steven Sage, vice-président, Développement durable et
                                    Innovation</h3>
                                <div class="pseudo-title">Individu</div>
                            </div>
                            <div class="block marketing">
                                <h3>Prix de mérite, Prix Gold Quill de l’AIPE, relations avec les médias – Cashmere</h3>
                                <div class="pseudo-title">Marketing</div>
                            </div>
                            <div class="block corporate">
                                <h3>L’un des meilleurs employeurs de la région du Grand Toronto</h3>
                                <div class="pseudo-title">Entreprise</div>
                            </div>
                            <div class="block marketing">
                                <h3>Prix de mérite de l’AIPE/Toronto OVATION : Relations avec les médias – Cashmere</h3>
                                <div class="pseudo-title">Marketing</div>
                            </div>
                            <div class="block customer">
                                <h3>Partenaire de l’année, Fournisseur de marque national de Walmart Canada</h3>
                                <div class="pseudo-title">Client</div>
                            </div>
                            <div class="block marketing">
                                <h3>Prix de mérite de l’AIPE/Toronto OVATION : Photographie – Cashmere</h3>
                                <div class="pseudo-title">Marketing</div>
                            </div>
                            <div class="block sustainability">
                                <h3>Lauréat du Programme de partenariat – ÉcoConnexions du CN</h3>
                                <div class="pseudo-title">Développement durable</div>
                            </div>
                        </div> -->
                        <!-- end 2016 -->
                        <!-- start 2015
                        <div class="swiper-slide">
                            <div class="block marketing">
                                <h3>Meilleur nouveau produit, catégories du papier, du plastique et de l’aluminium,
                                    Grand Prix du Conseil canadien du commerce de détail – SpongeTowels Ultra Fort</h3>
                                <div class="pseudo-title">Marketing</div>
                            </div>
                            <div class="block marketing">
                                <h3>Meilleur papier hygiénique global aux É.-U. par une éminente revue de recherche
                                    auprès des consommateurs – White Cloud Ultra Soft ‘n Thick</h3>
                                <div class="pseudo-title">Marketing</div>
                            </div>
                            <div class="block customer">
                                <h3>Au 1<sup>er</sup> rang parmi les fournisseurs de biens emballés au Canada, sondage
                                    du secteur – Division des produits aux consommateurs</h3>
                                <div class="pseudo-title">Client</div>
                            </div>
                            <div class="block sustainability">
                                <h3>Prix de l’impact sur l’environnement, Pratt Industries, KTG USA, Memphis</h3>
                                <div class="pseudo-title">Développement durable</div>
                            </div>
                            <div class="block customer">
                                <h3>Parternaire de l’année, Fournisseur de marque national de Walmart Canada, produits
                                    de consommation – Division des produits aux consommateurs</h3>
                                <div class="pseudo-title">Client</div>
                            </div>
                            <div class="block sustainability">
                                <h3>Lauréat du Programme de partenariat – ÉcoConnexions du CN</h3>
                                <div class="pseudo-title">Développement durable</div>
                            </div>
                            <div class="block customer">
                                <h3>Fournisseur de l’année de Métro – Division des produits aux consommateurs</h3>
                                <div class="pseudo-title">Client</div>
                            </div>
                            <div class="block sustainability">
                                <h3>Prix vert, Supply & Demand Chain Executive, Développement durable 2015</h3>
                                <div class="pseudo-title">Développement durable</div>
                            </div>
                            <div class="block customer">
                                <h3>Prix du partenariat de BJ’s Wholesale Supplier (marque maison aux É.-U.)</h3>
                                <div class="pseudo-title">Client</div>
                            </div>
                            <div class="block corporate">
                                <h3>L’un des meilleurs employeurs de la région du Grand Toronto</h3>
                                <div class="pseudo-title">Entreprise</div>
                            </div>
                            <div class="block sustainability">
                                <h3>Prix Greening the Supply Chain, Association canadienne des importateurs et
                                    exportateurs</h3>
                                <div class="pseudo-title">Développement durable</div>
                            </div>
                            <div class="block marketing">
                                <h3>Prix spécial : Produit le plus populaire auprès du consommateur, Grand Prix du
                                    Conseil canadien du commerce de détail – SpongeTowels Ultra Fort</h3>
                                <div class="pseudo-title">Marketing</div>
                            </div>
                            <div class="block sustainability">
                                <h3>Prix Business Achievement Award, Chambre de commerce de l’Ontario, Développement
                                    durable 2015 </h3>
                                <div class="pseudo-title">Développement durable</div>
                            </div>
                            <div class="block corporate">
                                <h3>L’un des meilleurs employeurs de la région du Grand Toronto</h3>
                                <div class="pseudo-title">Entreprise</div>
                            </div>
                            <div class="block sustainability">
                                <h3>Entreprises et associations d’entreprisees finaliste, Les phénix, de l’environment,
                                    Développement durable 2015</h3>
                                <div class="pseudo-title">Développement durable</div>
                            </div>
                        </div>
                        end 2015 -->
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </section>
    </div>
</div>

<?php require('footer.php'); ?>
