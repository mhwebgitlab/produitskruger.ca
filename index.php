<?php
$title = 'Accueil';
require('header.php');
?>

<section class="full-height wipe-in" data-section="Page Header">
    <div class="home__masonry">
        <div class="brick-wrap">
            <div class="brick" style="background-image: url(images/collage_paper.png);" role="img">
                <div class="shade"></div>
            </div>
            <div class="brick" style="background-image: url(images/collage_packed.jpg);" role="img">
                <div class="shade"></div>
            </div>
        </div>
        <div class="brick-wrap">
            <div class="brick" style="background-image: url(images/collage_pattern.jpg);" role="img">
                <div class="shade"></div>
            </div>
            <div class="brick" style="background-image: url(images/collage_white.jpg);" role="img">
                <div class="vertical-align missLogo">
                    <span class="logo"><img src="images/logo_pd-with-mission.png" alt="kruger products logo" /></span>
                </div>
            </div>
        </div>
        <div class="brick-wrap">
            <div class="brick" style="background-image: url(images/collage_girl.png);" role="img">
                <div class="shade"></div>
            </div>
        </div>
        <!--- WhiteCloud10337: This is the group shot change at the top -->
        <div class="brick-wrap" style="border: 1px solid #f5f5f5;">
            <div class="brick" style="background-image: url(images/15062_GroupShot_AllBrands4_WC_sRGB_2022_C3_v2.jpg);" role="img">
                <div class="shade"></div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<div class="wrapper">

    <section class="home__welcome text-in" data-section="Welcome">
        <div class="container">
            <div class="pseudo-title">WELCOME</div>
            <h1 class="h4" role="main">Le fabricant de<br> produits de papier<br> en 1<sup>re</sup> place au Canada</h1>
            <p>De son siège social à Mississauga, en Ontario, Produits Kruger emploie 2 700 personnes dans ses 9 usines
                de fabrication à travers l’Amérique du Nord. Partout où nous intervenons, nous investissons dans nos
                communautés et dans l’environnement avec des initiatives de développement durable et des programmes
                caritatifs locaux.</p>
        </div>
    </section>

    <section class="home__slider wipe-in" data-section="Product Slider">
        <div id="onSliderTrigger" style="position:relative; top: 50px;"></div>

        <div class="swiper-container">
            <div>

                <div class="swiper-slide slide slide-4 dark" onclick="this.focus();">

                    <div class="bg-grad" tabindex="0"></div>
                      <!--- WhiteCloud10337: This is the change for the white cloud for the carousel FOR DESKTOP -->
                    <div class="container">
                        <div class="logo vertical-align" style="width:18vw; left:-4%;">
                            <img src="images/KPL-WC-001_Logos_Navy.png"
                                alt="Dans plus de magasins, avec un nouveau look. Mais toujours la même qualité surprenante." />
                        </div>
                        <div class="text vertical-align">
                            <div>
                                <h3 class="h4">Un confort et <br>une résistance suprêmes</h3>
                                <a href="http://mywhitecloud.com/" class="btn pseudo-title" target="_blank" aria-label="White Cloud Website  - opens in a new tab">En savoir
                                    plus</a>
                            </div>
                            <!--<div class="image-container" role="button">
                                <img src="images/groupshot_whitecloud_2022.png" alt="">
                            </div>-->
                        </div>
                        <span class="onSliderTrigger" role="none"></span>
                    </div>
                </div>

                <div class="swiper-slide slide slide-5 dark" onclick="this.focus();">

                    <div class="bg-grad" tabindex="0"></div>

                    <div class="container">
                        <div class="logo vertical-align">
                            <img src="images/Purex-logo-2.png" alt="Purex est le confort à l’état pur." />
                        </div>
                        <div class="text vertical-align">
                            <h4>Le papier absordoux,<br> unique en son genre.</h4>
                            <a href="http://www.purex.ca/" class="btn pseudo-title" target="_blank" aria-label="purex - open in a new tab">En savoir
                                plus</a>
                        </div>
                        <span class="onSliderTrigger" role="none"></span>
                    </div>
                </div>

                <div class="swiper-slide slide slide-1" onclick="this.focus();">

                    <div class="bg-grad" tabindex="0"></div>

                    <div class="container">
                        <div class="logo vertical-align">
                            <img src="images/Cashmere-logo.png" alt="Mon irremplaçable douceur." />
                        </div>
                        <div class="text vertical-align">
                            <h4>Mon irremplaçable<br> douceur.<sup>®</sup></h4>
                            <a href="http://www.cashmere.ca/index_fr.html#home" class="btn pseudo-title"
                                target="_blank" aria-label="cashmere.ca - open in a new tab">En savoir plus</a>
                        </div>
                        <span class="onSliderTrigger" role="none"></span>
                    </div>
                </div>

                <div class="swiper-slide slide slide-3 dark" onclick="this.focus();">

                    <div class="bg-grad" tabindex="0"></div>

                    <div class="container">
                        <div class="logo vertical-align">
                            <img src="images/Scotties-logo.png" alt="Le mouchoir de circonstance." />
                        </div>
                        <div class="text vertical-align">
                            <h4>Le mouchoir<br> de circonstance.<sup>®</sup></h4>
                            <a href="http://www.scotties.ca/fr/" class="btn pseudo-title" target="_blank" aria-label="scotties.ca - open in a new tab">En savoir
                                plus</a>
                        </div>
                        <span class="onSliderTrigger" role="none"></span>
                    </div>
                </div>

                <div class="swiper-slide slide slide-2 dark" onclick="this.focus();">

                    <div class="bg-grad" tabindex="0"></div>

                    <div class="container">
                        <div class="logo vertical-align">
                            <img src="images/Sponge-Towels.png" alt="Absorbant pas à peu près." />
                        </div>
                        <div class="text vertical-align">
                            <h4>Vivez pleinement. <br> Absorbez tout.<sup>MC</sup></h4>
                            <a href="https://spongetowels.ca/fr/" class="btn pseudo-title"
                                target="_blank" aria-label="spongetowels.ca - open in a new tab">En savoir plus</a>
                        </div>
                        <span class="onSliderTrigger" role="none"></span>
                    </div>
                </div>

            </div>
        </div>

        <!-- <div class="swiper-pagination"></div> -->
    </section>

    <section class="home__slider_mob wipe-in" data-section="Product Slider" data-intro-func="startHomeSlider">
        <div id="onSliderTrigger" style="position:relative; top: 50px;"></div>

        <div class="swiper-container">
            <div class="swiper-wrapper">

                <div class="swiper-slide slide slide-1">
                    <div class="container">

                        <div class="bg" tabindex="0"></div>
                        <div class="ptr" tabindex="0"></div>

                        <div class="idx">1/5</div>

                        <div class="text vertical-align">
                            <h3 class="h4">Mon irremplaçable douceur.<sup>®</sup></h3>
                            <a href="http://www.cashmere.ca/index_fr.html#home" class="btn pseudo-title"
                                target="_blank" aria-label="cashmere.ca - open in a new tab">En savoir plus</a>
                        </div>
                        <span class="onSliderTrigger" role="none"></span>
                    </div>
                </div>

                <div class="swiper-slide slide slide-2">
                    <div class="container">

                        <div class="bg" tabindex="0"></div>
                        <div class="ptr" tabindex="0"></div>

                        <div class="idx">2/5</div>

                        <div class="text vertical-align">
                            <h4>Vivez pleinement. <br>Absorbez tout.<sup>MC</sup></h4>
                            <a href="https://spongetowels.ca/fr/" class="btn pseudo-title"
                                target="_blank" aria-label="Spongetowels.ca open in a new tab">En savoir plus</a>
                        </div>
                        <span class="onSliderTrigger" role="none"></span>
                    </div>
                </div>

                <div class="swiper-slide slide slide-3">
                    <div class="container">

                        <div class="bg" tabindex="0"></div>
                        <div class="ptr" tabindex="0"></div>

                        <div class="idx">3/5</div>

                        <div class="text vertical-align">
                            <h4>Le mouchoir de circonstance.<sup>®</sup></h4>
                            <a href="http://www.scotties.ca/fr/" class="btn pseudo-title" target="_blank" aria-label="scotties.ca opens in a new tab">En savoir
                                plus</a>
                        </div>
                        <span class="onSliderTrigger" role="none"></span>
                    </div>
                </div>

                <div class="swiper-slide slide slide-4">
                    <div class="container">

                        <div class="bg" tabindex="0"></div>
                        <div class="ptr" tabindex="0"></div>

                        <div class="idx">4/5</div>

                        <div class="text vertical-align">
                            <h4>Le papier absordoux, unique en son genre.</h4>
                            <a href="http://www.purex.ca/francais/" class="btn pseudo-title" target="_blank" aria-label="purex.ca open in a new tab">En savoir
                                plus</a>
                        </div>
                        <span class="onSliderTrigger" role="none"></span>
                    </div>
                </div>
          <!--- WhiteCloud10337: This is the change for the white cloud carousel FOR MOBILE -->
                <div class="swiper-slide slide slide-5">
                    <div class="container">

                        <div class="bg" tabindex="0" ></div>
                            <div class="ptr" tabindex="0"></div>

                          <!--  <div class="image-container">
                                <img src="images/whiteCloud_packs.png" alt="whiteCloud Packs">
                            </div> -->

                        <div class="idx">5/5</div>

                        <div class="text vertical-align">
                            <div>
                                <h4>Un confort et <br>une résistance suprêmes</h4>
                                <a href="http://mywhitecloud.com/" class="btn pseudo-title" target="_blank" aria-label="My white cloud open in a new tab">En savoir
                                    plus</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- <div class="swiper-pagination"></div> -->
    </section>

    <div class="home__slider_scroll_c"></div>
    <section class="home__welcome home__welcome_2 text-in">
        <div class="container">
            <h4>Nos <br>différences <br>nous rendent <br>plus forts.</h4>
            <p>Produits Kruger est un milieu de travail inclusif et équitable, dans lequel les employés peuvent donner le meilleur d’eux-mêmes et collaborer au sein d'équipes diversifiées qui sont le reflet de nos communautés, de nos clients et de nos consommateurs. Chaque individu apporte de l’excellence à cette entreprise grâce à ses compétences, à son expérience et à sa passion. Ensemble, nous donnons vie à nos valeurs, nous adoptons de nouvelles idées et des innovations, et nous faisons avancer l’entreprise.</p>
        </div>
        <div class="clearfix2"></div>
    </section>

    <!-- <section class="home__welcome">
      <div class="container">
        <div class="pseudo-title">THE COMPANY</div>
        <h4>At a Glance</h4>
      </div>
    </section> -->

    <section data-section="At A Glance">
        <div class="home__columns">

            <div class="col-block wipe-in down" style="background-color:#ffce00">
                <span class="triangle" style="border-color: transparent transparent #ffce00 transparent;" role="none"></span>
                <div class="thumb" style="background-image: url(images/home_grid_1.png);" role="img"></div>
                <h5 class="h6" role="Heading"><span>Le plus grand portefeuille de produits de papier en 1<sup>re</sup> place au Canada</span></h5>
            </div>

            <div class="col-block wipe-in down" style="background-color:#ff634a">
                <span class="triangle" style="border-color: transparent transparent #ff634a transparent;" role="none"></span>
                <div class="thumb" style="background-image: url(images/home_grid_2.jpg);" role="none"></div>
                <h6 role="Heading"><span>Le 5<sup>e</sup> meilleur partenaire qui soutient la lutte contre le cancer du sein de la
                        Société canadienne du cancer</span></h6>
            </div>

            <div class="col-block wipe-in down" style="background-color:#5b9fc0">
                <span class="triangle" style="border-color: transparent transparent #5b9fc0 transparent;" role="none"></span>
                <div class="thumb" style="background-image: url(images/home_grid_3.jpg);" role="img"></div>
                <h6 role="Heading"><span>Classé parmi les meilleurs employeurs chaque année depuis 2013</span></h6>
            </div>
            <!--- WhiteCloud10337: This is the net and newsworthy for the white cloud below the carousel -->
            <div class="col-block wipe-in down" style="background-color:#bfb7b1">
                <span class="triangle" style="border-color: transparent transparent #bfb7b1 transparent;" role=:none></span>
                <div class="thumb" style="background-image: url(images/home_grid_4_2022.png);" role="img"></div>
                <h6 role="Heading"><span>Produits de haute qualité auxquels vous pouvez faire confiance : de la salle de bain à la cuisine, et plus encore.</span></h6>
            </div>

            <div class="col-block wipe-in down" style="background-color:#51678e">
                <span class="triangle" style="border-color: transparent transparent #51678e transparent;" role="none"></span>
                <div class="thumb" style="background-image: url(images/home_grid_5.jpg);" role="img"></div>
                <h6 role="Heading"><span>Compagnie reconnue à l’échelle nationale en tant que pionnier de l’industrie en matière de
                        développement durable</span></h6>
            </div>

            <div class="col-block wipe-in down" style="background-color:#54596a">
                <span class="triangle" style="border-color: transparent transparent #54596a transparent;" role="none"></span>
                <div class="thumb" style="background-image: url(images/home_grid_6.jpg); background-color: #fff;" role="img"></div>
                <h6 role="Heading"><span>Le fabricant de produits de papier hors foyer en 1<sup>re</sup> place au Canada</span></h6>
            </div>

            <div class="col-block wipe-in down" style="background-color:#be7392">
                <span class="triangle" style="border-color: transparent transparent #be7392 transparent;" role="none"></span>
                <div class="thumb" style="background-image: url(images/image002.png);" role="img"></div>
                <h6 role="Heading"><span>Classé parmi les meilleurs fournisseurs de biens de consommation emballés selon les détaillants canadiens depuis 2010.</span></h6>
            </div>

            <div class="col-block wipe-in down" style="background-color:#d89d37">
                <span class="triangle" style="border-color: transparent transparent #d89d37 transparent;" role="none"></span>
                <div class="thumb" style="background-image: url(images/home_grid_7.png);" role="img"></div>
                <h6 role="Heading"><span>Un succès fabriqué au Québec</span></h6>
            </div>

        </div>
        <div class="clearfix"></div>
    </section>

    <section class="home__welcome lead-in text-in">
        <div class="container">
            <h2>Nouveautés notables</h2>
        </div>
    </section>

    <section data-section="News">
        <div class="home__news">
          <a class="wipe-in" href="#" data-featherlight="#uh-lightbox" style="display:none;"  aria-label="temporary removed link" >
            <!-- 2021-08-13 removed temporary
             <span class="bg" style="background-image: url(images/news_0.jpg);" role="img"></span>
                <span class="mini-wipe"></span>
                <span class="content">
                    <span class="cta">
                        <span>Regarder maintenant</span>
                    </span>
                </span>
                <!-- <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-4"></use>
                </svg> -->
        <!--    </a> -->
            <a class="wipe-in" aria-label="link to a-propos.php" href="a-propos.php">
                <span class="bg" style="background-image: url(images/news_1.jpg);" role=:img></span>
                <span class="mini-wipe"></span>
                <!-- <span class="content">
                    <h4>
                        <hr style="border-color: #ffc419;" />Partenaire officiel canadien de la Ligue nationale de
                        hockey (LNH<sup>®</sup>)</h4>
                    <span class="cta">
                        <span>En savoir plus</span>
                    </span>
                </span> -->
                <!-- <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-4"></use>
          </svg> -->
            </a>
            <!--- WhiteCloud10337: New and Networthy part of white cloud -->
      <!--      <a class="wipe-in" target="_blank" href="https://mywhitecloud.com" aria-label="temp - opens in a new tab" tabindex="0">
                <span class="bg" style="background-image: url(images/New_and_Noteworthy_White_Cloud.jpg"></span>
                <span class="mini-wipe"></span>
                <span class="content">
                  <h4 class="white_cloud_h4"><hr/><strong>Nous déployons un nouveau look! <br>Avec de nouveaux produits améliorés de haute qualité auxquels vous pouvez faire confiance : de la salle de bain à la cuisine, et plus encore.</strong></h4>
                  <span class="cta">
                    <span>En savoir plus</span>
                  </span>
                </span> -->
              <!-- <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-4"></use>
              </svg> -->
            </a>
            <a class="wipe-in" target="_blank"
                href="https://www.newswire.ca/fr/news-releases/produits-kruger-figure-pour-une-cinquieme-annee-consecutive-sur-la-prestigieuse-liste-des-meilleurs-employeurs-au-canada-2021-du-magazine-forbes-828472172.html" aria-label="New Wire 50 Best Corporate Citizens in Canada - opens in a new tab" tabindex="0">
                <span class="bg" style="background-image: url(images/news_3.jpg);" role="img"></span>
                <span class="mini-wipe"></span>
                <span class="content">
                    <h4>
                        <hr style="border-color: #83bf60;" />Nommé l’un des meilleurs employeurs au Canada par Forbes pour la cinquième année consécutive
                    </h4>
                    <span class="cta">
                        <span>En savoir plus</span>
                    </span>
                </span>
                <!-- <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-4"></use>
          </svg> -->

            <a class="wipe-in" target="_blank"
                href="https://www.newswire.ca/fr/news-releases/produits-kruger-double-la-production-de-sa-future-machine-a-papier-tissu-de-sherbrooke-870798473.html" aria-label="La nouvelle usine de Sherbrooke, QC double sa capacité de Papier Tissu avec un investissement de 111,5 millions $ - open in a new tab">
                <span class="bg" style="background-image: url(images/news_7.jpg);" role="img"></span>
                <span class="mini-wipe"></span>
                <span class="content">
                    <h4>
                        <hr style="border-color: #f67a42;" />La nouvelle usine de Sherbrooke, QC double sa capacité de papier tissu avec un investissement de <br> 111,5 millions $
                    </h4>
                    <span class="cta">
                        <span>En savoir plus</span>
                    </span>
                </span>
                <!-- <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-4"></use>
          </svg> -->
            </a>
            </a>
            <a class="wipe-in" target="_blank" href="https://scotties.ca/fr/" aria-label="scotties website - opens in a new tab" tabindex="0">
                <span class="bg" style="background-image: url(images/Scotties_Carousel_1280x620_FR_R1.jpg);" role="img"></span>
                <span class="mini-wipe"></span>
                <span class="content">
                   <!--  <h4>
                        <hr style="border-color: #ffa8c5;" />
                    </h4> -->
                    <span class="cta">
                        <span>En savoir plus</span>
                    </span>
                </span>
                <!-- <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-4"></use>
          </svg> -->
            </a>
            <a class="wipe-in" target="_blank" href="http://stoh.ca/fr/" aria-label="stoh.ca french site open in a new tab">
                <span class="bg" style="background-image: url(images/news_6_v2.jpeg);" role="img"></span>
                <span class="mini-wipe"></span>
                <span class="content">
                    <h4>
                        <hr style="border-color: #71acf6;" />Scotties<sup>®</sup> ’Tournoi des coeurs<sup>®</sup>
                    </h4>
                    <span class="cta">
                        <span>En savoir plus</span>
                    </span>
                </span>
            </a>
        </div>
    </section>

</div>

<?php require('footer.php'); ?>
