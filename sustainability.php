<?php
$title = 'Développement durable';
require('header.php');
?>

    <div class="sustainability-wrap">
      <section class="leaf-section">
        <div class="container">
          <div class="leaf-inner">
            <div id="2015_trigger" style="position:absolute;top:50vh;"></div>
            <h1 class="fadeInUp">Développement durable</h1>
            <div class="leaf-block slideInUp">
              <img src="images/leaf.png" alt="A green palm leaf">
            </div>
            <div class="year-block"></div>
            <div class="text-block">
              <h4>Développement durable 2015 a été notre premier programme de développement durable.</h4>
              <p>Lancé en 2010, il a eu pour but d’améliorer notre empreinte environnementale tout en répondant aux attentes des communautés au sein desquelles nous travaillons.</p>
            </div>
          </div>
        </div>
      </section>
      <section class="eight-section">
        <div id="trigger2" class="spacer s0" style="position:absolute;top:-400px;"></div>
        <div class="branch"></div>
        <div class="container">
          <div class="eight-inner">
            <span class="eight">
              <!-- <svg version="1.1" id="Layer_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
              	 width="244.5px" height="328.135px" viewBox="0 0 244.5 328.135" enable-background="new 0 0 244.5 328.135" xml:space="preserve">
              <path d="M154.508,134.9c59.249,20.668,88.87,53.324,88.87,97.985c0,29.17-11.096,52.338-33.269,69.504
              	c-22.185,17.165-51.046,25.745-86.596,25.745c-35.546,0-64.791-7.133-87.727-21.418c-22.945-14.279-34.409-34.023-34.409-59.25
              	c0-18.525,7.134-34.022,21.419-46.484c14.279-12.455,32.656-20.658,55.145-24.607c-17.929-7.9-32.743-19.213-44.435-33.955
              	C21.808,127.687,15.96,110.746,15.96,91.605c0-28.861,9.949-51.343,29.854-67.452C65.708,8.054,90.852,0,121.239,0
              	c29.467,0,54.075,6.382,73.831,19.141c19.743,12.762,29.621,29.782,29.621,51.043c0,18.232-7.065,32.814-21.189,43.752
              	C189.371,124.874,173.033,131.868,154.508,134.9z M82.955,179.106c-23.094,13.068-34.635,34.796-34.635,65.171
              	c0,22.186,6.833,40.267,20.507,54.236c13.673,13.977,31.903,20.961,54.687,20.961c19.75,0,35.624-5.161,47.628-15.49
              	c11.998-10.329,18.001-24.306,18.001-41.929c0-14.885-4.637-27.038-13.896-36.465c-9.271-9.408-23.631-18.068-43.072-25.974
              	L82.955,179.106z M67.461,62.438c0.296,24.312,15.494,42.689,45.576,55.145l36.457,15.04c20.659-14.585,30.988-35.549,30.988-62.893
              	c0-18.229-5.236-33.043-15.72-44.435c-10.483-11.393-24.998-17.092-43.523-17.092c-16.715,0-29.853,5.013-39.422,15.04
              	C72.245,33.271,67.461,46.336,67.461,62.438z"/></svg> -->
                <svg xmlns="http://www.w3.org/2000/svg" id="Layer_2" viewBox="0 0 244.5 328.135" width="244.5" height="328.135"><path d="M154.508 134.9c59.249 20.668 88.87 53.324 88.87 97.985 0 29.17-11.096 52.338-33.269 69.504-22.185 17.165-51.046 25.745-86.596 25.745-35.546 0-64.791-7.133-87.727-21.418-22.945-14.279-34.409-34.023-34.409-59.25 0-18.525 7.134-34.022 21.419-46.484 14.279-12.455 32.656-20.658 55.145-24.607-17.929-7.9-32.743-19.213-44.435-33.955-11.698-14.733-17.546-31.674-17.546-50.815 0-28.861 9.949-51.343 29.854-67.452C65.708 8.054 90.852 0 121.239 0c29.467 0 54.075 6.382 73.831 19.141 19.743 12.762 29.621 29.782 29.621 51.043 0 18.232-7.065 32.814-21.189 43.752-14.131 10.938-30.469 17.932-48.994 20.964zm-71.553 44.206c-23.094 13.068-34.635 34.796-34.635 65.171 0 22.186 6.833 40.267 20.507 54.236 13.673 13.977 31.903 20.961 54.687 20.961 19.75 0 35.624-5.161 47.628-15.49 11.998-10.329 18.001-24.306 18.001-41.929 0-14.885-4.637-27.038-13.896-36.465-9.271-9.408-23.631-18.068-43.072-25.974l-49.22-20.51zM67.461 62.438c.296 24.312 15.494 42.689 45.576 55.145l36.457 15.04c20.659-14.585 30.988-35.549 30.988-62.893 0-18.229-5.236-33.043-15.72-44.435-10.483-11.393-24.998-17.092-43.523-17.092-16.715 0-29.853 5.013-39.422 15.04-9.572 10.028-14.356 23.093-14.356 39.195z"/></svg>
            </span>
            <div class="text-block">
              <h4>Axé sur huit objectifs spécifiques.</h4>
              <p>Développement durable 2015 a marqué le début de notre évolution vers une approche plus systématique pour réduire notre impact sur l'environnement. Nous intégrons le développement durable dans nos produits et dans tous les aspects de nos activités, et ce, chaque jour et en tout temps.</p>
              <p>Que ce soit dans notre approvisionnement en fibres, notre façon d’impliquer les communautés locales, notre consommation d’électricité ou le réinvestissement de nos bénéfices, Produits Kruger fait toujours attention à son impact sur l’environnement.</p>
            </div>
            <div class="border-block"></div>
            <div class="cone-block"></div>
            <div class="clearfix2"></div>
          </div>
        </div>
      </section>
      <style>
        .inline{

          display: inline;
        }
     .sustainability-wrap .colored-section h2 {
        font-size: 37px;
     }
        .desktop {
          display: block;

        }
        .mobile {
          display: none;
        }

      @media screen and (max-width: 800px) {
     .sustainability-wrap .colored-section h2 {
        font-size: 21px;
      }
        .mobile {
          display: block;
          font-size: auto;
        }
        .desktop {
          display: none;
        }

           .sustainability-wrap .colored-section .quotes>div {
                          margin-top: 220px;
            }

      }

      @media screen and (max-width: 600px) {
    .sustainability-wrap .colored-section .quote-outer {

           margin-bottom: 135px;
        }
 .sustainability-wrap .colored-section .quotes>div {
                          margin-top: 70px;
            }


  }


    </style>
      <section class="colored-section">
        <div class="container">
          <div class="colored-inner">
            <div class="small-leaf"></div>
            <div class="quote-outer">
              <div class="thumbnail"></div>
              <div class="quotes"><div>
                <h2 class="desktop">« Le développement <br> durable est<br> au cœur de<br> notre entreprise <br>et nous avons fait <br>d'importants <br> progrès depuis <br> dix ans. »</h2>
                <h2 class="mobile">« Le développement durable est  au cœur de notre entreprise  et nous avons fait d'importants progrès depuis dix&nbsp;ans. »</h2>
                <div class="pseudo-title">Dino Bianco, CHEF DE LA DIRECTION</div>
              </div></div>
            </div>
            <p class="inline">Développement durable 2015 est un tournant dans notre approche philosophique qui nous permet de faire plus pour protéger notre planète. Nous comptons bien poursuivre nos progrès dans ce&nbsp;sens&nbsp;avec</p>
            <p class="inline">la présentation de notre programme Développement durable 2020 dont le lancement est prévu plus tard cette année. Nos objectifs ne sont ni futiles ni bon marché. Mais ils forcent Produits Kruger à être plus créative&nbsp;et&nbsp;impliquée.</p>
          </div>
        </div>
      </section>
      <section class="eco-section">
        <div class="container">
          <div class="text-block">
            <h4>Nous joignons l’acte à la parole.</h4>
            <p>Nos investissements financiers pour réduire notre empreinte environnementale sont considérables. Fort heureusement, un grand nombre de ces mesures, que ce soit grâce à l’amélioration de l’intensité énergétique, à la réduction des déchets ou à une chaîne d’approvisionnement plus efficace, aboutiront à des économies à long terme.</p>
            <p>Les investissements que nous réalisons aujourd’hui dans le développement durable permettront à nos enfants et aux vôtres d’hériter d’une planète plus saine qu’ils pourront transmettre à leurs propres enfants.</p>
          </div>
          <div class="border-block"></div>
        </div>
        <div class="location-block">SYSTÈME DE BIOGAZÉIFICATION NEXTERRA, NEW WESTMINSTER, C.-B.</div>
      </section>
      <!-- <section class="section-div"></section> -->
      <section class="progress-section">


        <!-- <svg version="1.1" stroke="#ffc419" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        	 width="1223px" height="3938px" viewBox="0 0 1223 3938" enable-background="new 0 0 1223 3938" xml:space="preserve">
        <path style="stroke-linecap: round; stroke-linejoin: round; stroke-dasharray: 1009.23px; stroke-dashoffset: 1009.23px;" fill="none" stroke="#ffc419" stroke-width="17" stroke-miterlimit="10" d="M-105,11.382h1317.256v936.906H399.984v528.621
        	h769.294v1405.36H408.579v605.98h459.858v449.113"/>
        </svg> -->
        <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" viewBox="0 0 1223 3938" width="1223" height="3938" stroke="#ffc419">
                  <path fill="none" stroke-width="17" stroke-miterlimit="10" stroke-dashoffset="1009.23" d="M-65 11.382h1217.256v890.906H399.984v680.621h769.294v1555.36H408.579v605.98h459.858v449.113" stroke-linecap="round" stroke-linejoin="round" stroke-dasharray="1009.23"/>
        </svg>

        <div class="container">
          <div class="progress-inner">
            <div class="border-block"></div>
            <div class="clearfix2"></div>
            <h2>Nos progrès</h2>
            <div id="trigger1" class="spacer s0"></div>
            <p>Huit objectifs spécifiques, concrets et réalisables étaient au cœur de l’initiative Développement durable 2015 de Produits Kruger. Ces objectifs ont été déterminés en se basant sur l’année 2009 comme point de référence. Collectivement, ils illustrent notre volonté farouche et continue de réduire notre empreinte environnementale et de créer des produits et des processus plus durables.<span class="progress-triangles"></span></p>
            <div class="progress-block cert">
              <div class="img"></div>
              <div class="text">
                <span class="green">
                  <i>
                    <svg class="icon">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checked"></use>
                    </svg>
                  </i>
                OBJECTIF ACCOMPLI</span>
                <h3>Certification FSC<sup>®</sup></h3>
                <p>Produits Kruger a été le premier fabricant canadien de produits de papier à obtenir la certification de chaîne de traçabilité « Chain of Custody » du FSC<sup>®</sup>, dans le cadre du programme de la Rainforest Alliance.</p>
              </div>
              <span class="clearfix2"></span>
            </div>
            <div class="progress-block fibre">
              <div class="img"></div>
              <div class="text">
                <span class="green">
                  <i>
                    <svg class="icon">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checked"></use>
                    </svg>
                  </i>
                  OBJECTIF ACCOMPLI</span>
                <h3>Fibre 100 % certifiée</h3>
                <p>100 % de nos fibres sont certifiées par une tierce partie. Cinquante-sept pour cent sont certifiées Forest Stewardship Council<sup>®</sup> (FSC<sup>®</sup>).</p>
              </div>
              <span class="clearfix2"></span>
            </div>
            <div class="progress-block reduce">
              <div class="img"></div>
              <div class="text">
                <span class="green">
                  <!-- <i> -->
                    <!-- <svg class="icon">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checked"></use>
                    </svg> -->
                    <img src="images/exceededIcon.png" alt="">
                  <!-- </i> -->
                  OBJECTIF DÉPASSÉ</span>
                <h3>Réduction de l’emballage de 5 %</h3>
                <p>Nous avons réduit nos emballages de 13 %! Le poids de ces matériaux d’emballage équivaut au poids de 162 Boeing 767. Nos caisses d’expédition, nos rouleaux en carton d’essuie-tout et de papier hygiénique et nos boîtes de papiers-mouchoirs sont tous faits à partir de fibres 100 % recyclées. Beaucoup de nos matériaux d’emballage sont recyclables là où les installations existent.</p>
              </div>
              <span class="clearfix2"></span>
            </div>
            <div class="progress-block prod">
              <div class="img"></div>
              <div class="text">
                <span class="green">
                  <i>
                    <svg class="icon">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checked"></use>
                    </svg>
                  </i>
                  OBJECTIF ACCOMPLI</span>
                <h3>Plus de 100 produits certifiés</h3>
                <p>Nous avons ajouté plus de 60 produits certifiés par une tierce partie, notamment des produits certifiés FSC<sup>®</sup> et/ou certifiés ÉCOLOGO de l’entreprise UL. Produits Kruger offre l’un des plus grands portefeuilles nord-américains de produits certifiés par une tierce partie et reste le chef de file en matière de fabrication de produits innovants.</p>
              </div>
              <span class="clearfix2"></span>
            </div>
            <div class="progress-block energy">
              <div class="img"></div>
              <div class="text">
                <span class="yellow">
                  <i>
                    <svg class="icon">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows"></use>
                    </svg>
                  </i>
                  AMÉLIORATION</span>
                <h3>Réduction de 15 % de notre consommation d’énergie</h3>
                <p>En 2015, nous avons réduit notre consommation globale d’énergie de 5 % et notre consommation d’énergie au Canada de 20 % : assez pour alimenter en électricité plus de 34 000 foyers canadiens.</p>
              </div>
              <span class="clearfix2"></span>
            </div>
            <div class="progress-block emissions">
              <div class="img"></div>
              <div class="text">
                <span class="green">
<!--                   <i>
                    <svg class="icon">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checked"></use>
                    </svg>
                  </i> -->
                  <img src="images/exceededIcon.png" alt="">
                  OBJECTIF DÉPASSÉ</span>
                <h3>Réduction des émissions de 15 %</h3>
                <p>Nous avons réduit nos émissions globales de GES de 18 % et nos émissions canadiennes de 27 % en diminuant notre consommation d'énergie traditionnelle et en augmentant notre consommation d'énergie de substitution et renouvelable. Cela représente l’équivalent de 29 millions d’arbres plantés.</p>
              </div>
              <span class="clearfix2"></span>
            </div>
            <div class="progress-block transport">
              <div class="img"></div>
              <div class="text">
                <span class="yellow">
                  <i>
                    <svg class="icon">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows"></use>
                    </svg>
                  </i>
                  AMÉLIORATION</span>
                <h3>Réduction de 15 % des émissions liées aux transports</h3>
                <p>Nous avons amélioré de 9 % notre efficacité en matière de transport, principalement en optimisant les volumes de livraison, ce qui a également permis d’améliorer notre logistique et l’efficacité de nos livraisons. Nous avons aussi utilisé des modes de transport plus économes en énergie carbone, tels que le transport ferroviaire.</p>
              </div>
              <span class="clearfix2"></span>
            </div>
            <div class="progress-block water">
              <div class="img"></div>
              <div class="text">
                <span class="green">
<!--                   <i>
                    <svg class="icon">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checked"></use>
                    </svg>
                  </i> -->
                  <img src="images/exceededIcon.png" alt="">
                  OBJECTIF DÉPASSÉ</span>
                <h3>Réduction de 15 % de notre consommation en eau</h3>
                <p>Nous avons pu réduire notre consommation globale en eau de 18 % sans compromettre nos normes strictes de traitement des eaux usées. Ces économies en eau représentent l’équivalent de plus de 1 800 piscines olympiques.</p>
              </div>
              <span class="clearfix2"></span>
            </div>
            <div class="spacer s2"></div>
          </div>
        </div>
      </section>
      <section class="footprint-section">
        <div class="square"></div>
        <div class="container">
          <div class="footprint-inner">
            <h2 class="bg-clip">Diminution de notre empreinte environnementale</h2>
            <h4>Coup d’œil sur les progrès en matière de développement durable</h4>
            <div class="cone"></div>
            <p>Le graphique suivant indique notre empreinte en 2009 à titre de point de référence (vert) dans six domaines mesurés dans le cadre de l’initiative Développement durable 2015. Notre empreinte environnementale réelle en 2015 (marron) montre une progression constante dans la plupart de ces domaines, à l’exception de la consommation d’énergie. Notre empreinte environnementale ciblée en 2015 (or) indique l’objectif final de notre programme.</p>
          </div>
          <div class="progress-visualisation">
            <div class="svg-wrap">
              <svg version="1.1" id="Layer_3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
               viewBox="0.522 -9.5 299.5 319.5" enable-background="new 0.522 -9.5 299.5 319.5"
              	 xml:space="preserve">
              <g>
              	<path fill="none" stroke="#DFDEDE" d="M150.075,4.78c80.771,0,146.25,65.476,146.25,146.245
              		c0,80.769-65.479,146.244-146.25,146.244c-80.771,0-146.25-65.476-146.25-146.244C3.824,70.256,69.302,4.78,150.075,4.78z"/>
              	<path fill="none" stroke="#DFDEDE" d="M150.075,29.154c67.309,0,121.875,54.561,121.875,121.864
              		c0,67.305-54.566,121.863-121.875,121.863c-67.311,0-121.875-54.561-121.875-121.863S82.764,29.154,150.075,29.154z"/>
              	<path fill="none" stroke="#DFDEDE" d="M150.075,77.902c40.385,0,73.125,32.738,73.125,73.124c0,40.384-32.74,73.121-73.125,73.121
              		c-40.387,0-73.125-32.737-73.125-73.121C76.949,110.641,109.688,77.902,150.075,77.902z"/>
              	<path fill="none" stroke="#DFDEDE" d="M150.075,53.542c53.84,0,97.486,43.645,97.486,97.483c0,53.838-43.646,97.481-97.486,97.481
              		c-53.842,0-97.488-43.646-97.488-97.481C52.587,97.187,96.233,53.542,150.075,53.542z"/>
              	<path fill="none" stroke="#00775B" stroke-width="1.5" d="M149.857,274.268L46.392,203.712L36.425,94.234l113.432-88.32
              		l115.7,87.263l13.867,123.062L149.857,274.268z"/>
              	<path fill="none" stroke="#DFDEDE" d="M150.075,104.227c25.846,0,46.799,20.949,46.799,46.791
              		c0,25.844-20.953,46.793-46.799,46.793c-25.848,0-46.801-20.949-46.801-46.793C103.274,125.176,124.227,104.227,150.075,104.227z"
              		/>
              	<path fill="none" stroke="#DFDEDE" d="M150.075,126.65c13.461,0,24.375,10.913,24.375,24.375c0,13.461-10.914,24.373-24.375,24.373
              		c-13.463,0-24.375-10.912-24.375-24.373C125.699,137.563,136.612,126.65,150.075,126.65z"/>
              	<path fill="none" stroke="#705c2f" stroke-width="1.5" d="M149.857,184.137l-22.967-20.328l-46.367-46.8l69.333-8.832l91-2.867
              		l-48.1,66.731L149.857,184.137z"/>
              	<path fill="none" stroke="#FFC419" stroke-width="1.5" d="M148.557,245.668l-22.967-80.127l-46.367-46.798l69.333-8.833
              		l62.833,11.865l37.266,81.897L148.557,245.668z"/>
              </g>
              <g>
              	<path fill="#C2CCCF" d="M144.224-4.396c0.163,0.212,0.392,0.362,0.687,0.449v0.016c-0.331,0.087-0.584,0.247-0.76,0.477
              		s-0.264,0.516-0.264,0.857c0,0.263,0.053,0.497,0.159,0.698c0.106,0.202,0.248,0.368,0.427,0.5
              		c0.178,0.131,0.384,0.231,0.616,0.298C145.322-1.033,145.565-1,145.818-1c0.253,0,0.496-0.033,0.729-0.101
              		c0.232-0.066,0.438-0.167,0.617-0.298c0.178-0.132,0.32-0.299,0.426-0.5c0.106-0.201,0.159-0.435,0.159-0.698
              		c0-0.341-0.088-0.627-0.264-0.857s-0.429-0.389-0.76-0.477v-0.016c0.295-0.087,0.523-0.237,0.687-0.449
              		c0.163-0.212,0.244-0.468,0.244-0.768c0-0.253-0.049-0.474-0.147-0.663c-0.099-0.188-0.23-0.346-0.396-0.473
              		c-0.166-0.126-0.359-0.221-0.582-0.283c-0.222-0.062-0.46-0.093-0.713-0.093c-0.253,0-0.492,0.031-0.714,0.093
              		c-0.222,0.062-0.417,0.156-0.585,0.283c-0.168,0.127-0.3,0.284-0.396,0.473c-0.096,0.189-0.144,0.41-0.144,0.663
              		C143.98-4.863,144.062-4.607,144.224-4.396z M145.33-5.427c0.124-0.119,0.287-0.179,0.489-0.179c0.201,0,0.364,0.06,0.488,0.179
              		c0.125,0.119,0.187,0.261,0.187,0.426c0,0.171-0.062,0.316-0.187,0.434c-0.124,0.119-0.287,0.179-0.488,0.179
              		c-0.202,0-0.365-0.06-0.489-0.179c-0.125-0.118-0.186-0.263-0.186-0.434C145.144-5.166,145.205-5.308,145.33-5.427z M145.26-3.256
              		c0.14-0.134,0.326-0.201,0.559-0.201c0.232,0,0.418,0.067,0.558,0.201c0.14,0.135,0.209,0.3,0.209,0.497
              		c0,0.192-0.07,0.354-0.209,0.489s-0.326,0.202-0.558,0.202c-0.233,0-0.419-0.067-0.559-0.202s-0.209-0.297-0.209-0.489
              		C145.05-2.956,145.12-3.122,145.26-3.256z"/>
              	<path fill="#C2CCCF" d="M149.558-1.092h3.832v-1.024h-2.437l1.668-1.435c0.104-0.093,0.199-0.188,0.287-0.287
              		s0.164-0.206,0.229-0.322c0.065-0.116,0.115-0.246,0.151-0.388s0.055-0.301,0.055-0.477c0-0.273-0.046-0.514-0.136-0.721
              		c-0.091-0.207-0.216-0.378-0.377-0.516c-0.16-0.137-0.349-0.24-0.565-0.31c-0.218-0.069-0.45-0.104-0.698-0.104
              		c-0.269,0-0.519,0.035-0.748,0.104c-0.23,0.07-0.431,0.175-0.602,0.314c-0.17,0.14-0.308,0.314-0.411,0.523
              		c-0.104,0.21-0.163,0.456-0.178,0.741l1.164,0.085c0.016-0.197,0.084-0.362,0.205-0.496c0.121-0.135,0.291-0.202,0.508-0.202
              		c0.182,0,0.339,0.053,0.474,0.159s0.202,0.251,0.202,0.438c0,0.166-0.055,0.31-0.164,0.434c-0.107,0.124-0.217,0.235-0.324,0.333
              		l-2.134,1.915V-1.092z"/>
              	<path fill="#C2CCCF" d="M153.368,302.029h-3.831v1.023h2.435l-1.667,1.435c-0.104,0.093-0.198,0.188-0.287,0.287
              		c-0.088,0.098-0.164,0.205-0.229,0.321s-0.115,0.245-0.151,0.388s-0.054,0.301-0.054,0.477c0,0.274,0.045,0.515,0.136,0.722
              		c0.09,0.206,0.216,0.378,0.375,0.515c0.16,0.138,0.35,0.241,0.566,0.311c0.217,0.07,0.45,0.105,0.698,0.105
              		c0.269,0,0.519-0.035,0.749-0.105c0.23-0.069,0.43-0.174,0.602-0.313c0.17-0.14,0.307-0.314,0.41-0.523s0.163-0.457,0.179-0.74
              		l-1.163-0.086c-0.016,0.197-0.084,0.362-0.206,0.496c-0.122,0.135-0.291,0.202-0.509,0.202c-0.18,0-0.338-0.053-0.473-0.159
              		c-0.135-0.105-0.201-0.252-0.201-0.438c0-0.166,0.055-0.311,0.162-0.435c0.109-0.124,0.218-0.235,0.326-0.333l2.133-1.916V302.029z
              		"/>
              	<path fill="#C2CCCF" d="M146.417,304.348h-0.271c-0.129,0-0.259-0.004-0.388-0.012c-0.129-0.008-0.246-0.032-0.349-0.074
              		c-0.104-0.041-0.188-0.104-0.252-0.189c-0.065-0.085-0.097-0.205-0.097-0.36c0-0.207,0.061-0.376,0.182-0.508
              		s0.309-0.198,0.562-0.198c0.248,0,0.436,0.058,0.562,0.171c0.127,0.113,0.211,0.269,0.252,0.466l1.225-0.287
              		c-0.113-0.491-0.348-0.851-0.702-1.078s-0.777-0.342-1.268-0.342c-0.269,0-0.523,0.034-0.764,0.102
              		c-0.24,0.067-0.45,0.169-0.628,0.306c-0.179,0.138-0.321,0.309-0.427,0.516s-0.159,0.447-0.159,0.722
              		c0,0.16,0.023,0.312,0.07,0.457c0.047,0.146,0.114,0.273,0.202,0.389c0.088,0.113,0.195,0.209,0.322,0.286
              		c0.126,0.078,0.272,0.127,0.438,0.147v0.023c-0.305,0.062-0.538,0.201-0.698,0.418c-0.16,0.218-0.24,0.479-0.24,0.784
              		c0,0.263,0.05,0.49,0.151,0.682c0.101,0.191,0.237,0.351,0.407,0.477c0.171,0.127,0.369,0.221,0.594,0.279
              		c0.225,0.06,0.459,0.09,0.702,0.09c0.233,0,0.454-0.025,0.663-0.074s0.399-0.125,0.57-0.229c0.17-0.104,0.318-0.239,0.442-0.407
              		s0.217-0.371,0.279-0.608l-1.28-0.264c-0.036,0.145-0.114,0.266-0.232,0.364c-0.119,0.098-0.269,0.147-0.45,0.147
              		c-0.191,0-0.353-0.055-0.484-0.163s-0.198-0.245-0.198-0.41c0-0.13,0.026-0.234,0.078-0.315c0.052-0.079,0.121-0.142,0.206-0.186
              		c0.085-0.044,0.18-0.073,0.283-0.089s0.209-0.023,0.318-0.023h0.38V304.348z"/>
              	<path fill="#C2CCCF" d="M283.002,78.881l1.915,3.318l0.887-0.512l-1.218-2.109l2.076,0.727c0.133,0.043,0.263,0.078,0.392,0.105
              		c0.13,0.027,0.261,0.039,0.394,0.037c0.133-0.002,0.271-0.022,0.411-0.063c0.142-0.04,0.288-0.103,0.44-0.191
              		c0.237-0.137,0.423-0.296,0.557-0.478c0.134-0.182,0.22-0.376,0.258-0.584c0.039-0.207,0.034-0.422-0.014-0.645
              		c-0.049-0.223-0.135-0.442-0.259-0.657c-0.135-0.233-0.289-0.432-0.465-0.596c-0.176-0.165-0.366-0.286-0.572-0.363
              		c-0.206-0.078-0.426-0.11-0.659-0.095c-0.232,0.015-0.477,0.087-0.73,0.216l0.508,1.05c0.178-0.084,0.355-0.108,0.532-0.07
              		c0.178,0.038,0.32,0.151,0.429,0.339c0.091,0.156,0.124,0.32,0.1,0.489s-0.118,0.301-0.279,0.394
              		c-0.143,0.083-0.295,0.108-0.457,0.076s-0.312-0.071-0.451-0.115l-2.726-0.89L283.002,78.881z"/>
              	<path fill="#C2CCCF" d="M288.486,83.742l0.135,0.236c0.065,0.112,0.127,0.225,0.185,0.341c0.058,0.116,0.095,0.229,0.11,0.339
              		c0.016,0.11,0.003,0.214-0.038,0.312c-0.042,0.099-0.13,0.187-0.264,0.265c-0.18,0.104-0.356,0.135-0.531,0.096
              		c-0.175-0.04-0.325-0.169-0.452-0.388c-0.124-0.215-0.169-0.406-0.134-0.572s0.128-0.317,0.276-0.451l-0.86-0.918
              		c-0.369,0.344-0.562,0.727-0.582,1.147c-0.021,0.421,0.093,0.843,0.338,1.269c0.135,0.233,0.291,0.437,0.47,0.611
              		c0.178,0.175,0.371,0.305,0.579,0.391c0.208,0.086,0.428,0.124,0.66,0.112c0.231-0.011,0.466-0.086,0.704-0.223
              		c0.138-0.08,0.259-0.176,0.36-0.289c0.103-0.112,0.181-0.235,0.235-0.368c0.055-0.134,0.084-0.274,0.088-0.422
              		c0.004-0.149-0.027-0.3-0.092-0.454l0.021-0.012c0.206,0.233,0.443,0.365,0.711,0.395c0.269,0.03,0.534-0.03,0.799-0.183
              		c0.229-0.132,0.4-0.29,0.516-0.472c0.114-0.183,0.185-0.38,0.209-0.591c0.024-0.212,0.006-0.429-0.055-0.654
              		c-0.062-0.224-0.152-0.442-0.274-0.652c-0.116-0.202-0.248-0.38-0.396-0.538c-0.146-0.157-0.308-0.283-0.482-0.379
              		c-0.175-0.096-0.366-0.156-0.574-0.179c-0.207-0.023-0.43-0.003-0.666,0.063l0.411,1.24c0.144-0.041,0.287-0.035,0.432,0.019
              		c0.145,0.054,0.263,0.159,0.353,0.316c0.096,0.166,0.13,0.333,0.102,0.501c-0.027,0.168-0.113,0.294-0.257,0.376
              		c-0.112,0.064-0.216,0.095-0.311,0.09c-0.096-0.005-0.184-0.033-0.265-0.085c-0.08-0.051-0.153-0.119-0.219-0.2
              		c-0.064-0.082-0.124-0.17-0.179-0.264l-0.189-0.33L288.486,83.742z"/>
              	<path fill="#C2CCCF" d="M6.405,85.148c0.265-0.035,0.509-0.158,0.733-0.369l0.013,0.007c-0.089,0.331-0.078,0.629,0.033,0.897
              		c0.111,0.267,0.314,0.486,0.61,0.657c0.228,0.131,0.456,0.202,0.684,0.21c0.228,0.009,0.443-0.03,0.646-0.119
              		c0.203-0.089,0.392-0.217,0.567-0.385c0.174-0.167,0.325-0.361,0.452-0.581c0.127-0.219,0.219-0.447,0.277-0.682
              		c0.058-0.235,0.075-0.463,0.05-0.683c-0.025-0.221-0.098-0.427-0.22-0.619c-0.122-0.193-0.297-0.355-0.525-0.487
              		c-0.295-0.17-0.587-0.237-0.874-0.2c-0.287,0.037-0.551,0.177-0.793,0.419l-0.014-0.008c0.071-0.299,0.056-0.572-0.046-0.819
              		c-0.102-0.248-0.283-0.446-0.543-0.596c-0.219-0.127-0.435-0.194-0.647-0.204c-0.213-0.009-0.415,0.026-0.607,0.106
              		s-0.371,0.201-0.536,0.362c-0.165,0.162-0.311,0.353-0.438,0.572c-0.126,0.219-0.219,0.441-0.276,0.664
              		c-0.058,0.224-0.074,0.44-0.048,0.649C4.929,84.15,5,84.343,5.116,84.521s0.283,0.329,0.502,0.456
              		C5.877,85.126,6.14,85.184,6.405,85.148z M6.064,83.676c-0.041-0.167-0.011-0.338,0.09-0.513c0.101-0.174,0.234-0.286,0.399-0.334
              		c0.165-0.048,0.319-0.03,0.462,0.052c0.147,0.086,0.242,0.212,0.283,0.379c0.041,0.167,0.011,0.338-0.09,0.512
              		c-0.101,0.175-0.233,0.286-0.398,0.334s-0.322,0.03-0.469-0.056C6.198,83.967,6.105,83.843,6.064,83.676z M7.91,84.822
              		c-0.046-0.188-0.012-0.383,0.105-0.584c0.116-0.202,0.268-0.329,0.454-0.383s0.364-0.032,0.535,0.066
              		c0.166,0.096,0.271,0.238,0.318,0.426c0.047,0.188,0.012,0.383-0.104,0.585C9.1,85.132,8.949,85.26,8.763,85.314
              		c-0.186,0.054-0.362,0.033-0.528-0.063C8.064,85.153,7.957,85.01,7.91,84.822z"/>
              	<path fill="#C2CCCF" d="M11.933,82.181l1.916-3.318l-0.886-0.512l-1.218,2.108l-0.408-2.161c-0.029-0.136-0.064-0.267-0.105-0.392
              		c-0.041-0.125-0.096-0.245-0.164-0.359c-0.068-0.114-0.155-0.223-0.26-0.325c-0.105-0.103-0.234-0.198-0.386-0.286
              		c-0.237-0.137-0.468-0.217-0.692-0.243c-0.224-0.025-0.436-0.002-0.634,0.068c-0.199,0.07-0.383,0.182-0.552,0.335
              		c-0.168,0.153-0.315,0.337-0.439,0.552c-0.134,0.233-0.229,0.467-0.284,0.701c-0.055,0.234-0.064,0.459-0.028,0.677
              		s0.118,0.423,0.248,0.618c0.13,0.194,0.314,0.369,0.552,0.525l0.655-0.965c-0.162-0.112-0.271-0.253-0.327-0.426
              		s-0.029-0.353,0.079-0.541c0.091-0.157,0.215-0.267,0.375-0.331c0.159-0.063,0.319-0.049,0.48,0.045
              		c0.143,0.083,0.241,0.202,0.294,0.358c0.053,0.156,0.095,0.305,0.126,0.448l0.592,2.805L11.933,82.181z"/>
              	<path fill="#C2CCCF" d="M290.563,208.96l-1.916,3.318l0.887,0.512l1.218-2.109l0.408,2.161c0.028,0.136,0.063,0.267,0.104,0.393
              		c0.042,0.125,0.097,0.244,0.164,0.358c0.069,0.114,0.155,0.223,0.261,0.325c0.104,0.102,0.233,0.197,0.386,0.285
              		c0.237,0.138,0.469,0.218,0.692,0.243c0.225,0.025,0.436,0.002,0.635-0.068c0.198-0.07,0.383-0.182,0.552-0.335
              		s0.315-0.337,0.439-0.552c0.135-0.233,0.229-0.467,0.283-0.701c0.055-0.234,0.064-0.46,0.029-0.678
              		c-0.036-0.217-0.118-0.423-0.248-0.617s-0.313-0.369-0.552-0.525l-0.655,0.966c0.162,0.111,0.271,0.253,0.326,0.426
              		s0.029,0.353-0.079,0.54c-0.091,0.157-0.216,0.268-0.374,0.331c-0.159,0.063-0.319,0.049-0.48-0.045
              		c-0.144-0.082-0.241-0.201-0.295-0.357c-0.053-0.156-0.095-0.306-0.126-0.449l-0.592-2.805L290.563,208.96z"/>
              	<path fill="#C2CCCF" d="M290.674,219.746l1.659-2.875l-2.594-1.569c-0.037,0.188-0.089,0.375-0.153,0.561
              		c-0.066,0.187-0.146,0.362-0.242,0.527c-0.075,0.13-0.161,0.254-0.259,0.37c-0.098,0.117-0.206,0.211-0.328,0.281
              		c-0.121,0.07-0.251,0.108-0.389,0.115c-0.139,0.007-0.286-0.034-0.442-0.125c-0.206-0.119-0.339-0.281-0.399-0.486
              		s-0.029-0.415,0.095-0.63c0.101-0.175,0.237-0.29,0.41-0.346c0.172-0.056,0.352-0.048,0.537,0.024l0.367-1.195
              		c-0.467-0.132-0.882-0.117-1.243,0.044s-0.661,0.448-0.899,0.859c-0.15,0.261-0.251,0.522-0.303,0.789
              		c-0.052,0.265-0.052,0.521,0.001,0.767c0.052,0.244,0.157,0.474,0.314,0.687s0.37,0.397,0.639,0.553
              		c0.242,0.14,0.483,0.226,0.725,0.257c0.241,0.032,0.473,0.015,0.693-0.052c0.22-0.066,0.425-0.181,0.612-0.34
              		c0.188-0.161,0.352-0.362,0.492-0.604c0.103-0.179,0.166-0.331,0.19-0.454l0.72,0.442l-1.09,1.888L290.674,219.746z"/>
              	<path fill="#C2CCCF" d="M286.442,219.602c-0.32-0.116-0.638-0.177-0.951-0.182s-0.611,0.063-0.893,0.205
              		c-0.282,0.143-0.525,0.39-0.729,0.744c-0.204,0.353-0.297,0.688-0.279,1.003c0.019,0.314,0.107,0.606,0.269,0.875
              		c0.161,0.27,0.372,0.514,0.634,0.733c0.261,0.22,0.532,0.41,0.814,0.573s0.583,0.303,0.904,0.419
              		c0.32,0.116,0.638,0.178,0.951,0.182c0.313,0.006,0.611-0.062,0.894-0.205c0.281-0.141,0.524-0.39,0.729-0.742
              		c0.205-0.354,0.298-0.688,0.279-1.004c-0.018-0.314-0.107-0.606-0.269-0.876s-0.372-0.514-0.633-0.733
              		c-0.262-0.219-0.533-0.41-0.815-0.573C287.064,219.858,286.762,219.719,286.442,219.602z M287.205,221.305
              		c0.168,0.112,0.318,0.239,0.451,0.382c0.133,0.142,0.227,0.296,0.28,0.462c0.053,0.165,0.027,0.339-0.079,0.523
              		c-0.104,0.179-0.24,0.287-0.412,0.325c-0.171,0.039-0.352,0.036-0.541-0.008s-0.375-0.11-0.556-0.2
              		c-0.182-0.09-0.335-0.171-0.46-0.243c-0.126-0.072-0.272-0.164-0.44-0.275c-0.168-0.112-0.318-0.239-0.451-0.382
              		s-0.226-0.298-0.277-0.465c-0.053-0.168-0.027-0.341,0.076-0.521c0.106-0.184,0.244-0.294,0.414-0.329
              		c0.17-0.036,0.35-0.033,0.539,0.011s0.375,0.11,0.557,0.2c0.181,0.09,0.334,0.171,0.459,0.243S287.037,221.192,287.205,221.305z"/>
              	<path fill="#C2CCCF" d="M283.614,224.5c-0.32-0.117-0.638-0.178-0.951-0.182c-0.313-0.006-0.611,0.062-0.893,0.205
              		c-0.282,0.142-0.525,0.39-0.729,0.743s-0.297,0.688-0.279,1.003c0.019,0.315,0.107,0.607,0.269,0.876s0.373,0.514,0.634,0.733
              		c0.262,0.219,0.533,0.41,0.814,0.573c0.282,0.162,0.584,0.302,0.904,0.419c0.321,0.116,0.638,0.177,0.952,0.182
              		c0.312,0.005,0.61-0.063,0.893-0.205c0.281-0.142,0.524-0.39,0.729-0.743c0.203-0.354,0.297-0.688,0.278-1.003
              		c-0.018-0.315-0.107-0.607-0.269-0.876c-0.161-0.27-0.372-0.514-0.633-0.733c-0.262-0.22-0.533-0.411-0.815-0.573
              		C284.237,224.756,283.935,224.616,283.614,224.5z M284.376,226.203c0.168,0.111,0.319,0.239,0.451,0.382
              		c0.133,0.142,0.227,0.296,0.28,0.461s0.027,0.34-0.078,0.523c-0.104,0.179-0.241,0.288-0.413,0.326
              		c-0.171,0.038-0.352,0.035-0.541-0.008c-0.189-0.044-0.375-0.111-0.556-0.2c-0.182-0.09-0.334-0.171-0.46-0.243
              		c-0.125-0.072-0.272-0.164-0.44-0.276s-0.318-0.239-0.451-0.382c-0.133-0.142-0.226-0.297-0.277-0.465
              		c-0.053-0.168-0.027-0.341,0.076-0.521c0.106-0.183,0.244-0.293,0.414-0.329s0.35-0.032,0.54,0.012
              		c0.189,0.044,0.375,0.11,0.556,0.2s0.334,0.171,0.459,0.243C284.062,225.999,284.208,226.091,284.376,226.203z"/>
              	<path fill="#C2CCCF" d="M7.268,221.656l1.66,2.875l2.656-1.462c-0.144-0.126-0.28-0.264-0.409-0.413
              		c-0.128-0.15-0.24-0.308-0.336-0.474c-0.075-0.13-0.139-0.267-0.191-0.409c-0.052-0.143-0.079-0.284-0.079-0.424
              		c0-0.141,0.031-0.272,0.094-0.396s0.173-0.229,0.33-0.32c0.207-0.119,0.413-0.153,0.621-0.103c0.208,0.05,0.374,0.183,0.498,0.396
              		c0.101,0.176,0.132,0.352,0.095,0.528c-0.038,0.177-0.134,0.329-0.289,0.453l0.851,0.915c0.348-0.338,0.542-0.704,0.583-1.098
              		c0.042-0.395-0.057-0.797-0.295-1.209c-0.15-0.26-0.327-0.479-0.531-0.656s-0.425-0.306-0.664-0.383
              		c-0.239-0.077-0.49-0.101-0.753-0.071c-0.263,0.03-0.529,0.123-0.798,0.277c-0.242,0.14-0.437,0.306-0.585,0.499
              		c-0.148,0.192-0.249,0.401-0.301,0.626c-0.053,0.225-0.057,0.458-0.012,0.701c0.044,0.243,0.137,0.485,0.276,0.727
              		c0.104,0.18,0.203,0.311,0.298,0.393l-0.744,0.402l-1.089-1.888L7.268,221.656z"/>
              	<path fill="#C2CCCF" d="M9.509,218.063c0.261-0.22,0.472-0.464,0.633-0.732c0.161-0.27,0.25-0.562,0.269-0.876
              		c0.018-0.315-0.075-0.649-0.28-1.004c-0.204-0.354-0.447-0.602-0.729-0.743c-0.282-0.142-0.579-0.21-0.893-0.205
              		c-0.313,0.005-0.63,0.065-0.951,0.182c-0.32,0.117-0.622,0.257-0.904,0.419c-0.282,0.163-0.554,0.354-0.815,0.574
              		c-0.261,0.22-0.472,0.463-0.633,0.732c-0.161,0.27-0.25,0.562-0.269,0.876c-0.018,0.315,0.075,0.649,0.279,1.003
              		c0.205,0.354,0.447,0.602,0.729,0.743c0.282,0.143,0.58,0.211,0.893,0.206c0.314-0.005,0.631-0.066,0.952-0.182
              		c0.321-0.117,0.622-0.257,0.904-0.419C8.977,218.474,9.248,218.283,9.509,218.063z M7.653,217.872
              		c-0.181,0.091-0.366,0.157-0.556,0.2c-0.189,0.044-0.369,0.048-0.54,0.012c-0.17-0.036-0.308-0.146-0.414-0.329
              		c-0.104-0.18-0.129-0.353-0.077-0.521c0.053-0.168,0.145-0.322,0.278-0.465s0.283-0.27,0.451-0.382
              		c0.168-0.111,0.315-0.204,0.44-0.276s0.278-0.153,0.459-0.243c0.181-0.089,0.367-0.156,0.556-0.2s0.37-0.046,0.542-0.008
              		s0.309,0.147,0.412,0.326c0.106,0.185,0.132,0.358,0.079,0.523c-0.054,0.165-0.147,0.319-0.28,0.462
              		c-0.133,0.142-0.283,0.269-0.452,0.381c-0.168,0.112-0.314,0.204-0.44,0.276C7.987,217.702,7.834,217.783,7.653,217.872z"/>
              </g>
              </svg>
              <div class="ghg">
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#suneps"></use>
                </svg>
                <div class="clearfix"></div>
                Domaine 1 <br>Émissions de GES
                <span>(MT CO2 e/TMSM)</span>
              </div>
              <div class="water">
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#untitled-5"></use>
                </svg>
                <div class="clearfix"></div>
                Consommation <br>en eau
                <span>(M3/TMSM)</span>
              </div>
              <div class="packing">
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#box"></use>
                </svg>
                <div class="clearfix"></div>
                Matériaux <br>d’emballage
                <span>(livres/caisse quota)</span>
              </div>
              <div class="fibre">
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#book"></use>
                </svg>
                <div class="clearfix"></div>
                Fibre certifiée par <br>une tierce partie
                <span>(% du total)</span>
              </div>
              <div class="optimization">
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#untitled-3"></use>
                </svg>
                <div class="clearfix"></div>
                Optimisation <br>des volumes
                <span>(m3/livraison)</span>
              </div>
              <div class="energy">
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#flash"></use>
                </svg>
                <div class="clearfix"></div>
                Consommation <br>d’énergie
                <span>(GJ/TMSM)</span>
              </div>
            </div>
            <div class="progress-exp">
              <p>Point de référence 2009</p>
              <p>Données réelles de 2015</p>
              <p>Cible pour 2015</p>
              <span>TMSM= Tonnes métriques séchées sur machine</span>
            </div>
            <div class="clearfix2"></div>
          </div>
        </div>
      </section>
      <section class="reports-section">
        <div class="square"></div>
        <div class="container">
          <div class="reports-inner">
            <h2 class="bg-clip">Nos rapports</h2>
            <div class="leaf"></div>
            <p>Chaque année, nous établissons un rapport de progression vers la réalisation de nos objectifs de développement durable. Cette présentation transparente de rapports nous oblige à prendre nos responsabilités et permet aux principaux intervenants de surveiller nos progrès.</p>
          </div>
          <div class="downloads">
            <div class="report">
              <div class="grow">
                <div class="vertical-align">
                  <div class="date">2015</div>
                  <a target="_blank" href="http://produitskruger.ca/2017/pdfs/Kruger%202015%20Program%20Results%20Sell%20Sheet%20FRENCH.pdf" class="btn">TÉLÉCHARGER LE PDF</a>
                </div>
              </div>
            </div>
            <div class="report">
              <div class="grow">
                <div class="vertical-align">
                  <div class="date">2014</div>
                  <a target="_blank" href="http://produitskruger.ca/2017/pdfs/KPL_2014_SummaryReport_FR.pdf" class="btn">TÉLÉCHARGER LE PDF</a>
                </div>
              </div>
            </div>
            <div class="report">
              <div class="grow">
                <div class="vertical-align">
                  <div class="date">2012/13</div>
                  <a target="_blank" href="http://produitskruger.ca/2017/pdfs/14174 Kruger Products Sustainability Report_FR 2012_2013 fa.pdf" class="btn">TÉLÉCHARGER LE PDF</a>
                </div>
              </div>
            </div>
            <div class="report">
              <div class="grow">
                <div class="vertical-align">
                  <div class="date">2011</div>
                  <a target="_blank" href="http://produitskruger.ca/2017/pdfs/Kruger-Products-2011-Annual-Sustainable-Development-Report_FR.pdf" class="btn">TÉLÉCHARGER LE PDF</a>
                </div>
              </div>
            </div>
            <!-- <div class="report">
              <div class="grow">
                <div class="vertical-align">
                  <div class="date">2010</div>
                  <a target="_blank" href="/pdfs/Sustainability_Reports/Kruger-Products-2010-Annual-Sustainable-Development-Report.pdf" class="btn">Download pdf</a>
                </div>
              </div>
            </div> -->
            <div class="clearfix"></div>
          </div>
        </div>
      </section>
      <section class="leadership-section">
        <div class="branch"></div>
        <div class="container">
          <div class="leadership-inner">
            <h2 class="bg-clip">Leadership éclairé</h2>
            <p>Chez Produits Kruger, nous avons conscience d’avoir une expérience précieuse à partager, mais aussi d’avoir de nombreuses choses à apprendre. C’est dans cet état d’esprit qu’en 2012, nous avons formé un partenariat avec le magazine Canadian Grocer afin de créer une série de tables rondes réunissant des leaders dans le domaine du développement durable. Notre objectif : trouver des solutions innovantes pour les fabricants et détaillants de biens de consommation courante et promouvoir les meilleures pratiques de l’industrie et la collaboration.</p>
            <div class="leaders-logo"></div>
          </div>
          <div class="downloads">
            <div class="pdf-c">
              <h5>La collaboration en temps d’incertitude (2016)</h5>
              <div class="grow">
                <h5>Pourquoi l’entreprise doit-elle développer une conscience collective?</h5>
                <a class="btn pseudo-title" href="http://produitskruger.ca/2017/pdfs/2016%20Sustainability%20White%20Paper%20FR.PDF" target="_blank">TÉLÉCHARGER LE PDF
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                </svg>
                </a>
              </div>
            </div>
            <div class="pdf-c">
              <h5>Bilan de la situation écologique (2015)</h5>
              <div class="grow">
                <h5>Atténuation des risques dans un mode incertain : une approche durable</h5>
                <a class="btn pseudo-title" href="http://produitskruger.ca/2017/pdfs/2015%20Sustainability%20White%20Paper%20FR.PDF" target="_blank">TÉLÉCHARGER LE PDF
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                </svg>
                </a>
              </div>
            </div>
            <div class="pdf-c">
              <h5>Logistique et transport durables (2014)</h5>
              <div class="grow">
                <h5>Transport et logistique durables dans l’industrie des biens emballés pour la vente au détail</h5>
                <a class="btn pseudo-title" href="http://produitskruger.ca/2017/pdfs/2014%20Sustainability%20White%20Paper%20FR.PDF" target="_blank">TÉLÉCHARGER LE PDF
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                </svg>
                </a>
              </div>
            </div>
            <div class="pdf-c">
              <h5>Approvisionnement responsable (2013)</h5>
              <div class="grow">
                <h5>L’approvisionnement responsable dans l’industrie des biens emballés pour la consommation : la force motrice</h5>
                <a class="btn pseudo-title" href="http://produitskruger.ca/2017/pdfs/2013%20Sustainability%20White%20Paper%20FR.PDF" target="_blank">TÉLÉCHARGER LE PDF
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                </svg>
                </a>
              </div>
            </div>
            <div class="pdf-c">
              <h5>Livre blanc (2012)</h5>
              <div class="grow">
                <h5>Aider les ménages canadiens à atteindre leurs objectifs de durabilité</h5>
                <a class="btn pseudo-title" href="http://produitskruger.ca/2017/pdfs/2012%20Sustainability%20White%20Paper%20FR.PDF" target="_blank">TÉLÉCHARGER LE PDF
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                </svg>
                </a>
              </div>
            </div>
            <div class="clearfix2"></div>
          </div>
          <div class="leadership-inner-2">
            <h5>Centre de ressources de durabilité</h5>
            <p>Nous avons travaillé avec CanadianGrocer.com pour créer et commanditer en exclusivité le centre de ressources de durabilité qui met en lumière les meilleures pratiques de l'industrie dans les secteurs suivants : les consommateurs écolos, l'approvisionnement responsable, les produits et emballages, les opérations et la logistique et le virage vert.</p>
            <a href="http://www.canadiangrocer.com/microsite/kruger-sustainability/" target="_blank">Visitez le centre de ressources de durabilité</a>
          </div>
        </div>
      </section>
      <section class="policies-section">
        <div class="square"></div>
        <div class="container">
          <div class="policies-inner">
            <h2 class="bg-clip">Nos politiques</h2>
            <p>Les politiques suivantes servent de lignes directrices pour nos actions de développement durable en interne, mais également avec les partenaires de notre chaîne d'approvisionnement.</p>
          </div>
          <div class="downloads">
            <div class="pdf-c">
              <div class="grow">
                <h5>Code d’éthique et de conduite professionnelle</h5>
                <!-- <a target="_blank" class="btn pseudo-title" href="/pdfs/Sustainability-Our_Policies/Code_of_Business_Conduct_and_Ethics_EN.pdf">TÉLÉCHARGER LE PDF -->
                <a target="_blank" class="btn pseudo-title" href="http://produitskruger.ca/2017/pdfs/Kruger_Code_etiques_FR_v6_FINAL.pdf">TÉLÉCHARGER LE PDF
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                </svg>

                </a>
              </div>
            </div>

            <div class="pdf-c">
              <div class="grow">
                <h5>Politique environnementale</h5>
                <!-- <a target="_blank" class="btn pseudo-title" href="/pdfs/Sustainability-Our_Policies/Kruger_Pol_env_EN_13_v2.pdf">TÉLÉCHARGER LE PDF -->
                <a target="_blank" class="btn pseudo-title" href="http://produitskruger.ca/2017/pdfs/Kruger_Pol_env_FR_13.pdf">TÉLÉCHARGER LE PDF
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                </svg>
                </a>
              </div>
            </div>

            <div class="pdf-c">
              <div class="grow">
                <h5>Politique d’approvisionnement en matière ligneuse</h5>
                <!-- <a target="_blank" class="btn pseudo-title" href="/pdfs/Sustainability-Our_Policies/Kruger_Pol_mat_lign_EN.pdf">TÉLÉCHARGER LE PDF -->
                <a target="_blank" class="btn pseudo-title" href="http://produitskruger.ca/2017/pdfs/Kruger_Pol_mat_lign_FR.pdf">TÉLÉCHARGER LE PDF
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                </svg>
                </a>
              </div>
            </div>

            <div class="pdf-c">
              <div class="grow">
                <h5>Énoncé de politique relatif aux fournisseurs et au développement durable</h5>
                <!-- <a target="_blank" class="btn pseudo-title" href="/pdfs/Sustainability-Our_Policies/sustainability_policies_supplier_en.pdf">TÉLÉCHARGER LE PDF -->
                <a target="_blank" class="btn pseudo-title" href="http://produitskruger.ca/2017/pdfs/sustainability_policies_supplier_fr.pdf">TÉLÉCHARGER LE PDF
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                </svg>
                </a>
              </div>
            </div>

            <div class="clearfix2"></div>

          </div>
        </div>
      </section>
      <section class="resources-section">
        <div class="container">
          <div class="resources-inner">
            <h2>Plus de ressources</h2>
          </div>
          <div class="downloads">
            <div class="pdf-c">
              <h5>Brochure Développement durable 2015</h5>
              <a href="http://produitskruger.ca/2017/pdfs/2015%20Sust%20brochure%20FR.PDF" target="_blank" class="btn pseudo-title">TÉLÉCHARGER LE PDF
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                </svg>
              </a>
            </div>
            <div class="pdf-c">
              <h5>Brochure FSC<sup>®</sup></h5>
              <a href="http://www.krugerproducts.ca/pdfs/Sustainability-More_Resources/13592_KP_Sustainability_2012_EN.pdf" target="_blank" class="btn pseudo-title">TÉLÉCHARGER LE PDF
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                </svg>
              </a>
            </div>
            <div class="pdf-c">
              <h5>Certificat FSC<sup>®</sup></h5>
              <a href="http://www.produitskruger.ca/pdfs/kruger-products-fsc-coc-certificate-2016-2021-fr.pdf" target="_blank" class="btn pseudo-title">TÉLÉCHARGER LE PDF
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                </svg>
              </a>
            </div>
            <div class="pdf-c">
              <h5>Brochure sur les immeubles écologiques</h5>
              <a href="http://www.krugerproducts.ca/pdfs/Sustainability-More_Resources/15307_KP_Green_Building_EN.pdf" target="_blank" class="btn pseudo-title">TÉLÉCHARGER LE PDF
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
              </svg>
              </a>
            </div>
<!--             <div class="pdf-c">
              <h5>AFH Green Products Listing</h5>
              <a href="/pdfs/Sustainability-More_Resources/EnvBrochure.pdf" target="_blank" class="btn pseudo-title">Download pdf
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                </svg>
              </a>
            </div> -->
            <div class="clearfix2"></div>
          </div>
        </div>
      </section>
    </div>

<?php require('footer.php'); ?>
