<?php
$title = 'Communauté';
require('header.php');
?>
<style>
.page-community .vertical-align {
    position: relative;
}

@media screen and (max-height:1280px) {
    .posts-texts-wrapper .swiper-slide .vertical-align {
        padding: 40px;
    }
}
</style>
<div class="community-wrap">
    <section class="swiper-container posts-images-wrapper">
        <div class="swiper-wrapper">
            <div class="swiper-slide js-community-image">
                <div class="swiper-zoom-container" style="background-image:url(images/community-9.jpg)" role="img"></div>
            </div>
            <div class="swiper-slide js-community-image">
                <div class="swiper-zoom-container" style="background-image:url(images/breast_cancer.jpg)" role="img"></div>
            </div>
            <div class="swiper-slide js-community-image">
                <div class="swiper-zoom-container" style="background-image:url(images/hearts_tournament.jpg)" role="img"></div>
            </div>
            <div class="swiper-slide js-community-image">
                <div class="swiper-zoom-container" style="background-image:url(images/2016-GutsyWalk-Kids-Final.jpg)" role="img">
                </div>
            </div>
            <div class="swiper-slide js-community-image">
                <div class="swiper-zoom-container" style="background-image:url(images/community-6.jpg)" role="img"></div>
            </div>
            <div class="swiper-slide js-community-image">
                <div class="swiper-zoom-container" style="background-image:url(images/community-7.jpg)" role="img"></div>
            </div>
            <div class="swiper-slide js-community-image">
                <div class="swiper-zoom-container" style="background-image:url(images/community_10_FR.png)" role="img">
                </div>
            </div>
        </div>
    </section>
    <section class="swiper-container posts-texts-wrapper">
        <div class="swiper-wrapper">
            <div class="swiper-slide community">
                <div class="post-text">
                    <div class="vertical-align ">
                        <span class="category-title">IMPLICATION COMMUNAUTAIRE</span>
                        <h1 class="h4" role="main">L’entreprise de papier<br> proche des gens</h1>
                        <p>Chez Produits Kruger, nous sommes conscients de l’impact positif que nous pouvons avoir sur
                            les communautés dans lesquelles nous vivons. Nous saisissons toutes les occasions de
                            soutenir la santé et le bien-être de nos voisins et partenaires grâce à des programmes
                            caritatifs locaux et des activités de développement communautaire.</p>

                        <div class="arrows mob">
                            <a href="#" onclick="return cScrollTo(1);" title="Next" role="button" tabindex="0">
                                <span>Next</span>
                            </a>
                        </div>

                    </div>
                </div>
                <a href="#" class="text-center slide-link" aria-label="slide down to next" role="button" tabindex="0">
                    <span></span>
                </a>

                <div class="arrows">
                    <a href="#" onclick="return cScrollTo(1);" title="Next"  role="button" tabindex="0">
                        <span>Next</span>
                    </a>
                </div>
            </div>

            <div class="swiper-slide community">
                <div class="post-text">
                    <div class="vertical-align">
                        <div class="imgrow">
                        <a target="_blank" class="category-img" href="http://www.cancer.ca/fr-ca/cancer-information/cancer-type/breast/breast-cancer/?region=qc"
                                title="Canadian Breast Cancer Foundation" aria-label="Canadian Breast Cancer Foundation - open in a new tab" >
                            <img src="images/ccs-logo-fr.png" alt="Canadian Breast Cancer Foundation"
                                    class="large-logo">
                        </a><a target="_blank" class="category-img FRBC" href="https://rubanrose.org/"
                                title="Canadian Breast Cancer Foundation" aria-label="Canadian Breast Cancer Foundation - open in a new tab">
                            <img src="images/FCSQ-logo.svg" alt="Canadian Breast Cancer Foundation"
                                    class="large-logo">
                        </a>
                        </div>
                        <h2 class="h4">Lutte contre le cancer du sein</h2>
                        <p>Depuis 2005, Produits Kruger change la vie des femmes canadiennes et de leurs familles
                            affectées par le cancer du sein. Produits Kruger est l'une des cinq plus grandes marques
                            partenaires de la Société canadienne du cancer en faveur de la lutte contre le cancer du
                            sein sur le plan de la contribution financière annuelle et des initiatives de
                            sensibilisation. Au fil des ans, notre soutien a aidé à financer les recherches novatrices
                            qui visent à réduire l'incidence du cancer du sein, à diminuer les taux de mortalité et à
                            soutenir les femmes et leurs familles affectées par le cancer du sein. Produits Kruger a
                            aussi pris l'engagement de soutenir la lutte contre le cancer du sein en partenariat avec la
                            Fondation du cancer du sein du Québec. Ensemble, nous contribuons à créer un avenir sans
                            cancer du sein. Pour plus de renseignements, <a
                                href="http://www.cancer.ca/fr-ca/cancer-information/cancer-type/breast/breast-cancer/?region=qc"
                                target="_blank" aria-label="cliquez ici - open in a new tab">cliquez ici.</a></p>
                        <p><a href="https://rubanrose.org/" target="_blank" aria-label="ou visitez - open in a new tab">ou visitez</a></p>

                        <div class="arrows mob">
                            <a href="#" onclick="return cScrollTo(2);" title="Next" role="button" tabindex="0">
                                <span>Next</span>
                            </a>
                            <a href="#" onclick="return cScrollTo(0);" title="Previous" role="button" tabindex="0">
                                <span>Previous</span>
                            </a>
                        </div>
                    </div>
                </div>
                <a href="#" class="text-center slide-link" aria-label="slide down to next" role="button" tabindex="0">
                    <span></span>
                </a>

                <div class="arrows">
                    <a href="#" onclick="return cScrollTo(2);" title="Next" role="button" tabindex="0">
                        <span>Next</span>
                    </a>
                    <a href="#" onclick="return cScrollTo(0);" title="Previous" role="button" tabindex="0">
                        <span>Previous</span>
                    </a>
                </div>
            </div>

            <div class="swiper-slide community">
                <div class="post-text">
                    <div class="vertical-align">
                        <a class="category-img" target="_blank" href="https://stoh.ca/fr/"
                            title="Scotties Tournament of Hearts">
                            <img src="images/STOH-logo.png" alt="Scotties Tournament of Hearts">
                        </a>
                        <h3 class="h4">Scotties<sup>&reg;</sup> ’Tournoi <br>des coeurs<sup>&reg;</sup></h3>
                        <p>En 2021, nous célébrons notre 40e année de soutien : nous sommes l’un des plus anciens commanditaires de sports amateurs au Canada.</p>
                        <p><a href="https://stoh.ca/fr/" target="_blank" aria-label="stoh.ca - open in a new tab">stoh.ca</a></p>

                        <div class="arrows mob">
                            <a href="#" onclick="return cScrollTo(3);" title="Next" role="button" tabindex="0">
                                <span>Next</span>
                            </a>
                            <a href="#" onclick="return cScrollTo(1);" title="Previous" role="button" tabindex="0">
                                <span>Previous</span>
                            </a>
                        </div>
                    </div>
                </div>
                <a href="#" class="text-center slide-link" aria-label="slide down to next" role="button" tabindex="0">
                    <span></span>
                </a>

                <div class="arrows">
                    <a href="#" onclick="return cScrollTo(3);" title="Next" role="button" tabindex="0">
                        <span>Next</span>
                    </a>
                    <a href="#" onclick="return cScrollTo(1);" title="Previous" role="button" tabindex="0">
                        <span>Previous</span>
                    </a>
                </div>
            </div>

            <div class="swiper-slide community">
                <div class="post-text">
                    <div class="vertical-align">
                        <a class="category-img" target="_blank" href="http://gutsyenmarche.ca/"
                            title="Crohn’s & Colitis Canada" aria-label="Crohn’s & Colitis Canada - open in a new tab">
                            <img src="images/CCC-logo.png" alt="Crohn’s & Colitis Canada">
                        </a>
                        <h4>Crohn et Colite<br> Canada</h4>
                        <p>Grâce à nos marques de papier hygiénique Cashmere<sup>®</sup> et Purex<sup>®</sup>, en 2014,
                            Produits Kruger s’est associée à Crohn et Colite Canada comme commanditaire national de la
                            randonnée Gutsy en marche. La randonnée Gutsy en marche est le plus grand événement
                            communautaire au Canada dont le but est de récolter des fonds pour la maladie de Crohn et la
                            colite ulcéreuse.</p>
                        <p><a href="http://gutsyenmarche.ca/" target="_blank" aria-label="gutsyenmarche.ca - open in a new tab">gutsyenmarche.ca</a></p>

                        <div class="arrows mob">
                            <a href="#" onclick="return cScrollTo(4);" title="Next" role="button" tabindex="0">
                                <span>Next</span>
                            </a>
                            <a href="#" onclick="return cScrollTo(2);" title="Previous" role="button" tabindex="0">
                                <span>Previous</span>
                            </a>
                        </div>

                    </div>
                </div>
                <a href="#" class="text-center slide-link" aria-label="slide down to next" role="button" tabindex="0">
                    <span></span>
                </a>
                <div class="arrows">
                    <a href="#" onclick="return cScrollTo(4);" title="Next" role="button" tabindex="0">
                        <span>Next</span>
                    </a>
                    <a href="#" onclick="return cScrollTo(2);" title="Previous" role="button" tabindex="0">
                        <span>Previous</span>
                    </a>
                </div>
            </div>


            <div class="swiper-slide community">
                <div class="post-text">
                    <div class="vertical-align">
                        <a class="category-img" target="_blank" href="https://www.rmhccanada.ca/fr"
                            title="Ronald McDonald House Charities" aria-label="Ronald McDonald House Charities - open in a new tab">
                            <img src="images/RMHC_logo.png" alt="Ronald McDonald House Charities">
                        </a>
                        <h4>Œuvre des Manoirs<br> Ronald McDonald</h4>
                        <p>Depuis 1990, 100 % de tous les produits de papier à usage domestique fournis aux 15 manoirs
                            Ronald McDonald à travers le Canada ont été intégralement offerts en don par la division de
                            produits hors foyer de Produits Kruger. Étant donné que 70 % des Canadiens ne vivent pas
                            dans une ville qui dispose d’un hôpital pour enfants, les 15 manoirs Ronald McDonald partout
                            au Canada fournissent un foyer aux familles venant de l’extérieur dont l’enfant est traité
                            dans un hôpital à proximité.</p>
                        <p><a href="https://www.rmhccanada.ca/fr" target="_blank" aria-label="Ronald McDonald House Charities - open in a new tab">rmhccanada.ca</a></p>
                        <div class="arrows mob">
                            <a href="#" onclick="return cScrollTo(5);" title="Next" role="button" tabindex="0">
                                <span>Next</span>
                            </a>
                            <a href="#" onclick="return cScrollTo(3);" title="Previous" role="button" tabindex="0">
                                <span>Previous</span>
                            </a>
                        </div>
                    </div>
                </div>
                <a href="#" class="text-center slide-link" aria-label="slide down to next" role="button" tabindex="0">
                    <span></span>
                </a>
                <div class="arrows">
                    <a href="#" onclick="return cScrollTo(5);" title="Next" role="button" tabindex="0">
                        <span>Next</span>
                    </a>
                    <a href="#" onclick="return cScrollTo(3);" title="Previous" role="button" tabindex="0">
                        <span>Previous</span>
                    </a>
                </div>
            </div>

            <div class="swiper-slide community">
                <div class="post-text">
                    <div class="vertical-align">
                        <a class="category-img" target="_blank" href="http://lesamisdenousaidons.org/"
                            title="Friends of We Care" aria-label="Friends of We Care - open in a new tab">
                            <img src="images/FWC_logo.png" alt="Friends of We Care">
                        </a>
                        <h4>Les Amis de<br> Nous Aidons</h4>
                        <p>Depuis plus de 10 ans, Produits Kruger aide les enfants en situation de handicap à participer
                            aux camps Easter Seals grâce à la commandite organisée entre notre division des produits
                            hors foyer et Les Amis de Nous Aidons. Chaque année, l’association Les Amis de Nous Aidons
                            donne à 5 000 enfants handicapés la chance de participer aux camps Easter Seals totalement
                            accessibles, partout au Canada.</p>
                        <p><a href="http://lesamisdenousaidons.org/" target="_blank" aria-label="lesamisdenousaidons.org - open in a new tab">lesamisdenousaidons.org</a></p>
                        <div class="arrows mob">
                            <a href="#" onclick="return cScrollTo(4);" title="Previous" role="button" tabindex="0">
                                <span>Previous</span>
                            </a>
                        </div>
                    </div>
                </div>
                <a href="#" class="text-center slide-link" aria-label="slide down to next" role="button" tabindex="0">
                    <span></span>
                </a>
                <div class="arrows">
                <a href="#" onclick="return cScrollTo(6);" title="Next" role="button" tabindex="0">
                        <span>Next</span>
                    </a>
                    <a href="#" onclick="return cScrollTo(4);" title="Previous" role="button" tabindex="0">
                        <span>Previous</span>
                    </a>
                </div>
            </div>

            <div class="swiper-slide community">
                <div class="post-text">
                    <div class="vertical-align">
                        <a class="category-img" target="_blank"
                            href="https://www.krugerbigassist.ca/fr/"
                            title="La Passe à la relève">
                            <img src="images/krugerbigassist-logo.png" alt="La Passe à la relève">
                        </a>
                        <h4>La Passe à la relève</h4>
                        <p>Le hockey est bien plus que le sport du Canada. C’est un moyen pour les enfants, les familles et les communautés partout au pays de se réunir, de partager leurs expériences et de faire partie de quelque chose qui nous unit tous.</p>
                        <p>Cependant, le hockey peut être coûteux pour de nombreuses familles. Le programme La passe à la relève Kruger a été conçu pour venir en aide. En collaboration avec des associations de hockey mineur partout au pays, nous donnons plus de 150 000 $ en aide financière pour aider les familles de joueurs de hockey dans le besoin.</p>
                        <p><a href="https://www.krugerbigassist.ca/fr/" target="_blank" aria-label="kruger big assist - opens in a new tab">Krugerbigassist.ca</a>
                        </p>
                        <div class="arrows mob">
                            <a href="#" onclick="return cScrollTo(6);" title="Previous" role="button" tabindex="0">
                                <span>Previous</span>
                            </a>
                        </div>
                    </div>
                </div>
                <a href="#" class="text-center slide-link" aria-label="slide down to next" role="button" tabindex="0">
                    <span></span>
                </a>
                <div class="arrows">
                    <a href="#" onclick="return cScrollTo(6);" title="Previous" role="button" tabindex="0">
                        <span>Previous</span>
                    </a>
                </div>
            </div>


        </div>

        <div class="swiper-pagination-outer">
            <div class="swiper-pagination-line"></div>
            <div class="swiper-pagination"></div>
        </div>

    </section>
    <!-- <div class="community-scrollbar"></div> -->
</div>


<div class="showbox animated">
    <!-- <div class="loader">
        <svg class="circular" viewBox="25 25 50 50">
          <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
        </svg>
      </div> -->
</div>

<script src="js/all.js"></script>
<?php //require('footer.php'); ?>
</body>
</html>
