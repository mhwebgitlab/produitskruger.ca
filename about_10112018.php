<?php
$title = 'À propos';
require('header.php');
?>
    <div class="about">
      <section class="about__story">
        <div class="about__story-inner" data-section="Page Header">
          <h1>Notre histoire</h1>
          <div class="about__slider swiper-container state-0">
            <div class="swiper-wrapper">
              <div class="about__slide swiper-slide slide">
                <div class="container">

                  <div class="img-block" style="background-image:url(images/row_1_1.jpg)">
                  </div>
                  <div class="img-block position" style="background-image:url(images/row_1_2.jpg)">
                  </div>
                  <div class="img-block" style="background-image:url(images/row_1_3.jpg)">
                  </div>
                  <div class="img-block" style="background-image:url(images/row_1_4.jpg)">
                  </div>

                  <div class="img-block" style="background-image:url(images/row_2_1.jpg)">
                  </div>
                  <div class="img-block position" style="background-image:url(images/about_sponge2.jpg)">
                  </div>
                  <div class="img-block" style="background-image:url(images/row_2_3.jpg)">
                  </div>
                  <div class="img-block" style="background-image:url(images/row_2_4.jpg)">
                  </div>

                  <div class="img-block" style="background-image:url(images/row_3_1.jpg)">
                  </div>
                  <div class="img-block" style="background-image:url(images/row_3_2.jpg)">
                  </div>
                  <div class="img-block position" style="background-image:url(images/row_3_3.jpg)">
                  </div>
                  <div class="img-block" style="background-image:url(images/4up_dudeswalking.jpg)">
                  </div>

                  <div class="img-block" style="background-image:url(images/row_4_1.jpg)">
                  </div>
                  <div class="img-block" style="background-image:url(images/row_4_2.jpg)">
                  </div>
                  <div class="img-block" style="background-image:url(images/row_4_3_v2.jpg)">
                  </div>
                  <div class="img-block" style="background-image:url(images/row_4_4.jpg)">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="home__welcome text-in" data-section="Overview Text">
          <div class="container">
            <div class="pseudo-title">OVERVIEW</div>
            <h4>Nous fabriquons<br/> les marques de produits <br/>de papier les plus<br/> populaires au Canada</h4>
            <p>De grandes choses peuvent être réalisées lorsque les gens travaillent avec passion vers un but commun. C’est ainsi que Produits Kruger est devenue le plus grand fabricant canadien de produits de papier de qualité, à usages domestique, industriel et commercial. Nous sommes fiers de proposer un portefeuille des marques de produits de papier préférées au Canada pour toutes les pièces de votre maison.</p>
          </div>
        </div>
      </section>
      <div class="wrapper">
        <section class="about__names">
          <!-- <div class="col">
            <div class="container">
              <div class="pseudo-title">CONSUMER PRODUCTS – CANADA</div>
              <h4>A family of Canadian household names</h4>
              <p>We offer a portfolio of Canada's favourite tissue brand for every room in your home.</p>
            </div>
          </div> -->
          <div class="col full-height">
            <div class="row wipe-in down" data-section="Product Grid">
              <div class="bg" style="background-image: url(images/woman_background.jpg)"></div>
              <div class="info">
                <!-- <a href="http://www.cashmere.ca/" target="_blank"> -->
                  <img src="images/names_bg_2_opt.png" alt="A package of Cashmere Bathroom Tissue">
                  <h6>La marque de papier hygiénique en 1<sup>re</sup> place au Canada</h6>
                <!-- </a> -->
                <div class="links">

                  <a href="https://www.facebook.com/Cashmere/" target="_blank">VISITEZ-NOUS SUR FACEBOOK</a>
                  <a href="http://www.cashmere.ca/index_fr.html#home" target="_blank">VISITEZ CASHMERE.CA</a>
                </div>
              </div>
            </div>
            <div class="row wipe-in down">
              <div class="bg" style="background-image: url(images/names_bg_2.jpg)"></div>
              <div class="info">
                <!-- <a href="http://www.spongetowels.ca/" target="_blank"> -->
                  <img src="images/names_bg_1_opt.png" alt="A package of Sponge Towels Paper Towels">
                  <h6>La marque d’essuie-tout préférée au Canada</h6>
                <!-- </a> -->
                <div class="links">
                  <a href="https://www.facebook.com/SpongeTowels/" target="_blank">VISITEZ-NOUS SUR FACEBOOK</a>
                  <a href="http://www.spongetowels.ca/index-fr.php" target="_blank">VISITEZ SPONGETOWELS.CA</a>
                </div>
              </div>
            </div>
            <div class="row wipe-in down">
              <div class="bg" style="background-image: url(images/names_bg_4.jpg)"></div>
              <div class="info">
                <!-- <a href="http://www.scotties.ca" target="_blank"> -->
                  <img src="images/names_bg_4_opt.png" alt="A package of Scottie's Facial Tissue">
                  <h6>La marque de papiers-mouchoirs en 1<sup>re</sup> place au Canada</h6>
                <!-- </a> -->
                <div class="links">
                  <a href="https://www.facebook.com/ScottiesTissue/" target="_blank">VISITEZ-NOUS SUR FACEBOOK</a>
                  <a href="http://www.scotties.ca/fr/" target="_blank">VISITEZ SCOTTIES.CA</a>
                </div>
              </div>
            </div>
            <div class="row wipe-in down">
              <div class="bg" style="background-image: url(images/names_bg_3.jpg)"></div>
              <div class="info">
                <!-- <a href="http://purex.ca/" target="_blank"> -->
                  <img src="images/names_bg_3.png" alt="A package of Purex Bathroom Tissue">
                  <h6>La marque de papier hygiénique en 1<sup>re</sup> place dans l’Ouest canadien</h6>
                <!-- </a> -->
                <div class="links">
                  <a href="https://www.facebook.com/PurexCanada/" target="_blank">VISITEZ-NOUS SUR FACEBOOK</a>
                  <a href="http://www.purex.ca/francais/" target="_blank">VISITEZ PUREX.CA</a>
                </div>
              </div>
            </div>
            <div class="row wipe-in down">
              <div class="bg" style="background-image: url(images/names_bg_6.jpg)"></div>
              <div class="info">
                <!-- <a href="http://www.cashmere.ca/" target="_blank"> -->
                  <img src="images/names_bg_5.png" alt="A package of Environmental branded Cashmere bathroom tissue">
                  <h6>Les premiers produits de papier haut de gamme faits de papier recyclé à 100 % au Canada</h6>
                <!-- </a> -->
                <!-- <div class="links">
                  <a href="https://www.facebook.com/Cashmere/" target="_blank">Visit Us On Facebook</a>
                  <a href="http://www.cashmere.ca/" target="_blank">Visit cashmere.ca</a>
                </div> -->
              </div>
            </div>
            <div class="row wipe-in down">
              <div class="bg" style="background-image: url(images/names_bg_5.jpg)"></div>
              <div class="info">
                <!-- <a href="http://www.whiteswan.ca/" target="_blank"> -->
                  <img src="images/names_bg_6.png" alt="A package of White Swan paper napkin" class="whiteswan_img">
                  <h6>La marque de serviettes de table en 1<sup>re</sup> place au Canada</h6>
                  <div class="links">
                <!-- </a> -->
                    <!-- <a href="https://www.facebook.com/Cashmere/" target="_blank">Visit Us On Facebook</a> -->
                    <a href="http://www.whiteswan.ca/francais.html" target="_blank">VISITEZ WHITESWAN.CA</a>
                  </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="clearfix2"></div>
        </section>
        <section class="about__consumer full-height wipe-in" data-section="White Cloud">
          <div class="container">
            <div>
              <div class="white-cloud">
                <img src="images/white_cloud.png" alt="A mother and child branded on White Cloud Bathroom Tissue">
              </div>
              <div class="text">
                <div class="pseudo-title vis">PRODUITS DE CONSOMMATION – ÉTATS-UNIS ET MEXIQUE</div>
                <!-- <h4>Ranked #1 bath tissue in America by a leading consumer ratings publication</h4> -->
                <h4>White Cloud<sup>®</sup> est le papier hygiénique classé en 1<sup>re</sup> place aux É.-U. selon une éminente publication consacrée aux cotes accordées par les consommateurs</h4>
                <p>Notre marque White Cloud<sup>®</sup> a un nouveau look et a récemment été reconnue comme le meilleur papier hygiénique aux États-Unis! C’est une des raisons pour lesquelles le papier hygiénique White Cloud<sup>®</sup> est maintenant offert dans plus de 12 000 magasins partout aux É.-U., et ce nombre continue de croître.</p>
                <p>La famille des produits White Cloud<sup>®</sup>, y compris le papier hygiénique, les papiers-mouchoirs et les essuie-tout, est fabriquée principalement à notre usine à Memphis, au Tennessee, usine qui produit aussi une grande variété de produits de papier de marque maison pour les gros détaillants partout en Amérique du Nord.</p>
                <p>Pour en savoir plus, visitez <a href="http://mywhitecloud.com" target="_blank">mywhitecloud.com</a></p>
              </div>
              <div class="clearfix2"></div>
            </div>
          </div>
        </section>
        <section class="about__dark full-height wipe-in left" data-section="Commercial Tissue">
          <div class="container">
            <div>
              <div class="pseudo-title vis">DIVISION DES PRODUITS HORS FOYER</div>
              <h4>Le plus important fabricant de produits de papier commerciaux au Canada</h4>
              <p>Notre division Produits hors foyer offre des produits économiques de haute qualité — du papier hygiénique, des papiers-mouchoirs, des essuie-tout, des serviettes de table, des chiffons, des produits de soins des mains et des distributrices — pour usage dans les segments des services alimentaires, de la gestion immobilière, de la santé, de la fabrication, de l’éducation et du logement à travers l’Amérique du Nord.</p>
              <p>Pour en savoir plus, visitez <a href="http://afh.krugerproducts.ca/home.aspx?lang=fr-CA&" target="_blank">afh.krugerproducts.ca</a></p>
            </div>
          </div>
        </section>
        <section class="about__ceo full-height wipe-in" data-section="Dino Bianco Quote">
          <div class="vertical-align">
            <div class="container">
              <img src="images/man-portrait.png" alt="Dino Bianco CEO">
              <div class="quotes">
                <h3>« Nous faisons tout notre possible pour surpasser les attentes de nos clients, consommateurs et employés. »</h3>
                <div class="pseudo-title">Dino Bianco, CHEF DE LA DIRECTION</div>
              </div>
              <div class="clearfix2"></div>
            </div>
          </div>
        </section>
        <section class="about__locations">

          <div class="map">

          </div>

          <div class="container">
            <div class="text text-in" data-section="Locations Map">
              <div class="pseudo-title">our locations</div>
              <h4>Trouvez-nous<br/> partout en<br/> Amérique du<br/> Nord</h4>
              <p>Nos emplacements sont alignés stratégiquement sur les ressources naturelles que nous utilisons pour fabriquer nos produits, ainsi que sur les centres de population qui représentent respectivement 90 % et 60 % des consommateurs canadiens et américains.</p>
              <p>Avec huit usines de fabrication, notre capacité totale de fabrication de papier surpasse tout juste le tiers de la capacité totale de fabrication de produits de papier du Canada. Nous sommes également fiers d’être la seule compagnie de produits de papier exerçant des activités de fabrication dans l’Ouest canadien.</p>
            </div>

            <div class="mobile-map">

            </div>

            <div class="locations-wrap">
              <div class="location"><div>
                <div class="pseudo-title red">FABRICATION DU PAPIER ET TRANSFORMATION</div>
                <h5>New Westminster (Colombie-Britannique)</h5>
                <a target="_blank" href="http://www.produitskruger.ca/pdfs/KrugerFactSheetNewWestminster_FR.pdf" class="btn pseudo-title">FEUILLET D’INFORMATION
                  <svg class="icon">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-3"></use>
                  </svg>
                </a>
              </div></div>
              <!-- <div class="location">
                <div class="pseudo-title red">Papermaking & converting</div>
                <h5>New Westminster, British Columbia</h5>
                <a target="_blank" href="javascript:void(0)" class="btn pseudo-title">FACT SHEET
                  <svg class="icon">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-3"></use>
                  </svg>
                </a>
              </div> -->
              <div class="location"><div>
                <div class="pseudo-title red">FABRICATION DU PAPIER ET TRANSFORMATION</div>
                <h5>Gatineau (Québec)</h5>
                <a target="_blank" href="http://www.produitskruger.ca/pdfs/KrugerFactSheetGatineau_FR.pdf" class="btn pseudo-title">FEUILLET D’INFORMATION
                  <svg class="icon">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-3"></use>
                  </svg>
                </a>
              </div></div>
              <div class="location"><div>
                <div class="pseudo-title red">FABRICATION DU PAPIER ET TRANSFORMATION</div>
                <h5>Crabtree (Québec)</h5>
                <a target="_blank" href="http://www.produitskruger.ca/pdfs/KrugerFactSheetCrabtree_FR.pdf" class="btn pseudo-title">FEUILLET D’INFORMATION
                  <svg class="icon">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-3"></use>
                  </svg>
                </a>
              </div></div>
              <div class="location"><div>
                <div class="pseudo-title red">FABRICATION DU PAPIER</div>
                <h5>Sherbrooke (Québec)</h5>
                <a target="_blank" href="http://www.produitskruger.ca/pdfs/KrugerFactSheetSherbrooke_FR.pdf" class="btn pseudo-title">FEUILLET D’INFORMATION
                  <svg class="icon">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-3"></use>
                  </svg>
                </a>
              </div></div>
              <div class="location"><div>
                <div class="pseudo-title red">SIÈGE SOCIAL</div>
                <h5>Mississauga (Ontario)</h5>
                <a target="_blank" href="javascript:void(0)" class="btn pseudo-title"></a>
              </div></div>
              <div class="location"><div>
                <div class="pseudo-title red">TRANSFORMATION</div>
                <h5>Scarborough (Ontario)</h5>
                <a target="_blank" href="javascript:void(0)" class="btn pseudo-title"></a>
              </div></div>
              <div class="location"><div>
                <div class="pseudo-title red">TRANSFORMATION</div>
                <h5>Trenton (Ontario)</h5>
                <a target="_blank" href="javascript:void(0)" class="btn pseudo-title"></a>
              </div></div>
              <div class="location"><div>
                <div class="pseudo-title red">FABRICATION DU PAPIER ET TRANSFORMATION</div>
                <h5>Memphis (Tennessee)</h5>
                <a target="_blank" href="http://www.produitskruger.ca/pdfs/KrugerFactSheetMemphis_FR.pdf" class="btn pseudo-title">FEUILLET D’INFORMATION
                  <svg class="icon">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-3"></use>
                  </svg>
                </a>
              </div></div>
              <div class="clearfix2"></div>
            </div>
          </div>
        </section>
        <section class="about__focus wipe-in" data-section="What We Stand For">
          <img src="images/cone_opened.png" alt="pinecone">
          <div>
            <div class="container">
              <div class="text">
                <h2>Nos valeurs</h2>
                <div class="slogan">Notre priorité : Que tout ce que nous faisons tourne autour de nos clients et de nos consommateurs.</div>
              </div>
              <div class="col-c">
                <div class="col">
                  <h5>Courage</h5>
                  <p>Avoir la volonté de prendre des risques de façon éclairée et d’entreprendre de nouveaux projets pour atteindre nos objectifs ambitieux, en agissant avec humilité, enthousiasme et discernement.</p>
                </div>
                <div class="col">
                  <h5>Dévouement</h5>
                  <p>Prendre un engagement envers notre mission, nos employés et nos marques; offrir des produits et services d’une valeur inégalée à nos clients et à nos actionnaires.</p>
                </div>
                <div class="col">
                  <h5>Excellence</h5>
                  <p>Nous démarquer dans toutes nos activités, en allant au-delà des attentes de nos clients et de nos consommateurs, tout en améliorant constamment nos produits et nos processus dans un environnement structuré et sécuritaire.</p>
                </div>
                <div class="col">
                  <h5>Intégrité</h5>
                  <p>Agir avec honnêteté et assumer la responsabilité de nos actes, en nous conformant aux normes d’éthique les plus élevées.</p>
                </div>
                <div class="col">
                  <h5>Travail en équipe</h5>
                  <p>Nous encourageons le travail en équipe, valorisons la diversité de nos effectifs et travaillons ensemble de manière coopérative et collective pour réaliser nos objectifs communs ambitieux.</p>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </section>
        <section class="about__history full-height wipe-in left" data-section="Our Story">
          <div class="triangles"></div>
          <div class="slidePrev" title="Previous Slide"></div>
          <div class="slideNext" title="Next Slide"></div>
          <div class="vertical-align" style="left:0;right:0;">
            <div class="container">
              <div class="pseudo-title vis">NOTRE HISTOIRE</div>
              <h4>Plus d’un siècle de succès</h4>
              <div class="swiper-container timeline-top">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/joseph_kruger.jpg); background-position: top -90px center;"></div>
                      <div class="pseudo-title year">1904</div>
                      <h5><span>La société Kruger Paper Company Limited, firme de distribution en gros de papiers, est fondée à Montréal par Joseph Kruger.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/1922.jpg);"></div>
                      <div class="pseudo-title year">1922</div>
                      <h5><span>Westminster Paper Mills Limited, maintenant notre usine de fabrication à New Westminster, en Colombie-Britannique, commence ses activités.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/1957_2.png);"></div>
                      <div class="pseudo-title year">1957</div>
                      <h5><span>Acquisition de l’usine de fabrication à Crabtree, au Québec.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/1978_2.png);"></div>
                      <div class="pseudo-title year">1978</div>
                      <h5><span>Acquisition de l’usine de fabrication à Sherbrooke, au Québec.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/stoh_horizontal.jpg); background-size: 90%;"></div>
                      <div class="pseudo-title year">1982</div>
                      <h5><span>Début de la commandite du Tournoi des cœurs de Scott (maintenant appelé le Tournoi des cœurs Scotties), championnat national de curling féminin du Canada.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/1988.png);"></div>
                      <div class="pseudo-title year">1988</div>
                      <h5><span>Acquisition de l’usine de fabrication à Gatineau, au Québec.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/1997.png);"></div>
                      <div class="pseudo-title year">1997</div>
                      <h5><span>Kruger Inc. fait l’acquisition des exploitations canadiennes de Papiers Scott, lui permettant ainsi de lancer des produits de papier de grande consommation au Canada.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/1999.png); background-position: top -75px center;"></div>
                      <div class="pseudo-title year">1999</div>
                      <h5><span>Relancement de la marque White Cloud aux États-Unis.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2002.jpeg);"></div>
                      <div class="pseudo-title year">2002</div>
                      <h5><span>Acquisition d’une grande usine à Memphis, au Tennessee, établissant notre présence manufacturière aux États-Unis.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2004_2.png);"></div>
                      <div class="pseudo-title year">2004</div>
                      <h5><span>Inauguration de la collection Blanc Cashmere, qui met en vedette certains des meilleurs créateurs de mode canadiens.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/cashmere_timeline.png);"></div>
                      <div class="pseudo-title year">2004</div>
                      <h5><span>Cottonnelle commence sa transition vers Cashmere. Aujourd’hui, Cashmere est le papier hygiénique le plus vendu au Canada.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/cbcf-logo-timeline.png);   background-size: 85%;"></div>
                      <div class="pseudo-title year">2005</div>
                      <h5><span>Début de notre commandite de la Fondation canadienne du cancer du sein. Nous sommes aujourd’hui l’un des 5 donateurs les plus importants à l’échelle nationale.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2005_1.png);   background-size: 140%;"></div>
                      <div class="pseudo-title year">2005</div>
                      <h5><span>ScotTowels commence sa transition vers SpongeTowels.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/kruger_products_logo.jpg); background-size: 90%;"></div>
                      <div class="pseudo-title year">2007</div>
                      <h5><span>Papiers Scott porte désormais le nom de Produits Kruger.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2008.png);   background-size: 80%;"></div>
                      <div class="pseudo-title year">2008</div>
                      <h5><span>Lancement de notre gamme de produits EnviroPlus, la première gamme de produits de papier haut de gamme au Canada fabriqués de papier recyclé à 100 %.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2015.png);   background-size: 70%;"></div>
                      <div class="pseudo-title year">2010</div>
                      <h5><span>Produits Kruger lance son programme Développement durable 2015, initiative de cinq ans ayant pour but la réduction de l’empreinte environnementale de la compagnie et le bien des communautés.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2011_2.png);   background-size: 90%;"></div>
                      <div class="pseudo-title year">2011</div>
                      <h5><span>Produits Kruger devient le premier fabricant canadien de produits de papier à obtenir la certification Forest Stewardship Council<sup>®</sup> (FSC<sup>®</sup>), créant l’un des plus grands portefeuilles de produits de papier certifiés par une tierce partie en Amérique du Nord.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2011_1.jpeg);"></div>
                      <div class="pseudo-title year">2011</div>
                      <h5><span>30<sup>e</sup> anniversaire en tant que commanditaire principal du Tournoi des cœurs Scotties (championnat national de curling féminin du Canada), l’une des commandites du sport amateur de plus longue date de l’histoire du Canada.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2012_2.jpg);"></div>
                      <div class="pseudo-title year">2012</div>
                      <h5><span>Produits Kruger gagne 5 prestigieux prix Cassies; elle est maintenant l’une des entreprises de marketing les plus décorées au Canada.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2012_1.png);   background-size: 80%;"></div>
                      <div class="pseudo-title year">2012</div>
                      <h5><span>KP Tissue Inc., qui détient une participation dans Produits Kruger s.e.c., devient une société ouverte inscrite à la Bourse de Toronto (symbole : KPT).</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2013_2_v2.jpg);"></div>
                      <div class="pseudo-title year">2013</div>
                      <h5><span>Lancement de la machine à papier à technologie de séchage à air traversant (SAT) de fine pointe à notre usine de Memphis, au Tennessee, ce qui augmente considérablement nos capacités nord-américaines.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2013_1.png);"></div>
                      <div class="pseudo-title year">2013</div>
                      <h5><span>Le défilé de la mode de la collection Blanc Cashmere du papier hygiénique Cashmere célèbre son 10<sup>e</sup> anniversaire, en faveur de la Fondation canadienne du cancer du sein.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2014.jpg);"></div>
                      <div class="pseudo-title year">2014</div>
                      <h5><span>Pour accélérer la croissance de sa division Produits hors foyer, Produits Kruger fait l’acquisition des actifs de transformation de papier de Metro Paper Industries Inc.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2015.jpg);"></div>
                      <div class="pseudo-title year">2015</div>
                      <h5><span>White Cloud est le papier hygiénique classé en 1<sup>re</sup> place aux États-Unis selon une éminente revue de recherche et de tests auprès des consommateurs.</span></h5>
                    </div>
                  </div>
                  <!-- <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2016_2.png);"></div>
                      <div class="pseudo-title year">2016-2</div>
                      <h5><span>SpongeTowels Ultra Strong wins two Canadian Grand Prix new product awards</span></h5>
                    </div>
                  </div> -->
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/home_grid_1.png);"></div>
                      <div class="pseudo-title year">2016</div>
                      <h5><span>Dans un sondage national auprès des détaillants canadiens, Produits Kruger est classée en 1<sup>re</sup> place parmi les fournisseurs de biens emballés pour une quatrième année consécutive, un accomplissement sans précédent.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/WC-LOGO.png);  background-size: 65%;"></div>
                      <div class="pseudo-title year">2017</div>
                      <h5><span>La marque White Cloud augmente considérablement sa distribution à plus de 12 000 magasins à travers les États-Unis, et ce nombre continue de croître.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2017_2.png);"></div>
                      <div class="pseudo-title year">2017</div>
                      <h5><span>Produits Kruger est nommée l’un des meilleurs employeurs de la RGT pour une cinquième année consécutive.</span></h5>
                    </div>
                  </div>
                </div>
              </div>
              <div class="year-title">1904</div>
              <!-- <div class="swiper-container timeline-bottom">
                  <div class="swiper-wrapper">
                    <div class="swiper-slide"><span class="pseudo-title">1941</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1942</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1943</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1944</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1945</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1946</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1947</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1948</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1949</span></div>
                  </div>
              </div> -->
              <input type="range" min="0" max="26" step="1" value="0" data-orientation="horizontal">
              <!-- <div class="swiper-button-next swiper-button-black"></div>
              <div class="swiper-button-prev swiper-button-black"></div> -->
            </div>
          </div>
        </section>
        <section class="about__ethical full-height wipe-in" data-section="Fair and Ethical">
          <div class="vertical-align">
            <div class="about__ethical-inner">
              <h2>Pratiques commerciales justes et éthiques</h2>
              <p>Notre politique est d’adopter une conduite éthique qui se conforme aux lois, aux règles et aux réglementations applicables dans tous les pays où sommes présents.</p>
              <a href="http://www.produitskruger.ca/pdfs/Kruger_Code_etiques_FR_v6_FINAL.pdf" target="_blank" class="btn pseudo-title">LIRE LE CODE DE CONDUITE</a>
            </div>
          </div>
        </section>
        <section class="about__organization text-in" data-section="Organization">
          <div class="pseudo-title">Structure de l’entreprise</div>
          <h2>Structure de l’entreprise</h2>
          <p>Notre organigramme illustre la relation qui existe entre Produits Kruger s.e.c. et ses actionnaires. <a href="http://www.kptissueinc.com/" target="_blank">Papiers Tissu KP Inc.</a> est une société ouverte créée afin d’acquérir une participation dans Produits Kruger s.e.c. (PK s.e.c.) et son activité se limite à la détention de cette participation. Les actions de <a href="http://www.kptissueinc.com/" target="_blank">Papiers Tissu KP Inc.</a> se négocient à la Bourse de Toronto sous le symbole KPT.</p>
          <div class="about__organization-scheme">
            <div class="container">
                <div class="rings-wrap">
                  <span class="ring"></span>
                  <h6>Public investisseur</h6>
                </div>
                <div class="jumper">
                  <svg class="icon">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-5"></use>
                  </svg>
                  100 %
                </div>
                <div class="rings-wrap">
                  <span class="ring"></span>
                  <h6>Papiers Tissu KP Inc.</h6>
                  <span>Canada</span>
                </div>
                <div class="jumper">
                  <svg class="icon">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-5"></use>
                  </svg>
                  16,1 %
                </div>
              <div class="circle">
                <div class="vertical-align">
                  <h6>Produits Kruger s.e.c.</h6>
                  <span>Québec</span>
                </div>
              </div>
              <div class="jumper">
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-6"></use>
                </svg>
                moins de 0,01 %
              </div>
              <div class="rings-wrap">
                <span class="ring"></span>
                <h6>KPGP Inc.</h6>
                <span>Canada</span>
              </div>
              <div class="jumper">
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-6"></use>
                </svg>
                100 %
              </div>
              <div class="rings-wrap">
                <span class="ring"></span>
                <h6>Kruger Inc.</h6>
              </div>
              <div class="over-jumper jumper">
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-6"></use>
                </svg>
                <span>83,9 %</span>
              </div>
              <div class="clearfix"></div>
              <span class="pseudo-title vis" style="margin-top: 50px; margin-bottom: 0; padding-bottom: 0; border: none; text-decoration: none;">EN DATE DU 3 MAI 2017</span>
            </div>
          </div>
          <!-- <svg version="1.1" class="ring" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="101.559px" height="101.176px" viewBox="0 0 101.559 101.176" enable-background="new 0 0 101.559 101.176" xml:space="preserve">
              <path fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#00a453" stroke-width="7" stroke-miterlimit="10" d="M50.779,3.5c26.111,0,47.279,21.082,47.279,47.088S76.891,97.676,50.779,97.676S3.5,76.594,3.5,50.588S24.668,3.5,50.779,3.5z"/>
          </svg> -->

        </section>
        <section class="about__highlights wipe-in left" data-section="Recognition Highlights">
          <div class="container">
            <div class="pseudo-title vis">MEILLEURE PERFORMANCE</div>
            <h2>Distinctions <br>honorifiques notables</h2>
            <div class="swiper-container timeline-left">
                <div class="swiper-wrapper">
                  <div class="swiper-slide pseudo-title">2017</div>
                  <div class="swiper-slide pseudo-title">2016</div>
                  <div class="swiper-slide pseudo-title">2015</div>
                  <div class="swiper-slide pseudo-title">2014</div>
                </div>
            </div>
            <div class="swiper-container timeline-right">
              <div class="swiper-wrapper">
                <div class="swiper-slide"><!-- 2017 -->
                  <div class="block corporate">
                    <h3>L’un des meilleurs employeurs de la région du Grand Toronto pour une 5<sup>e</sup> année de suite</h3>
                    <div class="pseudo-title">ENTREPRISE</div>
                  </div>
                  <div class="block sustainability">
                    <h3>Lauréat du Programme de partenariat – ÉcoConnexions du CN</h3>
                    <div class="pseudo-title">DÉVELOPPEMENT DURABLE</div>
                  </div>
                  <div class="block sustainability">
                    <h3>Classement des entreprises citoyennes les plus responsables de demain de <i>Corporate Knights</i></h3>
                    <div class="pseudo-title">DÉVELOPPEMENT DURABLE</div>
                  </div>
                  <div class="block marketing">
                    <h3>Prix du Meilleur nouveau produit – SpongeTowels Ultra Fort Minis</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block marketing">
                    <h3>Gagnant au Grand Prix canadien des produits nouveaux, Conseil canadien du commerce de détail – SpongeTowels Ultra Fort Minis</h3>
                    <div class="pseudo-title ">Marketing</div>
                  </div>
                  <div class="block marketing">
                    <h3>Projet primé du Grand Prix Grafika pour publicité extérieure en français, animation graphique – Scotties</h3>
                    <div class="pseudo-title ">Marketing</div>
                  </div>
                  <div class="block marketing">
                    <h3>Prix d’excellence de l’AIPE/Toronto OVATION, photographie – Cashmere</h3>
                    <div class="pseudo-title ">Marketing</div>
                  </div>
                  <div class="block marketing">
                    <h3>Prix de mérite de l’AIPE/Toronto OVATION, invitation – Cashmere</h3>
                    <div class="pseudo-title ">Marketing</div>
                  </div>
                  <div class="block marketing">
                    <h3><a href="http://strategyonline.ca/2017/06/13/marketing-awards-the-winners/" target="_blank">Prix de Marketing, prix d' or, campagne d'affiches dans les transports en commun – Scotties</a></h3>
                    <div class="pseudo-title ">Marketing</div>
                  </div>
                  <div class="block marketing">
                    <h3><a href="http://strategyonline.ca/2017/06/13/marketing-awards-the-winners/" target="_blank">Prix de Marketing, prix d'or, campagne dans les journaux – Scotties</a></h3>
                    <div class="pseudo-title ">Marketing</div>
                  </div>

                  <div class="block corporate">
                    <h3>Prix Fierté régionale de la Chambre de Commerce du Grand Joliette – Usine de Crabtree</h3>
                    <div class="pseudo-title ">ENTREPRISE</div>
                  </div>

                  <div class="block customer">
                    <h3>Au 1<sup>er</sup> rang parmi les fournisseurs de biens emballés au Canada, sondage du secteur pour une 5<sup>e</sup> année de suite</h3>
                    <div class="pseudo-title">Client</div>
                  </div>

             <div class="block marketing">
                    <h3><a href="http://theadcc.ca/wp-content/uploads/2017/11/Directions-2017-Winners-List.pdf" target="_blank">Prix d'argent d'Advertising & Design Club of Canada—Publicité dans les transports en commun, Scotties</a></h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>


                <div class="block marketing">
                    <h3><a href="http://theadcc.ca/wp-content/uploads/2017/11/Directions-2017-Winners-List.pdf" target="_blank">Prix de mérite d'Advertising & Design Club of Canada—Illustration publicitaire, Scotties</a></h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>

    <div class="block marketing">
                    <h3><a href="http://concours.infopresse.com/archive/crea/2017" target="_blank">Prix Créa, campagne dans les journaux, Scotties</a></h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
 
      <div class="block marketing">
                    <h3><a href="http://concours.infopresse.com/archive/crea/2017" target="_blank">Prix Créa, publicité extérieure, Scotties</a></h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>

      <div class="block marketing">
                    <h3><a href="https://www.appliedartsmag.com/winners_gallery/advertising/?id=511&year=2017&clip=1" target="_blank">Arts appliqués, série d'articles de journaux, Scotties</a></h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>

      <div class="block marketing">
                    <h3><a href="https://www.appliedartsmag.com/winners_gallery/advertising/?id=525&year=2017&clip=1" target="_blank">Arts appliqués, publicité extérieure, Scotties</a></h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>

       <div class="block marketing">
                    <h3><a href="http://awards.strategyonline.ca/Winners/Winner/2017" target="_blank">Prix Stratégie, <i>Connection Strategy Bronze</i>, Cashmere</a></h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>

       <div class="block marketing">
                    <h3><a href="http://theadcc.ca/wp-content/uploads/2017/11/Directions-2017-Winners-List.pdf" target="_blank">Prix de mérite d'Advertising & Design Club of Canada—Publicité, Cashmere</a></h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>

      <div class="block customer">
                    <h3><a href="https://www.grocerybusiness.ca/image-gallery/united-grocers-inc-recognizes-supplier-excellence" target="_blank">Prix Fournisseur de l'année de United Grocers Inc.</a></h3>
                    <div class="pseudo-title">Client</div>
                  </div>
<div class="block sustainability">
                    <h3><a href="http://shortyawards.com/2nd-socialgood/leaders-in-sustainable-thinking-3" target="_blank">Finaliste pour le prix Shorty Social Good, leaders en développement durable</a></h3>
                    <div class="pseudo-title">Développement durable</div>
                  </div>

 <div class="block corporate">
                    <h3>Prix innovation en santé et sécurité au travail de Kruger Inc., usine de Richelieu</h3>
                    <div class="pseudo-title">Entreprise</div>
                  </div>
   <div class="block marketing">
                    <h3>Prix Summit Creative Awards, Choix des juges, campagne de publicité imprimée B2B , produits hors foyer</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                </div><!-- end 2017 -->
                <div class="swiper-slide">
                  <div class="block customer">
                    <h3>Au 1<sup>er</sup> rang parmi les fournisseurs de biens emballés au Canada, sondage du secteur pour une 4<sup>e</sup> année de suite</h3>
                    <div class="pseudo-title">Client</div>
                  </div>
                  <div class="block sustainability">
                    <h3>Prix Durabilité des fournisseurs de Walmart Canada</h3>
                    <div class="pseudo-title">Développement durable</div>
                  </div>
                  <div class="block marketing">
                    <h3>Prix d’excellence de la SCRP, Campagne de relations avec les médias de l’année par une agence – Cashmere</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block individual">
                    <h3>Prix des femmes STAR de Canadian Grocer – Lucie Martin, directrice d’entreprise, Gestion des talents</h3>
                    <div class="pseudo-title">Individu</div>
                  </div>
                  <div class="block marketing">
                    <h3>Prix de mérite, Prix Gold Quill de l’AIPE : Photographie – Cashmere</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block individual">
                    <h3>Prix Canada’s Clean50 – Steven Sage, vice-président, Développement durable et Innovation</h3>
                    <div class="pseudo-title">Individu</div>
                  </div>
                  <div class="block marketing">
                    <h3>Prix de mérite, Prix Gold Quill de l’AIPE, relations avec les médias – Cashmere</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block corporate">
                    <h3>L’un des meilleurs employeurs de la région du Grand Toronto</h3>
                    <div class="pseudo-title">Entreprise</div>
                  </div>
                  <div class="block marketing">
                    <h3>Prix de mérite de l’AIPE/Toronto OVATION : Relations avec les médias  – Cashmere</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block customer">
                    <h3>Partenaire de l’année, Fournisseur de marque national de Walmart Canada</h3>
                    <div class="pseudo-title">Client</div>
                  </div>
                  <div class="block marketing">
                    <h3>Prix de mérite de l’AIPE/Toronto OVATION : Photographie  – Cashmere</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block sustainability">
                    <h3>Lauréat du Programme de partenariat – ÉcoConnexions du CN</h3>
                    <div class="pseudo-title">Développement durable</div>
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="block marketing">
                    <h3>Meilleur nouveau produit, catégories du papier, du plastique et de l’aluminium, Grand Prix du Conseil canadien du commerce de détail  – SpongeTowels Ultra Fort</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block marketing">
                    <h3>Meilleur papier hygiénique global aux É.-U. par une éminente revue de recherche auprès des consommateurs – White Cloud Ultra Soft ‘n Thick</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block customer">
                    <h3>Au 1<sup>er</sup> rang parmi les fournisseurs de biens emballés au Canada, sondage du secteur – Division des produits aux consommateurs</h3>
                    <div class="pseudo-title">Client</div>
                  </div>
                  <div class="block sustainability">
                    <h3>Prix de l’impact sur l’environnement, Pratt Industries, KTG USA, Memphis</h3>
                    <div class="pseudo-title">Développement durable</div>
                  </div>
                  <div class="block customer">
                    <h3>Parternaire de l’année, Fournisseur de marque national de Walmart Canada, produits de consommation – Division des produits aux consommateurs</h3>
                    <div class="pseudo-title">Client</div>
                  </div>
                  <div class="block sustainability">
                    <h3>Lauréat du Programme de partenariat – ÉcoConnexions du CN</h3>
                    <div class="pseudo-title">Développement durable</div>
                  </div>
                  <div class="block customer">
                    <h3>Fournisseur de l’année de Métro – Division des produits aux consommateurs</h3>
                    <div class="pseudo-title">Client</div>
                  </div>
                  <div class="block sustainability">
                    <h3>Prix vert, Supply & Demand Chain Executive, Développement durable 2015</h3>
                    <div class="pseudo-title">Développement durable</div>
                  </div>
                  <div class="block customer">
                    <h3>Prix du partenariat de BJ’s Wholesale Supplier (marque maison aux É.-U.)</h3>
                    <div class="pseudo-title">Client</div>
                  </div>
                  <div class="block corporate">
                    <h3>L’un des meilleurs employeurs de la région du Grand Toronto</h3>
                    <div class="pseudo-title">Entreprise</div>
                  </div>
                  <div class="block sustainability">
                    <h3>Prix Greening the Supply Chain, Association canadienne des importateurs et exportateurs</h3>
                    <div class="pseudo-title">Développement durable</div>
                  </div>
                  <div class="block marketing">
                    <h3>Prix spécial : Produit le plus populaire auprès du consommateur, Grand Prix du Conseil canadien du commerce de détail – SpongeTowels Ultra Fort</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block sustainability">
                    <h3>Prix Business Achievement Award, Chambre de commerce de l’Ontario, Développement durable 2015 </h3>
                    <div class="pseudo-title">Développement durable</div>
                  </div>
                  <div class="block corporate">
                    <h3>L’un des meilleurs employeurs de la région du Grand Toronto</h3>
                    <div class="pseudo-title">Entreprise</div>
                  </div>
                  <div class="block sustainability">
                    <h3>Entreprises et associations d’entreprisees finaliste, Les phénix, de l’environment, Développement durable 2015</h3>
                    <div class="pseudo-title">Développement durable</div>
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="block individual">
                    <h3>Les Canadiennes les plus influentes, WXN Réseau des femmes exécutives – Nancy Marcus, vice-présidente d’entreprise, Marketing</h3>
                    <div class="pseudo-title">Individu</div>
                  </div>
                  <div class="block corporate">
                    <h3>Prix Tissue Company of the Year de Pulp & Paper International</h3>
                    <div class="pseudo-title">Entreprise</div>
                  </div>
                  <div class="block marketing">
                    <h3>Prix Cassies d’argent, Succès prolongé – SpongeTowels</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block customer">
                    <h3>Au 1<sup>er</sup> rang parmi les fournisseurs de biens emballés au Canada, sondage du secteur – Division des produits aux consommateurs</h3>
                    <div class="pseudo-title">Client</div>
                  </div>
                  <div class="block marketing">
                    <h3>Prix d’excellence de l’AIPE/Toronto OVATION, graphisme/3D (autre) – Cashmere</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block customer">
                    <h3>Prix Partnership/Most Valuable Performance de United Grocers Inc., consommateurs canadiens</h3>
                    <div class="pseudo-title">Client</div>
                  </div>
                  <div class="block individual">
                    <h3>Prix des femmes STAR de Canadian Grocer – Nancy Marcus, vice-présidente d’entreprise, Marketing</h3>
                    <div class="pseudo-title">Individu</div>
                  </div>
                  <div class="block customer">
                    <h3>L’un des 10 meilleurs fournisseurs de Sysco, désignation d’excellence pour fournisseurs –  Division des produits hors foyer</h3>
                    <div class="pseudo-title">Client</div>
                  </div>
                  <div class="block marketing">
                    <h3>Prix de mérite Gold Quill de l’AIPE, rédaction – Cashmere</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block customer">
                    <h3>L’un des 3 meilleurs fournisseurs de Balpex Buying Group – Division des produits hors foyer</h3>
                    <div class="pseudo-title">Customer</div>
                  </div>
                  <div class="block marketing">
                    <h3>Finaliste au prix Cause + Action des CSR Awards, magazine <i>Strategy</i> – Cashmere</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block customer">
                    <h3>Prix Fournisseur de l’année, Adapt Buying Group – Division des produits hors foyer (la division maintient son statut au Club des présidents d’Adapt)</h3>
                    <div class="pseudo-title">Client</div>
                  </div>
                  <div class="block marketing">
                    <h3>Prix d’excellence de l’AIPE/Toronto OVATION, graphisme/3D (autre) – Cashmere</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block individual">
                    <h3>Prix des femmes STAR de <i>Canadian Grocer</i> – Janel Caldwell, directrice, Développement des affaires</h3>
                    <div class="pseudo-title">Individu</div>
                  </div>
                  <div class="block marketing">
                    <h3>Prix de mérite de l’AIPE/Toronto OVATION, communication marketing – Cashmere</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block corporate">
                    <h3>L’un des meilleurs employeurs de la région du Grand Toronto</h3>
                    <div class="pseudo-title">Enterprise</div>
                  </div>
                  <div class="block marketing">
                    <h3>Prix Ace Award, meilleur usage d’événements spéciaux, Société canadienne des relations publiques – Cashmere</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block sustainability">
                    <h3>Lauréat du Programme de partenariat – ÉcoConnexions du CN</h3>
                    <div class="pseudo-title">Développement durable</div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </section>
      </div>
    </div>

<?php require('footer.php'); ?>
