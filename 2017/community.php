<?php
$title = 'Communauté';
require('header.php');
?>
    <div class="community-wrap">
      <section class="swiper-container posts-images-wrapper">
        <div class="swiper-wrapper">
            <div class="swiper-slide js-community-image">
                <div class="swiper-zoom-container" style="background-image:url(images/paper.jpg)"></div>
            </div>
            <div class="swiper-slide js-community-image">
                <div class="swiper-zoom-container" style="background-image:url(images/breast_cancer.jpg)"></div>
            </div>
            <div class="swiper-slide js-community-image">
                <div class="swiper-zoom-container" style="background-image:url(images/hearts_tournament.jpg)"></div>
            </div>
            <div class="swiper-slide js-community-image">
                <div class="swiper-zoom-container" style="background-image:url(images/2016-GutsyWalk-Kids-Final.jpg)"></div>
            </div>
            <div class="swiper-slide js-community-image">
                <div class="swiper-zoom-container" style="background-image:url(images/earth_day.jpg)"></div>
            </div>
            <div class="swiper-slide js-community-image">
                <div class="swiper-zoom-container" style="background-image:url(images/community-6.jpg)"></div>
            </div>
            <div class="swiper-slide js-community-image">
                <div class="swiper-zoom-container" style="background-image:url(images/community-7.jpg)"></div>
            </div>
        </div>
      </section>
      <section class="swiper-container posts-texts-wrapper">
        <div class="swiper-wrapper">
          <div class="swiper-slide community">
              <div class="post-text">
                  <div class="vertical-align ">
                    <span class="category-title">IMPLICATION COMMUNAUTAIRE</span>
                    <h4>L’entreprise de papier<br> proche des gens</h4>
                    <p>Chez Produits Kruger, nous sommes conscients de l’impact positif que nous pouvons avoir sur les communautés dans lesquelles nous vivons. Nous saisissons toutes les occasions de soutenir la santé et le bien-être de nos voisins et partenaires grâce à des programmes caritatifs locaux et des activités de développement communautaire.</p>

                    <div class="arrows mob">
                      <a href="#" onclick="return cScrollTo(1);" title="Next">
                        <span>Next</span>
                      </a>
                    </div>

                  </div>
              </div>
              <a href="#" class="text-center slide-link">
                <span></span>
              </a>

              <div class="arrows">
                <a href="#" onclick="return cScrollTo(1);" title="Next">
                  <span>Next</span>
                </a>
              </div>
          </div>

          <div class="swiper-slide community">
              <div class="post-text">
                  <div class="vertical-align">
                    <a target="_blank" class="category-img" href="http://cancer.ca/cancerdusein" title="Canadian Breast Cancer Foundation">
                      <img src="images/_LOGO.png" alt="Canadian Breast Cancer Foundation" class="large-logo">
                    </a>
                    <h4>Lutte contre le cancer du sein</h4>
                    <p>Depuis 2005, Produits Kruger change la vie des femmes canadiennes et de leurs familles affectées par le cancer du sein. La Société canadienne du cancer est une organisation nationale qui s’appuie sur des bénévoles issus de la communauté et dont la mission est d'éradiquer le cancer et d'améliorer la qualité de vie des personnes qui en souffrent. Nos contributions financières par le biais de ce partenariat permettent de financer la recherche sur le cancer du sein, ainsi que des activités éducatives et de sensibilisation. Pour en savoir plus, <a href="http://www.produitskruger.ca/2017/pdfs/cbcf-press-release.pdf" target="_blank">cliquez ici.</a></p>
                    <p><a href="http://cancer.ca/cancerdusein" target="_blank">cancerdusein</a></p>

                    <div class="arrows mob">
                      <a href="#" onclick="return cScrollTo(2);" title="Next">
                        <span>Next</span>
                      </a>
                      <a href="#" onclick="return cScrollTo(0);" title="Previous">
                        <span>Previous</span>
                      </a>
                    </div>
                  </div>
              </div>
              <a href="#" class="text-center slide-link">
                <span></span>
              </a>

              <div class="arrows">
                <a href="#" onclick="return cScrollTo(2);" title="Next">
                  <span>Next</span>
                </a>
                <a href="#" onclick="return cScrollTo(0);" title="Previous">
                  <span>Previous</span>
                </a>
              </div>
          </div>

            <div class="swiper-slide community">
                <div class="post-text">
                    <div class="vertical-align">
                      <a class="category-img" target="_blank" href="http://stoh.ca/francais/index.php" title="Scotties Tournament of Hearts">
                        <img src="images/STOH-logo.png" alt="Scotties Tournament of Hearts">
                      </a>
                      <h4>Tournoi des<br> cœurs Scotties</h4>
                      <p>Depuis 1981, Produits Kruger commandite les championnats canadiens de curling féminin par le biais du tournoi des cœurs Scotties, ce qui en fait l’un des commanditaires ayant le plus longuement apporté son soutien au sport amateur au Canada.</p>
                      <p><a href="http://stoh.ca/francais/index.php" target="_blank">stoh.ca</a></p>

                      <div class="arrows mob">
                        <a href="#" onclick="return cScrollTo(3);" title="Next">
                          <span>Next</span>
                        </a>
                        <a href="#" onclick="return cScrollTo(1);" title="Previous">
                          <span>Previous</span>
                        </a>
                      </div>
                    </div>
                </div>
                <a href="#" class="text-center slide-link">
                  <span></span>
                </a>

                <div class="arrows">
                  <a href="#" onclick="return cScrollTo(3);" title="Next">
                    <span>Next</span>
                  </a>
                  <a href="#" onclick="return cScrollTo(1);" title="Previous">
                    <span>Previous</span>
                  </a>
                </div>
            </div>

            <div class="swiper-slide community">
                <div class="post-text">
                    <div class="vertical-align">
                      <a class="category-img" target="_blank" href="http://gutsyenmarche.ca/" title="Crohn’s & Colitis Canada">
                        <img src="images/CCC-logo.png" alt="Crohn’s & Colitis Canada">
                      </a>
                      <h4>Crohn et Colite<br> Canada</h4>
                      <p>Grâce à nos marques de papier hygiénique Cashmere<sup>®</sup> et Purex<sup>®</sup>, en 2014, Produits Kruger s’est associée à Crohn et Colite Canada comme commanditaire national de la randonnée Gutsy en marche. La randonnée Gutsy en marche est le plus grand événement communautaire au Canada dont le but est de récolter des fonds pour la maladie de Crohn et la colite ulcéreuse.</p>
                      <p><a href="http://gutsyenmarche.ca/" target="_blank">gutsyenmarche.ca</a></p>

                      <div class="arrows mob">
                        <a href="#" onclick="return cScrollTo(4);" title="Next">
                          <span>Next</span>
                        </a>
                        <a href="#" onclick="return cScrollTo(2);" title="Previous">
                          <span>Previous</span>
                        </a>
                      </div>

                    </div>
                </div>
                <a href="#" class="text-center slide-link">
                  <span></span>
                </a>
                <div class="arrows">
                  <a href="#" onclick="return cScrollTo(4);" title="Next">
                    <span>Next</span>
                  </a>
                  <a href="#" onclick="return cScrollTo(2);" title="Previous">
                    <span>Previous</span>
                  </a>
                </div>
            </div>

            <div class="swiper-slide community">
                <div class="post-text">
                    <div class="vertical-align">
                      <a class="category-img" target="_blank" href="http://jourdelaterre.ca/" title="Earth Day Canada">
                        <img src="images/earthday_logo.png" alt="Earth Day Canada">
                      </a>
                      <h4>Jour de la Terre<br> Canada</h4>
                      <p>En 2012, Produits Kruger est devenue un partenaire officiel du Jour de la Terre Canada. Nous partageons et soutenons ensemble une mission consistant à rapprocher les gens de la nature, à réduire notre empreinte et à encourager l’action en faveur de l’environnement, tout en communiquant la véritable valeur écologique de notre gamme de produits de papier recyclé à 100 % EnviroPlus® et en soulignant nos activités à impact réduit sur l’environnement.</p>
                      <p><a href="http://jourdelaterre.ca/" target="_blank">jourdelaterre.ca</a></p>
                      <div class="arrows mob">
                        <a href="#" onclick="return cScrollTo(5);" title="Next">
                          <span>Next</span>
                        </a>
                        <a href="#" onclick="return cScrollTo(3);" title="Previous">
                          <span>Previous</span>
                        </a>
                      </div>
                    </div>
                </div>
                <a href="#" class="text-center slide-link">
                  <span></span>
                </a>
                <div class="arrows">
                  <a href="#" onclick="return cScrollTo(5);" title="Next">
                    <span>Next</span>
                  </a>
                  <a href="#" onclick="return cScrollTo(3);" title="Previous">
                    <span>Previous</span>
                  </a>
                </div>
            </div>

            <div class="swiper-slide community">
                <div class="post-text">
                    <div class="vertical-align">
                      <a class="category-img" target="_blank" href="https://www.rmhccanada.ca/fr" title="Ronald McDonald House Charities">
                        <img src="images/RMHC_logo.png" alt="Ronald McDonald House Charities">
                      </a>
                      <h4>Œuvre des Manoirs<br> Ronald McDonald</h4>
                      <p>Depuis 1990, 100 % de tous les produits de papier à usage domestique fournis aux 15 manoirs Ronald McDonald à travers le Canada ont été intégralement offerts en don par la division de produits hors foyer de Produits Kruger. Étant donné que 70 % des Canadiens ne vivent pas dans une ville qui dispose d’un hôpital pour enfants, les 15 manoirs Ronald McDonald partout au Canada fournissent un foyer aux familles venant de l’extérieur dont l’enfant est traité dans un hôpital à proximité.</p>
                      <p><a href="https://www.rmhccanada.ca/fr" target="_blank">rmhccanada.ca</a></p>
                      <div class="arrows mob">
                        <a href="#" onclick="return cScrollTo(6);" title="Next">
                        <span>Next</span>
                      </a>
                      <a href="#" onclick="return cScrollTo(4);" title="Previous">
                        <span>Previous</span>
                      </a>
                    </div>
                    </div>
                </div>
                <a href="#" class="text-center slide-link">
                  <span></span>
                </a>
                <div class="arrows">
                  <a href="#" onclick="return cScrollTo(6);" title="Next">
                  <span>Next</span>
                </a>
                <a href="#" onclick="return cScrollTo(4);" title="Previous">
                  <span>Previous</span>
                </a>
              </div>
            </div>

            <div class="swiper-slide community">
                <div class="post-text">
                    <div class="vertical-align">
                      <a class="category-img" target="_blank" href="http://lesamisdenousaidons.org/" title="Friends of We Care">
                        <img src="images/FWC_logo.png" alt="Friends of We Care">
                      </a>
                      <h4>Les Amis de<br> Nous Aidons</h4>
                      <p>Depuis plus de 10 ans, Produits Kruger aide les enfants en situation de handicap à participer aux camps Easter Seals grâce à la commandite organisée entre notre division des produits hors foyer et Les Amis de Nous Aidons. Chaque année, l’association Les Amis de Nous Aidons donne à 5 000 enfants handicapés la chance de participer aux camps Easter Seals totalement accessibles, partout au Canada.</p>
                      <p><a href="http://lesamisdenousaidons.org/" target="_blank">lesamisdenousaidons.org</a></p>
                      <div class="arrows mob">
                        <a href="#" onclick="return cScrollTo(5);" title="Previous">
                          <span>Previous</span>
                        </a>
                      </div>
                    </div>
                </div>
                <a href="#" class="text-center slide-link">
                  <span></span>
                </a>
                <div class="arrows">
                  <a href="#" onclick="return cScrollTo(5);" title="Previous">
                    <span>Previous</span>
                  </a>
                </div>
            </div>

        </div>

        <div class="swiper-pagination-outer">
          <div class="swiper-pagination-line"></div>
          <div class="swiper-pagination"></div>
        </div>

      </section>
      <!-- <div class="community-scrollbar"></div> -->
    </div>


    <div class="showbox animated">
      <!-- <div class="loader">
        <svg class="circular" viewBox="25 25 50 50">
          <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
        </svg>
      </div> -->
    </div>

    <script src="js/all.js"></script>
<?php //require('footer.php'); ?>
