<?php
$title = 'Trademark Notice';
require('header.php');
?>
    <div class="accessibility">
      <div class="wrapper">
        <section class="text-content">
          <div class="container">
            <h1>Avis sur les marques de commerce</h1>
            <p>Certains noms, éléments graphiques, logos, icônes, illustrations, mots, slogans, titres et phrases sur notre site Web sont des marques déposées ou non déposées de Produits Kruger s.e.c., de sa société mère ou de ses sociétés affiliées (les « marques »). Ces marques sont identifiées par les symboles <sup>®</sup>, <sup>™</sup>, MC ou * par une différente police de caractères</p>
            <p>Certaines marques de commerce sur ce site Web sont la propriété d'autres parties qui ont accordé une licence à Produits Kruger pour utiliser leurs marques. Ces marques sont identifiées par des symboles autres que ceux réservés à Produits Kruger.</p>
            <p><sup>®</sup> Forest Stewardship Council et le logo du FSC -- Forest Stewardship Council, A.C.</p>
        </div>
        </section>
      </div>
    </div>

<?php require('footer.php'); ?>
