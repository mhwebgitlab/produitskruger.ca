<?php
$title = 'Carrières';
require('header.php');
?>
  <div class="careers">
    <div class="careers__intro full-height">
      <div class="vertical-align">

        <h1>Carrières</h1>
        <p>UNISSONS NOS FORCES</p>

      </div>
    </div>

<!--
    <div class="home__welcome home__welcome_2 text-in" data-section="Overview Text">
      <div class="container">
        <div class="pseudo-title">OVERVIEW</div>
        <h4>The right culture. <br>The right opportunities. <br>The right fit.</h4>
      </div>
    </div>
 -->

    <div class="home__welcome text-in" data-section="Overview Text">
      <div class="container">
        <h4>Joignez-vous à <br>notre équipe! Voyez les<br> postes disponibles et postulez<br> en ligne dès maintenant</h4>
      </div>
    </div>

    <div class="careers__blocks">
      <div>
        <div class="block wipe-in block-1" data-id="1">
          <a href="http://www.jobs.net/jobs/Kruger-Fr/fr-ca" target="_blank">
            <span class="wrap">
              <hr>
              <h4>Voir les postes<br> disponibles et<br> postuler en ligne</h4>
            </span>
          </a>
        </div>
        <div class="block wipe-in block-2 drawer" data-id="2">
          <a href="javascript:void(0)">
            <span class="wrap">
              <hr>
              <h4>Pourquoi <br>Produits Kruger?</h4>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#add-plus-button"></use>
              </svg>
            </span>
          </a>
        </div>
        <div class="block wipe-in block-3" data-id="3">
          <a href="http://www.jobs.net/jobs/Kruger-Fr/fr-ca" target="_blank">
            <span class="wrap">
              <hr>
              <h4>Pas encore <br> prêt à postuler? Rejoignez<br> notre réseau de talents</h4>
            </span>
          </a>
        </div>
        <div class="block wipe-in block-4 drawer" data-id="4">
          <a href="javascript:void(0)">
            <span class="wrap">
              <hr>
              <h4>Apprenez et <br>développez vos compétences</h4>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#add-plus-button"></use>
              </svg>
            </span>
          </a>
        </div>
        <div class="block wipe-in block-5 drawer" data-id="5">
          <a href="javascript:void(0)">
            <span class="wrap">
              <hr>
              <h4>Rémunération <br>concurrentielle</h4>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#add-plus-button"></use>
              </svg>
            </span>
          </a>
        </div>
        <div class="block wipe-in block-6 drawer" data-id="6">
          <a href="javascript:void(0)">
            <span class="wrap">
              <hr>
              <h4>Parcours <br>professionnels</h4>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#add-plus-button"></use>
              </svg>
            </span>
          </a>
        </div>
        <div class="block wipe-in block-7 deadlink">
          <a href="javascript:void(0)">
            <span class="wrap">
              <hr>
              <h4>« Nos accomplissements <br> 
                sont le fruit des efforts<br> 
                de nos employés, et <br> 
                nous n’oublierons jamais ça. »</h4>
              <h5>Mario Gosselin, CHEF DE LA DIRECTION</h5>
            </span>
          </a>
        </div>
        <div class="block wipe-in block-8 drawer" data-id="8">
          <a href="javascript:void(0)">
            <span class="wrap">
              <hr>
              <h4>Faites une<br> différence</h4>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#add-plus-button"></use>
              </svg>
            </span>
          </a>
        </div>
        <div class="block wipe-in block-9 drawer" data-id="9">
          <a href="javascript:void(0)">
            <span class="wrap">
              <hr>
              <h4>La culture<br> est importante</h4>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#add-plus-button"></use>
              </svg>
            </span>
          </a>
        </div>
        <div class="block wipe-in block-10 drawer" data-id="10">
          <a href="javascript:void(0)">
            <span class="wrap">
              <hr>
              <h4>Entreprise<br> primée</h4>
              <svg class="icon">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#add-plus-button"></use>
              </svg>
            </span>
          </a>
        </div>
      </div>
    </div>

    <div class="text-block" id="text-block-4">

      <svg class="icon">
        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#add-plus-button"></use>
      </svg>

      <div class="container">
        <hr>
        <h3>Faites progresser votre carrière</h3>
        <p class="col">Nous sommes conscients que le perfectionnement continu sur les plans personnel et professionnel est essentiel à la satisfaction au travail et à la réussite professionnelle à long terme. Dans cette optique, afin d’attirer les meilleurs talents, nous vous offrons de réelles occasions pour influencer l’orientation de notre entreprise. Si vous avez des idées, nous voulons les entendre et les mettre en œuvre. Vos contributions seront reconnues et récompensées par des possibilités d’avancement professionnel.</p>
        <p class="col">Nous voulons favoriser le perfectionnement des compétences de nos employés et leur croissance professionnelle de divers moyens : formation en milieu de travail, accompagnement, mentorat, partenariats avec des établissements d’enseignement, séminaires et ateliers.</p>
      </div>
    </div>
    <div class="text-block" id="text-block-6">

      <svg class="icon">
        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#add-plus-button"></use>
      </svg>


      <div class="container">
        <hr>
        <h3>Parcours de carrière</h3>
        <p>Nous recherchons des candidats talentueux possédant une solide expérience de travail, un esprit entrepreneurial et une forte volonté de réussir. Si vous souhaitez laisser votre marque et vous joindre à une équipe dynamique, nous voulons faire votre connaissance.</p>
        <p>
          <strong>Marketing</strong>: Projetez une image positive de Produits Kruger auprès des consommateurs en créant des publicités innovatrices pour la marque, des promotions, des programmes de guérilla marketing en magasin et plus encore.<br>
          <strong>Ventes</strong>: Joignez-vous à l’équipe des ventes la plus réputée de l’industrie et contribuez directement au résultat net de Kruger en recherchant et en saisissant de grandes occasions d’affaires.<br>
          <strong>Ingénierie et exploitation des usines</strong>:  Concevez des produits, des processus et des méthodes de production de haut calibre qui procureront à Kruger un avantage concurrentiel sur le marché.<br>
          <strong>Production et entretien</strong>: Soyez responsable de la productivité et de l’efficacité en veillant au bon déroulement des processus de production et au bon fonctionnement des usines.<br>
          <strong>Finances</strong>: Si vous avez la bosse des mathématiques, mettez à profit vos talents en orientant les grandes décisions de l’entreprise et en fournissant une solide expertise en matière de finances et de comptabilité.<br>
          <strong>Ressources humaines</strong>: Attirez, recrutez et retenez les meilleurs talents en favorisant l’excellence au travail et le perfectionnement continu.</p>
      </div>
    </div>
    <div class="text-block" id="text-block-8">

      <svg class="icon">
        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#add-plus-button"></use>
      </svg>


      <div class="container">
        <hr>
        <h3>Faites une différence</h3>
        <p class="col">Chez Produits Kruger, nous n’oublions jamais que, notre réussite, nous la devons à nos consommateurs. En témoignage de notre reconnaissance, nous nous faisons un point d’honneur de donner en retour à la communauté et de soutenir les initiatives environnementales. </p>
        <p class="col">En tant que membre de notre équipe, vous avez l’occasion de vous impliquer et de faire une différence! Vous pouvez faire du bénévolat dans une banque alimentaire, construire une maison pour une famille dans le besoin ou obtenir de l’appui pour une œuvre de bienfaisance qui vous tient à cœur. Peu importe la source de votre passion, nous espérons qu’elle vous animera tous les jours au travail. </p>
      </div>
    </div>
    <div class="text-block" id="text-block-9">

      <svg class="icon">
        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#add-plus-button"></use>
      </svg>


      <div class="container">
        <hr>
        <h3>La culture est importante</h3>
        <p>Avoir envie de venir travailler tous les matins est un atout inestimable. Et nous faisons tout notre possible pour nous assurer que tous les employés sont engagés, respectés, appréciés et récompensés. Pour ce faire, nous offrons des occasions pour le perfectionnement personnel, la réussite professionnelle et l’implication communautaire, ainsi qu’une bonne dose de divertissement.</p>
        <p>Nous proposons un milieu de travail stimulant avec des portes ouvertes et des couloirs achalandés. Chez Produits Kruger, vous pouvez forger votre propre parcours et prendre des décisions significatives pour l’entreprise. La tolérance, la sensibilité et la diversité dans le milieu de travail font en sorte que la culture de l’entreprise est axée sur la dignité et le respect mutuel.</p>
        <p>D’après un sondage, une grande majorité des employés de Produits Kruger ont affirmé qu’ils aimaient leurs collègues. Chez Produits Kruger, nous voulons que nos employés soient aussi heureux que nos clients.</p>
      </div>
    </div>
    <div class="text-block" id="text-block-10">

      <svg class="icon">
        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#add-plus-button"></use>
      </svg>

      <div class="container">
        <hr>
        <h3>Entreprise primée</h3>
        <p>Au fil des années, les efforts indépendants et collaboratifs des employés de Produits Kruger nous ont valu de nombreux prix. Lorsque vous vous joindrez à nous, vous ferez partie d’une entreprise qui excelle dans tous les domaines, et ce, grâce à la vision et au travail acharné de nos employés.</p>
      </div>
    </div>
    <div class="text-block" id="text-block-5">

      <svg class="icon">
        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#add-plus-button"></use>
      </svg>

      <div class="container">
        <hr>
        <h3>Rémunération concurrentielle</h3>
        <p class="col">Chez Produits Kruger, nous surveillons régulièrement le marché pour nous assurer que nos programmes de rémunération demeurent concurrentiels par rapport aux autres grands employeurs de l’industrie.</p>
        <p class="col">Nous encourageons la mobilité des employés en offrant de généreuses bourses d’études pour des collèges, des universités et des programmes d’accréditation professionnelle. Nous offrons des programmes d’avantages sociaux flexibles, des primes incitatives axées sur des objectifs, des programmes d’aide aux employés, des forfaits de vacances généreux et une variété de régimes de retraite.</p>
      </div>
    </div>

    <div class="text-block" id="text-block-2">

      <svg class="icon">
        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#add-plus-button"></use>
      </svg>

      <div class="container">
        <hr>
        <h3>Les occasions idéales. <br>La culture idéale. <br>Le milieu de travail idéal.</h3>
        <p class="col">Si vous recherchez l’endroit parfait où travailler, pensez à une carrière chez Produits Kruger, reconnue comme l’un des meilleurs employeurs dans la RGT pendant cinq années consécutives. Découvrez pourquoi des personnes talentueuses comme vous souhaitent se joindre à notre équipe et faire une différence dans une industrie nord-américaine dynamique et en plein essor.</p>
        <p class="col">Nous offrons une rémunération concurrentielle, un milieu de travail détendu et décontracté, des avantages sociaux et des régimes de retraite flexibles, ainsi que des parcours de carrière bien définis dans tous les services.</p>
      </div>
    </div>
  </div>

<?php require('footer.php'); ?>

