<?php
$title = 'Nous joindre';
require('header.php');
?>

    <div class="contact">
      <section class="contact__intro">
        <h1>Bonjour,</h1>
      </section>
      <section class="contact__cf">
        <div class="container">
          <a href="javascript:void(0)" class="close-form">
            <svg class="icon">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#multiply"></use>
            </svg>
          </a>
          <div class="pseudo-title">Renseignements généraux et commentaires</div>
          <h4>Renseignements généraux et commentaires</h4>
          <form id="send-mail">
            <input type="text" name="ip" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>" hidden>
            <div class="form-wrap clearfix2">
              <div class="col">
                <p>Veuillez noter:</p>
                <p>Veuillez conserver le produit et l'emballage étant donné que nous pourrions vous demander de les envoyer (tous les frais d'expédition seront couverts par Produits Kruger).</p>
                <p>Produits Kruger s.e.c. n'acceptera pas et ne répond pas à aucune demande provenant de personnes âgées de moins de treize ans. Si vous avez moins de treize ans, demandez à un parent ou à votre tuteur d'envoyer la demande en votre nom.</p>
                <p>Peu importe le moyen que vous choisissez pour communiquer avec nous, soyez assuré que les renseignements transmis ne seront en aucun cas partagés ni vendus, et seront utilisés seulement par Produits Kruger s.e.c. et ses sociétés affiliées. Pour en savoir plus, veuillez consulter notre consulter notre <a href="#">Politique de confidentialité</a>.</p>
              </div>
              <div class="col">
                <div class="styled-select">
                  <select name="title" required tabindex="1">
                    <option value="" disabled selected>*Titre</option>
                    <option value="Mr.">M.</option>
                    <option value="Mrs.">Madame.</option>
                    <option value="Ms.">Mme.</option>
                    <option value="Dr.">Dr.</option>
                  </select>
                  <svg class="icon">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-2"></use>
                  </svg>
                  <span class="error-msg">Veuillez choisir votre titre.</span>
                </div>

                <div>
                  <input type="text" name="name" placeholder="*Prénom" required oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="20" tabindex="1">
                  <span class="error-msg">Veuillez inscrire votre prénom.</span>
                </div>
                <div>
                  <input type="text" name="last_name" placeholder="*Nom de Famille" required oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="20" tabindex="1">
                  <span class="error-msg">Veuillez inscrire votre nom de famille.</span>
                </div>
                <div>
                  <input type="text" name="address_1" placeholder="*Adresse 1" required oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="20" tabindex="1">
                  <span class="error-msg">Veuillez inscrire votre adresse.</span>
                </div>
                <div>
                  <input type="text" name="address_2" placeholder="Adresse 2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="20" tabindex="1">
                </div>
                <div>
                  <input type="text" name="city" placeholder="*Ville" required oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="20" tabindex="1">
                  <span class="error-msg">Veuillez inscrire votre ville.</span>
                </div>
                <div class="styled-select">
                  <select name="province_teritory" required tabindex="1">
                    <option value="" disabled selected>*Province/Territoire</option>
                    <option value="AB">Alberta</option>
                    <option value="CB">Colombie-Britannique</option>
                    <option value="IP">Île-du-Prince-Édouard</option>
                    <option value="MB">Manitoba</option>
                    <option value="NB">Nouveau-Brunswick</option>
                    <option value="NE">Nouvelle-Écosse</option>
                    <option value="NU">Nunavut</option>
                    <option value="ON">Ontario</option>
                    <option value="PQ">Québec</option>
                    <option value="SK">Saskatchewan</option>
                    <option value="TL">Terre-Neuve-et-Labrador</option>
                    <option value="TN">Territoires du Nord-Ouest</option>
                    <option value="YT">Yukon</option>
                    <option value="other">Other</option>
                  </select>
                  <svg class="icon">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-2"></use>
                  </svg>
                  <span class="error-msg">Veuillez choisir votre province.</span>
                </div>
                <div class="">
                  <input type="text" name="postal_code" placeholder="*Code Postal" required oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="20" tabindex="1">
                  <span class="error-msg">Veuillez inscrire votre code postal.</span>
                </div>
                <div class="">
                  <input type="phone" name="phone" placeholder="*Numéro de Téléphone" required oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="20" tabindex="1">
                  <span class="error-msg">Veuillez inscrire votre numéro de téléphone.</span>
                </div>
                <span class="errorC" style="color:red;"></span>
                <span class="successC" style="color:green;"></span>
                <input type="submit" value="soumettre" class="disabled" tabindex="2">
              </div>
              <div class="col">
                <div class="">
                  <input type="email" name="email" placeholder="*Courriel" required tabindex="1">
                  <span class="error-msg">Assurez-vous que votre courriel a été inscrit et tapé correctement.</span>
                </div>
                <div class="">
                  <input type="email" name="confirm_email" placeholder="*Confirmez votre courriel" required tabindex="1">
                  <span class="error-msg">Assurez-vous que votre courriel a été inscrit et tapé correctement.</span>
                </div>
                <div class="styled-select">
                  <select name="product" required tabindex="1">
                    <option value="" selected disabled>*Produit</option>
                    <option>Cashmere</option>
                    <option>Cashmere Fresh Wipes</option>
                    <option>Purex</option>
                    <option>Scotties</option>
                    <option>Soft &amp; Pure</option>
                    <option>SpongeTowels</option>
                    <option>White Cloud</option>
                    <option>White Swan</option>
                    <option>Other – please specify in Comments box</option>
                  </select>
                  <svg class="icon">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-2"></use>
                  </svg>
                  <span class="error-msg">Veuillez choisir un produit.</span>
                </div>
                <div>
                  <input type="text" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" onkeydown="checkNum(event);" maxlength="5" name="upc_code" placeholder="UPC code (5 digits) " maxlength="5" tabindex="1">
                </div>
                <textarea name="remarks" rows="8" cols="80" placeholder="Commentaires" style="min-height: 173px" tabindex="1"></textarea>
                <!-- <span style="font-size:12px;">(last 5 digits)</span> -->
                <div class="g-recaptcha" data-sitekey="6LdhZhYUAAAAALqIJzHU-YRgDMM9tN7bD7tp9hr6" style="transform:scale(0.9);-webkit-transform:scale(0.9);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>
                <br>
                <br>
                <br>
              </div>
              <div class="clearfix"></div>
            </div>
          </form>
        </div>
      </section>
      <div class="wrapper">
        <section class="home__welcome home__welcome_2 text-in">
          <div class="container">
            <div class="">
              <div class="pseudo-title">GET IN TOUCH</div>
              <h4>Quelle que soit<br>la raison, nous sommes <br>impatients de vous entendre!</h4>
              <p>Veuillez nous appeler sans frais du lundi au vendredi, entre 8 h 30 et 17 h 30, heure de l'Est. Si notre bureau est fermé, veuillez nous laisser un message et nous vous rappellerons rapidement. Sinon, vous pouvez aussi remplir notre <a class="open-cf" href="javascript:void(0)">formulaire de contact</a>.</p>
            </div>
          </div>
        </section>
        <div class="clearfix2"></div>
        <section class="contact__phone wipe-in">
          <h2><a href="tel:18889421212">1-888-942-1212</a></h2>
        </section>
        <section class="contact__blocks">
          <div class="block wipe-in" onclick="void(0)">
            <div class="hover-block">
              <h4>Bons et promotions</h4>
              <p>Des bons et promotions sont fréquemment offerts dans toutes nos marques. Pour les offres du moment, visitez les sites Web de nos marques ou suivez vos marques préférées sur Facebook. Tous nos bons et promotions sont disponibles sur Facebook et certains sont exclusivement sur Facebook.</p>
              <p>Sites Web des marques:<br>
                <a target="_blank" href="http://www.cashmere.ca/index_fr.html#home">cashmere.ca</a> /
                <a target="_blank" href="http://www.purex.ca/francais/">purex.ca</a> /
                <a target="_blank" href="http://www.spongetowels.ca/index-fr.php">spongetowels.ca</a> /
                <a target="_blank" href="http://www.scotties.ca/fr/">scotties.ca</a> /
                <a target="_blank" href="http://www.whiteswan.ca/francais.html">whiteswan.ca</a> /
                <a target="_blank" href="http://mywhitecloud.com">mywhitecloud.com</a>
              </p>
              <p>Facebook:<br>
                <a target="_blank" href="https://www.facebook.com/Cashmere">Cashmere</a> /
                <a target="_blank" href="https://www.facebook.com/purexbathroomtissue/">Purex</a> /
                <a target="_blank" href="https://www.facebook.com/SpongeTowels">SpongeTowels</a> /
                <a target="_blank" href="https://www.facebook.com/ScottiesTissue">Scotties</a> /
                <a target="_blank" href="https://www.facebook.com/mywhitecloud/">White Cloud</a>
              </p>
            </div>
          </div>
          <div class="block wipe-in" onclick="void(0)">
            <div class="hover-block">
              <h4>Dons</h4>
              <p>Bien que nous accordions la plus haute importance à la responsabilité sociale et que nous souhaiterions pouvoir répondre à toutes les demandes que nous recevons, nous pensons être plus efficaces en soutenant plus particulièrement certaines initiatives au sein de nos communautés.</p>
              <p>Dans cette optique, nous sommes fiers d’être l’un des cinq donateurs les plus importants au Canada pour la Fondation canadienne du cancer du sein (FCCS), une organisation caritative nationale engagée à créer un avenir sans cancer du sein. Pour en savoir plus sur la FCCS et les autres organisations que nous soutenons, veuillez visiter notre <a href="./community.php">page Communauté</a></p>
            </div>
          </div>
          <div class="block wipe-in" onclick="void(0)">
            <div class="hover-block">
              <h4>Produits commerciaux</h4>
              <p>Si vous avez une demande concernant notre division Produits hors foyer (PHF), veuillez visiter notre <a href="http://afh.krugerproducts.ca/home.aspx?lang=fr-CA&">site Web dédié aux PHF</a>.</p>
            </div>
          </div>
          <div class="block wipe-in" onclick="void(0)">
            <div class="hover-block">
              <h4>Envoyez-nous une lettre</h4>
              <p>Notre adresse postale est :</p>
              <p>Réponse aux consommateurs <br>
              Produits Kruger s.e.c. <br>
              1900 Minnesota Court, bureau 200 <br>
              Mississauga (Ontario) <br>
              L5N 5R5</p>
            </div>
          </div>
          <div class="block wipe-in" onclick="void(0)">
            <div class="hover-block">
              <h4>Projets scolaires</h4>
              <p>Le monde du papier vous intéresse? Voici quelques ressources qui peuvent vous aider :</p>
              <p>Pour avoir des informations sur de nombreux sujets en lien avec le papier, veuillez visiter <a target="_blank" href="//www.tappi.org/">TAPPI</a>, la plus grande association technique pour l’industrie de la pâte, du papier et de la transformation à l’échelle planétaire.</p>
              <p>Pour en savoir plus sur Produits Kruger, visitez la section <a href="./about.php">« À propos de nous »</a> de notre site Web.</p>
              <p>Pour des informations sur notre société mère, visitez <a href="//www.kruger.com/" target="_blank">kruger.com</a>.</p>
          </div>
          </div>
          <div class="block wipe-in" onclick="void(0)">
            <div class="hover-block">
              <h4>Détaillants et distributeurs</h4>
              <p>Nous serons heureux de répondre à vos demandes par téléphone.  Veuillez nous appeler sans frais au <a href="tel:18006655610">1 800 665 5610</a> du lundi au vendredi, entre 8 h 00 et 17 h 30, heure de l'Est. Si notre bureau est fermé, nous vous rappellerons le jour ouvrable suivant. Les commandes et les demandes de renseignements peuvent nous être acheminées sans frais par télécopieur, 24 heures sur 24, au <a href="tel:18005637268">1 800 563 7268</a>.</p>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>
      </div>
    </div>

<?php require('footer.php'); ?>
