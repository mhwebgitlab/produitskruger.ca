<?php
$title = 'Avis juridique';
require('header.php');
?>
<div class="accessibility">
    <div class="wrapper">
        <section class="text-content">
            <div class="container">
                <h1 role="main">Avis juridique</h1>
                <p>Ce site Web (le « site Web ») et son contenu sont la propriété de <?php echo $company_name;?> (« Produits
                    Kruger » ou « nous ») qui l'exploite dans le cadre de son service à la clientèle. Votre utilisation
                    de ce site Web constitue votre acceptation des présentes modalités d’utilisation, auxquelles vous
                    acceptez de vous soumettre et qui vous lient à Produits Kruger. Si vous n'acceptez pas les présentes
                    modalités d'utilisation, vous êtes priés de ne pas utiliser ce site Web.</p>
                <p>Produits Kruger se réserve en tout temps les droits suivants sans avoir à déposer le moindre préavis
                    : a) apporter des modifications à ce site Web et à son contenu ainsi qu'aux présentes modalités
                    d’utilisation; b) surveiller et supprimer les propositions qui lui sont transmises; et c)
                    interrompre l'exploitation du site Web. Veuillez vous reporter périodiquement aux présentes
                    modalités d’utilisation pour prendre connaissance de leur version actualisée. Votre utilisation de
                    ce site Web affirme votre volonté de respecter l'ensemble des modalités d’utilisation telles que
                    révisées. Toute omission de la part de Produits Kruger d'agir par suite de l'inobservation des
                    présentes modalités d’utilisation par vous ou par toute autre partie, ne signifie en aucun cas que
                    Produits Kruger renonce à son droit d'agir par suite d'inobservations ultérieures ou similaires des
                    modalités. Les présentes modalités d’utilisation exposent l'entente entre Produits Kruger et vous en
                    ce qui a trait à l'utilisation de ce site Web.</p>
                <p>Ce site Web, son contenu et tous les produits et services qui y sont publicisés ne sont destinés
                    qu’aux résidants du Canada. Produits Kruger ne fait nullement valoir que les produits et services
                    apparaissant sur ce site Web sont offerts dans d'autres territoires ou que les éléments de ce site
                    Web conviennent à un usage dans d'autres territoires. Si vous décidez de votre propre chef d'accéder
                    à ce site Web à partir d'un autre territoire, c'est à vous qu'il incombe d'observer les lois locales
                    en vigueur.</p>
                <h2 class="h4">Droits d'auteur et marques de commerce</h2>
                <p>L'ensemble du matériel présenté sur ce site Web, à l'exception du matériel de tierces parties utilisé
                    sous licence, y compris le texte, les images, le logiciel, les programmes, les éléments graphiques,
                    les bandes vidéo, les bandes audio, les dessins et les marques de commerce que l'on y voit (les «
                    éléments du matériel »), sont la propriété de Produits Kruger et/ou sont exploités par cette
                    dernière et sont protégés par les lois internationales relatives au droit d'auteur et aux marques de
                    commerce. Produits Kruger vous accorde une licence limitée, personnelle, non exclusive et incessible
                    vous permettant d’utiliser et d’afficher les éléments du matériel uniquement sur votre ordinateur
                    personnel ou votre appareil mobile, et de télécharger et imprimer une copie de tout élément du
                    matériel, pour chaque cas à des fins personnelles et non commerciales seulement, pourvu que vous
                    respectiez tous les avis touchant les droits d’auteur, les marques de commerce et autres avis
                    exclusifs contenus dans les éléments du matériel. Sauf indication contraire, vous ne pouvez pas
                    copier, reproduire, télécharger, redistribuer, republier, transmettre, créer des œuvres dérivées ou
                    modifier les éléments du matériel ou les utiliser à des fins commerciales ou publiques sans le
                    consentement écrit préalable de Produits Kruger. La présente licence limitée sera annulée
                    automatiquement, sans préavis, si vous enfreignez les présentes modalités d’utilisation. À la
                    résiliation, vous devez immédiatement supprimer et détruire tout élément du matériel téléchargé
                    et/ou imprimé. Vous n’avez aucun droit, titre ou intérêt (et aucun droit d’auteur, de marque de
                    commerce ou autre droit de propriété intellectuelle) envers le présent site Web ou tout élément du
                    matériel.</p>
                <p>Les marques de commerce figurant sur ce site Web sont la propriété exclusive de Produits Kruger
                    lorsqu'elles sont suivies du sigle <sup>™</sup>, <sup>®</sup> ou d'un astérisque (*) et de
                    Kimberly-Clark Worldwide, Inc. (« Kimberly-Clark ») lorsqu'elles sont suivies du sigle <sup>®</sup>'
                    or <sup>®</sup>! . Les marques déposées de Kimberly-Clark sont utilisées sous licence par Produits
                    Kruger.</p>
                <p>Il est formellement interdit d'utiliser les marques de commerce figurant sur ce site Web autrement
                    qu'en vertu des présentes modalités d’utilisation. Pour obtenir davantage de renseignements à propos
                    des marques de commerce et des marques de tiers dont il est question sur ce site Web, veuillez vous
                    reporter à notre avis sur les marques de commerce.</p>
                <h3 class="h4">Autres restrictions d’utilisation</h3>
                <p>Vous convenez de ne pas accéder à ce site Web au moyen d’un « outil de moissonnage », d’un robot,
                    d’une araignée ou d’autres moyens automatisés. Vous convenez également de ne pas « encadrer » ni
                    créer une « image miroir » de ce site Web ou de tout élément du matériel, sur tout autre serveur ou
                    dispositif Internet sans l’autorisation écrite préalable de Produits Kruger. Tous les liens
                    autorisés à ce site Web doivent être conformes aux lois, règles et règlements en vigueur. Vous ne
                    pouvez pas utiliser des balises Méta ou d’autres formes de « texte caché » faisant usage de notre
                    nom ou de l’une des marques déposées sur ce site Web, sans notre autorisation écrite expresse. En
                    outre, vous convenez : a) de ne pas entreprendre toute action qui, à notre entière discrétion,
                    infligera, ou pourrait infliger, de manière déraisonnable ou disproportionnée, une surcharge à notre
                    infrastructure informatique; b) de ne pas nuire ou tenter de nuire au bon fonctionnement de ce site
                    Web ou à toute activité menée sur ce site Web; ou c) de ne pas contourner toute mesure que nous
                    pourrions utiliser pour prévenir ou restreindre l’accès au site Web.</p>
                <h4>Disponibilité</h4>
                <p>Produits Kruger ne garantit pas un accès continu, ininterrompu, sans erreur ou sécurisé sur ce site
                    Web. L'exploitation de ce site Web pourrait être entravée par des facteurs sur lesquels Produits
                    Kruger n'exerce aucun contrôle. Produits Kruger ne garantit pas que les serveurs permettant l'accès
                    à ce site Web sont exempts de virus informatiques et d'autres éléments nuisibles, ni que toute
                    défectuosité sera réparée sur-le-champ. Produits Kruger ne surveille pas régulièrement les
                    propositions que vous lui transmettez ni vos activités sur le site Web, mais se réserve le droit de
                    le faire. Si Produits Kruger découvre que ce site Web est utilisé de façon inappropriée, Produits
                    Kruger réagira en prenant toute mesure qu’elle juge appropriée, à son entière discrétion. Vous
                    reconnaissez que Produits Kruger pourrait signaler aux autorités chargées de l’application des lois
                    toute action ou tout renseignement qui pourraient être considérés comme étant illégaux ou qui sont
                    pertinents afin de satisfaire aux exigences de la loi ou de la réglementation, de se protéger ou
                    d'exploiter ce site Web comme il se doit.</p>
                <p>Vous reconnaissez que toute utilisation non autorisée de tout élément du matériel, y compris des
                    marques de commerce, pourrait nous causer des préjudices irréparables, et vous convenez que, dans le
                    cas d’une telle utilisation non autorisée, nous aurons le droit de demander une injonction en plus
                    de tout autre recours en justice ou en équité.</p>
                <h4>Avis de non-responsabilité</h4>
                <p>Votre utilisation de ce site Web et des éléments du matériel est à vos propres risques. Ce site Web
                    et tous les éléments du matériel sur ce site Web sont fournis « tels quels », sans garantie
                    implicite ou explicite d'aucune sorte en ce qui concerne la qualité marchande, la convenance à un
                    usage particulier et l’absence de contrefaçon. Produits Kruger emploie des efforts raisonnables afin
                    d’inclure sur le ce site Web des renseignements exacts et à jour, mais ne fait aucune déclaration ni
                    ne donne aucune garantie quant à l’exactitude ou l’exhaustivité des éléments du matériel.</p>
                <h4>Limitation de la responsabilité</h4>
                <p>En aucun cas, Produits Kruger, sa société mère, ses sociétés affiliées ou ses fournisseurs ne seront
                    tenus responsables, envers vous ou un tiers, de quelque dommage que ce soit (y compris, mais sans
                    s’y limiter, les dommages accessoires, particuliers, consécutifs ou punitifs, ou les dommages
                    découlant d’une perte de profits, d’une interruption de l’exploitation, d’une perte de
                    renseignements, de programmes ou d’autres données) pouvant découler de ou être lié à ce site Web, y
                    compris, mais sans s’y limiter, votre utilisation ou votre incapacité d’utiliser ce site Web, son
                    contenu ou ses éléments du matériel, même si Produits Kruger a été informée de la possibilité de
                    tels dommages. Produits Kruger n'est aucunement tenue de procéder à la mise à jour de ce site Web,
                    de son contenu ou de ses éléments du matériel et ne pourra être tenue responsable de toute omission
                    à cet égard. Vous assumez l'entière responsabilité par rapport à tous les frais liés aux dommages,
                    aux réparations et/ou à l’entretien que vous engagez. Votre seul et unique recours est de cesser
                    d’utiliser ce site Web.</p>
                <h4>Propositions des utilisateurs</h4>
                <p>Produits Kruger désire vivement recevoir vos commentaires et répondre à vos questions à propos de ses
                    produits ou de l'entreprise même. Toutefois, nous ne sommes pas à la recherche, pas plus que nous
                    n'acceptons d'idées, de propositions ou de matériel concernant le développement, la conception, la
                    fabrication ou la commercialisation de nos produits ou services (les « idées non sollicitées »).
                    Bien que nous vous invitions à nous faire part de vos remarques, commentaires et questions, veuillez
                    vous abstenir de nous transmettre toute idée non sollicitée.</p>
                <p>En affichant sur ce site Web tout document, y compris des idées non sollicitées, des messages, des
                    éditoriaux écrits, des photographies, des logiciels ou des vidéoclips (les « propositions »), vous
                    consentez à ce qu'ils soient tous réputés non confidentiels et non exclusifs, et à ce qu'aucun d'eux
                    ne vous soit retourné. Vous accordez à Produits Kruger une licence non exclusive, libre de
                    redevances, mondiale, cessible sans restriction et perpétuelle pour publier, afficher, reproduire,
                    montrer, diffuser, retransmettre, modifier, retoucher ou autrement utiliser (en tout ou en partie,
                    peu importe le type de média, de format ou de technologie utilisé, connu ou découvert
                    ultérieurement) les propositions, avec le droit d’accorder à un tiers en vertu d’une sous-licence le
                    droit illimité d’exercer l’un des droits précédents. La licence prévoira le droit d'exploiter tous
                    droits exclusifs relativement aux propositions, y compris, sans toutefois s'y limiter, les droits
                    relatifs aux droits d'auteur et aux marques de commerce, sans qu’il soit nécessaire de vous fournir
                    tout autre préavis ou toute autre rémunération, ou d’obtenir votre permission. Produits Kruger n'a
                    aucune obligation en ce qui touche les propositions, et vous consentez à n'exercer aucun recours
                    contre Produits Kruger relativement à toute violation réelle ou présumée, imitation frauduleuse ou
                    appropriation illicite de tous droits exclusifs en rapport aux propositions. Vous acceptez également
                    de renoncer à tout droit moral applicable tel qu’il est indiqué dans les propositions pour l’une des
                    utilisations indiquées ci-dessus. Produits Kruger se réserve le droit de supprimer en tout temps
                    toute proposition qui, à son entière discrétion, serait répréhensible, offensante, illicite ou qui
                    irait à l'encontre des présentes modalités d’utilisation.</p>
                <p>Vous consentez à n'empêcher aucun individu de visiter ce site Web et vous consentez à ne pas afficher
                    ou transmettre la moindre proposition qui serait illicite, outrageante, diffamatoire, menaçante ou
                    obscène. Vous consentez à ne pas afficher ou transmettre la moindre proposition qui violerait ou
                    enfreindrait les droits d'autrui, y compris les propositions qui contreviendraient aux droits de la
                    protection des renseignements personnels ou de la publicité ou qui seraient protégées par des droits
                    d'auteur, des marques de commerce ou d'autres droits exclusifs sans obtenir au préalable la
                    permission du détenteur des droits d'origine. Vous consentez à ne pas afficher ou transmettre la
                    moindre proposition chargée de virus ou d'autres éléments nuisibles. Tout acte de ce genre constitue
                    une violation des présentes modalités d’utilisation et met immédiatement fin à votre droit de
                    fréquenter ce site Web. Produits Kruger se réserve le droit de prendre toutes les actions en justice
                    et administratives qui s'imposent contre quiconque viole les présentes modalités. En transmettant
                    une proposition, vous acceptez de libérer, de ne pas tenir responsable et d’indemniser en votre nom
                    et en celui de vos successeurs, ayants droit et représentants, Produits Kruger, sa société mère, ses
                    sociétés affiliées et ses fournisseurs, ainsi que chacun de leurs dirigeants, administrateurs et
                    employés respectifs, de toutes réclamations, poursuites, actions, demandes, responsabilités et de
                    tous dommages, de quelque nature que ce soit, résultant ou découlant de notre utilisation d’une
                    telle proposition, y compris, mais sans s’y limiter, toute réclamation pour fausse publicité,
                    violation du droit d’auteur, atteinte à la vie privée, violation du droit de publicité ou des droits
                    moraux, et/ou la diffamation.</p>
                <h4>Sites Web de tiers</h4>
                <p>Produits Kruger n'assume aucune responsabilité de quelque nature que ce soit quant aux sites Web de
                    tiers auxquels vous pouvez accéder à partir de celui-ci. Produits Kruger n’exerce aucun contrôle sur
                    de tels sites Web de tiers et n’assume aucune responsabilité quant aux dommages ou aux préjudices
                    découlant de votre utilisation de tels sites Web. De plus, nos politiques de confidentialité ne
                    s'appliquent pas aux sites Web de tiers.</p>
                <h4>Confidentialité</h4>
                <p>En utilisant ce site Web, il est possible que vous fournissiez à Produits Kruger des renseignements
                    personnels, tels vos nom et prénom, adresse complète, numéro de téléphone et adresse électronique.
                    Produits Kruger utilise les renseignements personnels conformément à sa politique de
                    confidentialité. Veuillez vous reporter aux modalités de notre politique de confidentialité avant de
                    nous fournir tout renseignement confidentiel.</p>
                <h4>Lois applicables et différends</h4>
                <p>Les présentes modalités d’utilisation sont régies par les lois du Canada et de la province de
                    l’Ontario, sans égard aux principes de conflit de lois. Vous convenez que tous les différends
                    découlant directement ou indirectement des présentes modalités d’utilisation ou liés à ce site Web,
                    à son contenu ou à ses éléments du matériel, seront résolus exclusivement dans les tribunaux
                    compétents situés dans la ville de Toronto, en Ontario. Par les présentes, vous consentez de façon
                    irrévocable à avoir recours à de tels tribunaux et reconnaissez la compétence exclusive d’un tel
                    tribunal sur un différend. Si l'on devait déterminer que l'une ou l'autre des modalités
                    d’utilisation est invalide, nulle, illicite ou inexécutable pour quelque raison, la validité et le
                    caractère exécutoire des autres modalités ne seraient nullement touchés. Les présentes modalités
                    d’utilisation constituent la totalité de l’entente entre vous et Produits Kruger en ce qui concerne
                    votre accès ou votre utilisation de ce site Web ou de son contenu ou ses éléments du matériel.</p>
            </div>
        </section>
    </div>
</div>

<?php require('footer.php'); ?>
