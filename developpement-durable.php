<?php
$title = 'Développement durable';
require('header.php');
?>

<link rel="stylesheet" href="css/sustainability.css">

<div class="maindiv-sustainability">
    <section class="video">
        <div class="container">
            <video loop autoplay muted>
                <source src="videos/Reimagine-2030-fr.mp4" type="video/mp4">
                Votre navigateur ne prend pas en charge la balise vidéo.
            </video>
        </div>
    </section>
    <div class="container">
        <hr class="newhr" />
    </div>
    <section class="reimagine2030">
        <div class="container">
            <div class="heading">
                <h1 role="main">Pourquoi Reconcevoir 2030?</h1>
            </div>
            <div class="row">
                <div class="col-6 imgBox">
                    <img src="images/whyreimagine-girl-img.png" alt="Girl face" />
                </div>
                <div class="col-6 textBox">
                    <p>Notre mission en matière de développement durable accorde la priorité à l’environnement et à notre impact social pour rendre la vie quotidienne plus confortable.
                      En 2020, nous avons réfléchi à où nous voulions être dans 10 ans, 10 ans et ce que nous espérons pour la planète. C’est ce qui a inspiré Reconcevoir 2030, une décennie de développement, de transformation et d’innovation durable.</p>
                    <p><a class="grn-highlights" href="#" data-featherlight="#reimagine-lightbox" aria-label="Video" role="button">Cliquez ici pour découvrir comment nous allons Reconcevoir 2030</a></p>
                    <p class="GeorgiaFont">Notre héritage en termes de progrès est important, mais nous reconnaissons que nous devons aller encore plus loin, faire mieux et avoir plus d’impact. Reconcevoir 2030 est un plan sur 10 ans. Cet horizon à long terme nous permettra d’avoir des ambitions plus grandes et d’engager les investissements nécessaires pour réussir. Notre stratégie s’articule autour de piliers de développement durable nouvellement définis sur le plan de l’entreprise, de nos produits, de la planète, des gens et des communautés, ainsi que d’un ensemble de questions relatives aux matériaux, lesquelles couvrent nos impacts environnementaux et sociaux les plus importants. Nous avons créé de nouvelles cibles quantitatives et qualitatives au sein de chaque domaine d’intérêt et planifions élaborer de nouveaux objectifs à mesure que nous poursuivons notre chemin sur la voie de la croissance transformatrice et de l’innovation durable.</p>
                    <p class="GeorgiaFont">Nous intégrons le développement durable dans chacune des décisions que nous prenons et dans chacune de nos actions. C’est un sujet qui fait de plus en plus partie de notre ADN d’entreprise. Nos décisions et nos actions d’aujourd’hui façonneront le monde de demain.</p>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <hr class="newhr" />
    </div>
    <section class="growthsection">
        <div class="container">
            <h2 class="h1size">Une décennie de croissance transformatrice et d’innovation durable</h2>
            <div class="growth-chart">
                <div class="chart-circle">
                    <div class="circle-txt">
                        <img class="chrt-kplogo" src="images/chart-kruger-logo.png" alt="kruger Products logo" />
                        <div class="vsn-misn-row">
                            <div class="visonbox">
                                <h5 class="upercase ">Vision</h5>
                                <p class="GeorgiaFont boxFix">Être l’entreprise de produits de papier la plus appréciée et à laquelle les gens font le plus confiance en Amérique du Nord</p>
                            </div>
                            <div class="missonbox">
                                <h5 class="upercase">Mission</h5>
                                <p class="GeorgiaFont boxFix">Rendre la vie quotidienne​ <br class="boxAppear" style="display: none;">plus confortable​</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="chart-mdle-box">
                    <div class="chrt-pilrbox one">
                        <div class="chrt-pilrtxt">
                            <h6 class="upercase">PILIER : RESPONSABILISATION DES <br>PRODUITS</h6>
                            <ul class="pilrul upercase">
                                <li>FIBRE ET FORÊTS</li>
                                <li>EMBALLAGE</li>
                                <li>INNOVATION DES PRODUITS</li>
                            </ul>
                        </div>
                    </div>
                    <div class="chrt-pilrbox two">
                        <div class="chrt-pilrtxt">
                            <h6 class="upercase">Pilier :  Conscience de la planète  </h6>
                            <ul class="pilrul upercase">
                                <li>CHANGEMENT CLIMATIQUE</li>
                                <li>EAU</li>
                                <li>DÉCHETS</li>
                            </ul>
                        </div>
                    </div>
                    <div class="chrt-pilrbox three">
                        <div class="chrt-pilrtxt">
                            <h6 class="upercase">PILIER : IMPACT POUR LES EMPLOYÉS</h6>
                            <ul class="pilrul upercase">
                                <li>SANTÉ ET SÉCURITÉ</li>
                                <li>DIVERSITÉ ET CULTURE</li>
                                <li>FORMATION ET DÉVELOPPEMENT</li>
                            </ul>
                        </div>
                    </div>
                    <div class="chrt-pilrbox four">
                        <div class="chrt-pilrtxt">
                            <h6 class="upercase">PILIER : IMPLICATION DANS NOS COMMUNAUTÉS</h6>
                            <ul class="pilrul upercase">
                                <li>INVESTISSEMENT COMMUNAUTAIRE</li>
                                <li>BÉNÉVOLAT DES EMPLOYÉS</li>
                                <li>RELATIONS AVEC LA COMMUNAUTÉ​</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <img class="mobileimg" src="images/growth-plan-chart-mob.png" alt="growth and sustainable innocation chart" />
            </div>
        </div>
    </section>
    <section class="pillars-section">
        <div class="container">
            <div class="pillarBox plrOne">
                <div class="pilr-titl one">
                    <h2>Pilier 1 : Responsabilisation des produits</h2>
                </div>
                <div class="row">
                    <div class="col-6 pilrone-cntnt-box">
                        <p>Grâce à nos produits, nous nous efforçons d’inspirer et de donner aux gens les moyens de vivre de façon confortable et durable. Alors que nous naviguons vers Reconcevoir 2030, nous chercherons à améliorer la durabilité de tous nos produits, tout en nous assurant toujours de leur qualité supérieure et de leur confort pour nos clients et consommateurs.</p>
                        <h3>Fibres et forêts</h3>
                        <h4>Cible de Reconcevoir 2030 : <br><span>100 % de fibres certifiées par une tierce partie</span></h4>
                        <p class="GeorgiaFont">Chez Produits Kruger, nous savons que nos activités reposent sur des forêts durables et notre utilisation des ressources forestières, c’est-à-dire notre source de fibres. Il s’agit du domaine dans lequel nous pouvons avoir le plus grand impact positif.</p>
                        <p class="GeorgiaFont">Les forêts saines et durables sont essentielles pour notre bien-être social et économique, et notre capacité à prévenir les crises mondiales concernant le climat et la biodiversité. Des millions de personnes dépendent des forêts et des produits forestiers pour leur subsistance. Les forêts ont également une grande importance culturelle et offrent des possibilités de loisirs aux gens du monde entier. Nous sommes engagés à nous approvisionner en fibres durables et à soutenir la gestion durable des écosystèmes forestiers.</p>
                    </div>
                    <div class="col-6 tpforest">
                        <img src="images/forest-tissue-paper-rolls.png"
                            alt="Tissue Paper Rolls with Forest image" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 txtCenter">
                        <img class="brndPack" src="images/brands-pack-img2022.png" alt="Packges of brand image">
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <h3 class="mrgTop0">Emballage</h3>
                        <h4>Cible de Reconcevoir 2030 : <br><span>Réduire l’utilisation de plastique vierge dans les emballages de produits de marque de 50 %<sup>†</sup>.</span></h4>
                        <p class="GeorgiaFont">La réduction de l’utilisation de plastique est un sujet qui retient l’attention partout dans le monde. Nous avons exploré des solutions de rechange aux emballages de plastique pour nos produits de marque, afin de faire notre part pour réduire la quantité de déchets plastiques.</p>
                        <h3>Certification des produits</h3>
                        <p class="GeorgiaFont">En tant que plus grand fabricant de produits de papier en Amérique du Nord, nous croyons qu’il est de notre responsabilité de montrer l’exemple. Plusieurs de nos produits sont certifiés par les systèmes de certifications tiers les plus respectés et les plus crédibles de notre industrie. Aujourd’hui, nous offrons plus de 250 produits certifiés par des tiers, rassurant ainsi les clients et les consommateurs.</p>
                    </div>
                    <div class="col-6">
                        <p class="GeorgiaFont">Notre gamme de papier hygiénique, de papiers-mouchoirs et d'essuie-tout Bonterra<sup style='font-size:8px;'>MC</sup> est une offre de produits axée sur le développement durable qui utilise des matériaux provenant de sources responsables (les produits sont faits à 100 % de papier recyclé et sont dotés de la certification du Forest Stewardship Council<sup>®</sup> [FSC<sup>®</sup>]), des emballages sans plastique et une fabrication neutre en carbone. L'ensemble de la gamme de produits est également fabriqué au Canada.  </p>
                        <p class="GeorgiaFont">
                        De même, plusieurs de nos produits hors foyer sont certifiés ÉcoLogo de l’entreprise UL et/ou certifiés par le FSC<sup>®</sup> afin de garantir aux clients qu’ils choisissent des produits qui sont respectueux de l’environnement pour leurs établissements.

                        </p>
                        <h3>Innovation</h3>
                        <p class="GeorgiaFont">Notre quête incessante de la durabilité nous poussera à penser différemment, à aller plus loin et à trouver de nouvelles solutions. Grâce à l’innovation, à la recherche et au développement, ainsi qu’à des partenariats, nous continuerons de réduire notre empreinte environnementale et d’améliorer nos performances en matière de durabilité dans l’ensemble de notre chaîne de valeur : les ressources naturelles que nous utilisons, nos processus de fabrication, nos emballages de produits, le transport et la logistique.</p>
                        <p class="spcal-note"><sup>†</sup> Basé sur l’intensité c. le point de référence de 2020</p>
                    </div>
                </div>
            </div>
            <div class="pillarBox plrtwo">
                <div class="pilr-titl two">
                    <h2>Pilier 2 : Conscience de la planète </h2>
                </div>
                <div class="row">
                    <div class="col-12 txtCenter">
                        <img class="factoryImg" src="images/factory-img.jpg" alt="Factory image">
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <p>Nous encourageons d’autres personnes à rejoindre notre quête de laisser cette planète en meilleure santé que lorsque nous l’avons trouvée. C’est la seule planète que nous avons et c’est de notre devoir de la protéger.</p>
                        <p>Le monde qui nous entoure évolue rapidement; nous reconnaissons la nécessité de mener une réflexion plus audacieuse, afin que nous ayons un impact positif sur un plus grand nombre de personnes et sur la planète. Dans le cadre de Reconcevoir 2030, nous explorons comment ancrer la durabilité encore plus profondément dans nos opérations et dans notre chaîne de valeur, dans les domaines où nous pouvons avoir le plus d’impact, c’est-à-dire la lutte contre le changement climatique, la protection des ressources en eau et la réduction des déchets, et ce, en adoptant les principes d’économie circulaire dans la fabrication.</p>
                        <h3>Changement climatique</h3>
                        <h4>Cible de Reconcevoir 2030 : <br><span>Réduire nos émissions de gaz à effet de serre de 25 %<sup>*</sup>.</span></h4>
                        <p class="GeorgiaFont">La lutte contre le changement climatique est une nécessité mondiale et un impératif stratégique pour notre entreprise. Nous identifions et gérons de manière proactive les risques et les occasions liés au changement climatique sur nos activités.</p>
                        <p class="GeorgiaFont">Dans le cadre de nos activités de fabrication, nous concentrons nos efforts d’atténuation du changement climatique sur l’amélioration de l’efficacité énergétique, le déploiement de technologies d’atténuation des GES et l’utilisation d’énergie renouvelable. Nous investissons régulièrement dans un portefeuille de projets en capital et dans les nouvelles technologies pour réduire notre empreinte environnementale.</p>
                    </div>
                    <div class="col-6">
                        <p class="GeorgiaFont">Le transport de nos produits de nos installations de fabrication au Canada et aux É.-U. vers nos clients finaux est essentiel à nos activités et nous sommes conscients que le transport contribue pour beaucoup aux émissions de GES. Nous nous dirigeons vers un système de transport durable à faible émission de carbone, en améliorant l’efficacité de notre réseau de transport. </p>
                        <p class="spcal-note"><sup>*</sup> Basé sur l’intensité de la Portée 1 et de la Portée 2 des émissions du point de référence de 2009.</p>
                        <h3 class="mrgTop0">Eau</h3>
                        <h4>Cible de Reconcevoir 2030 : <br><span>Réduire notre consommation d’eau de 50 %<sup>^</sup>.</span></h4>
                        <p class="GeorgiaFont">Les ressources en eau durables sont essentielles pour notre entreprise et nous reconnaissons la nécessité d’une gestion rigoureuse de l’eau. La pression sur les ressources en eau augmente dans de nombreuses régions à travers le monde; il existe également des préoccupations croissantes quant à l’insécurité et au stress hydrique. Bien qu’aucune de nos installations de fabrication ne soit située dans une région où l’eau est limitée, nous nous engageons à nous assurer que les ressources en eau à proximité de nos installations demeurent des écosystèmes durables et sains.</p>
                        <p class="spcal-note"><sup>^</sup> Basé sur l’intensité c. le point de référence de 2009.</p>
                        <h3>Déchets</h3>
                        <p class="GeorgiaFont">Nous adoptons une approche circulaire de la gestion des déchets. Cela améliore l’efficacité de l’ensemble de nos opérations et contribue à la réduction de notre empreinte environnementale. Nous utilisons les ressources au maximum et réutilisons les matériaux afin de minimiser l’envoi de déchets dans les sites d’enfouissement. Nous axons également nos efforts sur la réduction des déchets, c’est pourquoi nous nous efforçons de travailler avec des fournisseurs et des partenaires qui partagent notre engagement envers le développement durable.</p>
                    </div>
                </div>
            </div>
            <div class="pillarBox plrthree">
                <div class="pilr-titl three">
                    <h2>Pilier 3 : Impact pour les employés</h2>
                </div>
                <div class="row">
                    <div class="col-8 pilrthree-cntnt-box">
                        <p>Nous cultivons nos pratiques sur le lieu de travail pour que chaque employé se sente inspiré et encore plus encouragé à atteindre ses objectifs et à avoir l’impact désiré.</p>
                        <p class="GeorgiaFont">Nous voulons nous assurer que Produits Kruger est un endroit où chacun peut donner le meilleur de soi-même et révéler son plein potentiel. Notre personnel est notre plus grande force; nous avons concentré nos efforts à impacter positivement notre culture pour la rendre encore plus progressiste et axée sur les objectifs. Nous continuons à répondre aux besoins changeants de nos employés, en célébrant nos différences et en devenant plus forts tous ensemble.</p>
                        <p class="GeorgiaFont">Nos employés nourrissent nos efforts d’innovation qui sont le moteur de notre croissance commerciale et de nos progrès en matière de développement durable. Notre environnement de travail permet à notre personnel d’atteindre son plein potentiel, tout en encourageant la pensée créative et en célébrant la diversité des idées.</p>
                        <h3>Santé et sécurité</h3>
                        <p class="GeorgiaFont">Offrir à tous nos employés l’environnement de travail le plus sûr possible est dans notre ADN. Nous avons une stratégie à l’échelle de l’entreprise qui nous aide à maintenir des normes élevées en matière de santé et de sécurité, et chacune de nos installations travaille fort pour garantir un environnement de travail sécuritaire.</p>
                    </div>
                    <div class="col-4 txtCenter pilrthree-img-box">
                        <img class="hatgirl" src="images/hard-hat-girl-factory.png" alt="Hard hat girl" />
                    </div>
                </div>
                <div class="row mrgTop50 diversityBox">
                    <div class="col-4 txtCenter">
                        <img class="solarpanelgirl" src="images/solar-panel-girl.png" alt="solar panel girl" />
                    </div>
                    <div class="col-8">
                        <h3 class="mrgTop0">Diversité, équité et inclusion</h3>
                        <p class="GeorgiaFont">Notre déclaration de Diversité, d’équité et d’inclusion (DEI), « Nous voyons l’excellence en vous : vos différences nous rendent plus forts » démontre notre engagement à garantir que nos équipes sont diversifiées et sont le reflet des communautés, des clients et des consommateurs que nous avons le privilège de servir. Ce sont nos collègues qui, chaque jour, donnent vie à cette vision, pas seulement par nos actions, mais aussi par notre façon de faire les choses.</p>
                        <p class="GeorgiaFont">La diversité de pensée, d’expériences et de points de vue que nos employés apportent contribue à la haute performance de nos équipes. Nous sommes persuadés que la diversité des compétences, des formations, des perspectives et des talents qui compose notre force de travail fait de notre entreprise un lieu de travail incroyable. </p>
                        <h3>Formation et développement</h3>
                        <p class="GeorgiaFont">Nous sommes investis dans la croissance et le développement de nos employés. Nous avons élaboré un Cadre de gestion des talents qui définit nos priorités et les programmes que nous proposons pour aider nos employés. Nous accordons priorité au développement des employés. Nos programmes d’apprentissage et nos offres de développement font le lien entre ce qui motive personnellement chaque membre de l’équipe et ce sur quoi il doit travailler, et ce dont l’entreprise a besoin en termes de compétences ou de capacités. </p>
                        <p class="GeorgiaFont">Alors que nous continuons de croître, nous poursuivrons nos efforts pour améliorer et faire évoluer les nombreuses initiatives d’aide aux employés pour renforcer notre personnel et nos équipes. Notre avenir commun est rempli de possibilités stimulantes.</p>
                    </div>
                </div>
            </div>
            <div class="pillarBox plrfour">
                <div class="pilr-titl four">
                    <h2>Pilier 4 : Implication dans nos communautés </h2>
                </div>
                <div class="row">
                    <div class="col-7 pilrfour-cntnt-box">
                        <p>Nous croyons que nous devrions toujours faire une différence dans la vie de nos employés, de nos clients et des membres de nos communautés. Nous investissons du temps dans des activités de renforcement communautaire. Ainsi, nous contribuons à la bonne santé, à la prospérité et au bien-être social de nos voisins et partenaires partout au Canada et aux États-Unis.</p>
                        <h3>Investissements communautaires</h3>
                        <p class="GeorgiaFont">Nous nous rallions aux efforts communautaires et philanthropiques qui sont importants pour nos consommateurs, nos employés et nos clients, en particulier dans les domaines de la recherche contre le cancer, de la santé des enfants, de la protection de l’environnement et du sport féminin. Nos investissements communautaires sont ciblés de façon stratégique et nous tenons à développer des relations à long terme avec les organisations et les initiatives que nous soutenons.</p>

                        <p class="GeorgiaFont">Nous nous sommes associés à One Tree Planted pour planter plus de 100 000 arbres au cours des trois prochaines années; en collaboration avec
                          les associations de hockey mineur à travers le Canada, nous avons fourni une aide financière de plus de 400 000 $ aux familles de joueurs de hockey dans le besoin; et par le biais du tournoi des cœurs Scotties, nous soutenons le curling féminin au Canada depuis plus de 40 ans.
                        </p>
                        <p class="GeorgiaFont">Nous sommes les partenaires et commanditaires nationaux à long terme de : </p>
                        <p class="GeorgiaFont">
                          <a class="communityEmbraceLink" href="https://krugerproducts.ca/community" target="_blank" aria-label="Communities link">
                            La Société canadienne du cancer, soutenant la lutte contre le cancer du sein <br>
                            Les Manoirs Ronald McDonald du Canada <br>
                            Commanditaire officiel en matière de produits de papier de l’Omnium Banque Nationale présenté par Rogers <br>
                            Partenaire officiel de la LNH en matière de produits de papier
                          </a>
                        </p>

                    </div>
                    <div class="col-5 communityrinkimg">
                        <img class="mb4" src="images/community-rink-guy.png" alt="Community rink guy" />
                    </div>
                </div>
                <div class="row mrgTop50 EmpVolunteer">
                    <div class="col-6">
                        <h3 class="mrgTop0">Bénévolat des employés</h3>
                        <p class="GeorgiaFont">Nous encourageons nos employés à avoir un impact positif en effectuant du bénévolat dans nos communautés et de donner leur temps à des œuvres caritatives ou à des causes qui les passionnent. Nos employés ont participé à des activités telles que la préparation et le service de repas, la plantation d’arbres ou la distribution de produits à des œuvres caritatives locales. Nous avons aussi élargi les opportunités de bénévolat aux enfants de nos employés afin d’encourager la jeunesse et la nouvelle génération à donner en retour.</p>
                        <p class="GeorgiaFont">En s’investissant bénévolement dans des causes qui leur tiennent à cœur, nos nombreux établissements ont aussi un impact positif unique auprès des communautés locales. Plusieurs de nos installations disposent d’un Comité d’implication communautaire qui planifie des occasions pour nos employés de s’impliquer et de contribuer à nos communautés locales. Dans l’ensemble de notre entreprise, nous soutenons plus de 30 initiatives et organisations locales.</p>
                    </div>
                    <div class="col-6">
                        <h3 class="mrgTop0">Relations avec la communauté</h3>
                        <p class="GeorgiaFont">Diriger avec bienveillance est une des priorités dans le cadre de notre engagement auprès des communautés. Notre intention est d’aider les communautés à prendre conscience de leur potentiel. Il est important pour nous de cultiver des relations à long terme avec nos communautés. Notre Comité d’implication communautaire a la responsabilité d’organiser des initiatives caritatives tout au long de l’année et est entièrement dirigé par nos employés. En s’appuyant sur nos forces, notre portée et nos partenaires, nous espérons relever les défis sociaux et environnementaux les plus complexes auxquels nos communautés font face.</p>
                        <p class="GeorgiaFont">Pour optimiser nos efforts bénévoles et caritatifs, nous collaborons avec des organisations locales et des organisations à but non lucratif et mettons la priorité sur la construction de relations pertinentes. Grâce à ces relations, nous avons une vision précise des problèmes que rencontrent les communautés et des solutions de soutien déjà mises en place par les personnes qui comprennent le mieux les besoins locaux.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <hr class="newhr" />
    </div>
    <section class="legacy-leadership">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>Voici quelques-unes de nos avancées à ce jour :</h2>
                    <p>Nous avons complété deux programmes de développement durable de cinq ans - Développement 2015 et Développement durable 2020. Tout au long de ce cheminement de dix ans, nous avons tiré de nombreuses leçons, partagé nos connaissances et réalisé des progrès considérables. Certains des moments dont nous sommes les plus fiers comprennent :</p>
                    <div class="progressChart">
                        <div class="row row-cols-2">
                            <div class="col prgres58">
                                <div class="progressBox">
                                    <img class="progressimg" src="images/ProgressArrows/upArrow66PC-fr.svg" alt="Improved 66 Percentage" />
                                    <div class="progressbody">
                                        <p><b>Amélioration</b> de la santé et de la sécurité de 66 % (OSHA TIR)</p>
                                        <p class="vsYear">c. le point de référence de 2015</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col prgres15">
                                <div class="progressBox">
                                    <img class="progressimg" src="images/ProgressArrows/downArrow3PC-fr.svg"
                                        alt="Reduced 3 Percentage" />
                                    <div class="progressbody">
                                        <p><b>Réduction</b> de 3 % <br> de nos emballages<br> de produits de marque <br>en plastique vierge<br> (livres/caisses quota produites)</p>
                                        <p class="vsYear">c. le repère 2020 </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col prgres26">
                                <div class="progressBox">
                                    <img class="progressimg"  src="images/ProgressArrows/downArrow12PC-fr.svg"
                                        alt="Reduced 12 Percentage" />
                                    <div class="progressbody">
                                        <p><b>Réduction</b> de nos émissions de GES de 12 % (CO<sub style="font-size:75%;">2</sub>e/TMSM)</p>
                                        <p class="vsYear">c. le point de référence de 2009</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col prgres37">
                                <div class="progressBox">
                                    <img class="progressimg" src="images/ProgressArrows/downArrow39PC-fr.svg"
                                        alt="Reduced 39 Percentage" />
                                    <div class="progressbody">
                                        <p><b>Réduction</b> de notre consommation d’eau de 39 % (M<sup style="font-size:75%;">3</sup>/TMSM)</p>
                                        <p class="vsYear">c. le point de référence de 2009</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col prgresfsc">
                                <div class="progressBox">
                                    <img class="progressimg" src="images/fsclogo.png" alt="FSC logo" />
                                    <div class="progressbody">
                                        <p><b>2011</b> - Premier fabricant canadien de produits de papiers à recevoir le certificat de la chaîne de traçabilité du Forest Stewardship Council<sup>&reg;</sup>. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col prgres100">
                                <div class="progressBox">
                                    <img class="progressimg" src="images/100percent.svg" alt="Reduced 15 Percentage" />
                                    <div class="progressbody">
                                        <p><b>100 %</b> de nos fibres sont certifiées par une tierce partie</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <!--    <h3>Coup d’œil sur les progrès en matière de développement durable</h3>
                    <p class="GeorgiaFont georFont20">Le graphique indique notre empreinte en 2009 à titre de point de référence (rouge) dans trois domaines environnementaux mesurés dans le cadre de l’initiative Développement durable 2020. Nos résultats de 2020 (en vert) démontrent nos progrès : L’intensité de consommation d’énergie a diminué de 15 %, passant de 21,72 à 18,48 GJ/TMSM; l’intensité de la consommation d’eau a diminué de 37 %, passant de 76 à 48 m<sup style="font-size:75%;">3</sup>/TMSM; et l’intensité des émissions de gaz à effet de serre a diminué de 26 %, passant de 783 à 580 kg CO<sub style="font-size:75%;">2</sub>e/TMSM.</p> -->
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <hr class="newhr" />
    </div>
    <section class="policies-section">
        <div class="container">
            <h2>Politiques</h2>
            <p>Les politiques suivantes servent de lignes directrices pour nos actions de développement durable en interne, mais également avec les partenaires de notre chaîne de valeur. </p>
            <div class="row">
                <div class="col plcyone">
                    <div class="policiesBox mb4">
                        <h6>Code d’éthique et de conduite professionnelle</h6>
                        <a class="downlaod-pdf" href="pdfs/Code_ethique_PKI_2023.pdf"
                        target="_blank" aria-label="code of business - opens in a new tab">TÉLÉCHARGER LE PDF</a>
                    </div>
                </div>
                <div class="col plcytwo">
                    <div class="policiesBox mb4">
                        <h6>Politique environnementale</h6>
                        <a class="downlaod-pdf" href="pdfs/Produits_Kruger_Politique_Environnementale_juin2023_FR_v1.pdf"
                        target="_blank" aria-label="Environmental policy - opens in a new tab">TÉLÉCHARGER LE PDF</a>
                    </div>
                </div>
                <div class="col plcythree">
                    <div class="policiesBox mb4">
                        <h6>Politique d’approvisionnement en matière ligneuse</h6>
                        <a class="downlaod-pdf" href="pdfs/Produits_Kruger_Politique_Matiere_ligneuse_juin2023_FR_v1.pdf"
                        target="_blank" aria-label="Fibre procurement policy - opens in a new tab">TÉLÉCHARGER LE PDF</a>
                    </div>
                </div>
                <div class="col plcyfour">
                    <div class="policiesBox mb4">
                        <h6>Code de conduite<br>des fournisseurs</h6>
                        <a class="downlaod-pdf" href="pdfs/Code_de_conduite_des_fournisseurs_Produits_Kruger_Inc_Juillet_2023_FR.pdf" target="_blank" aria-label="Supplier sustainability policy - opens in a new tab">TÉLÉCHARGER LE PDF</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <hr class="newhr" />
    </div>
    <section class="Reports-section">
        <div class="container">
            <h2>Rapports et ressources</h2>
            <p>Nous établissons régulièrement un rapport de progression vers la réalisation de nos objectifs de développement durable. Cette présentation transparente de rapports nous oblige à prendre nos responsabilités et permet aux principaux intervenants de surveiller nos progrès.</p>
            <div class="row">
                <div class="col rprtone mb4">
                    <div class="reportsBox">
                        <h6>Rapport de 2022</h6>
                        <a class="downlaod-pdf" href="pdfs/kruger-2022-sustainability-report-fr.pdf" target="_blank" aria-label="2022 report - opens in a new tab">TÉLÉCHARGER LE PDF</a>
                    </div>
                </div>
                <div class="col rpr2021 mb4">
                    <div class="reportsBox">
                        <h6>Rapport de 2021</h6>
                        <a class="downlaod-pdf" href="pdfs/Reportof2021_FR.pdf" aria-label="2021 report - opens in a new tab"
                        target="_blank">TÉLÉCHARGER LE PDF</a>
                    </div>
                </div>
                <div class="col rprttwo mb4">
                    <div class="reportsBox">
                        <h6>Rapport de 2020</h6>
                        <a class="downlaod-pdf" href="pdfs/Reportof2020_FR.pdf" aria-label="2020 report - opens in a new tab"
                        target="_blank">TÉLÉCHARGER LE PDF</a>
                    </div>
                </div>
                <div class="col rprtthree mb4">
                    <div class="reportsBox">
                        <h6>Assurance<br> des GES</h6>
                        <a class="downlaod-pdf" href="pdfs/FR_Limited_GHG_Assurance_Report_Kruger_Products_FInal_2022.pdf"
                        target="_blank" aria-label="Assurance des GES de 2022 - opens in a new tab">TÉLÉCHARGER LE PDF</a>
                    </div>
                </div>
                <div class="col rprtfour mb4">
                    <div class="reportsBox">
                        <h6>Certificat FSC<sup>&reg;</sup></h6>
                        <a class="downlaod-pdf" href="pdfs/2023_FSC_FR.pdf" target="_blank" aria-label="Certificat FSC - opens in a new tab">TÉLÉCHARGER LE PDF</a>
                    </div>
                </div>
                <div class="col rprtfive mb4">
                    <div class="reportsBox">
                        <h6>Certificat SFI</h6>
                        <a class="downlaod-pdf" href="pdfs/2023_SFI_COC_FR.pdf" target="_blank" aria-label="Certificat SFI - opens in a new tab">TÉLÉCHARGER LE PDF</a>
                    </div>
                </div>
                <!-- <div class="col-2 reportsBox">
                    <p>UL EcoLogo product listing</p>
                    <a class="downlaod-pdf" href="" target="_blank">Download pdf</a>
                </div> -->
            </div>
        </div>
    </section>
</div>

<?php require('footer.php'); ?>
