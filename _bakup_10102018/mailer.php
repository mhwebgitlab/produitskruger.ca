<?php

if (isset($_POST['title'])){
  $post = $_POST;
  $email_title = strip_tags($_POST['title']);
  $name = strip_tags($_POST['name']);
  $lastname = strip_tags($_POST['last_name']);
  $email = strip_tags($_POST['email']);
  $product = strip_tags($_POST['product']);
  $address_1 = strip_tags($_POST['address_1']);
  $address_2 = strip_tags($_POST['address_2']);
  $city = strip_tags($_POST['city']);
  $province_teritory = strip_tags($_POST['province_teritory']);
  $postal_code = strip_tags($_POST['postal_code']);
  $phone = strip_tags($_POST['phone']);
  $remarks = strip_tags($_POST['remarks']);
  $upc = strip_tags($_POST['upc_code']);
  $ip = strip_tags($_SERVER['REMOTE_ADDR']);

  if('' != $upc) {
	$upc = '61328' . $upc;
  }

$body = "French - ".$product."
Title: ".$email_title."
First name: ".$name."
Last name: ".$lastname."
Address 1: ".$address_1."
Address 2: ".$address_2."
City: ".$city."
Province: ".$province_teritory."
Postal code: ".$postal_code."
Telephone: ".str_replace(' ', '-', $phone)."
Email: ".$email."
Product: ".$product."
UPC code: ".$upc."
Comments: ".$remarks."
IP Address: ".$ip;

	$response = $_POST["g-recaptcha-response"];
	$url = 'https://www.google.com/recaptcha/api/siteverify';
	$data = array(
		'secret' => '6Le8YykUAAAAANBx5ItIyW1Y7uGEA0ZKEDOfo0O8',
		'response' => $_POST["g-recaptcha-response"]
	);
	$options = array(
		'http' => array (
			'method' => 'POST',
			'content' => http_build_query($data)
		)
	);
	$context  = stream_context_create($options);
	$verify = file_get_contents($url, false, $context);
	$captcha_success = json_decode($verify);
	if ($captcha_success->success == false) {
		die('recapcha_error');
	}

  // require_once '../vendor/swiftmailer/swiftmailer/lib/swift_required.php';
  require_once 'vendor/swiftmailer/swiftmailer/lib/swift_required.php';

  // Create the mail transport configuration
  $transport = Swift_MailTransport::newInstance();

  // Create the message
  $message = Swift_Message::newInstance();

  $message->setTo(array(
    "consumerresponse@krugerproducts.ca" => "Consumer Response"));
  $message->setBcc(array(
    "kplp@propellerdigital.com" => "Propeller Digital"));

  $message->setSubject("French - ". $product);
  $message->setBody($body);
  $message->setFrom('kplp@propellerdigital.com', $name . ' ' . $lastname);

  // Send the email
  $mailer = Swift_Mailer::newInstance($transport);
  $mailer->send($message);
  die('sent');
} else {
  die('error');
}
