<?php
$title = 'Accessibility';
require('header.php');
?>
    <div class="accessibility">
      <div class="wrapper">
        <section class="text-content">
          <div class="container">
            <h1>Nous croyons à l’importance de l’accessibilité.</h1>
            <h2>L’accessibilité pour les personnes handicapées de l’Ontario</h2>
            <p>Produits Kruger s.e.c. vise l’excellence. Nous nous sommes engagés à fournir des biens, des services, de l’information, des communications et un milieu physique respectueux de la dignité et de l’autonomie des personnes handicapées.</p>
            <h2>Politique d’entreprise</h2>
            <p>Notre politique concernant les <a href="http://krugerproducts.ca/francais/pdf/CorporatePolicy_FRENCH_AODA_final.pdf" target="_blank">Normes d’accessibilité pour les personnes handicapées de l’Ontario</a> confirme l’engagement de Produits Kruger à respecter les besoins des personnes handicapées et à se conformer aux exigences de la <a href="http://www.e-laws.gov.on.ca/html/statutes/english/elaws_statutes_05a11_e.htm" target="_blank">Loi de 2005 sur l’accessibilité pour les personnes handicapées de l’Ontario (LAPHO)</a> en assurant la prévention et l’élimination des obstacles à l’accessibilité.</p>
            <p><a href="http://krugerproducts.ca/francais/pdf/Customer Service Policy_FR.pdf" target="_blank">La Politique sur les services à la clientèle de Produits Kruger</a> a pour but d’assurer notre conformité aux <a href="http://www.e-laws.gov.on.ca/html/source/regs/english/2007/elaws_src_regs_r07429_e.htm" target="_blank">Normes d’accessibilité pour les services à la clientèle selon le Règlement de l’Ontario 429/07</a>.</p>
            <p>Cette information est disponible sous d’autres formes sur demande.</p>
            <h2>Plan pluriannuel 2014-2018</h2>
            <p><a href="http://krugerproducts.ca/francais/pdf/AODA_Accessibility_Plan 2014-2018_FR.pdf" target="_blank">Le plan pluriannuel de Produits Kruger</a> expose les stratégies de la société en vue de répondre aux exigences du <a href="http://www.e-laws.gov.on.ca/html/regs/english/elaws_regs_110191_e.htm" target="_blank">Règlement sur les normes d’accessibilité intégrées</a> qui établit les normes d’accessibilité pour les quatre secteurs que sont l’information et les communications, l’emploi et le transport.</p>
            <h2>Contactez-nous</h2>
            <p>Nous serons attentifs à vos commentaires. Voici nos coordonnées :</p>
            <h3>En ligne</h3>
            <p><a href="contact.php">Formulaire de rétroaction sur l’accessibilité</a></p>
            <h3>Par courriel</h3>
            <p><a href="mailto:accessibilityfeedback@krugerproducts.ca">accessibilityfeedback@krugerproducts.ca</a></p>
            <h3>Par la poste</h3>
            <p>Rétroaction LAPHO<br>
            Service des Ressources humaines<br>
            Produits Kruger s.e.c.<br>
            1900 Minnesota Court<br>
            Bureau 200<br>
            Mississauga (Ontario)<br>
            L5N 5R5<br>
            </p>
            <h2>Avis de non-responsabilité</h2>
            <p>Les renseignements personnels fournis dans le cadre des services d’accessibilité sont recueillis conformément à la <a href="http://www.e-laws.gov.on.ca/html/statutes/english/elaws_statutes_05a11_e.htm" target="_blank">Loi de 2005 sur l'accessibilité pour les personnes handicapées de l'Ontario, au Règlement de l'Ontario 191/11</a> ainsi qu'au <a href="http://www.e-laws.gov.on.ca/html/source/regs/english/2007/elaws_src_regs_r07429_e.htm" target="_blank">Règlement de l'Ontario 429/07</a>. Ils seront utilisés pour répondre aux rétroactions sur les services d’accessibilité pour les personnes handicapées. Toute question relative à cette collecte d’informations peut être soumise au Coordonnateur de l’accessibilité au moyen de l’une des méthodes ci-dessus.</p>
            <p>
            Consultez le 
            <a href="http://www.mcss.gov.on.ca/fr/mcss/programs/accessibility/index.aspx">http://www.mcss.gov.on.ca/fr/mcss/programs/accessibility/index.aspx</a> pour plus d’informations sur l’accessibilité.</p>
        	</div>
        </section>
      </div>
    </div>

<?php require('footer.php'); ?>
