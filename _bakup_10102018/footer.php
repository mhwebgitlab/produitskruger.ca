</div>
<!-- end of wrapper -->

<footer>
  <div class="wrapper">
    <nav class="sub">
      <ul>
        <li><a target="_blank" href="./privacy.php">Politique de confidentialité</a></li>
        <li><a target="_blank" href="./trademark.php">Avis sur les marques de commerce</a></li>
        <li><a target="_blank" href="./legal.php">Avis juridique</a></li>
        <li><a target="_blank" href="./accessibility.php">Accessibilité</a></li>
        <li class="photo-credits"><a target="_blank" href="#" data-featherlight="#lightbox-photo-credits">Crédits photo</a></li>
      </ul>
    </nav>
    <nav class="sub">
      <ul>
        <li><a target="_blank" href="http://kptissueinc.com/_fr/">Investisseurs</a></li>
        <li><a target="_blank" href="http://kptissueinc.com/_fr/investors/gov.php">Gouvernance</a></li>
        <li><a target="_blank" href="http://www.kruger.com/fr/">Kruger Inc.</a></li>
        <li><a target="_blank" href="http://afh.krugerproducts.ca/home.aspx?lang=fr-CA&">Produits hors foyer</a></li>
        <li><a target="_blank" href="/sign-up.php">Promotions/alertes électroniques</a></li>
      </ul>
    </nav>
    <div class="links">

      <a href="#" class="proudly-canadian" onclick="return false;">
        <img src="images/quebec.png" alt="Un succès fabriqué au Québec">
      </a>

      <a target="_blank" href="http://stoh.ca/francais/index.php">
        <img src="images/STOH-logo.png" alt="Scotties">
      </a>

<!--       <a target="_blank" href="http://www.cbcf.org"><img src="images/canadian_breast_cancer_logo.png" alt="Canadian Breast Cancer"></a> -->

      <a target="_blank" href="https://ca.fsc.org/fr-ca">
        <img style="max-height: 90px;" src="images/FSC_Promo_for_Website_FR.png" alt="FSC Promo">
      </a>
    </div>
    <div class="clearfix">
    </div>
  </div>
</footer>

<div style="display: none;">
  <div id="lightbox-photo-credits">
    <h4>Crédits photo</h4>
    <p>Images de la collection Blanc Cashmere par Caitlin Cronenberg</p>
  </div>
</div>

<div class="showbox animated">
  <!-- <div class="loader">
    <svg class="circular" viewBox="25 25 50 50">
      <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
  </div> -->
</div>

<script src="js/all.js?0"></script>

</body>
</html>
