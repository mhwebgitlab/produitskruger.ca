<?php
$title = 'Sign Up';
require('header.php');
?>

    <div class="signup">
      <section class="signup__intro">
        <div class="intro-wrap">
          <h1>Restons <br>en contact.</h1>
        </div>
      </section>
      <div class="wrapper">
        <section class="home__welcome home__welcome_2 text-in">
          <div class="container clearfix2">
            <div class="title clearfix2">
              <h4>Abonnez-vous pour courir la chance de GAGNER* une provision de un an en produits Kruger !</h4>
            </div>
            <div class="image">
              <img src="images/packs_reflection.jpg" alt="Kruger product lineup">
            </div>
            <div class="copy clearfix2">
              <p>Nous ne voulons pas vous voir rater quoi que ce soit! Abonnez-vous à notre infolettre afin de vous tenir au courant de promotions et d’autres nouvelles. Ce faisant, vous serez inscrit(e) pour gagner* une provision de un an en papier hygiénique Cashmere<sup>®</sup>, en papiers-mouchoirs Scotties<sup>®</sup><sup style="font-size: 5px;">’</sup>, en essuie-tout SpongeTowels<sup>®</sup> et en papier hygiénique Purex<sup>®</sup> !</p>
            </div>
          </div>
        </section>
      </div>
      <section class="signup__form">
        <div class="container">
          <div class="iframe-wrap">
            <iframe src="http://staging.kruger.dcmforms.com/signup/fr" width="1280" height="480"></iframe>
          </div>
        </div>
        <div class="legal">
          <span>Produits Kruger s.e.c. (papier hygiénique Cashmere<sup>®</sup>, papier hygiénique Purex<sup>®</sup>, essuie-tout SpongeTowels<sup>®</sup> et papiers-mouchoirs Scotties<sup>®</sup><sup style="font-size: 5px;">’</sup>)  1900, Minnesota Court, bureau 200, Mississauga (Ontario) http://www.krugerproducts.ca 888-620-1212</span>
        </div>
      </section>
    </div>

<?php require('footer.php'); ?>

