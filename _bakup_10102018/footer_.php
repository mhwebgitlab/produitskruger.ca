</div>
<!-- end of wrapper -->

<footer>
  <div class="wrapper">
    <nav class="sub">
      <ul>
        <li><a target="_blank" href="./privacy.php">Privacy Policy</a></li>
        <li><a target="_blank" href="./trademark.php">Trademark Notice</a></li>
        <li><a target="_blank" href="./legal.php">Legal Notice</a></li>
        <li><a target="_blank" href="./accessibility.php">Accessibility</a></li>
        <li class="photo-credits"><a target="_blank" href="#" data-featherlight="#lightbox-photo-credits">Photo credits</a></li>
      </ul>
    </nav>
    <nav class="sub">
      <ul>
        <li><a target="_blank" href="http://www.kptissueinc.com">Investors</a></li>
        <li><a target="_blank" href="http://kptissueinc.com/investors/gov.php">Governance</a></li>
        <li><a target="_blank" href="http://www.kruger.com/en/">Kruger Inc.</a></li>
        <li><a target="_blank" href="http://afh.krugerproducts.ca/home.aspx?lang=en-CA&">Away From Home</a></li>
        <li><a target="_blank" href="http://scotties.us7.list-manage.com/subscribe/post?u=c9a10a8ac0d4087992f697a5a&id=13ebe15349">Promotions/Email Alerts</a></li>
      </ul>
    </nav>
    <div class="links">

      <a href="#" class="proudly-canadian" onclick="return false;">
        <img src="images/logo_proudly_canadian.png" alt="Proudly Canadian">
      </a>

      <a target="_blank" href="http://stoh.ca">
        <img src="images/STOH-logo.png" alt="Scotties">
      </a>

<!--       <a target="_blank" href="http://www.cbcf.org"><img src="images/canadian_breast_cancer_logo.png" alt="Canadian Breast Cancer"></a> -->

      <a target="_blank" href="https://ca.fsc.org/en-ca">
        <img style="max-height: 90px;" src="images/fsc_promo.png" alt="FSC Promo">
      </a>
    </div>
    <div class="clearfix">
    </div>
  </div>
</footer>

<div style="display: none;">
  <div id="lightbox-photo-credits">
    <h4>Photo Credits</h4>
    <p>White Cashmere Collection images by Caitlin Cronenberg</p>
  </div>
</div>

<div class="showbox animated">
  <!-- <div class="loader">
    <svg class="circular" viewBox="25 25 50 50">
      <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
  </div> -->
</div>

<script src="js/all.js"></script>

</body>
</html>
