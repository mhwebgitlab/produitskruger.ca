<?php
$title = 'About Us';
require('header.php');
?>
    <div class="about">
      <section class="about__story">
        <div class="about__story-inner" data-section="Page Header">
          <h1>Our Story</h1>
          <div class="about__slider swiper-container state-0">
            <div class="swiper-wrapper">
              <div class="about__slide swiper-slide slide">
                <div class="container">

                  <div class="img-block" style="background-image:url(images/row_1_1.jpg)">
                  </div>
                  <div class="img-block position" style="background-image:url(images/row_1_2.jpg)">
                  </div>
                  <div class="img-block" style="background-image:url(images/row_1_3.jpg)">
                  </div>
                  <div class="img-block" style="background-image:url(images/row_1_4.jpg)">
                  </div>

                  <div class="img-block" style="background-image:url(images/row_2_1.jpg)">
                  </div>
                  <div class="img-block position" style="background-image:url(images/about_sponge2.jpg)">
                  </div>
                  <div class="img-block" style="background-image:url(images/row_2_3.jpg)">
                  </div>
                  <div class="img-block" style="background-image:url(images/row_2_4.jpg)">
                  </div>

                  <div class="img-block" style="background-image:url(images/row_3_1.jpg)">
                  </div>
                  <div class="img-block" style="background-image:url(images/row_3_2.jpg)">
                  </div>
                  <div class="img-block position" style="background-image:url(images/row_3_3.jpg)">
                  </div>
                  <div class="img-block" style="background-image:url(images/4up_dudeswalking.jpg)">
                  </div>

                  <div class="img-block" style="background-image:url(images/row_4_1.jpg)">
                  </div>
                  <div class="img-block" style="background-image:url(images/row_4_2.jpg)">
                  </div>
                  <div class="img-block" style="background-image:url(images/row_4_3.jpg)">
                  </div>
                  <div class="img-block" style="background-image:url(images/row_4_4.jpg)">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="home__welcome text-in" data-section="Overview Text">
          <div class="container">
            <div class="pseudo-title">OVERVIEW</div>
            <h4>We manufacture<br/> Canada’s most popular<br/> tissue brands</h4>
            <p>Great things can happen when people work passionately towards a common goal. That’s how Kruger Products grew to become Canada’s leading manufacturer of quality tissue products for household, industrial, and commercial use. We are proud to offer a portfolio of Canada's favourite tissue brand for every room in your home.</p>
          </div>
        </div>
      </section>
      <div class="wrapper">
        <section class="about__names">
          <!-- <div class="col">
            <div class="container">
              <div class="pseudo-title">CONSUMER PRODUCTS – CANADA</div>
              <h4>A family of Canadian household names</h4>
              <p>We offer a portfolio of Canada's favourite tissue brand for every room in your home.</p>
            </div>
          </div> -->
          <div class="col full-height">
            <div class="row wipe-in down" data-section="Product Grid">
              <div class="bg" style="background-image: url(images/woman_background.jpg)"></div>
              <div class="info">
                <!-- <a href="http://www.cashmere.ca/" target="_blank"> -->
                  <img src="images/names_bg_2_opt.png" alt="A package of Cashmere Bathroom Tissue">
                  <h6>Canada’s #1 Bathroom Tissue Brand</h6>
                <!-- </a> -->
                <div class="links">

                  <a href="https://www.facebook.com/Cashmere/" target="_blank">Visit Us On Facebook</a>
                  <a href="http://www.cashmere.ca/" target="_blank">Visit cashmere.ca</a>
                </div>
              </div>
            </div>
            <div class="row wipe-in down">
              <div class="bg" style="background-image: url(images/names_bg_2.jpg)"></div>
              <div class="info">
                <!-- <a href="http://www.spongetowels.ca/" target="_blank"> -->
                  <img src="images/names_bg_1_opt.png" alt="A package of Sponge Towels Paper Towels">
                  <h6>Canada's Favourite Paper Towel Brands</h6>
                <!-- </a> -->
                <div class="links">
                  <a href="https://www.facebook.com/SpongeTowels/" target="_blank">Visit Us On Facebook</a>
                  <a href="http://www.spongetowels.ca/" target="_blank">Visit spongetowels.ca</a>
                </div>
              </div>
            </div>
            <div class="row wipe-in down">
              <div class="bg" style="background-image: url(images/names_bg_4.jpg)"></div>
              <div class="info">
                <!-- <a href="http://www.scotties.ca" target="_blank"> -->
                  <img src="images/names_bg_4_opt.png" alt="A package of Scottie's Facial Tissue">
                  <h6>Canada’s #1 Facial Tissue Brand</h6>
                <!-- </a> -->
                <div class="links">
                  <a href="https://www.facebook.com/ScottiesTissue/" target="_blank">Visit Us On Facebook</a>
                  <a href="http://www.scotties.ca" target="_blank">Visit scotties.ca</a>
                </div>
              </div>
            </div>
            <div class="row wipe-in down">
              <div class="bg" style="background-image: url(images/names_bg_3.jpg)"></div>
              <div class="info">
                <!-- <a href="http://purex.ca/" target="_blank"> -->
                  <img src="images/names_bg_3.png" alt="A package of Purex Bathroom Tissue">
                  <h6>Western Canada’s #1 Bathroom Tissue Brand</h6>
                <!-- </a> -->
                <div class="links">
                  <a href="https://www.facebook.com/PurexCanada/" target="_blank">Visit Us On Facebook</a>
                  <a href="http://purex.ca/" target="_blank">Visit purex.ca</a>
                </div>
              </div>
            </div>
            <div class="row wipe-in down">
              <div class="bg" style="background-image: url(images/names_bg_6.jpg)"></div>
              <div class="info">
                <!-- <a href="http://www.cashmere.ca/" target="_blank"> -->
                  <img src="images/names_bg_5.png" alt="A package of Environmental branded Cashmere bathroom tissue">
                  <h6>Canada’s first premium tissue<br> products made from 100% recycled paper</h6>
                <!-- </a> -->
                <!-- <div class="links">
                  <a href="https://www.facebook.com/Cashmere/" target="_blank">Visit Us On Facebook</a>
                  <a href="http://www.cashmere.ca/" target="_blank">Visit cashmere.ca</a>
                </div> -->
              </div>
            </div>
            <div class="row wipe-in down">
              <div class="bg" style="background-image: url(images/names_bg_5.jpg)"></div>
              <div class="info">
                <!-- <a href="http://www.whiteswan.ca/" target="_blank"> -->
                  <img src="images/names_bg_6.png" alt="A package of White Swan paper napkin">
                  <h6>Canada’s #1 Paper Napkin Brand</h6>
                  <div class="links">
                <!-- </a> -->
                    <!-- <a href="https://www.facebook.com/Cashmere/" target="_blank">Visit Us On Facebook</a> -->
                    <a href="http://www.whiteswan.ca/" target="_blank">Visit whiteswan.ca</a>
                  </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="clearfix2"></div>
        </section>
        <section class="about__consumer full-height wipe-in" data-section="White Cloud">
          <div class="container">
            <div>
              <div class="white-cloud">
                <img src="images/white_cloud.png" alt="A mother and child branded on White Cloud Bathroom Tissue">
              </div>
              <div class="text">
                <div class="pseudo-title vis">CONSUMER PRODUCTS – UNITED STATES AND MEXICO</div>
                <!-- <h4>Ranked #1 bath tissue in America by a leading consumer ratings publication</h4> -->
                <h4>White Cloud<sup>®</sup> ranked #1 bath tissue in America by a leading consumer ratings publication</h4>
                <p>Our White Cloud<sup>®</sup> brand has a new look and was recently recognized as the best bathroom tissue in the United States! It’s one of the reasons White Cloud<sup>®</sup> is now available in over 12,000 stores across America, and growing.</p>
                <p>The White Cloud<sup>®</sup> family of products, including bathroom tissue, facial tissue and paper towels, is manufactured primarily at our Memphis, TN plant, which also produces a wide range of private label tissue products for major retailers across North America.</p>
                <p>Learn more at <a href="http://mywhitecloud.com" target="_blank">mywhitecloud.com</a></p>
              </div>
              <div class="clearfix2"></div>
            </div>
          </div>
        </section>
        <section class="about__dark full-height wipe-in left" data-section="Commercial Tissue">
          <div class="container">
            <div>
              <div class="pseudo-title vis">AWAY FROM HOME DIVISION</div>
              <h4>Canada’s leading commercial tissue manufacturer</h4>
              <p>Our Away From Home division offers high-quality, cost-effective products — bathroom tissue, facial tissue, paper towels, paper napkins, wipers, hand care, and dispensing systems — for use in the food service, property management, healthcare, manufacturing, education, and lodging segments across North America.</p>
              <p>Learn more at <a href="http://afh.krugerproducts.ca" target="_blank">afh.krugerproducts.ca</a></p>
            </div>
          </div>
        </section>
        <section class="about__ceo full-height wipe-in" data-section="Mario Gosselin Quote">
          <div class="vertical-align">
            <div class="container">
              <img src="images/man-portrait.jpg" alt="Mario Gosselin, CEO">
              <div class="quotes">
                <h3><i>“</i>At the core of our success is the skill and dedication of our people.<i>”</i></h3>
                <div class="pseudo-title">Mario Gosselin, CEO</div>
              </div>
              <div class="clearfix2"></div>
            </div>
          </div>
        </section>
        <section class="about__locations">

          <div class="map">

          </div>

          <div class="container">
            <div class="text text-in" data-section="Locations Map">
              <div class="pseudo-title">our locations</div>
              <h4>Find us across<br/> North America</h4>
              <p>Our locations are strategically aligned to the natural resources we use to make our products, and to key population centres that represent 90% of Canadian and 60% of U.S. consumers.</p>
              <p>With eight manufacturing plants, our total papermaking capacity is just over one-third of Canada’s total tissue manufacturing capacity. We are also proud to be the only tissue products company with manufacturing operations in Western Canada.</p>
            </div>

            <div class="mobile-map">

            </div>

            <div class="locations-wrap">
              <div class="location"><div>
                <div class="pseudo-title red">Papermaking & converting</div>
                <h5>New Westminster, British Columbia</h5>
                <a target="_blank" href="/pdfs/Plant-Fact_Sheets/KrugerFactSheetNewWestminster_EN.pdf" class="btn pseudo-title">FACT SHEET
                  <svg class="icon">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-3"></use>
                  </svg>
                </a>
              </div></div>
              <!-- <div class="location">
                <div class="pseudo-title red">Papermaking & converting</div>
                <h5>New Westminster, British Columbia</h5>
                <a target="_blank" href="javascript:void(0)" class="btn pseudo-title">FACT SHEET
                  <svg class="icon">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-3"></use>
                  </svg>
                </a>
              </div> -->
              <div class="location"><div>
                <div class="pseudo-title red">Papermaking & converting</div>
                <h5>Gatineau, Quebec</h5>
                <a target="_blank" href="/pdfs/Plant-Fact_Sheets/KrugerFactSheetGatineau_EN.pdf" class="btn pseudo-title">FACT SHEET
                  <svg class="icon">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-3"></use>
                  </svg>
                </a>
              </div></div>
              <div class="location"><div>
                <div class="pseudo-title red">Papermaking & converting</div>
                <h5>Crabtree, Quebec</h5>
                <a target="_blank" href="/pdfs/Plant-Fact_Sheets/KrugerFactSheetCrabtree_EN.pdf" class="btn pseudo-title">FACT SHEET
                  <svg class="icon">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-3"></use>
                  </svg>
                </a>
              </div></div>
              <div class="location"><div>
                <div class="pseudo-title red">Papermaking</div>
                <h5>Sherbrooke, Quebec</h5>
                <a target="_blank" href="/pdfs/Plant-Fact_Sheets/KrugerFactSheetSherbrooke_EN.pdf" class="btn pseudo-title">FACT SHEET
                  <svg class="icon">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-3"></use>
                  </svg>
                </a>
              </div></div>
              <div class="location"><div>
                <div class="pseudo-title red">Corporate Headquarters</div>
                <h5>Mississauga, Ontario</h5>
                <a target="_blank" href="javascript:void(0)" class="btn pseudo-title"></a>
              </div></div>
              <div class="location"><div>
                <div class="pseudo-title red">Converting</div>
                <h5>Scarborough, Ontario</h5>
                <a target="_blank" href="javascript:void(0)" class="btn pseudo-title"></a>
              </div></div>
              <div class="location"><div>
                <div class="pseudo-title red">Converting</div>
                <h5>Trenton, Ontario</h5>
                <a target="_blank" href="javascript:void(0)" class="btn pseudo-title"></a>
              </div></div>
              <div class="location"><div>
                <div class="pseudo-title red">Papermaking & converting</div>
                <h5>Memphis, Tennessee</h5>
                <a target="_blank" href="/pdfs/Plant-Fact_Sheets/KrugerFactSheetMemphis_EN.pdf" class="btn pseudo-title">FACT SHEET
                  <svg class="icon">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-3"></use>
                  </svg>
                </a>
              </div></div>
              <div class="clearfix2"></div>
            </div>
          </div>
        </section>
        <section class="about__focus wipe-in" data-section="What We Stand For">
          <img src="images/cone_opened.png" alt="pinecone">
          <div>
            <div class="container">
              <div class="text">
                <h2>What We<br> Stand For</h2>
                <div class="slogan">Our Focus: Everything we do revolves around our customers and consumers.</div>
              </div>
              <div class="col-c">
                <div class="col">
                  <h5>Courage</h5>
                  <p>Willing to take informed risk and undertake new projects to reach our ambitious goals, acting with humility, eagerness and intelligence.</p>
                </div>
                <div class="col">
                  <h5>Dedication</h5>
                  <p>Committed to our mission, our employees and our brands; developing superior value for our customers and shareholders.</p>
                </div>
                <div class="col">
                  <h5>Excellence</h5>
                  <p>To be the best in everything we do, exceeding customer and consumer expectations, while continuously improving our products and processes in a disciplined and safe environment.</p>
                </div>
                <div class="col">
                  <h5>Integrity</h5>
                  <p>Act with honesty and personal responsibility for our actions, practicing highest ethical standards.</p>
                </div>
                <div class="col">
                  <h5>Teamwork</h5>
                  <p>We embrace teamwork, value the diversity of our workforce and work together cooperatively and collectively to achieve ambitious common goals.</p>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </section>
        <section class="about__history full-height wipe-in left" data-section="Our Story">
          <div class="triangles"></div>
          <div class="slidePrev" title="Previous Slide"></div>
          <div class="slideNext" title="Next Slide"></div>
          <div class="vertical-align" style="left:0;right:0;">
            <div class="container">
              <div class="pseudo-title vis">OUR HISTORY</div>
              <h4>Over a century of success</h4>
              <div class="swiper-container timeline-top">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/joseph_kruger.jpg); background-position: top -90px center;"></div>
                      <div class="pseudo-title year">1904</div>
                      <h5><span>Kruger Paper Company Limited, a wholesale paper distribution firm, is founded in Montreal, QC by Joseph Kruger.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/1922.jpg);"></div>
                      <div class="pseudo-title year">1922</div>
                      <h5><span>Westminster Paper Mills Limited, now our New Westminster, BC plants, commences operation.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/1957_2.png);"></div>
                      <div class="pseudo-title year">1957</div>
                      <h5><span>Crabtree, QC plant acquired.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/1978_2.png);"></div>
                      <div class="pseudo-title year">1978</div>
                      <h5><span>Sherbrooke, QC plant acquired.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/stoh_horizontal.jpg); background-size: 90%;"></div>
                      <div class="pseudo-title year">1982</div>
                      <h5><span>Sponsorship of Scott Tournament of Hearts (now Scotties Tournament of Hearts), Canada's National Women's Curling Championship, begins.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/1988.png);"></div>
                      <div class="pseudo-title year">1988</div>
                      <h5><span>Gatineau, QC plant acquired.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/1997.png);"></div>
                      <div class="pseudo-title year">1997</div>
                      <h5><span>Kruger Inc. purchases the Canadian operations of Scott Paper, launching Kruger's consumer tissue products business in Canada.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/1999.png); background-position: top -75px center;"></div>
                      <div class="pseudo-title year">1999</div>
                      <h5><span>Relaunch of White Cloud brand in the United States.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2002.jpeg);"></div>
                      <div class="pseudo-title year">2002</div>
                      <h5><span>Acquisition of major plant in Memphis, TN, establishing our US manufacturing presence.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2004_2.png);"></div>
                      <div class="pseudo-title year">2004</div>
                      <h5><span>Debut of the White Cashmere Collection, featuring some of Canada's top fashion designers.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/cashmere_timeline.png);"></div>
                      <div class="pseudo-title year">2004</div>
                      <h5><span>Cottonelle begins its transition to Cashmere. Today, Cashmere is Canada's best selling bathroom tissue.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/cbcf-logo-timeline.png);   background-size: 85%;"></div>
                      <div class="pseudo-title year">2005</div>
                      <h5><span>Our sponsorship of the Canadian Breast Cancer Foundation begins. Today, we are one of the Foundation's Top 5 national contributors.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2005_1.png);   background-size: 140%;"></div>
                      <div class="pseudo-title year">2005</div>
                      <h5><span>ScotTowels begins its transition to SpongeTowels.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/kruger_products_logo.jpg); background-size: 90%;"></div>
                      <div class="pseudo-title year">2007</div>
                      <h5><span>Scott Paper changes its name to Kruger Products.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2008.png);   background-size: 80%;"></div>
                      <div class="pseudo-title year">2008</div>
                      <h5><span>Launch of our EnviroCare family of products, Canada's first line of premium tissue products made from 100% recycled paper.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2015.png);   background-size: 70%;"></div>
                      <div class="pseudo-title year">2010</div>
                      <h5><span>Kruger Products launches Sustainability 2015, a five year initiative to reduce our environmental footprint and benefit our communities.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2011_2.png);   background-size: 90%;"></div>
                      <div class="pseudo-title year">2011</div>
                      <h5><span>Kruger Products becomes the first Canadian tissue manufacturer to earn Forest Stewardship Council<sup>®</sup> (FSC<sup>®</sup>) certification, creating one of the largest portfolios of third-party certified tissue products in North America.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2011_1.jpeg);"></div>
                      <div class="pseudo-title year">2011</div>
                      <h5><span>30th anniversary as Presenting Sponsor of the Scotties Tournament of Hearts (Canada's National Women's Curling Championship), one of the longest sponsorships of amateur athletics in Canadian history.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2012_2.jpg);"></div>
                      <div class="pseudo-title year">2012</div>
                      <h5><span>Kruger Products wins 5 prestigious Cassies awards, and is now one of Canada's most decorated marketing organizations.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2012_1.png);   background-size: 80%;"></div>
                      <div class="pseudo-title year">2012</div>
                      <h5><span>KP Tissue Inc., which holds an equity interest in Kruger Products L.P., becomes a publicly-traded entity on the Toronto Stock Exchange (symbol: KPT).</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2013_2.png);"></div>
                      <div class="pseudo-title year">2013</div>
                      <h5><span>State-of-the-art Through-Air Dried (TAD) tissue machine commences operation at our Memphis, TN plant, significantly expanding our North American capacity.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2013_1.png);"></div>
                      <div class="pseudo-title year">2013</div>
                      <h5><span>Cashmere bathroom tissue's White Cashmere Collection fashion show celebrates its 10th anniversary, in support of the Canadian Breast Cancer Foundation.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2014.jpg);"></div>
                      <div class="pseudo-title year">2014</div>
                      <h5><span>To accelerate Away From Home Division growth, Kruger Products acquires the Canadian converting assets of Metro Paper Industries Inc.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2015.jpg);"></div>
                      <div class="pseudo-title year">2015</div>
                      <h5><span>White Cloud rated #1 bath tissue product in the United States by a leading consumer research and testing magazine.</span></h5>
                    </div>
                  </div>
                  <!-- <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2016_2.png);"></div>
                      <div class="pseudo-title year">2016-2</div>
                      <h5><span>SpongeTowels Ultra Strong wins two Canadian Grand Prix new product awards</span></h5>
                    </div>
                  </div> -->
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/home_grid_1_second.png);"></div>
                      <div class="pseudo-title year">2016</div>
                      <h5><span>In a national survey of Canadian retailers, Kruger Products is ranked #1 packaged goods supplier for an unprecedented fourth consecutive year.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/WC-LOGO.png);  background-size: 65%;"></div>
                      <div class="pseudo-title year">2017</div>
                      <h5><span>White Cloud brand significantly expands distribution to over 12,000 retail stores across the United States, and growing.</span></h5>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="block">
                      <div class="thumb" style="background-image: url(images/2017_2.png);"></div>
                      <div class="pseudo-title year">2017</div>
                      <h5><span>Kruger Products named one of Greater Toronto's Top Employers for the fifth consecutive year.</span></h5>
                    </div>
                  </div>
                </div>
              </div>
              <div class="year-title">1904</div>
              <!-- <div class="swiper-container timeline-bottom">
                  <div class="swiper-wrapper">
                    <div class="swiper-slide"><span class="pseudo-title">1941</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1942</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1943</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1944</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1945</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1946</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1947</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1948</span></div>
                    <div class="swiper-slide"><span class="pseudo-title">1949</span></div>
                  </div>
              </div> -->
              <input type="range" min="0" max="26" step="1" value="0" data-orientation="horizontal">
              <!-- <div class="swiper-button-next swiper-button-black"></div>
              <div class="swiper-button-prev swiper-button-black"></div> -->
            </div>
          </div>
        </section>
        <section class="about__ethical full-height wipe-in" data-section="Fair and Ethical">
          <div class="vertical-align">
            <div class="about__ethical-inner">
              <h2>Fair and Ethical Business</h2>
              <p>Our policy is to conduct ourselves ethically and in conformance with the applicable laws, rules and regulations of every country in which we do business.</p>
              <a href="http://www.krugerproducts.ca/pdfs/Kruger_Code_étiques_EN_v6_FINAL.pdf" target="_blank" class="btn pseudo-title">Read code of conduct</a>
            </div>
          </div>
        </section>
        <section class="about__organization text-in" data-section="Organization">
          <div class="pseudo-title">CORPORATE STRUCTURE</div>
          <h2>Corporate Structure</h2>
          <p>Our organizational chart illustrates the relationship between Kruger Products L.P. and its shareholders. <a href="http://www.kptissueinc.com/" target="_blank">KP Tissue Inc.</a> is a publicly traded entity created to acquire, and its business is limited to holding, a limited partnership interest in Kruger Products L.P. (KPLP). <a href="http://www.kptissueinc.com/" target="_blank">KP Tissue Inc.</a> trades on the Toronto Stock Exchange under the symbol KPT.</p>
          <div class="about__organization-scheme">
            <div class="container">
                <div class="rings-wrap">
                  <span class="ring"></span>
                  <h6>Public Investors</h6>
                </div>
                <div class="jumper">
                  <svg class="icon">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-5"></use>
                  </svg>
                  100%
                </div>
                <div class="rings-wrap">
                  <span class="ring"></span>
                  <h6>KP Tissue Inc.</h6>
                  <span>Canada</span>
                </div>
                <div class="jumper">
                  <svg class="icon">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-5"></use>
                  </svg>
                  16.1%
                </div>
              <div class="circle">
                <div class="vertical-align">
                  <h6>Kruger Products L.P.</h6>
                  <span>Québec</span>
                </div>
              </div>
              <div class="jumper">
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-6"></use>
                </svg>
                <0.01%
              </div>
              <div class="rings-wrap">
                <span class="ring"></span>
                <h6>KPGP Inc.</h6>
                <span>Canada</span>
              </div>
              <div class="jumper">
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-6"></use>
                </svg>
                100%
              </div>
              <div class="rings-wrap">
                <span class="ring"></span>
                <h6>Kruger Inc.</h6>
              </div>
              <div class="over-jumper jumper">
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-6"></use>
                </svg>
                <span>83.9%</span>
              </div>
              <div class="clearfix"></div>
              <span class="pseudo-title vis" style="margin-top: 50px; margin-bottom: 0; padding-bottom: 0; border: none; text-decoration: none;">As At May 3, 2017</span>
            </div>
          </div>
          <!-- <svg version="1.1" class="ring" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="101.559px" height="101.176px" viewBox="0 0 101.559 101.176" enable-background="new 0 0 101.559 101.176" xml:space="preserve">
              <path fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#00a453" stroke-width="7" stroke-miterlimit="10" d="M50.779,3.5c26.111,0,47.279,21.082,47.279,47.088S76.891,97.676,50.779,97.676S3.5,76.594,3.5,50.588S24.668,3.5,50.779,3.5z"/>
          </svg> -->

        </section>
        <section class="about__highlights wipe-in left" data-section="Recognition Highlights">
          <div class="container">
            <div class="pseudo-title vis">PEAK PERFORMANCE</div>
            <h2>Recognition<br> Highlights</h2>
            <div class="swiper-container timeline-left">
                <div class="swiper-wrapper">
                  <div class="swiper-slide pseudo-title">2017</div>
                  <div class="swiper-slide pseudo-title">2016</div>
                  <div class="swiper-slide pseudo-title">2015</div>
                  <div class="swiper-slide pseudo-title">2014</div>
                </div>
            </div>
            <div class="swiper-container timeline-right">
              <div class="swiper-wrapper">
                <div class="swiper-slide">
                  <div class="block corporate">
                    <h3>Greater Toronto Area Top Employer for 5th consecutive year</h3>
                    <div class="pseudo-title">Corporate</div>
                  </div>
                  <div class="block sustainability">
                    <h3>CN EcoConnexions Partnership Award for Sustainability</h3>
                    <div class="pseudo-title">Sustainability</div>
                  </div>
                  <div class="block sustainability">
                    <h3>Corporate Knights Future 40 Responsible Leaders</h3>
                    <div class="pseudo-title">Sustainability</div>
                  </div>
                  <div class="block marketing">
                    <h3>Best New Product Awards, SpongeTowels Ultra Strong Minis</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block marketing">
                    <h3>Retail Council of Canada Grand Prix Finalist, SpongeTowels Minis</h3>
                    <div class="pseudo-title ">Marketing</div>
                  </div>
                  <div class="block marketing">
                    <h3>Grand Prix Grafika French OOH Advertising, Scotties</h3>
                    <div class="pseudo-title ">Marketing</div>
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="block customer">
                    <h3>Ranked #1 Canadian Packaged Goods Supplier, Industry Survey for 4th consecutive year</h3>
                    <div class="pseudo-title">Customer</div>
                  </div>
                  <div class="block sustainability">
                    <h3>Walmart Canada Vendor Sustainability Award</h3>
                    <div class="pseudo-title">Sustainability</div>
                  </div>
                  <div class="block marketing">
                    <h3>CPRS Award of Excellence – Canadian Media Relations Agency Campaign of the Year, Cashmere</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block individual">
                    <h3>Canadian Grocery’s STAR Women of Grocery, Lucie Martin, Corporate Director, Talent Management</h3>
                    <div class="pseudo-title">Individual</div>
                  </div>
                  <div class="block marketing">
                    <h3>IABC Gold Quill Award – Award of Merit: Photography, Cashmere</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block individual">
                    <h3>Canada’s Clean50, Steven Sage, VP Sustainability & Innovation</h3>
                    <div class="pseudo-title">Individual</div>
                  </div>
                  <div class="block marketing">
                    <h3>IABC Gold Quill Award – Award of Merit: Media Relations, Cashmere</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block corporate">
                    <h3>Greater Toronto Area Top Employer List</h3>
                    <div class="pseudo-title">Corporate</div>
                  </div>
                  <div class="block marketing">
                    <h3>IABC/Toronto OVATION Award – Award of Merit: Media Relations, Cashmere</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block customer">
                    <h3>Walmart Canada National Brand Vendor Partner of the Year</h3>
                    <div class="pseudo-title">Customer</div>
                  </div>
                  <div class="block marketing">
                    <h3>IABC/Toronto OVATION Award – Award of Merit: Photography, Cashmere</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block sustainability">
                    <h3>CN EcoConnexions Partnership Award for Sustainability</h3>
                    <div class="pseudo-title">Sustainability</div>
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="block marketing">
                    <h3>Retail Council of Canada Grand Prix--Best New  Product--Paper, Plastic & Foil categories, SpongeTowels Ultra Strong</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block marketing">
                    <h3>Best All-Around Bathroom Tissue in U.S. by a leading consumer magazine, White Cloud Ultra Soft ‘n Thick</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block customer">
                    <h3>Ranked #1 Canadian Packaged Goods Supplier, Industry Survey, Consumer Division</h3>
                    <div class="pseudo-title">Customer</div>
                  </div>
                  <div class="block sustainability">
                    <h3>Pratt Industries Environmental Impact Award, KTG USA, Memphis</h3>
                    <div class="pseudo-title">Sustainability</div>
                  </div>
                  <div class="block customer">
                    <h3>Walmart Canada National Brand Vendor Partner of the Year – Consumables, Consumer Division</h3>
                    <div class="pseudo-title">Customer</div>
                  </div>
                  <div class="block sustainability">
                    <h3>CN EcoConnexions Partnership Award for Sustainability</h3>
                    <div class="pseudo-title">Sustainability</div>
                  </div>
                  <div class="block customer">
                    <h3>Metro Vendor of the Year, Consumer Division</h3>
                    <div class="pseudo-title">Customer</div>
                  </div>
                  <div class="block sustainability">
                    <h3>Supply & Demand Chain Executive Green Award, Sustainability 2015</h3>
                    <div class="pseudo-title">Sustainability</div>
                  </div>
                  <div class="block customer">
                    <h3>BJ’s Wholesale Supplier Partnership Award, U.S. Private Label</h3>
                    <div class="pseudo-title">Customer</div>
                  </div>
                  <div class="block corporate">
                    <h3>Greater Toronto Area Top Employer List</h3>
                    <div class="pseudo-title">Corporate</div>
                  </div>
                  <div class="block sustainability">
                    <h3>CATIE Greening the Supply Chain Award</h3>
                    <div class="pseudo-title">Sustainability</div>
                  </div>
                  <div class="block marketing">
                    <h3>Retail Council of Canada Grand Prix-Special Award--Consumer Acceptance, SpongeTowels Ultra Strong</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block sustainability">
                    <h3>Ontario Chamber of Commerce Business Achievement Award, Sustainability 2015</h3>
                    <div class="pseudo-title">Sustainability</div>
                  </div>
                  <div class="block corporate">
                    <h3>Greater Toronto Area Top Employer List</h3>
                    <div class="pseudo-title">Corporate</div>
                  </div>
                  <div class="block sustainability">
                    <h3>Entreprises et associations d’entreprisees finaliste, Les phénix, de l’environment, Sustainability 2015</h3>
                    <div class="pseudo-title">Sustainability</div>
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="block individual">
                    <h3>Women’s Executive Network’s Canada’s Most Powerful Women, Nancy Marcus, Corporate VP Marketing</h3>
                    <div class="pseudo-title">Individual</div>
                  </div>
                  <div class="block corporate">
                    <h3>Tissue Company of the Year, Pulp & Paper International</h3>
                    <div class="pseudo-title">Corporate</div>
                  </div>
                  <div class="block marketing">
                    <h3>Cassies Silver—Sustained Success, SpongeTowels</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block customer">
                    <h3>Ranked #1 Canadian Packaged Goods Supplier, Industry Survey, Consumer Division</h3>
                    <div class="pseudo-title">Customer</div>
                  </div>
                  <div class="block marketing">
                    <h3>Canadian Ovation Award of Excellence, Other Graphic Design/3D, Cashmere</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block customer">
                    <h3>United Grocers Inc. Partnership/Most Valuable Performance, Consumer Canada</h3>
                    <div class="pseudo-title">Customer</div>
                  </div>
                  <div class="block individual">
                    <h3>Canadian Grocery’s STAR Women of Grocery, Nancy Marcus, Corporate VP Marketing</h3>
                    <div class="pseudo-title">Individual</div>
                  </div>
                  <div class="block customer">
                    <h3>Sysco Top 10 Supplier—Supplier Excellence Designation, Away From Home Division</h3>
                    <div class="pseudo-title">Customer</div>
                  </div>
                  <div class="block marketing">
                    <h3>IABC Gold Quill Award of Merit, Writing, Cashmere</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block customer">
                    <h3>Top 3 Supplier, Balpex Buying Group, Away From Home Division</h3>
                    <div class="pseudo-title">Customer</div>
                  </div>
                  <div class="block marketing">
                    <h3>Strategy Cause + Action CSR Awards Finalist, Cashmere</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block customer">
                    <h3>Top Supplier, Adapt Buying Group, maintaining status in Adapt’s President’s Club, Away From Home Division</h3>
                    <div class="pseudo-title">Customer</div>
                  </div>
                  <div class="block marketing">
                    <h3>Canadian Ovation Award of Excellence, Other Graphic Design/3D, Cashmere</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block individual">
                    <h3>Canadian Grocery’s STAR Women of Grocery, Janel Caldwell, Business Development Manager</h3>
                    <div class="pseudo-title">Individual</div>
                  </div>
                  <div class="block marketing">
                    <h3>Canadian Ovation Award of Merit, Marketing Communication, Cashmere</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block corporate">
                    <h3>Greater Toronto Area Top Employer List</h3>
                    <div class="pseudo-title">Corporate</div>
                  </div>
                  <div class="block marketing">
                    <h3>Canadian Public Relations Society Ace Award, Best Use of Special Events, Cashmere</h3>
                    <div class="pseudo-title">Marketing</div>
                  </div>
                  <div class="block sustainability">
                    <h3>CN EcoConnexions Partnership Award for Sustainability</h3>
                    <div class="pseudo-title">Sustainability</div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </section>
      </div>
    </div>

<?php require('footer.php'); ?>
