<?php
$title = 'Accueil';
require('header.php');
?>

  <section class="full-height wipe-in" data-section="Page Header">
    <div class="home__masonry">
      <div class="brick-wrap">
        <div class="brick" style="background-image: url(images/collage_paper.png);">
          <div class="shade"></div>
        </div>
        <div class="brick" style="background-image: url(images/leaf_half.jpg);">
          <div class="shade"></div>
        </div>
      </div>
      <div class="brick-wrap">
        <div class="brick" style="background-image: url(images/collage_pattern.jpg);">
          <div class="shade"></div>
        </div>
        <div class="brick" style="background-image: url(images/collage_white.jpg);">
          <div class="vertical-align">
            <span class="logo"><img src="images/Kruger_FR_1.png"</span>
            <h1>Kruger Produits</h1>
          </div>
        </div>
      </div>
      <div class="brick-wrap">
        <div class="brick" style="background-image: url(images/collage_girl.png);">
          <div class="shade"></div>
        </div>
      </div>
      <div class="brick-wrap">
        <div class="brick" style="background-image: url(images/collage_packed.jpg);">
          <div class="shade"></div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </section>

  <div class="wrapper">

    <section class="home__welcome text-in" data-section="Welcome">
      <div class="container">
        <div class="pseudo-title">WELCOME</div>
        <h4>Le fabricant de<br> produits de papier<br> en 1<sup>re</sup> place au Canada</h4>
        <p>De son siège social à Mississauga, en Ontario, Produits Kruger emploie 2 500 personnes dans ses 8 usines de fabrication à travers l’Amérique du Nord. Partout où nous intervenons, nous investissons dans nos communautés et dans l’environnement avec des initiatives de développement durable et des programmes caritatifs locaux.</p>
      </div>
    </section>

    <section class="home__slider wipe-in" data-section="Product Slider">
      <div id="onSliderTrigger" style="position:relative; top: 50px;"></div>

      <div class="swiper-container">
        <div>

          <div class="swiper-slide slide slide-4" onclick="this.focus();">

            <div class="bg-grad"></div>

            <div class="container">
              <div class="logo vertical-align">
                <img src="images/White-Cloud-logo.png" alt="Dans plus de magasins, avec un nouveau look. Mais toujours la même qualité surprenante."/>
              </div>
              <div class="text vertical-align">
                <div>
                  <h4>Dans plus de magasins, avec un nouveau look. <br>Mais toujours la même qualité surprenante.</h4>
                  <a href="http://mywhitecloud.com/" class="btn pseudo-title" target="_blank">En savoir plus</a>
                </div>
                <div class="image-container">
                  <img src="images/whiteCloud_packs.png" alt="">
                </div>
              </div>
              <span class="onSliderTrigger"></span>
            </div>
          </div>

          <div class="swiper-slide slide slide-5 dark" onclick="this.focus();">

            <div class="bg-grad"></div>

            <div class="container">
              <div class="logo vertical-align">
                <img src="images/Purex-logo-2.png" alt="Purex est le confort à l’état pur."/>
              </div>
              <div class="text vertical-align">
                <h4>Purex est le<br> confort à l’état pur.<sup>®</sup></h4>
                <a href="http://www.purex.ca/francais/" class="btn pseudo-title" target="_blank">En savoir plus</a>
              </div>
              <span class="onSliderTrigger"></span>
            </div>
          </div>

          <div class="swiper-slide slide slide-1" onclick="this.focus();">

            <div class="bg-grad"></div>

            <div class="container">
              <div class="logo vertical-align">
                <img src="images/Cashmere-logo.png" alt="Mon irremplaçable douceur."/>
              </div>
              <div class="text vertical-align">
                <h4>Mon irremplaçable<br> douceur.<sup>®</sup></h4>
                <a href="http://www.cashmere.ca/index_fr.html#home" class="btn pseudo-title" target="_blank">En savoir plus</a>
              </div>
              <span class="onSliderTrigger"></span>
            </div>
          </div>

          <div class="swiper-slide slide slide-3" onclick="this.focus();">

            <div class="bg-grad"></div>

            <div class="container">
              <div class="logo vertical-align">
                <img src="images/Scotties-logo.png" alt="Le mouchoir de circonstance."/>
              </div>
              <div class="text vertical-align">
                <h4>Le mouchoir<br> de circonstance.<sup>®</sup></h4>
                <a href="http://www.scotties.ca/fr/" class="btn pseudo-title" target="_blank">En savoir plus</a>
              </div>
              <span class="onSliderTrigger"></span>
            </div>
          </div>

          <div class="swiper-slide slide slide-2 dark" onclick="this.focus();">

            <div class="bg-grad"></div>

            <div class="container">
              <div class="logo vertical-align">
                <img src="images/Sponge-Towels.png" alt="Absorbant pas à peu près."/>
              </div>
              <div class="text vertical-align">
                <h4>Absorbant pas<br> à peu près.<sup>™</sup></h4>
                <a href="http://www.spongetowels.ca/index-fr.php" class="btn pseudo-title" target="_blank">En savoir plus</a>
              </div>
              <span class="onSliderTrigger"></span>
            </div>
          </div>

        </div>
      </div>

      <!-- <div class="swiper-pagination"></div> -->
    </section>

    <section class="home__slider_mob wipe-in" data-section="Product Slider" data-intro-func="startHomeSlider">
      <div id="onSliderTrigger" style="position:relative; top: 50px;"></div>

      <div class="swiper-container">
        <div class="swiper-wrapper">

          <div class="swiper-slide slide slide-1">
            <div class="container">

              <div class="bg"></div>
              <div class="ptr"></div>

              <div class="idx">1/5</div>

              <div class="text vertical-align">
                <h4>Mon irremplaçable douceur.<sup>®</sup></h4>
                <a href="http://www.cashmere.ca/index_fr.html#home" class="btn pseudo-title" target="_blank">En savoir plus</a>
              </div>
              <span class="onSliderTrigger"></span>
            </div>
          </div>

          <div class="swiper-slide slide slide-2">
            <div class="container">

              <div class="bg"></div>
              <div class="ptr"></div>

              <div class="idx">2/5</div>

              <div class="text vertical-align">
                <h4>Absorbant pas à peu près.<sup>™</sup></h4>
                <a href="http://www.spongetowels.ca/index-fr.php" class="btn pseudo-title" target="_blank">En savoir plus</a>
              </div>
              <span class="onSliderTrigger"></span>
            </div>
          </div>

          <div class="swiper-slide slide slide-3">
            <div class="container">

              <div class="bg"></div>
              <div class="ptr"></div>

              <div class="idx">3/5</div>

              <div class="text vertical-align">
                <h4>Le mouchoir de circonstance.<sup>®</sup></h4>
                <a href="http://www.scotties.ca/fr/" class="btn pseudo-title" target="_blank">En savoir plus</a>
              </div>
              <span class="onSliderTrigger"></span>
            </div>
          </div>

          <div class="swiper-slide slide slide-4">
            <div class="container">

              <div class="bg"></div>
              <div class="ptr"></div>

              <div class="idx">4/5</div>

              <div class="text vertical-align">
                <h4>Purex est le confort à l’état pur.<sup>®</sup></h4>
                <a href="http://www.purex.ca/francais/" class="btn pseudo-title" target="_blank">En savoir plus</a>
              </div>
              <span class="onSliderTrigger"></span>
            </div>
          </div>

          <div class="swiper-slide slide slide-5">
            <div class="container">

              <div class="bg">
              <div class="ptr"></div>

                <div class="image-container">
                  <img src="images/whiteCloud_packs.png" alt="">
                </div>

              </div>

              <div class="idx">5/5</div>

              <div class="text vertical-align">
                <div>
                  <h4>Dans plus de magasins, avec un nouveau look. <br>Mais toujours la même qualité surprenante.</h4>
                  <a href="http://mywhitecloud.com/" class="btn pseudo-title" target="_blank">En savoir plus</a>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

      <!-- <div class="swiper-pagination"></div> -->
    </section>

    <div class="home__slider_scroll_c"></div>
    <section class="home__welcome home__welcome_2 text-in">
      <div class="container">
        <h4>Les gens.<br> La passion.<br> Notre raison d’être.</h4>
        <p>Nos collègues partout en Amérique du Nord s’engagent ensemble à aider nos consommateurs et nos clients à répondre à tous leurs besoins quotidiens en leur offrant des produits de papier de qualité, tout en soutenant fièrement nos communautés. Cet engagement durable, d’ailleurs, continue d’être au cœur de notre succès.</p>
      </div>
      <div class="clearfix2"></div>
    </section>

    <!-- <section class="home__welcome">
      <div class="container">
        <div class="pseudo-title">THE COMPANY</div>
        <h4>At a Glance</h4>
      </div>
    </section> -->

    <section data-section="At A Glance">
      <div class="home__columns">

          <div class="col-block wipe-in down" style="background-color:#ffce00">
            <span class="triangle" style="border-color: transparent transparent #ffce00 transparent;"></span>
            <div class="thumb" style="background-image: url(images/home_grid_1.png);"></div>
            <h6><span>Le plus grand portefeuille de produits de papier en 1<sup>re</sup> place au Canada</span></h6>
          </div>

          <div class="col-block wipe-in down" style="background-color:#ff634a">
            <span class="triangle" style="border-color: transparent transparent #ff634a transparent;"></span>
            <div class="thumb" style="background-image: url(images/home_grid_2.jpg);"></div>
            <h6><span>L’un des 5 donateurs les plus importants au Canada pour la Fondation canadienne du cancer du sein</span></h6>
          </div>

          <div class="col-block wipe-in down" style="background-color:#5b9fc0">
            <span class="triangle" style="border-color: transparent transparent #5b9fc0 transparent;"></span>
            <div class="thumb" style="background-image: url(images/home_grid_3.jpg);"></div>
            <h6><span>Gagnant du prix des meilleurs employeurs dans la RGT pendant 5 années consécutives</span></h6>
          </div>

          <div class="col-block wipe-in down" style="background-color:#bfb7b1">
            <span class="triangle" style="border-color: transparent transparent #bfb7b1 transparent;"></span>
            <div class="thumb" style="background-image: url(images/home_grid_4.png);"></div>
            <h6><span>White Cloud est le papier hygiénique classé en 1<sup>re</sup> place aux É.-U. selon une éminente publication consacrée aux cotes accordées par les consommateurs</span></h6>
          </div>

          <div class="col-block wipe-in down" style="background-color:#51678e">
            <span class="triangle" style="border-color: transparent transparent #51678e transparent;"></span>
            <div class="thumb" style="background-image: url(images/home_grid_5.jpg);"></div>
            <h6><span>Compagnie reconnue à l’échelle nationale en tant que pionnier de l’industrie en matière de développement durable</span></h6>
          </div>

          <div class="col-block wipe-in down" style="background-color:#54596a">
            <span class="triangle" style="border-color: transparent transparent #54596a transparent;"></span>
            <div class="thumb" style="background-image: url(images/home_grid_6.jpg); background-color: #fff;"></div>
            <h6><span>Le fabricant de produits de papier hors foyer en 1<sup>re</sup> place au Canada</span></h6>
          </div>

          <div class="col-block wipe-in down" style="background-color:#be7392">
            <span class="triangle" style="border-color: transparent transparent #be7392 transparent;"></span>
            <div class="thumb" style="background-image: url(images/image002.png);"></div>
            <h6><span>Classé en 1<sup>re</sup> place parmi les fournisseurs de biens de consommation courante au Canada pendant 4 années consécutives</span></h6>
          </div>

          <div class="col-block wipe-in down" style="background-color:#d89d37">
            <span class="triangle" style="border-color: transparent transparent #d89d37 transparent;"></span>
            <div class="thumb" style="background-image: url(images/home_grid_7.png);"></div>
            <h6><span>Un succès fabriqué au Québec</span></h6>
          </div>

      </div>
      <div class="clearfix"></div>
    </section>

    <section class="home__welcome lead-in text-in">
      <div class="container">
        <h4>Nouveautés notables</h4>
      </div>
    </section>

    <section data-section="News">
      <div class="home__news">
        <a class="wipe-in" target="_blank" href="http://stoh.ca/francais/index.php">
          <span class="bg" style="background-image: url(images/news_1.jpg);"></span>
          <span class="mini-wipe"></span>
          <span class="content">
            <h4><hr style="border-color: #ffc419;"/>Tournoi des cœurs Scotties<sup>®</sup><span class="soTiny">’</span></h4>
            <span class="cta">
              <span>En savoir plus</span>
            </span>
          </span>
          <!-- <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-4"></use>
          </svg> -->
        </a>
        <a class="wipe-in" target="_blank" href="/sign-up.php">
          <span class="bg" style="background-image: url(images/news_2.jpg);"></span>
          <span class="mini-wipe" style="background-color: #5ea0bf;"></span>
          <span class="content">
            <h4><hr style="border-color: #a1e7ff;"/>Gagnez une provision de un an de nos produits</h4>
            <span class="cta">
              <span>En savoir plus</span>
            </span>
          </span>
          <!-- <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-4"></use>
          </svg> -->
        </a>
        <a class="wipe-in" target="_blank" href="http://produitskruger.ca/2017/sustainability.php">
          <span class="bg" style="background-image: url(images/news_3.jpg);"></span>
          <span class="mini-wipe"></span>
          <span class="content">
            <h4><hr style="border-color: #83bf60;"/>Résultats finals du programme Développement durable 2015</h4>
            <span class="cta">
              <span>En savoir plus</span>
            </span>
          </span>
          <!-- <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-4"></use>
          </svg> -->
        </a>
        <a class="wipe-in" target="_blank" href="https://www.collectionblanccashmere2017.ca/">
          <span class="bg" style="background-image: url(images/news_4.jpg);"></span>
          <span class="mini-wipe"></span>
          <span class="content">
            <h4><hr style="border-color: #ffa8c5;"/>Collection Blanc Cashmere 2017</h4>
            <span class="cta">
              <span>En savoir plus</span>
            </span>
          </span>
          <!-- <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-4"></use>
          </svg> -->
        </a>
        <a class="wipe-in" target="_blank" href="http://www.canadastop100.com/toronto/">
          <span class="bg" style="background-image: url(images/news_5.jpg);"></span>
          <span class="mini-wipe"></span>
          <span class="content">
            <h4><hr style="border-color: #f67a42;"/>L’un des meilleurs employeurs dans la RGT pendant cinq années consécutives</h4>
            <span class="cta">
              <span>En savoir plus</span>
            </span>
          </span>
          <!-- <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-4"></use>
          </svg> -->
        </a>
        <a class="wipe-in" target="_blank" href="http://spongetowels.ca/index-fr.php">
          <span class="bg" style="background-image: url(images/news_6.jpg);"></span>
          <span class="mini-wipe"></span>
          <span class="content">
            <h4><hr style="border-color: #71acf6;"/>Les SpongeTowels<sup>®</sup> Minis sont arrivés</h4>
            <span class="cta">
              <span>En savoir plus</span>
            </span>
          </span>
          <!-- <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-4"></use>
          </svg> -->
        </a>
      </div>
      <!-- <div class="clearfix"></div> -->
    </section>

  </div>

    <!-- <section class="home__welcome home__linkedin">
      <div class="container">
        <a href="javascript:void(0)" class="link-block">
          <img src="images/linkedin.png" alt="">
          <span class="pseudo-title btn">Follow Us</span>
          <span class="string pseudo-title">4.057 followers</span>
        </a>
        <div class="home__linkedin-inner">
          <p><i>"</i>Kruger Products L.P. As a proud sponsor of the Canadian Breast Cancer Foundation for over a decade, Scotties launched the Show Us Your Boa contest! Visit their Facebook page to enter: <a href="https://lnkd.in/duNJ2h5">https://lnkd.in/duNJ2h5</a><i>"</i></p>
          <div class="string pseudo-title">2 days ago</div>
        </div>
      </div>
    </section> -->

<?php require('footer.php'); ?>
