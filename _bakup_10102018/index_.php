<?php
$title = 'Home';
require('header.php');
?>

  <section class="full-height wipe-in" data-section="Page Header">
    <div class="home__masonry">
      <div class="brick-wrap">
        <div class="brick" style="background-image: url(images/collage_paper.png);">
          <div class="shade"></div>
        </div>
        <div class="brick" style="background-image: url(images/leaf_half.jpg);">
          <div class="shade"></div>
        </div>
      </div>
      <div class="brick-wrap">
        <div class="brick" style="background-image: url(images/collage_pattern.jpg);">
          <div class="shade"></div>
        </div>
        <div class="brick" style="background-image: url(images/collage_white.jpg);">
          <div class="vertical-align">
            <span class="logo"><img src="images/logo_pd.png"</span>
            <h1>Kruger Products</h1>
          </div>
        </div>
      </div>
      <div class="brick-wrap">
        <div class="brick" style="background-image: url(images/collage_girl.png);">
          <div class="shade"></div>
        </div>
      </div>
      <div class="brick-wrap">
        <div class="brick" style="background-image: url(images/collage_packed.jpg);">
          <div class="shade"></div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </section>

  <div class="wrapper">

    <section class="home__welcome text-in" data-section="Welcome">
      <div class="container">
        <div class="pseudo-title">WELCOME</div>
        <h4>Canada's #1 tissue<br> products manufacturer</h4>
        <p>From our headquarters in Mississauga, Ontario, Kruger Products employs 2,500 people in 8 manufacturing plants located across North America. Everywhere we operate, we invest in our communities and the environment through sustainability initiatives and local philanthropy.</p>
      </div>
    </section>

    <section class="home__slider wipe-in" data-section="Product Slider">
      <div id="onSliderTrigger" style="position:relative; top: 50px;"></div>

      <div class="swiper-container">
        <div>

          <div class="swiper-slide slide slide-4" onclick="this.focus();">

            <div class="bg-grad"></div>

            <div class="container">
              <div class="logo vertical-align">
                <img src="images/White-Cloud-logo.png" alt="White Cloud. Nothing Absorbs Like It."/>
              </div>
              <div class="text vertical-align">
                <div>
                  <h4>New stores, new look. <br>Same incredible quality.</h4>
                  <a href="http://mywhitecloud.com/" class="btn pseudo-title" target="_blank">LEARN MORE</a>
                </div>
                <div class="image-container">
                  <img src="images/whiteCloud_packs.png" alt="">
                </div>
              </div>
              <span class="onSliderTrigger"></span>
            </div>
          </div>

          <div class="swiper-slide slide slide-5" onclick="this.focus();">

            <div class="bg-grad"></div>

            <div class="container">
              <div class="logo vertical-align">
                <img src="images/Purex-logo-2.png" alt="Purex is Pure Comfort."/>
              </div>
              <div class="text vertical-align">
                <h4>Purex is<br> Pure Comfort.<sup>®</sup></h4>
                <a href="http://purex.ca/" class="btn pseudo-title" target="_blank">LEARN MORE</a>
              </div>
              <span class="onSliderTrigger"></span>
            </div>
          </div>

          <div class="swiper-slide slide slide-1" onclick="this.focus();">

            <div class="bg-grad"></div>

            <div class="container">
              <div class="logo vertical-align">
                <img src="images/Cashmere-logo.png" alt="Cashmere.  Nothing absorbs like it."/>
              </div>
              <div class="text vertical-align">
                <h4>Nothing feels<br> like Cashmere.<sup>®</sup></h4>
                <a href="http://www.cashmere.ca/" class="btn pseudo-title" target="_blank">LEARN MORE</a>
              </div>
              <span class="onSliderTrigger"></span>
            </div>
          </div>

          <div class="swiper-slide slide slide-3" onclick="this.focus();">

            <div class="bg-grad"></div>

            <div class="container">
              <div class="logo vertical-align">
                <img src="images/Scotties-logo.png" alt="Scotties.  Nothing Absorbs Like It."/>
              </div>
              <div class="text vertical-align">
                <h4>The tissue<br> for any issue.<sup>®</sup></h4>
                <a href="http://www.scotties.ca/" class="btn pseudo-title" target="_blank">LEARN MORE</a>
              </div>
              <span class="onSliderTrigger"></span>
            </div>
          </div>

          <div class="swiper-slide slide slide-2 dark" onclick="this.focus();">

            <div class="bg-grad"></div>

            <div class="container">
              <div class="logo vertical-align">
                <img src="images/Sponge-Towels.png" alt="Sponge Towels Ultra.  Nothing Absorbs Like It."/>
              </div>
              <div class="text vertical-align">
                <h4>Nothing absorbs<br> like it.<sup>™</sup></h4>
                <a href="http://www.spongetowels.ca/" class="btn pseudo-title" target="_blank">LEARN MORE</a>
              </div>
              <span class="onSliderTrigger"></span>
            </div>
          </div>

        </div>
      </div>

      <!-- <div class="swiper-pagination"></div> -->
    </section>

    <section class="home__slider_mob wipe-in" data-section="Product Slider" data-intro-func="startHomeSlider">
      <div id="onSliderTrigger" style="position:relative; top: 50px;"></div>

      <div class="swiper-container">
        <div class="swiper-wrapper">

          <div class="swiper-slide slide slide-1">
            <div class="container">

              <div class="bg"></div>
              <div class="ptr"></div>

              <div class="idx">1/5</div>

              <div class="text vertical-align">
                <h4>Nothing feels like Cashmere.<sup>®</sup></h4>
                <a href="http://www.cashmere.ca/" class="btn pseudo-title" target="_blank">LEARN MORE</a>
              </div>
              <span class="onSliderTrigger"></span>
            </div>
          </div>

          <div class="swiper-slide slide slide-2">
            <div class="container">

              <div class="bg"></div>
              <div class="ptr"></div>

              <div class="idx">2/5</div>

              <div class="text vertical-align">
                <h4>Nothing absorbs like it.<sup>™</sup></h4>
                <a href="http://www.spongetowels.ca/" class="btn pseudo-title" target="_blank">LEARN MORE</a>
              </div>
              <span class="onSliderTrigger"></span>
            </div>
          </div>

          <div class="swiper-slide slide slide-3">
            <div class="container">

              <div class="bg"></div>
              <div class="ptr"></div>

              <div class="idx">3/5</div>

              <div class="text vertical-align">
                <h4>The tissue for any issue.<sup>®</sup></h4>
                <a href="http://www.scotties.ca/" class="btn pseudo-title" target="_blank">LEARN MORE</a>
              </div>
              <span class="onSliderTrigger"></span>
            </div>
          </div>

          <div class="swiper-slide slide slide-4">
            <div class="container">

              <div class="bg"></div>
              <div class="ptr"></div>

              <div class="idx">4/5</div>

              <div class="text vertical-align">
                <h4>Purex is Pure Comfort.<sup>®</sup></h4>
                <a href="http://purex.ca/" class="btn pseudo-title" target="_blank">LEARN MORE</a>
              </div>
              <span class="onSliderTrigger"></span>
            </div>
          </div>

          <div class="swiper-slide slide slide-5">
            <div class="container">

              <div class="bg">
              <div class="ptr"></div>

                <div class="image-container">
                  <img src="images/whiteCloud_packs.png" alt="">
                </div>

              </div>

              <div class="idx">5/5</div>

              <div class="text vertical-align">
                <div>
                  <h4>New stores, new look. <br>Same incredible quality.</h4>
                  <a href="http://mywhitecloud.com/" class="btn pseudo-title" target="_blank">LEARN MORE</a>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

      <!-- <div class="swiper-pagination"></div> -->
    </section>

    <div class="home__slider_scroll_c"></div>
    <section class="home__welcome home__welcome_2 text-in">
      <div class="container">
        <h4>People.<br> Passion.<br> Purpose.</h4>
        <p>Our colleagues across North America share a passionate commitment to helping our consumers and customers fulfill their everyday needs through our offering of quality tissue products, while proudly supporting our communities. That enduring commitment, in fact, continues to be the core of our success.</p>
      </div>
      <div class="clearfix2"></div>
    </section>

    <!-- <section class="home__welcome">
      <div class="container">
        <div class="pseudo-title">THE COMPANY</div>
        <h4>At a Glance</h4>
      </div>
    </section> -->

    <section data-section="At A Glance">
      <div class="home__columns">

          <div class="col-block wipe-in down" style="background-color:#ffce00">
            <span class="triangle" style="border-color: transparent transparent #ffce00 transparent;"></span>
            <div class="thumb" style="background-image: url(images/home_grid_1.png);"></div>
            <h6><span>Canada’s largest portfolio of #1 consumer tissue brands</span></h6>
          </div>

          <div class="col-block wipe-in down" style="background-color:#ff634a">
            <span class="triangle" style="border-color: transparent transparent #ff634a transparent;"></span>
            <div class="thumb" style="background-image: url(images/home_grid_2.jpg);"></div>
            <h6><span>Top 5 national contributor to the Canadian Breast Cancer Foundation</span></h6>
          </div>

          <div class="col-block wipe-in down" style="background-color:#5b9fc0">
            <span class="triangle" style="border-color: transparent transparent #5b9fc0 transparent;"></span>
            <div class="thumb" style="background-image: url(images/home_grid_3.jpg);"></div>
            <h6><span>Winner of  GTA Top Employer award for 5 consecutive years</span></h6>
          </div>

          <div class="col-block wipe-in down" style="background-color:#bfb7b1">
            <span class="triangle" style="border-color: transparent transparent #bfb7b1 transparent;"></span>
            <div class="thumb" style="background-image: url(images/home_grid_4.png);"></div>
            <h6><span>White Cloud ranked #1 bath tissue in America by a leading consumer ratings publication</span></h6>
          </div>

          <div class="col-block wipe-in down" style="background-color:#51678e">
            <span class="triangle" style="border-color: transparent transparent #51678e transparent;"></span>
            <div class="thumb" style="background-image: url(images/home_grid_5.jpg);"></div>
            <h6><span>Nationally recognized as an industry leader in sustainability</span></h6>
          </div>

          <div class="col-block wipe-in down" style="background-color:#54596a">
            <span class="triangle" style="border-color: transparent transparent #54596a transparent;"></span>
            <div class="thumb" style="background-image: url(images/home_grid_6.jpg); background-color: #fff;"></div>
            <h6><span>Canada’s #1 Away From Home tissue products manufacturer</span></h6>
          </div>

          <div class="col-block wipe-in down" style="background-color:#be7392">
            <span class="triangle" style="border-color: transparent transparent #be7392 transparent;"></span>
            <div class="thumb" style="background-image: url(images/image002.png);"></div>
            <h6><span>Ranked #1 consumer packaged goods supplier in Canada for 4 consecutive years</span></h6>
          </div>

          <div class="col-block wipe-in down" style="background-color:#d89d37">
            <span class="triangle" style="border-color: transparent transparent #d89d37 transparent;"></span>
            <div class="thumb" style="background-image: url(images/home_grid_7.jpg);"></div>
            <h6><span>Proudly Canadian</span></h6>
          </div>

      </div>
      <div class="clearfix"></div>
    </section>

    <section class="home__welcome lead-in text-in">
      <div class="container">
        <h4>New and Noteworthy</h4>
      </div>
    </section>

    <section data-section="News">
      <div class="home__news">
        <a class="wipe-in" target="_blank" href="http://stoh.ca/english/index.php">
          <span class="bg" style="background-image: url(images/news_1.jpg);"></span>
          <span class="mini-wipe"></span>
          <span class="content">
            <h4><hr style="border-color: #ffc419;"/>Scotties<sup>®</sup><span class="soTiny">’</span> Tournament of Hearts<sup>®</sup></h4>
            <span class="cta">
              <span>Learn More</span>
            </span>
          </span>
          <!-- <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-4"></use>
          </svg> -->
        </a>
        <a class="wipe-in" target="_blank" href="http://scotties.us7.list-manage.com/subscribe?u=c9a10a8ac0d4087992f697a5a&id=13ebe15349&MERGE3=krugerproducts.ca">
          <span class="bg" style="background-image: url(images/news_2.jpg);"></span>
          <span class="mini-wipe" style="background-color: #5ea0bf;"></span>
          <span class="content">
            <h4><hr style="border-color: #a1e7ff;"/>Win our products for one year</h4>
            <span class="cta">
              <span>Learn More</span>
            </span>
          </span>
          <!-- <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-4"></use>
          </svg> -->
        </a>
        <a class="wipe-in" target="_blank" href="http://sustainability2015.ca/Kruger%202015%20Program%20Results%20Sell%20Sheet%20ENGLISH.pdf">
          <span class="bg" style="background-image: url(images/news_3.jpg);"></span>
          <span class="mini-wipe"></span>
          <span class="content">
            <h4><hr style="border-color: #83bf60;"/>Sustainability 2015 Final Results</h4>
            <span class="cta">
              <span>Learn More</span>
            </span>
          </span>
          <!-- <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-4"></use>
          </svg> -->
        </a>
        <a class="wipe-in" target="_blank" href="http://www.whitecashmerecollection2016.ca">
          <span class="bg" style="background-image: url(images/news_4.jpg);"></span>
          <span class="mini-wipe"></span>
          <span class="content">
            <h4><hr style="border-color: #ffa8c5;"/>White Cashmere Collection 2016</h4>
            <span class="cta">
              <span>Learn More</span>
            </span>
          </span>
          <!-- <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-4"></use>
          </svg> -->
        </a>
        <a class="wipe-in" target="_blank" href="http://www.canadastop100.com/toronto/">
          <span class="bg" style="background-image: url(images/news_5.jpg);"></span>
          <span class="mini-wipe"></span>
          <span class="content">
            <h4><hr style="border-color: #f67a42;"/>GTA Top Employer for five consecutive years</h4>
            <span class="cta">
              <span>Learn More</span>
            </span>
          </span>
          <!-- <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-4"></use>
          </svg> -->
        </a>
        <a class="wipe-in" target="_blank" href="http://spongetowels.ca/index-en.php">
          <span class="bg" style="background-image: url(images/news_6.jpg);"></span>
          <span class="mini-wipe"></span>
          <span class="content">
            <h4><hr style="border-color: #71acf6;"/>SpongeTowels<sup>®</sup> Minis are here</h4>
            <span class="cta">
              <span>Learn More</span>
            </span>
          </span>
          <!-- <svg class="icon">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-4"></use>
          </svg> -->
        </a>
      </div>
      <!-- <div class="clearfix"></div> -->
    </section>

  </div>

    <!-- <section class="home__welcome home__linkedin">
      <div class="container">
        <a href="javascript:void(0)" class="link-block">
          <img src="images/linkedin.png" alt="">
          <span class="pseudo-title btn">Follow Us</span>
          <span class="string pseudo-title">4.057 followers</span>
        </a>
        <div class="home__linkedin-inner">
          <p><i>"</i>Kruger Products L.P. As a proud sponsor of the Canadian Breast Cancer Foundation for over a decade, Scotties launched the Show Us Your Boa contest! Visit their Facebook page to enter: <a href="https://lnkd.in/duNJ2h5">https://lnkd.in/duNJ2h5</a><i>"</i></p>
          <div class="string pseudo-title">2 days ago</div>
        </div>
      </div>
    </section> -->

<?php require('footer.php'); ?>
