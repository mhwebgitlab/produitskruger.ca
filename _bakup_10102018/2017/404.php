<?php
$title = 'Legal Notice';
require('header.php');
?>
    <div class="accessibility">
      <div class="wrapper">
        <section class="text-content">
          <div class="container" style="min-height: 45vh;">
            <h1>404 Not Found</h1>
            <p>The page or content requested could not be found.</p>
            <p><a href="http://www.krugerproducts.ca/">Home</a></p>
        </div>
        </section>
      </div>
    </div>

<?php require('footer.php'); ?>


