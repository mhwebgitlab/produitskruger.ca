<!-- end of wrapper -->

<footer id="footer" role="footer">
    <div class="wrapper">
        <nav class="sub">
            <ul>
                <li><a href="./politique-de-confidentialite.php" aria-label="Politique de confidentialité" tabindex="0">Politique de confidentialité</a></li>
                <li><a href="./avis-sur-les-marques-de-commerce.php" aria-label="Avis sur les marques de
                        commerce" tabindex="0">Avis sur les marques de
                        commerce</a></li>
                <li><a href="./avis-juridique.php" aria-label="Avis juridique" tabindex="0">Avis juridique</a></li>
                <li><a href="./accessibilite.php" aria-label="Accessibilité" tabindex="0">Accessibilité</a></li>
                <li class="photo-credits"><a href="#"
                        data-featherlight="#lightbox-photo-credits" aria-label="Crédits photo Popup" role="button">Crédits photo</a></li>
            </ul>
        </nav>
        <nav class="sub">
            <ul>
                <li><a target="_blank" href="http://kptissueinc.com/_fr/" aria-label="Investisseurs - opens in a new tab" tabindex="0">Investisseurs</a></li>
                <li><a target="_blank" href="http://kptissueinc.com/_fr/investors/gov.php" aria-label="Gouvernance - opens in a new tab" tabindex="0">Gouvernance</a></li>
                <li><a target="_blank" href="https://www.kruger.com/fr/" aria-label="Kruger Inc - opens in a new tab" tabindex="0">Kruger Inc.</a></li>
                <li><a target="_blank" href="http://afh.krugerproducts.ca/home.aspx?lang=fr-CA&" aria-label="Produits hors foyer - opens in a new tab" tabindex="0">Produits hors foyer</a>
                </li>
                <li><a href="/sign-up.php" aria-label="Promotions/alertes électroniques - opens in a new tab" tabindex="0">Promotions/alertes électroniques</a></li>
            </ul>
        </nav>
        <div class="links">
            <!--<a target="_blank" href="http://stoh.ca/francais/index.php">
                <img src="images/STOH-logo.png" alt="Scotties">
            </a>-->
            <a href="#" onclick="return false;" role="button" tabindex="0">
                <!-- class="proudly-canadian" -->
                <img src="images/quebec.png" alt="Un succès fabriqué au Québec">
            </a>
            <a target="_blank" href="https://adstandards.ca/fr/accueil/" aria-label="ADstandards - opens in a new tab" tabindex="0">
                <img src="images/ad-standards-logo.png" alt="Normes de la publicite">
            </a>
        </div>
        <div class="clearfix">
        </div>
    </div>
</footer>

<div style="display: none;">
    <div id="lightbox-photo-credits">
        <h4>Crédits photo</h4>
        <p>Images de la collection Cashmere par Caitlin Cronenberg et Arline Malakian.</p>
    </div>
</div>

<div style="top: 50%; left: 50%; display: none;">
  <div id="uh-lightbox" class="uhvideo" style="padding:10px;">
    <video style="width: 100%; height: auto;" controls>
      <source src="/images/kruger_recut_60s_east_f.mp4" type="video/mp4">
    </video>
  </div>
</div>

<div style="top: 50%; left: 50%; display: none;">
  <div id="reimagine-lightbox" class="reimaginevideo" style="padding:10px;">
    <iframe src="https://player.vimeo.com/video/531339108" frameborder="0" allowfullscreen></iframe>
  </div>
</div>

<!-- <div style="top: 50%; left: 50%; display: none;">
    <div id="lightbox-CONID" class="conid" style="padding:10px;">
        <img class="headimg" src="images/info-covid-19.jpg" alt="COVID-19 Information" />
        <h4 style="text-align:center; font-size:22px; margin-top:20px;">Rendre la vie de tous les jours aussi
            confortable que possible</h4>
        <p>Le 19 mars 2020</p>
        <p>Nous surveillons de près la situation concernant la COVID-19. Les choses évoluent rapidement et les
            organisations en Amérique du Nord et dans le monde travaillent dur pour s’assurer que les gens aient ce dont
            ils ont besoin pendant ces moments difficiles.</p>
        <p>Nos huit usines en Amérique du Nord tournent à plein régime et optimisent leur production pour travailler
            avec nos consommateurs pour qu’ils puissent stocker des produits de papier. Nos employés mettent tout en
            œuvre pour concrétiser cela, et je suis extrêmement fier de leur réponse à la situation.</p>
        <p>Étant donnée la nature de nos produits, nos usines de production sont parmi les plus propres et les plus
            saines de toutes. Nous avons renforcé les protocoles relatifs à la santé et la sécurité de nos employés et
            seuls ceux qui sont jugés essentiels sont autorisés à entrer dans ces installations. Toute personne
            souhaitant y entrer peut être soumise à un contrôle.</p>
        <p>Au cours de mes 30 années dans l’industrie alimentaire et l’industrie des BCE, j’ai pu constater la force et
            la résilience de ces industries, mais j’ai également vu combien elles sont déterminées à faire en sorte que
            les gens aient les produits essentiels. Bien que la situation soit unique, ces industries la surmontent et
            ont l’approvisionnement nécessaire en produits et denrées alimentaires.</p>
        <p>Nous sommes tous dans le même bateau. En tant qu’entreprise, nous faisons tout ce qui est en notre pouvoir
            pour que la vie de tous les jours soit plus confortable. Pendant ces temps incertains, nous nous appuyons
            sur nos valeurs centrales pour prendre des décisions. Nous nous efforçons de faire ce qu’il faut pour nos
            employés, nos clients, nos communautés et nos affaires.</p>
        <p>Faites preuve de bienveillance et d’altruisme. Un petit geste gentil peut accomplir beaucoup de choses.</p>
        <p>Cordialement,</p>
        <img src="images/Dino-Bianco-sign.jpg" alt="Dino Bianco sign" />
        <p>Dino Bianco, chef de la direction <br>Produits Kruger</p>
        <p>Pour en apprendre davantage, <a href="pdfs/fr-Kruger-conid19-detail.pdf" target="_blank">cliquez ici</a>.</p>
    </div>
</div> -->

<div class="showbox animated">
    <!-- <div class="loader">
    <svg class="circular" viewBox="25 25 50 50">
      <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
  </div> -->
</div>

<script src="js/all.js?1"></script>

</body>

</html>