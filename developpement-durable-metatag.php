<meta name="title" content="Reconcevoir 2030 de Produits Kruger : Développement durable">
<meta name="description" content="Reconcevoir 2030 comprend des objectifs tangibles répartis sur quatre piliers pour réduire notre empreinte environnementale encore davantage que nos résultats actuels. PK s.e.c. est le premier fabricant de produits de papier ayant obtenu la certification de la chaîne de traçabilité (CoC) du FSC®.">
<meta name="keywords" content="Reconcevoir 2030, Développement durable, de Produits Kruger, Reconcevoir, 2030">
