<?php
  //header("Location: nous-joindre.php");
  //die();
?>
<?php
$title = 'Sign Up';
require('header.php');
?>

    <div class="signup">
      <section class="signup__intro">
        <div class="intro-wrap" role="main">
          <h1>Restons <br>en contact.</h1>
        </div>
      </section>
      <div class="wrapper">
        <section class="home__welcome home__welcome_2 text-in">
          <div class="container clearfix2">
            <div class="title clearfix2">
              <h2 class="h4">Participez et vous pourriez GAGNER une provision d'un an en produits Kruger!</h2>
            </div>
            <div class="image">
              <img src="images/packs_reflection.jpg" alt="Kruger product lineup">
            </div>
            <div class="copy clearfix2">
              <p>Participez dès maintenant pour courir la chance de gagner une provision d'un an en produits Kruger, incluant du papier hygiénique Cashmere<sup>®</sup> ou Purex<sup>®</sup>, des essuie-tout SpongeTowels<sup>®</sup> et des papiers-mouchoirs Scotties<sup>®</sup>!</p>
              <p>Nous ne voulons pas vous voir rater quoi que ce soit! Abonnez-vous à notre infolettre afin de vous tenir au courant des promotions et d’autres nouvelles.</p>
            </div>
          </div>
        </section>
      </div>
      <section class="signup__form">
        <div class="container">
          <div class="iframe-wrap">
            <iframe src="https://cloud.email.krugerproductsbrands.ca/signupFR" width="1280" height="540" aria-label="signup form"></iframe>
          </div>
        </div>
        <div class="legal">
          <span><?php echo $company_name;?> (papier hygiénique Cashmere<sup>®</sup>, papier hygiénique Purex<sup>®</sup>, essuie-tout SpongeTowels<sup>®</sup> et papiers-mouchoirs Scotties<sup>®</sup><sup style="font-size: 5px;">’</sup>) 2, boulevard Prologis, bureau 500, Mississauga (Ontario) L5W 0G8, www.produitskruger.ca 888-620-1212</span>
        </div>
      </section>
    </div>

<?php require('footer.php'); ?>
