<?php
$title = 'Développement durable';
require('header.php');
?>

<div class="sustainability-wrap">
    <section class="leaf-section">
        <div class="container">
            <div class="leaf-inner">
                <div id="2015_trigger" style="position:absolute;top:50vh;"></div>
                <h1 class="fadeInUp">Développement durable</h1>
                <div class="leaf-block slideInUp">
                    <img src="images/leaf.png" alt="A green palm leaf">
                </div>
                <div class="year-block"></div>
                <div class="text-block">
                    <h4>Développement durable 2020 est notre programme de développement durable le plus récent.</h4>
                    <p>C'est une collection d'initiatives axées sur des objectifs et centrées à la fois sur une gérance
                        environnementale et les communautés dans lesquelles nous intervenons.</p>
                    <img src="images/sustainability-2020.jpg" alt="sustainability 2020 vision commitment action" />
                </div>
            </div>
        </div>
    </section>
    <section class="eight-section">
        <div id="trigger2" class="spacer s0" style="position:absolute;top:-400px;"></div>
        <div class="branch"></div>
        <div class="container">
            <div class="eight-inner">
                <span class="eight">
                    <svg xmlns="http://www.w3.org/2000/svg" id="Layer_2" viewBox="0 0 244.5 328.135" width="244.5"
                        height="328.135">
                        <path
                            d="M154.508 134.9c59.249 20.668 88.87 53.324 88.87 97.985 0 29.17-11.096 52.338-33.269 69.504-22.185 17.165-51.046 25.745-86.596 25.745-35.546 0-64.791-7.133-87.727-21.418-22.945-14.279-34.409-34.023-34.409-59.25 0-18.525 7.134-34.022 21.419-46.484 14.279-12.455 32.656-20.658 55.145-24.607-17.929-7.9-32.743-19.213-44.435-33.955-11.698-14.733-17.546-31.674-17.546-50.815 0-28.861 9.949-51.343 29.854-67.452C65.708 8.054 90.852 0 121.239 0c29.467 0 54.075 6.382 73.831 19.141 19.743 12.762 29.621 29.782 29.621 51.043 0 18.232-7.065 32.814-21.189 43.752-14.131 10.938-30.469 17.932-48.994 20.964zm-71.553 44.206c-23.094 13.068-34.635 34.796-34.635 65.171 0 22.186 6.833 40.267 20.507 54.236 13.673 13.977 31.903 20.961 54.687 20.961 19.75 0 35.624-5.161 47.628-15.49 11.998-10.329 18.001-24.306 18.001-41.929 0-14.885-4.637-27.038-13.896-36.465-9.271-9.408-23.631-18.068-43.072-25.974l-49.22-20.51zM67.461 62.438c.296 24.312 15.494 42.689 45.576 55.145l36.457 15.04c20.659-14.585 30.988-35.549 30.988-62.893 0-18.229-5.236-33.043-15.72-44.435-10.483-11.393-24.998-17.092-43.523-17.092-16.715 0-29.853 5.013-39.422 15.04-9.572 10.028-14.356 23.093-14.356 39.195z" />
                        </svg>
                </span>
                <div class="text-block">
                    <h4>Nous construisons des fondations solides.</h4>
                    <p>Produits Kruger a annoncé son premier programme de développement durable il y a environ dix ans.
                        Nous sommes fiers d'avoir fait d'importants progrès concernant la réduction de la consommation
                        d'énergie et d'eau, des émissions de gaz à effet de serre et des matériaux d'emballage, tout en
                        faisant croître l'efficacité des transports.</p>
                    <p>Nous offrons l'un des plus grands portefeuilles de produits certifiés par une tierce partie en
                        Amérique du Nord et nous avons été le premier fabricant de papier hygiénique canadien à obtenir
                        la certification FSC<sup>®</sup>.</p>
                </div>
                <div class="border-block"></div>
                <div class="cone-block"></div>
                <div class="clearfix2"></div>
            </div>
        </div>
    </section>
    <style>
    .inline {

        display: inline;
    }

    .sustainability-wrap .colored-section h2 {
        font-size: 37px;
    }

    .desktop {
        display: block;

    }

    .mobile {
        display: none;
    }

    @media screen and (max-width: 800px) {
        .sustainability-wrap .colored-section h2 {
            font-size: 21px;
        }

        .mobile {
            display: block;
            font-size: auto;
        }

        .desktop {
            display: none;
        }

        .sustainability-wrap .colored-section .quotes>div {
            margin-top: 220px;
        }

    }

    @media screen and (max-width: 600px) {
        .sustainability-wrap .colored-section .quote-outer {

            margin-bottom: 135px;
        }

        .sustainability-wrap .colored-section .quotes>div {
            margin-top: 70px;
        }


    }
    </style>
    <section class="colored-section">
        <div class="container">
            <div class="colored-inner">
                <div class="small-leaf"></div>
                <div class="quote-outer">
                    <div class="thumbnail"></div>
                    <div class="quotes">
                        <div>
                            <h2 class="desktop">« Le développement <br> durable est<br> au cœur de<br> notre entreprise
                                <br>et nous avons fait <br>d'importants <br> progrès depuis <br> dix ans. »</h2>
                            <h2 class="mobile">« Le développement durable est au cœur de notre entreprise et nous avons
                                fait d'importants progrès depuis dix&nbsp;ans. »</h2>
                            <div class="pseudo-title">Dino Bianco, CHEF DE LA DIRECTION</div>
                        </div>
                    </div>
                </div>
                <p class="inline">Développement durable 2020 est centré sur quatre cibles où nous pouvons avoir le plus
                    d'impact : réduire l'utilisation d'énergie, la consommation d'eau et les émissions de gaz à effet de
                    serre. Comme toujours, la santé et la sécurité de nos employés est notre priorité.</p>
            </div>
        </div>
    </section>
    <section class="eco-section">
        <div class="container">
            <div class="text-block">
                <h4>Nous joignons l’acte à la parole.</h4>
                <p>Nos investissements financiers pour réduire notre empreinte environnementale sont considérables. Fort
                    heureusement, un grand nombre de ces mesures, que ce soit grâce à l’amélioration de l’intensité
                    énergétique, à la réduction des déchets ou à une chaîne d’approvisionnement plus efficace,
                    aboutiront à des économies à long terme.</p>
                <p>Les investissements que nous réalisons aujourd’hui dans le développement durable permettront à nos
                    enfants et aux vôtres d’hériter d’une planète plus saine qu’ils pourront transmettre à leurs propres
                    enfants.</p>
            </div>
            <div class="border-block"></div>
        </div>
        <div class="location-block">SYSTÈME DE BIOGAZÉIFICATION NEXTERRA, NEW WESTMINSTER, C.-B.</div>
    </section>
    <!-- <section class="section-div"></section> -->
    <section class="progress-section">


        <!-- <svg version="1.1" stroke="#ffc419" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        	 width="1223px" height="3938px" viewBox="0 0 1223 3938" enable-background="new 0 0 1223 3938" xml:space="preserve">
        <path style="stroke-linecap: round; stroke-linejoin: round; stroke-dasharray: 1009.23px; stroke-dashoffset: 1009.23px;" fill="none" stroke="#ffc419" stroke-width="17" stroke-miterlimit="10" d="M-105,11.382h1317.256v936.906H399.984v528.621
        	h769.294v1405.36H408.579v605.98h459.858v449.113"/>
        </svg> -->
        <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" viewBox="0 0 1223 3938" width="1223" height="3938"
            stroke="#ffc419">
            <path fill="none" stroke-width="17" stroke-miterlimit="10" stroke-dashoffset="1009.23"
                d="M-65 11.382h1217.256v890.906H399.984v680.621h769.294v1555.36H408.579v605.98h459.858v449.113"
                stroke-linecap="round" stroke-linejoin="round" stroke-dasharray="1009.23" />
        </svg>

        <div class="container">
            <div class="progress-inner">
                <div class="border-block"></div>
                <div class="clearfix2"></div>
                <h2>Nos progrès</h2>
                <div id="trigger1" class="spacer s0"></div>
                <p>Huit objectifs spécifiques, concrets et réalisables étaient au cœur de l’initiative Développement
                    durable 2015 de Produits Kruger. Ces objectifs ont été déterminés en se basant sur l’année 2009
                    comme point de référence. Collectivement, ils illustrent notre volonté farouche et continue de
                    réduire notre empreinte environnementale et de créer des produits et des processus plus
                    durables.<span class="progress-triangles"></span></p>
                <div class="progress-block cert">
                    <div class="img"></div>
                    <div class="text">
                        <span class="green">
                            <i>
                                <svg class="icon">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checked"></use>
                                </svg>
                            </i>
                            OBJECTIF ACCOMPLI</span>
                        <h3>Certification FSC<sup>®</sup></h3>
                        <p>Produits Kruger a été le premier fabricant canadien de produits de papier à obtenir la
                            certification de chaîne de traçabilité « Chain of Custody » du FSC<sup>®</sup>, dans le
                            cadre du programme de la Rainforest Alliance.</p>
                    </div>
                    <span class="clearfix2"></span>
                </div>
                <div class="progress-block fibre">
                    <div class="img"></div>
                    <div class="text">
                        <span class="green">
                            <i>
                                <svg class="icon">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checked"></use>
                                </svg>
                            </i>
                            AMÉLIORATION</span>
                        <h3>Santé et sécurité des employés améliorées de 50 %</h3>
                        <p>Nos employés représentent notre plus grand atout et leur santé et sécurité nous sont
                            essentielles. Dans cette optique, nous sommes engagés à améliorer notre taux d'incidence
                            total OSHA de 50 % comparé à 2015. À ce jour, nous l'avons amélioré de 48 %.</p>
                    </div>
                    <span class="clearfix2"></span>
                </div>
                <div class="progress-block reduce">
                    <div class="img"></div>
                    <div class="text">
                        <span class="green">
                            <!-- <i> -->
                            <!-- <svg class="icon">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checked"></use>
                    </svg> -->
                            <img src="images/exceededIcon.png" alt="">
                            <!-- </i> -->OBJECTIF DÉPASSÉ</span>
                        <h3>Réduction de l’emballage de 5 %</h3>
                        <p>Nous avons réduit nos emballages de 13 %! Le poids de ces matériaux d’emballage équivaut au poids de 162 Boeing 767. Nos caisses d’expédition, nos rouleaux en carton d’essuie-tout et de papier hygiénique et nos boîtes de papiers-mouchoirs sont tous faits à partir de fibres 100 % recyclées. Beaucoup de nos matériaux d’emballage sont recyclables là où les installations existent.</p>
                    </div>
                    <span class="clearfix2"></span>
                </div>
                <div class="progress-block prod">
                    <div class="img"></div>
                    <div class="text">
                        <span class="green">
                            <i>
                                <svg class="icon">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checked"></use>
                                </svg>
                            </i>
                            OBJECTIF ACCOMPLI</span>
                        <h3>Plus de 100 produits certifiés</h3>
                        <p>Nous avons ajouté plus de 60 produits certifiés par une tierce partie, notamment des produits
                            certifiés FSC<sup>®</sup> et/ou certifiés ÉCOLOGO de l’entreprise UL. Produits Kruger offre
                            l’un des plus grands portefeuilles nord-américains de produits certifiés par une tierce
                            partie et reste le chef de file en matière de fabrication de produits innovants.</p>
                    </div>
                    <span class="clearfix2"></span>
                </div>
                <div class="progress-block energy">
                    <div class="img"></div>
                    <div class="text">
                        <span class="yellow">
                            <i>
                                <svg class="icon">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows"></use>
                                </svg>
                            </i>
                            AMÉLIORATION</span>
                        <h3>Réduire la consommation d'énergie de 15 %</h3>
                        <p>Nous réduisons la consommation d'énergie de manière drastique. Depuis 2010, nous avons observé une réduction de 10 % envers l'atteinte de notre objectif de 15 %. Les innovations telles que notre nouveau système de récupération de la chaleur à Crabtree, au Québec, nous aidera à atteindre notre cible d'ici 2020.</p>
                    </div>
                    <span class="clearfix2"></span>
                </div>
                <div class="progress-block emissions">
                    <div class="img"></div>
                    <div class="text">
                        <span class="green">
                            <!--                   <i>
                    <svg class="icon">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checked"></use>
                    </svg>
                  </i> -->
                            <img src="images/exceededIcon.png" alt="">
                            Amélioration</span>
                        <h3>Réduire les émissions totales de 27 %</h3>
                        <p>Augmenter notre utilisation d'énergie renouvelable et de substitution nous a aidé à réduire nos émissions totales de 22 % depuis 2009. Les technologies innovatrices, comme notre tout premier système de gazéification de la biomasse à New Westminster, en C.-B., nous aidera à atteindre notre objectif d'ici 2020.</p>
                    </div>
                    <span class="clearfix2"></span>
                </div>
                <div class="progress-block transport">
                    <div class="img"></div>
                    <div class="text">
                        <span class="yellow">
                            <i>
                                <svg class="icon">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows"></use>
                                </svg>
                            </i>
                            AMÉLIORATION</span>
                        <h3>Réduction de 15 % des émissions liées aux transports</h3>
                        <p>Nous avons amélioré de 9 % notre efficacité en matière de transport, principalement en
                            optimisant les volumes de livraison, ce qui a également permis d’améliorer notre logistique
                            et l’efficacité de nos livraisons. Nous avons aussi utilisé des modes de transport plus
                            économes en énergie carbone, tels que le transport ferroviaire.</p>
                    </div>
                    <span class="clearfix2"></span>
                </div>
                <div class="progress-block water">
                    <div class="img"></div>
                    <div class="text">
                        <span class="green">
                            <!--                   <i>
                    <svg class="icon">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checked"></use>
                    </svg>
                  </i> -->
                            <img src="images/exceededIcon.png" alt="">
                            Amélioration</span>
                        <h3>Réduire notre consommation d'eau de 35 %</h3>
                        <p>Depuis 2009, nous avons pu réduire notre consommation globale en eau de 28 % sans compromettre nos normes strictes de traitement des eaux usées. Notre but d'ici 2020 est de réduire notre consommation d'eau de 35 %. À ce jour, nous avons atteint une réduction de 28 %.</p>
                    </div>
                    <span class="clearfix2"></span>
                </div>
                <div class="spacer s2"></div>
            </div>
        </div>
    </section>
    <section class="footprint-section">
        <div class="square"></div>
        <div class="container">
            <div class="footprint-inner">
                <h2 class="bg-clip">Diminution de notre empreinte environnementale</h2>
                <h4>Coup d’œil sur les progrès en matière de développement durable</h4>
                <div class="cone"></div>
                <p>Le graphique indique notre empreinte en 2009 à titre de point de référence (rouge) dans trois domaines environnementaux mesurés dans le cadre de l’initiative Développement durable 2020. Notre empreinte environnementale réelle en 2018 (jaune) montre une progression constante vers l'empreinte environnementale en 2020 (vert), l'objectif final de notre programme.</p>
            </div>
            <div class="progress-visualisation">
                <div class="progress-exp">
                    <p>2009 Année de référence</p>
                    <p>2018 Résultat</p>
                    <p>2020 Objectif</p>
                </div>
                <div class="svg-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 426.97 335.28">
                        <style>
                        .footprint-graph-1 {
                            isolation: isolate;
                        }

                        .footprint-graph-12,
                        .footprint-graph-13,
                        .footprint-graph-14,
                        .footprint-graph-2,
                        .footprint-graph-3,
                        .footprint-graph-4,
                        .footprint-graph-5,
                        .footprint-graph-6 {
                            fill: none;
                        }

                        .footprint-graph-2,
                        .footprint-graph-3,
                        .footprint-graph-4 {
                            stroke: #bcbec0;
                            stroke-width: 0.46px;
                        }

                        .footprint-graph-2,
                        .footprint-graph-3,
                        .footprint-graph-4,
                        .footprint-graph-5,
                        .footprint-graph-6 {
                            stroke-miterlimit: 10;
                        }

                        .footprint-graph-3 {
                            stroke-dasharray: 5.57 3.72;
                        }

                        .footprint-graph-4 {
                            stroke-dasharray: 5.49 3.66;
                        }

                        .footprint-graph-5,
                        .footprint-graph-6 {
                            stroke: #939598;
                        }

                        .footprint-graph-5 {
                            stroke-width: 0.69px;
                        }

                        .footprint-graph-6 {
                            stroke-width: 0.7px;
                        }

                        .footprint-graph-7 {
                            font-size: 6.96px;
                        }

                        .footprint-graph-7,
                        .footprint-graph-8 {
                            fill: #939598;
                        }

                        .footprint-graph-10,
                        .footprint-graph-11,
                        .footprint-graph-7,
                        .footprint-graph-8,
                        .footprint-graph-9 {
                            font-family: Avenir-Roman, Avenir;
                        }

                        .footprint-graph-8 {
                            font-size: 6.96px;
                        }

                        .footprint-graph-10,
                        .footprint-graph-11,
                        .footprint-graph-9 {
                            font-size: 8.24px;
                        }

                        .footprint-graph-9 {
                            fill: #a6ce39;
                        }

                        .footprint-graph-10 {
                            fill: #f47920;
                        }

                        .footprint-graph-11 {
                            fill: #fcaf17;
                        }

                        .footprint-graph-12 {
                            stroke: #a6ce39;
                        }

                        .footprint-graph-12,
                        .footprint-graph-13,
                        .footprint-graph-14 {
                            stroke-linejoin: round;
                            stroke-width: 3.66px;
                            mix-blend-mode: multiply;
                        }

                        .footprint-graph-13 {
                            stroke: #f47920;
                        }

                        .footprint-graph-14 {
                            stroke: #ffc20e;
                        }
                        </style>
                        <g class="footprint-graph-1">
                            <g id="Layer_2" data-name="Layer 2">
                                <g id="Type">
                                    <line class="footprint-graph-2" x1="213.49" y1="75.3" x2="213.49" y2="78.05" />
                                    <line class="footprint-graph-3" x1="213.49" y1="81.76" x2="213.49" y2="330.67" />
                                    <line class="footprint-graph-2" x1="213.49" y1="332.53" x2="213.49" y2="335.28" />
                                    <line class="footprint-graph-4" y1="144.01" x2="426.97" y2="144.01" />
                                    <line class="footprint-graph-5" x1="389.9" y1="318.11" x2="213.77" y2="141.98" />
                                    <line class="footprint-graph-6" x1="393.3" y1="314.73" x2="386.49" y2="321.54" />
                                    <line class="footprint-graph-6" x1="363.76" y1="285.19" x2="356.95" y2="292" />
                                    <line class="footprint-graph-6" x1="334.22" y1="255.65" x2="327.41" y2="262.46" />
                                    <line class="footprint-graph-6" x1="304.68" y1="226.11" x2="297.87" y2="232.92" />
                                    <line class="footprint-graph-6" x1="275.14" y1="196.57" x2="268.33" y2="203.38" />
                                    <line class="footprint-graph-6" x1="245.6" y1="167.03" x2="238.79" y2="173.84" />
                                    <text class="footprint-graph-7"
                                        transform="translate(397.42 315.71) rotate(-45)">800</text><text
                                        class="footprint-graph-7"
                                        transform="translate(366.62 285.42) rotate(-45)">750</text><text
                                        class="footprint-graph-7"
                                        transform="translate(337.49 255.79) rotate(-45)">700</text><text
                                        class="footprint-graph-7"
                                        transform="translate(250.78 166.07) rotate(-45)">550</text><text
                                        class="footprint-graph-7"
                                        transform="translate(309.54 226.34) rotate(-45)">650</text><text
                                        class="footprint-graph-7"
                                        transform="translate(280.07 195.37) rotate(-45)">600</text>
                                    <line class="footprint-graph-5" x1="40.99" y1="314.67" x2="213.77" y2="141.89" />
                                    <line class="footprint-graph-6" x1="44.52" y1="318.2" x2="37.71" y2="311.39" />
                                    <line class="footprint-graph-6" x1="62.77" y1="299.95" x2="55.96" y2="293.14" />
                                    <line class="footprint-graph-6" x1="81.02" y1="281.7" x2="74.21" y2="274.89" />
                                    <line class="footprint-graph-6" x1="99.28" y1="263.45" x2="92.46" y2="256.64" />
                                    <line class="footprint-graph-6" x1="117.53" y1="245.2" x2="110.72" y2="238.39" />
                                    <line class="footprint-graph-6" x1="135.78" y1="226.95" x2="128.97" y2="220.14" />
                                    <line class="footprint-graph-6" x1="154.03" y1="208.7" x2="147.22" y2="201.88" />
                                    <line class="footprint-graph-6" x1="172.28" y1="190.44" x2="165.47" y2="183.63" />
                                    <line class="footprint-graph-6" x1="190.53" y1="172.19" x2="183.72" y2="165.38" />
                                    <text class="footprint-graph-7"
                                        transform="translate(44.52 321.53) rotate(45)">22.0</text><text
                                        class="footprint-graph-7"
                                        transform="translate(81.19 284.37) rotate(45)">21.0</text><text
                                        class="footprint-graph-7"
                                        transform="translate(135.6 229.51) rotate(45)">19.5</text><text
                                        class="footprint-graph-7"
                                        transform="translate(190.26 174.79) rotate(45)">18.0</text><text
                                        class="footprint-graph-7"
                                        transform="translate(171.84 192.46) rotate(45)">18.5</text><text
                                        class="footprint-graph-7"
                                        transform="translate(63.27 303.54) rotate(45)">21.5</text><text
                                        class="footprint-graph-7"
                                        transform="translate(117.42 248.88) rotate(45)">20.0</text><text
                                        class="footprint-graph-7"
                                        transform="translate(99.43 267.13) rotate(45)">20.5</text><text
                                        class="footprint-graph-7"
                                        transform="translate(153.43 211.62) rotate(45)">19.0</text>
                                    <line class="footprint-graph-5" x1="213.77" y1="2.11" x2="213.77" y2="141.98" />
                                    <line class="footprint-graph-6" x1="218.18" y1="125.27" x2="208.55" y2="125.27" />
                                    <line class="footprint-graph-6" x1="218.18" y1="104.75" x2="208.55" y2="104.75" />
                                    <line class="footprint-graph-6" x1="218.18" y1="84.22" x2="208.55" y2="84.22" />
                                    <line class="footprint-graph-6" x1="218.18" y1="63.69" x2="208.55" y2="63.69" />
                                    <line class="footprint-graph-6" x1="218.18" y1="43.16" x2="208.55" y2="43.16" />
                                    <line class="footprint-graph-6" x1="218.18" y1="22.64" x2="208.55" y2="22.64" />
                                    <line class="footprint-graph-6" x1="218.18" y1="2.11" x2="208.55" y2="2.11" /><text
                                        class="footprint-graph-8" transform="translate(221.84 126.96)">50</text><text
                                        class="footprint-graph-8" transform="translate(221.84 106.62)">55</text><text
                                        class="footprint-graph-8" transform="translate(221.84 86.25)">60</text><text
                                        class="footprint-graph-8" transform="translate(221.84 44.77)">70</text><text
                                        class="footprint-graph-8" transform="translate(221.84 65.77)">65</text><text
                                        class="footprint-graph-8" transform="translate(221.84 5.95)">80</text><text
                                        class="footprint-graph-8" transform="translate(221.84 25.05)">75</text><text
                                        class="footprint-graph-9" transform="translate(381.19 301.82)">783</text><text
                                        class="footprint-graph-9" transform="translate(190.5 22.64)">76</text><text
                                        class="footprint-graph-9" transform="translate(84.66 311.03)">21.72</text><text
                                        class="footprint-graph-10"
                                        transform="translate(132.37 248.88)">19.69</text><text
                                        class="footprint-graph-10" transform="translate(281.75 206.29)">609</text><text
                                        class="footprint-graph-10" transform="translate(195.23 106.96)">55</text><text
                                        class="footprint-graph-11" transform="translate(183.23 126.96)">49</text><text
                                        class="footprint-graph-11" transform="translate(267 181.08)">572</text><text
                                        class="footprint-graph-11" transform="translate(187.28 199.97)">18.46</text>
                                    <polygon class="footprint-graph-12"
                                        points="213.49 18.27 377.93 306.15 56.93 298.73 213.49 18.27" />
                                </g>
                                <g id="graphics">
                                    <polygon class="footprint-graph-13"
                                        points="213.77 104.75 277.07 205.29 121.27 234.4 213.77 104.75" />
                                    <polygon class="footprint-graph-14"
                                        points="213.77 126.96 264.25 192.46 168.88 186.78 213.77 126.96" />
                                </g>
                            </g>
                        </g>
                    </svg>
                    <div class="ghg">
                        <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.7 36.52">
                            <style>
                            .water-drop-1 {
                                fill: #ffc419;
                            }

                            .water-drop-2 {
                                fill: #fff;
                            }
                            </style>
                            <g id="water-layer-2" data-name="water layer 2">
                                <g id="graphics">
                                    <path class="water-drop-1"
                                        d="M10.57,7.41,9.2,4.73,7.83,7.41C7,9,0,22.84,0,27.31a9.21,9.21,0,1,0,18.41,0C18.41,22.84,11.37,9,10.57,7.41Z" />
                                    <path class="water-drop-2"
                                        d="M16.81,6.83C14.57,11.47,12,17.4,12,19.28a4.82,4.82,0,0,0,9.63,0C21.63,17.4,19.06,11.47,16.81,6.83Z" />
                                    <path class="water-drop-1"
                                        d="M18.18,2.67,16.81,0,15.45,2.67C14.36,4.8,8.93,15.59,8.93,19.28a7.89,7.89,0,1,0,15.77,0C24.7,15.59,19.27,4.8,18.18,2.67ZM12,19.28c0-1.88,2.57-7.81,4.81-12.45,2.25,4.64,4.82,10.57,4.82,12.45a4.82,4.82,0,0,1-9.63,0Z" />
                                </g>
                            </g>
                        </svg>
                        <div class="clearfix"></div>
                        Consommation <br>d'eau <span>(M<sup>3</sup>/MDMT)</span>
                    </div>
                    <div class="fibre">
                        <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.24 39.28">
                            <style>
                            .energy-cls-1 {
                                fill: #ffc419;
                            }
                            </style>
                            <g id="energy-2" data-name="energy 2">
                                <g id="graphics">
                                    <polygon class="energy-cls-1"
                                        points="25.24 8.97 16.49 8.97 20.12 0 7.42 0 0 18.65 8.55 18.65 1.03 39.28 25.24 8.97" />
                                </g>
                            </g>
                        </svg>
                        <div class="clearfix"></div>
                        Consommation <br>d'énergie <span>(GJ/TMSM)</span>
                    </div>
                    <div class="optimization">
                        <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 44.06 30.52">
                            <style>
                            .cloud-icon-1 {
                                fill: #ffc419;
                            }
                            </style>
                            <g id="cloud-icon-2" data-name="cloud-icon-2">
                                <g id="graphics">
                                    <path class="cloud-icon-1"
                                        d="M14.36,30.52A9.68,9.68,0,0,1,6.78,27a6.83,6.83,0,0,1-6.78-7,6.62,6.62,0,0,1,5.41-6.84,8.87,8.87,0,0,1,8.08-5.91,7.83,7.83,0,0,1,1.45.14A12.63,12.63,0,0,1,26.26,0C33.09,0,37,5.23,38,10.7c5.09,2,6.42,6,5.93,9.1A8.67,8.67,0,0,1,35.48,27h0a9.62,9.62,0,0,1-2.84-.45,7.51,7.51,0,0,1-6.28,3,9,9,0,0,1-5-1.49A11.75,11.75,0,0,1,14.36,30.52Zm-6.1-6.73.51.71a6.71,6.71,0,0,0,5.59,2.94A9,9,0,0,0,20.31,25l1.11-1,1,1.07a5.55,5.55,0,0,0,3.9,1.48A4.41,4.41,0,0,0,30.55,24l.63-1.44,1.43.66a6.8,6.8,0,0,0,2.87.68,5.56,5.56,0,0,0,5.45-4.62c.42-2.66-1.35-4.91-4.75-6L35.26,13l-.12-1c-.84-6.61-5.31-8.95-8.88-8.95a9.7,9.7,0,0,0-9,6.61l-.55,1.47-1.45-.57a4.8,4.8,0,0,0-1.77-.34,5.86,5.86,0,0,0-5.4,4.59L7.84,16l-1.21,0a3.49,3.49,0,0,0-3.55,3.83,3.82,3.82,0,0,0,4.3,4Z" />
                                </g>
                            </g>
                        </svg>
                        <div class="clearfix"></div>
                        Émissions de GES de <br>Scope 1 <span>(kg CO<sub>2</sub>e/MDMT)</span>
                    </div>
                </div>
                <div class="clearfix2"></div>
            </div>
        </div>
    </section>
    <section class="sdg-section">
        <div class="square"></div>
        <div class="container">
            <div class="sdg-inner">
                <h2 class="bg-clip">Soutenir les objectifs de développement durable de l’ONU</h2>
                <p>PK s.e.c. soutient les objectifs de développement durable de l’ONU. Élaborés dans le cadre du programme de 2030 en matière de développement durable et adoptés par l’ensemble des États membres de l’Organisation des Nations Unies en 2015, les objectifs de développement durable de l’ONU offrent un plan directeur pour la paix et la prospérité de la population et de la planète, pour aujourd’hui et pour l’avenir.</p>
                <p>Chez PK s.e.c., nos pratiques commerciales et nos efforts en matière de développement durable contribuent à l’atteinte de plusieurs des cibles et indicateurs se trouvant dans le cadre des objectifs de développement durable.</p>
            </div>
            <div class="sdg-content">
                <img class="sdg-logo" src="images/sdg-logo.jpg" />
                <img class="sdg-chart" src="images/sdg-chart.jpg" />
                <img class="sdg-chart-mobile" src="images/sdg-chart-mobile.jpg" />
            </div>
        </div>
    </section>
    <section class="reports-section">
        <div class="square"></div>
        <div class="container">
            <div class="reports-inner">
                <h2 class="bg-clip">Nos rapports</h2>
                <div class="leaf"></div>
                <p>Nous établissons régulièrement un rapport de progression vers la réalisation de nos objectifs de développement durable.  Cette comptabilité transparente nous oblige à prendre nos responsabilités et permet aux principaux intervenants de surveiller nos progrès.</p>
            </div>
            <div class="downloads">
            <div class="report">
                    <div class="grow">
                        <div class="vertical-align">
                            <div class="date">2019</div>
                            <a target="_blank"
                                href="pdfs/sustainability-our-reports-2019-fr.pdf"
                                class="btn">TÉLÉCHARGER LE PDF</a>
                        </div>
                    </div>
                </div>
                <div class="report">
                    <div class="grow">
                        <div class="vertical-align">
                            <div class="date">2015</div>
                            <a target="_blank"
                                href="http://produitskruger.ca/2017/pdfs/Kruger%202015%20Program%20Results%20Sell%20Sheet%20FRENCH.pdf"
                                class="btn">TÉLÉCHARGER LE PDF</a>
                        </div>
                    </div>
                </div>
                <div class="report">
                    <div class="grow">
                        <div class="vertical-align">
                            <div class="date">2014</div>
                            <a target="_blank" href="http://produitskruger.ca/2017/pdfs/KPL_2014_SummaryReport_FR.pdf"
                                class="btn">TÉLÉCHARGER LE PDF</a>
                        </div>
                    </div>
                </div>
                <div class="report">
                    <div class="grow">
                        <div class="vertical-align">
                            <div class="date">2012/13</div>
                            <a target="_blank"
                                href="http://produitskruger.ca/2017/pdfs/14174 Kruger Products Sustainability Report_FR 2012_2013 fa.pdf"
                                class="btn">TÉLÉCHARGER LE PDF</a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </section>
    <section class="leadership-section">
        <div class="branch"></div>
        <div class="container">
            <div class="leadership-inner">
                <h2 class="bg-clip">Leadership éclairé</h2>
                <p>Chez Produits Kruger, nous avons conscience d’avoir une expérience précieuse à partager, mais aussi
                    d’avoir de nombreuses choses à apprendre. C’est dans cet état d’esprit qu’en 2012, nous avons formé
                    un partenariat avec le magazine Canadian Grocer afin de créer une série de tables rondes réunissant
                    des leaders dans le domaine du développement durable. Notre objectif : trouver des solutions
                    innovantes pour les fabricants et détaillants de biens de consommation courante et promouvoir les
                    meilleures pratiques de l’industrie et la collaboration.</p>
                <div class="leaders-logo"></div>
            </div>
            <div class="downloads">
            <div class="pdf-c">
                    <h5>L’innovation durable des produits</h5>
                    <div class="grow">
                        <a class="btn pseudo-title"
                            href="pdfs/product-innovation.pdf"
                            target="_blank">TÉLÉCHARGER LE PDF
                            <svg class="icon">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="pdf-c">
                    <h5>La collaboration en temps d’incertitude</h5>
                    <div class="grow">
                        <h5>Pourquoi l’entreprise doit-elle développer une conscience collective?</h5>
                        <a class="btn pseudo-title"
                            href="http://produitskruger.ca/2017/pdfs/2016%20Sustainability%20White%20Paper%20FR.PDF"
                            target="_blank">TÉLÉCHARGER LE PDF
                            <svg class="icon">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="pdf-c">
                    <h5>Bilan de la situation écologique</h5>
                    <div class="grow">
                        <h5>Atténuation des risques dans un mode incertain : une approche durable</h5>
                        <a class="btn pseudo-title"
                            href="http://produitskruger.ca/2017/pdfs/2015%20Sustainability%20White%20Paper%20FR.PDF"
                            target="_blank">TÉLÉCHARGER LE PDF
                            <svg class="icon">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="pdf-c">
                    <h5>Logistique et transport durables</h5>
                    <div class="grow">
                        <h5>Transport et logistique durables dans l’industrie des biens emballés pour la vente au détail
                        </h5>
                        <a class="btn pseudo-title"
                            href="http://produitskruger.ca/2017/pdfs/2014%20Sustainability%20White%20Paper%20FR.PDF"
                            target="_blank">TÉLÉCHARGER LE PDF
                            <svg class="icon">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="pdf-c">
                    <h5>Approvisionnement responsable</h5>
                    <div class="grow">
                        <h5>L’approvisionnement responsable dans l’industrie des biens emballés pour la consommation :
                            la force motrice</h5>
                        <a class="btn pseudo-title"
                            href="http://produitskruger.ca/2017/pdfs/2013%20Sustainability%20White%20Paper%20FR.PDF"
                            target="_blank">TÉLÉCHARGER LE PDF
                            <svg class="icon">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="pdf-c">
                    <h5>Livre blanc</h5>
                    <div class="grow">
                        <h5>Aider les ménages canadiens à atteindre leurs objectifs de durabilité</h5>
                        <a class="btn pseudo-title"
                            href="http://produitskruger.ca/2017/pdfs/2012%20Sustainability%20White%20Paper%20FR.PDF"
                            target="_blank">TÉLÉCHARGER LE PDF
                            <svg class="icon">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="clearfix2"></div>
            </div>
        </div>
    </section>
    <section class="policies-section">
        <div class="square"></div>
        <div class="container">
            <div class="policies-inner">
                <h2 class="bg-clip">Nos politiques</h2>
                <p>Les politiques suivantes servent de lignes directrices pour nos actions de développement durable en
                    interne, mais également avec les partenaires de notre chaîne d'approvisionnement.</p>
            </div>
            <div class="downloads">
                <div class="pdf-c">
                    <div class="grow">
                        <h5>Code d’éthique et de conduite professionnelle</h5>
                        <!-- <a target="_blank" class="btn pseudo-title" href="/pdfs/Sustainability-Our_Policies/Code_of_Business_Conduct_and_Ethics_EN.pdf">TÉLÉCHARGER LE PDF -->
                        <a target="_blank" class="btn pseudo-title"
                            href="pdfs/Kruger_Code_etiques_FR_v6_FINAL.pdf">TÉLÉCHARGER LE PDF
                            <svg class="icon">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                            </svg>

                        </a>
                    </div>
                </div>

                <div class="pdf-c">
                    <div class="grow">
                        <h5>Politique environnementale</h5>
                        <!-- <a target="_blank" class="btn pseudo-title" href="/pdfs/Sustainability-Our_Policies/Kruger_Pol_env_EN_13_v2.pdf">TÉLÉCHARGER LE PDF -->
                        <a target="_blank" class="btn pseudo-title"
                            href="http://produitskruger.ca/2017/pdfs/Kruger_Pol_env_FR_13.pdf">TÉLÉCHARGER LE PDF
                            <svg class="icon">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                            </svg>
                        </a>
                    </div>
                </div>

                <div class="pdf-c">
                    <div class="grow">
                        <h5>Politique d’approvisionnement en matière ligneuse</h5>
                        <!-- <a target="_blank" class="btn pseudo-title" href="/pdfs/Sustainability-Our_Policies/Kruger_Pol_mat_lign_EN.pdf">TÉLÉCHARGER LE PDF -->
                        <a target="_blank" class="btn pseudo-title"
                            href="http://produitskruger.ca/2017/pdfs/Kruger_Pol_mat_lign_FR.pdf">TÉLÉCHARGER LE PDF
                            <svg class="icon">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                            </svg>
                        </a>
                    </div>
                </div>

                <div class="pdf-c">
                    <div class="grow">
                        <h5>Énoncé de politique relatif aux fournisseurs et au développement durable</h5>
                        <!-- <a target="_blank" class="btn pseudo-title" href="/pdfs/Sustainability-Our_Policies/sustainability_policies_supplier_en.pdf">TÉLÉCHARGER LE PDF -->
                        <a target="_blank" class="btn pseudo-title"
                            href="pdfs/kruger-FR-KPLP-Supplier-Sustainability-Policy-Aug-2019-final.pdf">TÉLÉCHARGER
                            LE PDF
                            <svg class="icon">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                            </svg>
                        </a>
                    </div>
                </div>

                <div class="clearfix2"></div>

            </div>
        </div>
    </section>
    <section class="resources-section">
        <div class="container">
            <div class="resources-inner">
                <h2>Plus de ressources</h2>
            </div>
            <div class="downloads">
                <div class="pdf-c">
                    <h5>Certificat FSC<sup>®</sup></h5>
                    <a href="pdfs/krugerproductslp_fsc_cw_certificate.pdf"
                        target="_blank" class="btn pseudo-title">TÉLÉCHARGER LE PDF
                        <svg class="icon">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                        </svg>
                    </a>
                </div>
                <div class="pdf-c">
                    <h5>Certificat SFI</h5>
                    <a href="pdfs/Kruger-sfi-Cert-2019-01.pdf" target="_blank" class="btn pseudo-title">TÉLÉCHARGER LE PDF
                        <svg class="icon">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                        </svg>
                    </a>
                </div>
                <!--             <div class="pdf-c">
              <h5>AFH Green Products Listing</h5>
              <a href="/pdfs/Sustainability-More_Resources/EnvBrochure.pdf" target="_blank" class="btn pseudo-title">Download pdf
                <svg class="icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrows-7"></use>
                </svg>
              </a>
            </div> -->
                <div class="clearfix2"></div>
            </div>
        </div>
    </section>
</div>

<?php require('footer.php'); ?>