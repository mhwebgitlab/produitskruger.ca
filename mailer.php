<?php

if (isset($_POST['title'])){
  $post = $_POST;
  $email_title = strip_tags($_POST['title']);
  $name = strip_tags($_POST['name']);
  $lastname = strip_tags($_POST['last_name']);
  $email = strip_tags($_POST['email']);
  $product = strip_tags($_POST['product']);
  $address_1 = strip_tags($_POST['address_1']);
  $address_2 = strip_tags($_POST['address_2']);
  $city = strip_tags($_POST['city']);
  $province_teritory = strip_tags($_POST['province_teritory']);
  $postal_code = strip_tags($_POST['postal_code']);
  $phone = strip_tags($_POST['phone']);
  $remarks = strip_tags($_POST['remarks']);
  $upc = strip_tags($_POST['upc_code']);
  $prod_code = strip_tags($_POST['production_code']);
  $file_attachment = strip_tags($_FILES['FileAttachment']['name']);
  $ip = strip_tags($_SERVER['REMOTE_ADDR']);

  if('' != $upc) {
	$upc = '61328' . $upc;
  }

$body = "French - ".$product."
Title: ".$email_title."
First name: ".$name."
Last name: ".$lastname."
Address 1: ".$address_1."
Address 2: ".$address_2."
City: ".$city."
Province: ".$province_teritory."
Postal code: ".$postal_code."
Telephone: ".str_replace(' ', '-', $phone)."
Email: ".$email."
Product: ".$product."
UPC code: ".$upc."
Production code: ".$prod_code."
Files: ".$file_attachment."
Comments: ".$remarks."
IP Address: ".$ip;

	 $response = $_POST["g-recaptcha-response"];
	 $url = 'https://www.google.com/recaptcha/api/siteverify';
	 $data = array(
	 	// Old secret key
    //'secret' => '6Le8YykUAAAAANBx5ItIyW1Y7uGEA0ZKEDOfo0O8',

    //New Secret Key
    'secret' => '6LeFKM4cAAAAAJ7QPIHB-USy3x1yz0rQnufcBphr',
	 	'response' => $_POST["g-recaptcha-response"]
	 );
	 $options = array(
	 	'http' => array (
	 		'method' => 'POST',
	 		'content' => http_build_query($data)
	 	)
	 );
	 $context  = stream_context_create($options);
	 $verify = file_get_contents($url, false, $context);
	 $captcha_success = json_decode($verify);
	 if ($captcha_success->success == false) {
	 	die('recapcha_error');
	 }

  // require_once '../vendor/swiftmailer/swiftmailer/lib/swift_required.php';
  require_once 'vendor/swiftmailer/swiftmailer/lib/swift_required.php';

  // Create the mail transport configuration
  $transport = Swift_MailTransport::newInstance();

  // Create the message
  $message = Swift_Message::newInstance();

  $message->setTo(array(
    "ConsumerResponse@krugerproducts.ca" => "Consumer Response"
    //"npatel@mh.ca" => "Consumer Response"
  ));
  // $message->setBcc(array(
  //   "kplp@propellerdigital.com" => "Propeller Digital"));

  /*$message->setBcc(array(
    "kruger_relay@mh.ca" => "M&H"
  ));*/

    if($_FILES['FileAttachment']['name'] != '') {

      // Are we able to get the file infos
      if($_FILES['FileAttachment']['error'] > 0) {
          die('Error : file error.');
      }

      // Is the file is an image or a pdf
      if(!in_array(pathinfo($_FILES['FileAttachment']['name'], PATHINFO_EXTENSION), array('gif', 'png', 'jpg', 'jpeg', 'pdf', 'mp4', 'mov'))) {
          die('Error : Not an image or PDF file.');
      }

      // If the file size is more than (7Mb)
      if(($_FILES['FileAttachment']['size'] >= 7000000) || ($_FILES['FileAttachment']['size'] == 0)) {
          die('Error : File size should be under 7Mb.');
      }

      else {
          // No error, we can attached the file to the email
          $message->attach(Swift_Attachment::fromPath($_FILES['FileAttachment']['tmp_name'])->setFilename($_FILES['FileAttachment']['name']));
      }
    }

  $message->setSubject("French - ". $product);
  $message->setBody($body);
  // $message->setFrom('kplp@propellerdigital.com', $name . ' ' . $lastname);
  // $message->setFrom('kruger_relay@mh.ca', $name . ' ' . $lastname);
  $message->setFrom('ConsumerResponse@krugerproducts.ca', $name . ' ' . $lastname);

  // Send the email
  $mailer = Swift_Mailer::newInstance($transport);
  $mailer->send($message);
  die('sent');
} else {
  die('error');
}
